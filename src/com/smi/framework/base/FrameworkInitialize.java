package com.smi.framework.base;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

public class FrameworkInitialize extends Base {


	public void InitializeApp(String path, String WiniumServer) throws MalformedURLException{

		DesktopOptions options = new DesktopOptions();
		options.setApplicationPath(path);
		options.setDebugConnectToRunningApp(true);

		WiniumDriver driver=new WiniumDriver(new URL(WiniumServer), options);
		
		//set Driver for context
		 DriverContext.set_Driver(driver);
	}
	
}
