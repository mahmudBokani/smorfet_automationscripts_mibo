package com.smi.framework.uiltilies;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.smi.framework.config.Setting;

public class DatadUltilities {
	
	
	
	public static Connection Connect_DB(String host,String dbName)
	{
		 Connection conn = null;
		    try
		    {
		      Class.forName("org.postgresql.Driver");
		      String url = "jdbc:postgresql://"+host+":5432/"+dbName;
		      conn = DriverManager.getConnection(url,Setting.UserName_DB, Setting.Password_DB);
		    }
		    catch (ClassNotFoundException e)
		    {
		      e.printStackTrace();
		      System.exit(1);
		    }
		    catch (SQLException e)
		    {
		      e.printStackTrace();
		      System.exit(2);
		    }
		    return conn;
	}
	
	public static ResultSet ExecuteQuery (String query, Connection conn)
	 {
	  Statement statement=null;
	  try
	  {
	    statement=conn.createStatement();
	    ResultSet resultSet= statement.executeQuery(query);
	    
	    return resultSet;
	  }
	  catch(Exception e)
	  {
		  System.out.print("execute query error: " + e);
	  }
		return null;
	}
}


