package com.smi.framework.uiltilies;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.smi.framework.base.Base;
import com.smi.framework.config.Setting;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;


public class ReportTestLink extends Base{

//    public static String DEVKEY="852737ec74133c46ccb020cfb5d5bcbf";
    public static String DEVKEY="7132addcbcf190a11d5c9cdb2ea762d4";    

    public static TestLinkAPI api;
 
     public ReportTestLink() throws MalformedURLException{
        
    	URL testLinkURL=new URL("http://10.1.10.12/testlink/lib/api/xmlrpc/v1/xmlrpc.php");
    	api= new TestLinkAPI(testLinkURL,DEVKEY);
    }

    public String reportResult(Integer testCaseId, String notes,ExecutionStatus status) throws TestLinkAPIException, IOException 
    {
    	Integer testPlanId = getTestPlan();
    	Integer buildId = getLastBuildIDInPlan();
    	String buildName = getLastBuildNameInPlan();
    	ReportTCResultResponse rs=api.setTestCaseExecutionResult(testCaseId, 0, testPlanId, status, buildId, buildName, notes, Boolean.TRUE, "",0, "", null, Boolean.TRUE);
    	return rs.getMessage();
    }
    
    public int GetTestCaseIDByName(String TestSuiteName,String TestCaseName) throws TestLinkAPIException, IOException
    {
    	int testSuiteID = this.getSuiteIDByName(TestSuiteName);
    	TestCase[] tc= api.getTestCasesForTestSuite(testSuiteID,Boolean.TRUE,TestCaseDetails.SIMPLE);
		for (TestCase c : tc)
		{
			String[] s = TestCaseName.split("_",4);
			String testname = s[0]+"_"+s[1]+"_"+s[2];

			if(testname.equals(c.getFullExternalId().toString().replaceAll("--","_")))
			{
			    return c.getId(); 
			}
		}
		return 0;
    }
    
    public int getSuiteIDByName(String TestSuiteName) throws TestLinkAPIException, IOException
    {
    	TestSuite[] ts= api.getTestSuitesForTestPlan(getTestPlan());
    	
    	String[] testSuiteNames = null;
    	if(TestSuiteName.contains("/"))
    		testSuiteNames = TestSuiteName.split("/");
    	
    	List<String> suiteNamePath = new ArrayList<String>();
    	
    	if(testSuiteNames == null)
    		suiteNamePath.add(TestSuiteName);
    	else
    		{
    			for(String s : testSuiteNames)
    				suiteNamePath.add(s);
    		}
    	
    	
    	int loops = suiteNamePath.size()-1;
    	String suiteName = suiteNamePath.get(suiteNamePath.size()-1);
    	
    	for (TestSuite c : ts)
		{
			if(suiteName.equals(c.getName()))
			{
				int id = c.getId();
				int count=1;
				for(int i=0;i<loops;i++)
				{
					int parentId = getParentId(id);
					String parentName = getSuiteNameById(parentId);
					if(suiteNamePath.contains(parentName))
						count++;
					id = parentId;
				}
		    	
				if(count == suiteNamePath.size())
					return c.getId(); 			
			}		
		}
		return 0;
    }

	public int getTestPlan() throws IOException {
    	TestPlan testPlan = api.getTestPlanByName(Setting.TestLinkPlan, Setting.TestLinkProject);
    	return testPlan.getId();
	}

	public int getLastBuildIDInPlan() throws IOException {
		int testPlanId = getTestPlan();
		Build lastBuild = api.getLatestBuildForTestPlan(testPlanId);
		return lastBuild.getId();
	}
	
	public String getLastBuildNameInPlan() throws IOException {
		int testPlanId = getTestPlan();
		Build lastBuild = api.getLatestBuildForTestPlan(testPlanId);
		return lastBuild.getName();
	}
	
	 public String getSuiteNameById(int TestSuiteId) throws TestLinkAPIException, IOException
	    {
	    	TestSuite[] ts= api.getTestSuitesForTestPlan(getTestPlan());
	    	for (TestSuite c : ts)
	    		if(c.getId() == TestSuiteId)
	    			return c.getName();
	    	return null;
	    }
	 
	 public int getParentId(int TestSuiteId) throws TestLinkAPIException, IOException
	    {
	    	TestSuite[] ts= api.getTestSuitesForTestPlan(getTestPlan());
	    	for (TestSuite c : ts)
	    		if(c.getId() == TestSuiteId)
	    			return c.getParentId();
	    	return -1;
	    }

}
