package smi.smorfet.test.pages;


import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept.PIX;
import org.bytedeco.javacpp.tesseract.TessBaseAPI;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class CorpusTab extends BasePage {
	
	// Corpus tab
	@FindBy(how=How.NAME,using ="Corpus")
	public WebElement tabCorpus;
	
	// Dictionary whole frame tab
	@FindBy(how=How.ID,using ="TitleBar")
	public WebElement TitleBar;
	
	// Audience drop down
	@FindBy(how=How.ID,using ="dd_audience")
	public WebElement dd_audience;
	
	// Complexity drop down
	@FindBy(how=How.ID,using ="dd_complexity")
	public WebElement dd_complexity;
	
	// formality drop down
	@FindBy(how=How.ID,using ="dd_formality")
	public WebElement dd_formality;
	
	// Medium drop down
	@FindBy(how=How.ID,using ="dd_medium")
	public WebElement dd_medium;
	
	// Social Status list
	@FindBy(how=How.ID,using ="lb_social_status")
	public WebElement lb_social_status;
	
	// Sentence Type list
	@FindBy(how=How.ID,using ="lb_sentence_type")
	public WebElement lb_sentence_type;
	
	// Speech Act list 
	@FindBy(how=How.ID,using ="lb_speech_act")
	public WebElement lb_speech_act;
	
	// Lingo list
	@FindBy(how=How.ID,using ="lb_lingo")
	public WebElement lb_lingo;
	
	// Mood list
	@FindBy(how=How.ID,using ="lb_mood")
	public WebElement lb_mood;
	
	// Dialect list
	@FindBy(how=How.ID,using ="lb_dialect")
	public WebElement lb_dialect;
	
	// Corpus Sentences
	@FindBy(how=How.ID,using ="lb_corpus_records")
	public WebElement lb_corpus_records;
	
	// dd_condition_field
	@FindBy(how=How.ID,using ="dd_condition_field")
	public WebElement cmpCondition_field;
	
	// dd_condition_operation
	@FindBy(how=How.ID,using ="dd_condition_operation")
	public WebElement cmpCondition_operation;
	
	// dd_condition_value
	@FindBy(how=How.ID,using ="dd_condition_value")
	public WebElement cmpCondition_value;
	
	// dd_all_or_next
	@FindBy(how=How.ID,using ="dd_all_or_next")
	public WebElement cmpCondition_all_next;
	
	// Retrieve button
	@FindBy(how=How.ID,using ="bt_retrieve_same")
	public WebElement btnRetrieve_same;
	
	// Next arrow button
	@FindBy(how=How.ID,using ="bt_next")
	public WebElement bt_next;
	
	// Previous arrow button
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement bt_prev;
	
	// New button
	@FindBy(how=How.ID,using ="bt_new")
	public WebElement bt_new;
	
	// New action Ok button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement bt_ok_new;
	
	// Save button
	@FindBy(how=How.ID,using ="bt_save")
	public WebElement bt_save;
	
	// save action Ok button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement bt_ok_save;
	
	// Delete button
	@FindBy(how=How.ID,using ="bt_delete")
	public WebElement bt_delete;
	
	// Revive button
	@FindBy(how=How.ID,using ="bt_delete")
	public WebElement bt_revive;
	
	// Retrieved records number message
	@FindBy(how=How.ID,using ="tb_message")
	public WebElement tb_retrieved_records;
	
	// Tagging window
	@FindBy(how=How.ID,using ="pn_phrase")
	public WebElement pn_phrase;
	
	// Go to field
	@FindBy(how=How.ID,using ="tb_goto_line")
	public WebElement tb_goto_line;
	
	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Corpus tagging area
	@FindBy(how=How.ID,using ="pn_readings")
	public WebElement pn_readings;
	
	// W button in footer
	@FindBy(how=How.ID,using ="bt_condition_whole_word")
	public WebElement 	bt_condition_whole_word;
	
	// Update button
	@FindBy(how=How.ID,using ="bt_update")
	public WebElement 	bt_update;
	
	// Revert button
	@FindBy(how=How.ID,using ="bt_revert")
	public WebElement bt_revert;
	
	// Sentences properties
	@FindBy(how=How.ID,using ="record_properties_layout")
	public WebElement record_properties_layout;
	
	// OK button on the Revert file popup 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement revertFileMsgOK;
	
	// Mass button
	@FindBy(how=How.ID,using ="bt_mass_attribute")
	public WebElement bt_mass_attribute;
	
	// Mass "change its" dropdown
	@FindBy(how=How.ID,using ="dd_utility_field")
	public WebElement massChangeItsDD;
	
	// Mass "to be" dropdown
	@FindBy(how=How.ID,using ="dd_utility_value")
	public WebElement massToBeDD;
	
	// Mass "change" button
	@FindBy(how=How.ID,using ="bt_utility_accept")
	public WebElement bt_utility_accept;
	
	// Mass "Corpus Attributing Message" dialog - No button
	@FindBy(how=How.NAME,using ="No")
	public WebElement massNoBtn;
	
	// Mass "Corpus Attributing Message" dialog - Yes button
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement massYesBtn;
	
	// Sort by combobox
	@FindBy(how=How.ID,using ="dd_sort_mode")
	public WebElement dd_sort_mode;
	
	// Reset search button
	@FindBy(how=How.ID,using ="bt_reset_search")
	public WebElement bt_reset_search;
	
	// load button
	@FindBy(how=How.ID,using ="bt_load")
	public WebElement bt_load;
	
	// POS filter for tagging records
	@FindBy(how=How.ID,using ="cb_filter_pos")
	public WebElement cb_filter_pos;
	
	// Form Type filter for tagging records
	@FindBy(how=How.ID,using ="cb_filter_form_type")
	public WebElement cb_filter_form_type;
	
	// ll_dictionary_key 
	@FindBy(how=How.ID,using ="ll_dictionary_key")
	public WebElement ll_dictionary_key;
	
	// Root
	@FindBy(how=How.ID,using ="")
	public WebElement root;
	
	// Desktop root element
	@FindBy(how=How.ID,using ="")
	public WebElement desktopRoot;
	
	// cb_show combobox
	@FindBy(how=How.ID,using="cb_show")
	public WebElement cb_show;
	
	// bt_help button
	@FindBy(how=How.ID,using="bt_help")
	public WebElement bt_help;
	
	// pn_legend window
	@FindBy(how=How.ID,using="pn_legend")
	public WebElement pn_legend;
	
	// Close pn_legend window
	@FindBy(how=How.ID,using="Close")
	public WebElement close_pn_legend;
	
	// Merge button
	@FindBy(how=How.ID,using="bt_merge_files")
	public WebElement bt_merge_files;

   // find number of elements in dropdown/list
   public String getTaggingWindowStatus() throws InterruptedException
{
		//get all children
	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
	
	String taggingWindow= itemlist.get(0).getAttribute("Name");
	
	return taggingWindow;
	
	//   		for(WebElement e : itemlist)
	//   		{
	//   			
	//   			
	//   			itemNames.add(e.getAttribute("Name"));
	//   			i++;
	//   			if (i>amount) break;
	//   		}
	//   		return itemNames;
}
   
   /**
* Get chosen value in a dropdown
* @param lstBox
* @return
* @throws InterruptedException
*/
   public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
{
	   	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
	
	ArrayList<String> itemNames= new ArrayList<String>();
	for(WebElement e : itemlist)
	{
		boolean isSelected = e.isSelected();
		if (isSelected)
			itemNames.add(e.getAttribute("Name"));
		}
		return itemNames;
}
   
   /**
* Fetch the text and title of popup windows within Smorfet
* @param test
* @param logger
* @param titleToCheck
* @param textToCheck
* @return
*/
   public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
	{
	   	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
	List<WebElement> text = null;
	List<WebElement> title = null;
	
	boolean titleIn = false;
	boolean textIn = false;
	
	// Check the main app for popup windows
	for(WebElement e : windows)
	{ 
		text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
	title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
	}
	
	// Fetch the found title
	for(WebElement e : title)
	{
		LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
	if (e.getAttribute("Name").contains(titleToCheck))
			titleIn = true;
	}
	
	// Fetch the found title    		
	for(WebElement e : text)
	{ 
		LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
	if (e.getAttribute("Name").contains(textToCheck))
			textIn = true;
	}
	
	LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
		
	    if (titleIn && textIn)
	    	return true;
	    else return false;
} // close function
   
   /**
    * Search in corpus
    * @param test
    * @param logger
 	* @param firstDropdownValue
 	* @param conditionDropdownValue
 	* @param searchWord
 	* @param filterDropdownValue
    * @return
 * @throws InterruptedException 
    */
   public void searchInCorpus(ExtentTest test,Logger logger,String firstDropdownValue,String conditionDropdownValue,String searchWord,String filterDropdownValue) throws InterruptedException
{
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Choose value from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDropdownValue);
	LogUltility.log(test, logger, firstDropdownValue+" is chosen from the first search dropdown");
	
	// Choose value from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
	LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");
	
	//Type in the search field
	CorpusTab.cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "Type in the search field:"+searchWord);
	
	// Change the last filter value, in case it was changed by other TC
	CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
	LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
	
	// Click the Retrieve button
	CorpusTab.btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
} // close function
   
   
   /**
	 * Get random text 
    * @param length - enter the length of the text
	 */
       public String RandomString(int length) {
       	   StringBuilder sb = new StringBuilder();
       	   Random random = new Random();
       	   char[] chars = null;
       	switch(Setting.Language)
       	{
	        	case "EN":
		        chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();break;
	        	case "HE":
	        	chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray();break;
       	}
       	
       	for (int i = 0; i < length; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
       	}
       	
        String output = sb.toString();
        return output;
       
       }

   /**
* Get clean records names from the corpus.txt files
 @return
 @throws FileNotFoundException
*/
public List<String> getRecordsFromCorpus() throws FileNotFoundException {
   			
   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language+"\\Data\\corpus.txt"), "UTF-8");
   		List<String> lines = new ArrayList<>();
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    try { 
   		    	lines.add(splitted[14]);
   		    	} catch (Exception e) {
   		    		e.printStackTrace();
			}
   		       
   		}
   		
   		in.close();
		return lines;
}

/**
 * Get sentence status from corpus.txt files
 *@param sentence
  @return
  @throws FileNotFoundException
 */
 public String getSenteceStatus(String sentence) throws FileNotFoundException {
   			
   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt"), "UTF-8");
   		String status = null;
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    try { 
   		    	if(sentence.equals(splitted[14]))
   		    	{
   		    		status = splitted[2];
   		    		return status;
   		    	}
   		    			
   		    	} catch (Exception e) {
   		    		e.printStackTrace();
			}
   		       
   		}
   		
   		in.close();
		return status;
 }
 
 /**
  * Get sentence status from corpus.txt files according to its number
  *@param sentence
   @return
   @throws FileNotFoundException
  */
  public String getSenteceStatusAcoordingNumber(String sentenceNum) throws FileNotFoundException {
	   			
	   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt"), "UTF-8");
	   		String status = null;
	   		while(in.hasNextLine()) {
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");
	   		    try { 
	   		    	if(sentenceNum.equals(splitted[0]))
	   		    	{
	   		    		status = splitted[2];
	   		    		return status;
	   		    	}
	   		    			
	   		    	} catch (Exception e) {
				}
	   		       
	   		}
	   		
	   		in.close();
			return status;
  }
  
 /**
  * Choose value from dropdown that is contains LISTITEM
  * @param lstBox
  * @param ItemName
  * @throws InterruptedException
  */
 public void chooseValueInDropdownContains(WebElement lstBox, String ItemName) throws InterruptedException
{
	// Open the listbox
//            	 List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//     			dropdownbutton1.get(0).click();
//             	lstBox.click();
 	
 	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

	// finally, for all
	for(WebElement e : itemlist)
	{
		System.out.println(e.getAttribute("Name"));
		if (e.getAttribute("Name").contains(ItemName))
			{
			  e.click();
			  return;
			}
	}
}
 
 /**
  * Get sentence records
  * @throws InterruptedException
  */
 public List<String> getSentencesRecords() throws InterruptedException
{

 	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));

 	List<String> recordsList = new ArrayList<String>();
	for(WebElement e : itemlist)
	{
//         			recordsList.add(e.getAttribute("Name"));
		recordsList.add(e.getText());
//         			System.out.println(e.getAttribute("Name"));
	}
	return recordsList;
}
 
 /**
  * Choose sentence record
  * @param record
 * @return 
  * @throws InterruptedException
  */
 public void clickSentencesRecord(String record, int index) throws InterruptedException
{

 	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
 	
 	int count = 0;
	for(WebElement e : itemlist)
	{
//         			if (e.getAttribute("Name").contains(record) && count == index)
		if(e.getText().equals(record) && count == index )
			e.click();
		count++;
	}

}
 
 /**
  * Choose composition record
  * @param record
 * @return 
  * @throws InterruptedException
 * @throws AWTException 
  */
 public void clickSentencesCompositionRecord(String record, int index) throws InterruptedException, AWTException
{

 	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
 	
 	int count = 0;
	for(WebElement e : itemlist)
	{
		if (e.getAttribute("Name").contains(record) && count == index)
		{
       		// Point at the element in order to get its color
         	String attachPlace = e.getAttribute("ClickablePoint");
     		String[] XY = attachPlace.split(",");

                 		Robot robot = new Robot();
                 	    int XPoint = 0, YPoint = 0;
                 		
                 	    XPoint = Integer.parseInt(XY[0]) - 40;
                 	    YPoint = Integer.parseInt(XY[1]);
                 		
	                     robot.mouseMove(XPoint, YPoint);
	                     robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
	                     robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
         			}
         			count++;
         		}
         		
  
			
         	}
             
        
             
             /**
  * Get sentence record element
  * @param record
 * @return 
  * @throws InterruptedException
  */
 public WebElement sentencesRecordElement(String record, int index) throws InterruptedException
{
 	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
 	int count = 0;
	for(WebElement e : itemlist)
	{
//         			System.out.println(e.getText());
//         			if (e.getAttribute("Name").contains(record) && count == index)
		if (e.getText().contains(record) && count == index)
			return e;
		count++;
	}
	return null;

}
 
 /**
  * Get sentence records
  * @param lstBox
  * @param ItemName
  * @throws InterruptedException
  */
 public List<List<String>> getTaggingRecords() throws InterruptedException
{
	
  	List<WebElement> paneList= pn_readings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
  	
  	List<List<String>> rows = new ArrayList<List<String>>();
  	List<String> row = new ArrayList<String>();
  	
  	for(WebElement e : paneList)
	{
  		// Get info within each row
  		List<WebElement> recordInfo= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
  		
  		String record =  recordInfo.get(1).getAttribute("Name").split("\\}")[1].split("~")[0];
  		String pos = recordInfo.get(1).getAttribute("Name").split("\\{")[1].split("\\}")[0];
  		String formType = recordInfo.get(3).getAttribute("Name");
  		row.add(record);
  		row.add(pos);
  		row.add(formType);
//              		for (int i = 1; i < 4; i++) {
//              			WebElement f = recordInfo.get(i);
//              			System.out.println(f.getAttribute("Name"));
//              			row.add(f.getAttribute("Name"));
//					}
  		
  		rows.add(row);
  		row = new ArrayList<String>();
     }
  		return rows;
	}
 
 /**
  * Get sentence records
  * @param lstBox
  * @param ItemName
  * @throws InterruptedException
  */
 public ArrayList<HashMap<String, String>> getTaggingRecordsRawAllInfo() throws InterruptedException
{            	
	// To save the table
 	ArrayList<HashMap<String, String>> corpusTable = new ArrayList<HashMap<String, String>>();
 	// For every row
 	HashMap<String, String> row = null;
 	
  	List<WebElement> paneList= pn_readings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
 	
  	for(WebElement e : paneList)
	{
  		// Get info within each row
  		List<WebElement> recordInfoPane= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
  		
//              		System.out.println();
//              		for (WebElement webElement : recordInfoPane) {
//						System.out.println(webElement.getAttribute("Name"));
//					}
  		
  		row = new HashMap<String, String>();
  		row.put("Index", recordInfoPane.get(0).getAttribute("Name"));
		row.put("Dictionary Key", recordInfoPane.get(1).getAttribute("Name"));
		row.put("POS", recordInfoPane.get(2).getAttribute("Name"));
		row.put("Form Type", recordInfoPane.get(3).getAttribute("Name"));
		row.put("Frames", recordInfoPane.get(4).getAttribute("Name"));
		row.put("Semantic Fields", recordInfoPane.get(5).getAttribute("Name"));
		row.put("Semantic Group", recordInfoPane.get(6).getAttribute("Name"));
		row.put("Transcription", recordInfoPane.get(7).getAttribute("Name"));
  		
		// Table
		corpusTable.add(row);

     }
  		return corpusTable;
	}
 
 /**
  * Get saved record tagging from the Corpus file 
  * @throws InterruptedException
  * @throws FileNotFoundException 
  */
 public List<String> getRecordTaggingFromCorpusFile(String sentenceNumber, String record) throws InterruptedException, FileNotFoundException
{
//	 sentence = sentence.substring(6);
	 String info = "";
	 Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\corpus.txt"), "UTF-8");
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		
   		    try { 
//    		   		    	System.out.println(sentence);
//    		   		    	System.out.println(splitted[14]);
   		    		if(sentenceNumber.equals(splitted[0]))
   		    	{
//    		   		    			System.out.println(splitted[14]);
   		    			for (int i = 15; i < splitted.length; i++) {
//    		   		    				System.out.println(splitted[i]);
//    		   		    				String mibo = splitted[i];
//    		   		    				String[] recordSplit = mibo.split("Ã–ï¿½");
   		    				
//    		   		    				String[] recordSplit = splitted[i].split("Ã–ï¿½");
//    		   		    				System.out.println(splitted[i].split("?"));
//    		   		    				System.out.println(splitted[i].split("?")[0]);
   		    				if(splitted[i].split("֏")[0].equals(record) )
   		    					info = splitted[i]; 
//    		   		    				if(splitted[i].contains(record))
//    		   		    					info = splitted[i]; 
						}
   		    	}
   		    			
   		    	} catch (Exception e) {
   		    		System.out.println(e);
			}
   		       
   		}
   		
   		List<String> chosenInfo = new ArrayList<String>();
   		
//    		   		String[] test = info.split("Ã–ï¿½");
   		// Get Record, POS and Form type
   		try {
   			
	   		String recordName = info.split("\\}")[1].split("\\~")[0];
	   		String pos = info.split("\\{")[1].split("\\}")[0];
//        		   		String[] pos0 = info.split("\\{");
//        		   		String pos = info.split("{")[1];
//        		   		String pos1 = pos.split("}")[0];
	   		String FormType = info.split("\\|")[1];
	   		FormType = Setting.Language.equals("HE")? FormType.split("§")[0]:FormType;
	   		in.close();
	   		
			// Return chosen record
	   		chosenInfo.add(recordName);
	   		chosenInfo.add(pos);
	   		chosenInfo.add(FormType);
			return chosenInfo;
   			
   		}catch (Exception e) {
			// the word is not tagged, return empty list
   			in.close();
   			return chosenInfo;
		}
}
 
/**
 * Choose tagging option
 * @param randomTaggingOption
 */
public void chooseTaggingOption(List<String> randomTaggingOption) {
	
	System.out.println("---chooseTaggingOption function");
	List<WebElement> paneList= pn_readings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
  	for(WebElement e : paneList)
	{
  		// Get info within each row
  		List<WebElement> recordInfo= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
  		String record =  recordInfo.get(1).getAttribute("Name").split("\\}")[1].split("~")[0];
  		String pos = recordInfo.get(1).getAttribute("Name").split("\\{")[1].split("\\}")[0];
  		String formType = recordInfo.get(3).getAttribute("Name");
  		
  		if(record.equals(randomTaggingOption.get(0)) && pos.equals(randomTaggingOption.get(1)) && formType.equals(randomTaggingOption.get(2))) {
  			System.out.println("---click the button");
  			List<WebElement> Buttons = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
  			System.out.println("---buttons elements: " + Buttons);
  			Buttons.get(0).click();
  			System.out.println("---button is clicked");
  			return;
  		}
     }
	
}

public void keyType(ExtentTest test, Logger logger, String value) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InterruptedException, AWTException
{
//				boolean shiftOn = false;
//				String key = null;
//				int keyCode;
	Robot r = new Robot();
	char[] character = new char[value.length()];
	for (int index = 0; index < value.length(); index++)
	{
		character[index] = value.charAt(index);
	}
	for (int index = 0; index < character.length; index++)
	{
		switch (character[index])
		{
		case ' ': r.keyPress(KeyEvent.VK_SPACE); r.keyRelease(KeyEvent.VK_SPACE); break;
		case 'a': r.keyPress(KeyEvent.VK_A); r.keyRelease(KeyEvent.VK_A); break;
		case 'b': r.keyPress(KeyEvent.VK_B); r.keyRelease(KeyEvent.VK_B); break;
		case 'c': r.keyPress(KeyEvent.VK_C); r.keyRelease(KeyEvent.VK_C); break;
		case 'd': r.keyPress(KeyEvent.VK_D); r.keyRelease(KeyEvent.VK_D); break;
		case 'e': r.keyPress(KeyEvent.VK_E); r.keyRelease(KeyEvent.VK_E); break;
		case 'f': r.keyPress(KeyEvent.VK_F); r.keyRelease(KeyEvent.VK_F); break;
		case 'g': r.keyPress(KeyEvent.VK_G); r.keyRelease(KeyEvent.VK_G); break;
		case 'h': r.keyPress(KeyEvent.VK_H); r.keyRelease(KeyEvent.VK_H); break;
		case 'i': r.keyPress(KeyEvent.VK_I); r.keyRelease(KeyEvent.VK_I); break;
		case 'j': r.keyPress(KeyEvent.VK_J); r.keyRelease(KeyEvent.VK_J); break;
		case 'k': r.keyPress(KeyEvent.VK_K); r.keyRelease(KeyEvent.VK_K); break;
		case 'l': r.keyPress(KeyEvent.VK_L); r.keyRelease(KeyEvent.VK_L); break;
		case 'm': r.keyPress(KeyEvent.VK_M); r.keyRelease(KeyEvent.VK_M); break;
		case 'n': r.keyPress(KeyEvent.VK_N); r.keyRelease(KeyEvent.VK_N); break;
		case 'o': r.keyPress(KeyEvent.VK_O); r.keyRelease(KeyEvent.VK_O); break;
		case 'p': r.keyPress(KeyEvent.VK_P); r.keyRelease(KeyEvent.VK_P); break;
		case 'q': r.keyPress(KeyEvent.VK_Q); r.keyRelease(KeyEvent.VK_Q); break;
		case 'r': r.keyPress(KeyEvent.VK_R); r.keyRelease(KeyEvent.VK_R); break;
		case 's': r.keyPress(KeyEvent.VK_S); r.keyRelease(KeyEvent.VK_S); break;
		case 't': r.keyPress(KeyEvent.VK_T); r.keyRelease(KeyEvent.VK_T); break;
		case 'u': r.keyPress(KeyEvent.VK_U); r.keyRelease(KeyEvent.VK_U); break;
		case 'v': r.keyPress(KeyEvent.VK_V); r.keyRelease(KeyEvent.VK_V); break;
		case 'w': r.keyPress(KeyEvent.VK_W); r.keyRelease(KeyEvent.VK_W); break;
		case 'x': r.keyPress(KeyEvent.VK_X); r.keyRelease(KeyEvent.VK_X); break;
		case 'y': r.keyPress(KeyEvent.VK_Y); r.keyRelease(KeyEvent.VK_Y); break;
		case 'z': r.keyPress(KeyEvent.VK_Z); r.keyRelease(KeyEvent.VK_Z); break;
		case 'A': r.keyPress(KeyEvent.VK_A); r.keyRelease(KeyEvent.VK_A); break;
		case 'B': r.keyPress(KeyEvent.VK_B); r.keyRelease(KeyEvent.VK_B); break;
		case 'C': r.keyPress(KeyEvent.VK_C); r.keyRelease(KeyEvent.VK_C); break;
		case 'D': r.keyPress(KeyEvent.VK_D); r.keyRelease(KeyEvent.VK_D); break;
		case 'E': r.keyPress(KeyEvent.VK_E); r.keyRelease(KeyEvent.VK_E); break;
		case 'F': r.keyPress(KeyEvent.VK_F); r.keyRelease(KeyEvent.VK_F); break;
		case 'G': r.keyPress(KeyEvent.VK_G); r.keyRelease(KeyEvent.VK_G); break;
		case 'H': r.keyPress(KeyEvent.VK_H); r.keyRelease(KeyEvent.VK_H); break;
		case 'I': r.keyPress(KeyEvent.VK_I); r.keyRelease(KeyEvent.VK_I); break;
		case 'J': r.keyPress(KeyEvent.VK_J); r.keyRelease(KeyEvent.VK_J); break;
		case 'K': r.keyPress(KeyEvent.VK_K); r.keyRelease(KeyEvent.VK_K); break;
		case 'L': r.keyPress(KeyEvent.VK_L); r.keyRelease(KeyEvent.VK_L); break;
		case 'M': r.keyPress(KeyEvent.VK_M); r.keyRelease(KeyEvent.VK_M); break;
		case 'N': r.keyPress(KeyEvent.VK_N); r.keyRelease(KeyEvent.VK_N); break;
		case 'O': r.keyPress(KeyEvent.VK_O); r.keyRelease(KeyEvent.VK_O); break;
		case 'P': r.keyPress(KeyEvent.VK_P); r.keyRelease(KeyEvent.VK_P); break;
		case 'Q': r.keyPress(KeyEvent.VK_Q); r.keyRelease(KeyEvent.VK_Q); break;
		case 'R': r.keyPress(KeyEvent.VK_R); r.keyRelease(KeyEvent.VK_R); break;
		case 'S': r.keyPress(KeyEvent.VK_S); r.keyRelease(KeyEvent.VK_S); break;
		case 'T': r.keyPress(KeyEvent.VK_T); r.keyRelease(KeyEvent.VK_T); break;
		case 'U': r.keyPress(KeyEvent.VK_U); r.keyRelease(KeyEvent.VK_U); break;
		case 'V': r.keyPress(KeyEvent.VK_V); r.keyRelease(KeyEvent.VK_V); break;
		case 'W': r.keyPress(KeyEvent.VK_W); r.keyRelease(KeyEvent.VK_W); break;
		case 'X': r.keyPress(KeyEvent.VK_X); r.keyRelease(KeyEvent.VK_X); break;
		case 'Y': r.keyPress(KeyEvent.VK_Y); r.keyRelease(KeyEvent.VK_Y); break;
		case 'Z': r.keyPress(KeyEvent.VK_Z); r.keyRelease(KeyEvent.VK_Z); break;
		case '0': r.keyPress(KeyEvent.VK_0); r.keyRelease(KeyEvent.VK_0); break;
		case '1': r.keyPress(KeyEvent.VK_1); r.keyRelease(KeyEvent.VK_1); break;
		case '2': r.keyPress(KeyEvent.VK_2); r.keyRelease(KeyEvent.VK_2); break;
		case '3': r.keyPress(KeyEvent.VK_3); r.keyRelease(KeyEvent.VK_3); break;
		case '4': r.keyPress(KeyEvent.VK_4); r.keyRelease(KeyEvent.VK_4); break;
		case '5': r.keyPress(KeyEvent.VK_5); r.keyRelease(KeyEvent.VK_5); break;
		case '6': r.keyPress(KeyEvent.VK_6); r.keyRelease(KeyEvent.VK_6); break;
		case '7': r.keyPress(KeyEvent.VK_7); r.keyRelease(KeyEvent.VK_7); break;
		case '8': r.keyPress(KeyEvent.VK_8); r.keyRelease(KeyEvent.VK_8); break;
		case '9': r.keyPress(KeyEvent.VK_9); r.keyRelease(KeyEvent.VK_9); break;
		case ',': r.keyPress(KeyEvent.VK_COMMA); r.keyRelease(KeyEvent.VK_COMMA); break;
		case '.': r.keyPress(KeyEvent.VK_PERIOD); r.keyRelease(KeyEvent.VK_PERIOD); break;
		case '\b': r.keyPress(KeyEvent.VK_BACK_SPACE); r.keyRelease(KeyEvent.VK_BACK_SPACE); break;
		case '\'': r.keyPress(KeyEvent.VK_QUOTE); r.keyRelease(KeyEvent.VK_QUOTE); break;
		
		default: LogUltility.log(test, logger, "Keyboard: Invalid Value"); 	break;
		}
//					int keyToClick = Integer.parseInt("KeyEvent." + key);
		
//					r.keyPress(KeyEvent.VK_9);
//					Thread.sleep(1000);
//					r.keyRelease(keyToClick);
		
//					Robot bot = new Robot();
//		            KeyEvent ke = new KeyEvent(new JTextField(), 0, 0, 0, 0, ' ');  
//		            Class clazz = ke.getClass();  
//		            Field field = clazz.getField( key );  
//		            keyCode = field.getInt(ke);
//					if (shiftOn == true)
//					{
//						bot.keyPress(KeyEvent.VK_SHIFT);
//					}
//					bot.keyPress(keyCode);
//					if (shiftOn == true)
//					{
//						bot.keyRelease(KeyEvent.VK_SHIFT);
//					}
//					int max = 100;
//					int min = 80;
//					sleep(random.nextInt(max - min + 1) + min);

	}
}

/**
 * Get sentence from corpus.txt files
 *@param sentence
  @return
  @throws FileNotFoundException
 */
 public List<String> getFirst15Senteces() throws FileNotFoundException {
   			
   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\corpus.txt"), "UTF-8");
   		List<String> sentences = new ArrayList<String>();
   		
   		int amount = 15;
   		while(in.hasNextLine() && amount!=0) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    sentences.add(splitted[14]);
   		    amount--;
   		}
   		
   		in.close();
		return sentences;
 }
 
 /**
  * 
  * @return
 * @throws FileNotFoundException 
  */
 public boolean isRecordPartOfCompound(String record) throws FileNotFoundException {
	 
	 // Get all compounds records
		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\compounds.txt"), "UTF-8");
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    String[] removeLeft = splitted[0].split("}");
   		    String[] removeRight = removeLeft[1].split("~");
   		    // remove spaces in the beginning 
//    		   		    removeRight[0]=  removeRight[0].replaceAll(" ", "");
   		    removeRight[0] = removeRight[0].replace("_", " ");
   		    removeRight[0] = removeRight[0].replace("-", " ");
   		    
   		    String[] splitRecords = removeRight[0].split(" ");
   		    for (String part : splitRecords) {
				if (part.equals(record)) {
					in.close();
					return true;
				}
			}
   		}
   		
   		in.close();
	 
	 return false;
 }
 
 /**
  * Get the lists and dropdowns in the sentences properties area
  * @param randomTaggingOption
 * @return 
  */
public List<WebElement> getListsAndDropdowns(String type) {
	
	List<WebElement> itemList = new ArrayList<WebElement>();
	
	if (type.equals("lb"))
	itemList= record_properties_layout.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
	else if (type.equals("dd"))
	itemList= record_properties_layout.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
   	
	List<WebElement> items = new ArrayList<WebElement>();
	
	for(WebElement e : itemList) {
	
	try {
		String IdInfo = e.getAttribute("AutomationId").substring(0, 2);
		if (type.equals("dd") && IdInfo.equals("dd"))
			items.add(e);
		else if (type.equals("lb") && IdInfo.equals("lb"))
			items.add(e);
		
	} catch (Exception f) {
		System.out.println("exception: " + f);
	}
	}
	
	return items;
}

 /**
  * Get sentence values from the corpus.txt file
  * @return
 * @throws IOException 
  */
  public  HashMap<String, String> getSentenceDropdownsListsValuesFromFile(String sentence) throws IOException {
	  
	  InputStream file = new FileInputStream(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt");
	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	  HashMap<String, String> info = new HashMap<>();
	  String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		    line = line.trim();
	   		    String[] splitted = line.split("	");   		

	   		 try {		   		  
	   		    if(sentence.equals(splitted[14]))
	   		    {
	   		    	info.put("audience", splitted[4].replaceAll("_", " "));
	   		    	info.put("social status",splitted[5].replaceAll("_", " "));
	   		    	info.put("sentence type",splitted[6].replaceAll("_", " "));
	   		    	info.put("complexity", splitted[7].replaceAll("_", " "));
	   		    	info.put("formality", splitted[8].replaceAll("_", " "));
	   		    	info.put("speech act", splitted[9]);
	   		    	info.put("medium", splitted[10].replaceAll("_", " "));
	   		    	info.put("lingo", splitted[11].replaceAll("_", " "));
	   		    	info.put("mood", splitted[12]);
	   		    	info.put("dialect", splitted[13].replaceAll("_", " "));
	   		   
	   		    	reader.close();
	   		    	return info;
	   		    }
	   		 }
		    catch(ArrayIndexOutOfBoundsException e )
	   		    {  	}
	   		    	
	     }

	   		reader.close();
			return info;
  }
  
  
 /**
  * Get sentence to load from sentences_for_load.txt file 
  * @return
 * @throws IOException 
  */
  public  List<String> sentencesFromLoadFile() throws IOException {
	  InputStream file = new FileInputStream(Setting.AppPath +Setting.Language +"\\Input_Corpus\\sentences_for_load.txt");
	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	  List<String> records = new ArrayList<String>();
	  String line;
	   	while((line = reader.readLine()) != null)
	   		   records.add(	CommonFunctions.cleanStringNikud(line));
   		reader.close();
   		return records;
  } 
  
  
 /**
  * Build sentences_for_load.txt file
  * @return
 * @throws IOException 
  */
  public void buildSentencesForLoad() throws IOException {
	  // Create Input_Corpus if doesn't exist
	  String input_corpus_path = Setting.AppPath + Setting.Language+"\\Input_Corpus";
	  File input_corpus_file = new File(input_corpus_path);
	  if(!input_corpus_file.exists())
		  input_corpus_file.mkdir();
			  
	  OutputStream file = new FileOutputStream(input_corpus_path+"\\sentences_for_load.txt");
	  BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(file,"utf-8"));
	  Random randomizer = new Random();
	  
	 List<String> wordSet = getRecordsFromDictionary();
	  // Add up to five sentences
	  int numberofSentences = randomizer.nextInt(5)+1;
	  while(numberofSentences-- > 0)
	  {
		  int len = 7; // number of words in line
		  String line="";
		  while(len-- > 0)
			   line += wordSet.get(randomizer.nextInt(wordSet.size())) + " ";
		 writer.write(line);
		 writer.newLine();
	  }

	 
      writer.close();
  } 
  
  
  /**
* Get clean records names from the dictionary.txt files 
* @return set of words
* @throws FileNotFoundException
*/
public List<String> getRecordsFromDictionary() throws FileNotFoundException {
   			
   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\Dictionary.txt"), "UTF-8");
   		List<String> lines = new ArrayList<>();
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    
   		    String[] removeLeft = splitted[0].split("}");
   		    String[] removeRight = removeLeft[1].split("~");
   		    lines.add(removeRight[0]);
   		}
   		
   		in.close();
		return lines;
}

	/**
    * Check if the string contains only characters
    */
 public boolean onlyCharacters(String str) {
	 return Setting.Language.equals("EN") ? str.matches("[a-zA-Z]+") : str.matches (".*[א-ת]+.*") ;
  }
		  
  /**
  * Get sentence number
   @return
   @throws FileNotFoundException
  */
  public String  getSentenceNumber(String sentence) throws FileNotFoundException {
	   			
	   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt"), "UTF-8");	 
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    try { 
   		    	if(sentence.equals(splitted[14]))
   		    	{		in.close();
   		    		return splitted[0];
   		    	}
   		    	} catch (Exception e) {
   		    		e.printStackTrace();
				}
	   		       
	   		}
	   		
	   		in.close();
			return null;
  }
  
  
  /**
   * Get saved record tagging for composition from the Corpus file
   * @throws InterruptedException
   * @throws FileNotFoundException 
   */
  public  ArrayList<ArrayList<String>> getCompositionRecordTaggingFromCorpusFile(String sentence, String record) throws InterruptedException, FileNotFoundException
{
 	 sentence = sentence.substring(6);
 	 String info = "";
 Scanner in = new Scanner(new File(Setting.AppPath + "EN\\Data\\corpus.txt"), "UTF-8");
 ArrayList<ArrayList<String>> compositionInfo = new ArrayList<ArrayList<String>>();
	while(in.hasNextLine()) {
	    String line = in.nextLine().trim();
	    String[] splitted = line.split("	");
	
	    try { 
	    		if(sentence.equals(splitted[14]))
	    	{
	    			for (int i = 15; i < splitted.length; i++) {
	    				if(splitted[i].split("֏")[0].equals(record) )
	    					info = splitted[i]; 
									}
	    	}
	    			
	    	} catch (Exception e) {
	    		System.out.println(e);
		}
	       
	}
	
	// Get Record, POS and Form type
	// for first part
	String recordName = info.split("¡")[1].split("¿")[0];
	String pos = info.split("\\{")[1].split("\\}")[0];
	String FormType = info.split("\\|")[1].split("¦")[0];
			
	// Return chosen record
	ArrayList<String> chosenInfo = new ArrayList<String>();
	chosenInfo.add(recordName);
	chosenInfo.add(pos);
	chosenInfo.add(FormType);
	compositionInfo.add(chosenInfo);
	
	// for second part
	recordName = info.split("¦")[1].split("¿")[0];
	pos = info.split("¦")[1].split("\\{")[1].split("\\}")[0];
	FormType = info.split("\\|")[2];
   		chosenInfo = new ArrayList<String>();
   		chosenInfo.add(recordName);
   		chosenInfo.add(pos);
   		chosenInfo.add(FormType);
   		compositionInfo.add(chosenInfo);
   		
   		in.close();
   		return compositionInfo;
}
  
  
  /**\
   * Convert POS name to POS code
   * @param POSInput
   * @return
   */
  public String POSConverter(String POSInput) {
  	POSInput = POSInput.replace(" ", "_");

// Put manually the values here, later fetch from DB
Map<String,String> POS = new HashMap<String,String>();
POS.put("accusative_word", "W");  POS.put("action_name", "e"); 
POS.put("adadjective", "d"); POS.put("adjective", "a"); 
POS.put("adverb", "r"); POS.put("article", "t"); 
POS.put("classifier", "I"); POS.put("clause_opener", "s"); 
POS.put("conjunction", "c"); POS.put("copula", "u"); 
POS.put("demonstrative", "z"); POS.put("diacritic", "D"); 
POS.put("existential", "C"); POS.put("foreign_script", "L"); 
POS.put("foreign_word", "S"); POS.put("genetive_word", "F"); 
POS.put("infinitive", "i"); POS.put("interjection", "j"); 
POS.put("modal", "m"); POS.put("name", "x"); 
POS.put("negation", "g"); POS.put("noun", "n"); 
POS.put("number", "N"); POS.put("phrase_begins", "B"); 
POS.put("phrase_ends", "E"); POS.put("possessive", "V"); 
POS.put("prefix", "f"); POS.put("preposition", "p"); 
POS.put("pronoun", "o"); POS.put("punctuation", "P"); 
POS.put("quantifier", "q"); POS.put("question", "Q"); 
POS.put("suffix", "U"); POS.put("title", "l"); 
POS.put("verb", "v"); POS.put("verb_particle", "A");

          	return POS.get(POSInput);
          }
          
         
   

          /**
   * Get saved record dictionary key from the Corpus file
   * @throws InterruptedException
   * @throws FileNotFoundException 
   */
  public String getDictionaryKeyFromFile(String sentence, String record,int index) throws InterruptedException, FileNotFoundException
{
 	 sentence = sentence.substring(6);
 	 String info = "";
	 int count = 0;
	 Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\corpus.txt"), "UTF-8");
	 while(in.hasNextLine()) {
	    String line = in.nextLine().trim();
	    String[] splitted = line.split("	");
	
	    try { 
	    		if(sentence.equals(splitted[14]))
	    	{
// 		   		    			System.out.println(splitted[14]);
	    			for (int i = 15; i < splitted.length; i++) {
	    				if(splitted[i].split("֏")[0].equals(record) && count == index )
	    					info = splitted[i];  
	    				count++;
						}
	    	}
	    			
	    	} catch (Exception e) {
	    		System.out.println(e);
	    		}	
	    }
	String dictionaryKey = "";
	try {
	 dictionaryKey = info.split("¿")[1].split("\\|")[0];		
	
	}catch (Exception e) {
		// Exception if the word is not tagged
		dictionaryKey = "Not tagged";
		}
   		
		in.close();
   		return dictionaryKey;
}
  
  /**
  * Get random sentences
  * @return
 * @throws IOException 
  */
  public String getRandomSentence() throws IOException {

	  Random randomizer = new Random();
	  
	 List<String> wordSet = getRecordsFromDictionary();
	 String sentence="";
	 int size =  randomizer.nextInt(7) + 2; // number of words in line
	  
	 while(size-- > 0)
			 sentence += wordSet.get(randomizer.nextInt(wordSet.size())) + " ";	

	 return sentence.trim();

  } 
  
  
  /**
      * get record menu
      * @param record
      * @param index
     * @return 
      * @throws InterruptedException
      */
     public List<WebElement> getRecordMenu(String record, int index) throws InterruptedException
 	{

     	List<WebElement> itemlist= pn_phrase.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
     	List<WebElement> menuItems = null;
     	int count = 0;
 		for(WebElement e : itemlist)
 		{
 			if (e.getAttribute("Name").contains(record) && count == index)
 			{
 				Actions action = new Actions(DriverContext._Driver);
 				WebElement element = e;
 				// right click on element
 				action.contextClick(element).perform();
 				
 				List<WebElement> menuTop= root.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Menu')]"));
 				// get menu items list
 				for(WebElement item : menuTop)
 					if(item.getAttribute("Name").equals("Menu"))
 					{
 						System.out.println("chosen menu: " + item.getAttribute("Name"));
 					 menuItems= item.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.MenuItem')]"));
 					for(WebElement e2 : menuItems) {
 							
 					System.out.println(e2.getAttribute("AutomationId"));
 					System.out.println(e2.getAttribute("Name"));
 					System.out.println(e2.getAttribute("Value"));
 				
 					}
 					 return menuItems;
 					}
 			}
 			count++;
 		}
	return menuItems;
 	}
     
     /**
      * Get sentences with required\specific word
      *@param sentence
       @return
       @throws FileNotFoundException
      */
      public List<String> getSentencesWithSpecificWord(String specificWord) throws FileNotFoundException {
		   			
		   		Scanner in = new Scanner(new File(Setting.AppPath + "EN\\Data\\corpus.txt"), "UTF-8");
		   		ArrayList<String> sentences = new ArrayList<String>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		    try { 
		   		    	if(splitted[14].contains(specificWord) && splitted[2].equals("fully_tagged"))
		   		    	{
		   		    		String[] wordsInSen = splitted[14].split(" ");
		   		    		boolean found = false;
		   		    		for(int i=0;i<wordsInSen.length;i++)
		   		    			if(wordsInSen[i].equals(specificWord))
		   		    				found = true;
		   		    		if(found)
		   		    			sentences.add(splitted[14]);
		   		    	}
		   		    			
		   		    	} catch (Exception e)
		   		    			{
		   		    			}     
		   		}
		   		
		   		in.close();
				return sentences;
      }
      
      /**
       * Get sentence records by separating them with spaces
       * @throws InterruptedException
       */
      public List<String> getSentencesRecords(String sentence) throws InterruptedException
  	{

    	String[] splitted = sentence.split(" ");
    	List<String> recordsList = new ArrayList<String>();     
  		for(int i=0;i<splitted.length;i++)
  			if(!splitted[i].isEmpty())
  				recordsList.add(splitted[i]);
  		return recordsList;
  	}
      
      /**
       * Gets all the sentence tagging information for its words
       * @throws InterruptedException
     * @throws IOException 
       */
      public List<String> getSentenceRecordsTaggingInfo(String sentence) throws InterruptedException, IOException
   	{
     	 sentence = sentence.substring(6);
     	 List<String> wordsTaggingInfo = new ArrayList<String>();
     	 File file = new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt");
		 BufferedReader reader = new BufferedReader(new FileReader(file));
		 String line;
			while((line = reader.readLine()) != null) {
	   		     line = line.trim();
	   		    String[] splitted = line.split("	");
	   		
	   		    try { 
//	     		   		    	System.out.println(sentence);
//	     		   		    	System.out.println(splitted[14]);
	   		    		if(sentence.equals(splitted[14]))
	   		    	{
//	     		   		    			System.out.println(splitted[14]);
	   		    			for (int i = 15; i < splitted.length; i++) 
	   		    				wordsTaggingInfo.add(splitted[i]);
	   		    	}
	   		    			
	   		    	} catch (Exception e) {
//	     		   		    		System.out.println(e);
				}
	   		       
	   		}
      
	   		reader.close();
	   		
	   		return wordsTaggingInfo;
   	}
//	              public String fixUnicodeSymbols(String wrongUnicode) throws FileNotFoundException
//	              {
//	            	  
//	            		
//      		   		Scanner in = new Scanner(new File("symbols_to_encode.txt"), "UTF-8");
//      		   		Map<String,String> uniodes = new HashMap<String,String>();
//      		   		while(in.hasNextLine()) {
//      		   		  String line = in.nextLine().trim();
//      		   		  String[] splitted = line.split("	");
//      		   		  uniodes.put(splitted[0], splitted[1]);
//      		   				}
//      		   	in.close();
//      		
//      		 return uniodes.get(wrongUnicode);
//      		   	
//	              }
  
      
//      /**
//       * Gets all the sentence tagging information for its words
//       * @throws InterruptedException
//     * @throws IOException 
//       */
//      public void readCorpus() throws InterruptedException, IOException
//   	{	 
//          RandomAccessFile file = new RandomAccessFile(Setting.AppPath + "EN\\Data\\corpus.txt", "r");
//          FileChannel channel = file.getChannel();
//          System.out.println("File size is: " + channel.size());
//          ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
//          channel.read(buffer);
//          buffer.flip();//Restore buffer to position 0 to read it
//          
//          System.out.println("Reading content and printing ... ");
//          for (int i = 0; i < channel.size(); i++) {
////	                	  String converted = new String(buffer.array(), "UTF-8");
//        	System.out.println((char) buffer.get());
//          }
//
//          channel.close();
//          file.close();
//   	}
      
      /**
       * Get WebElement from dropdown that is LISTITEM
       * @param lstBox
       * @param ItemName
       * @throws InterruptedException
       */
      public  WebElement getWebElementInDropdown(WebElement lstBox, String ItemName) throws InterruptedException
  	{
   	// Scan for buttons first
          List<WebElement> dropdownbutton= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
          
       // Open the listbox
//	            	List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
    	 if(!dropdownbutton.isEmpty() && lstBox.getAttribute("ControlType").equals("ControlType.ComboBox"))
    		dropdownbutton.get(0).click();
//	             	lstBox.click();
   	
    	 // Need to get the scroll after opening the dropdown
    	dropdownbutton= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
    	 
     	 // Scan after for elemnts in lstBOX, lists and scrolls
     	 List<WebElement> allElelements= lstBox.findElements(By.xpath(".//*"));

          List<WebElement> listsElements = new ArrayList<WebElement>();
     	 List<WebElement> scrollsElements  = new ArrayList<WebElement>();
     	
     	 for (WebElement webElement : allElelements) {
     		 if(webElement.getAttribute("ControlType").equals("ControlType.ListItem"))
     			 listsElements.add(webElement);
     		 else if(webElement.getAttribute("ControlType").equals("ControlType.ScrollBar"))
     			 scrollsElements.add(webElement);
 		}
 		
     	String firstShownElementInWindow = "";
 		System.out.println();
 		if (!scrollsElements.isEmpty()) {
      	
      	// get list of all the available values
    		List<String> allValues = new ArrayList<String>();
    		int count = 0;
    		for(WebElement e : listsElements) {
    			allValues.add(e.getAttribute("Name"));
    			if(e.getAttribute("ClickablePoint") != null) {
    				count++;
    				if (firstShownElementInWindow.equals(""))
    					firstShownElementInWindow = e.getAttribute("Name");
    			}
    		}

    		// Calculates moves
    		if (count != 0) {
    		
    		// Check the distance of the needed value
    		int place = allValues.indexOf(ItemName);
    		if (place >= count)
    			place += place / count;
    		int moves = place / count;
//	            		int reset = allValues.size()/count;
//	            		if (allValues.size() > count) reset += 1;
//	            		int reset = allValues.indexOf(firstShownElementInWindow)/count;
    		
    		int resetPlace = allValues.indexOf(firstShownElementInWindow);
    		if (resetPlace >= count)
    			resetPlace += resetPlace / count;
    		float resetHelp = resetPlace / count;
    		
    		int reset = 0;
    		if (resetPlace % count != 0) reset = (int) (resetHelp + 1);
    		else reset = (int) resetHelp;
    		
    		if (reset==0 && !firstShownElementInWindow.equals(allValues.get(0)))
    			reset = 1;
			
//	            		if (moves == 0) reset -= 1;
    		
//	            		if (reset > 0) {
    		// Look for scrolls
//	            		List<WebElement> scroll= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
      	// Look for scroll button
    		for (WebElement e : scrollsElements)
    			if (e.getAttribute("ControlType").equals("ControlType.ScrollBar")) {
    			
    		List<WebElement> scrollButtons= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
    			
    		// Reset scroll
    		for(WebElement f : scrollButtons)
  		{
    			if (reset > 0) {
     			// Don't reset if the first value in list is already shown
     			if (!firstShownElementInWindow.equals(allValues.get(0))) {
	     			// Reset
	     			if (f.getAttribute("Name").equals("Back by large amount"))
		  				{
		   				for (int i = 0; i < reset; i++)
		   					f.click();
		  				}
     			}
    			}
  		}
    			
    			// To solve sum cases where need to move only one up value for the reset
    			dropdownbutton.get(0).click();
    			
    			// Moves scroll to place
    			for(WebElement f : scrollButtons)
    			{
    			// Scorll to place
  			if (f.getAttribute("Name").equals("Forward by large amount"))
 				{
  				for (int i = 0; i < moves; i++)  
  					f.click();
 				}
  		}
      	}
//	            		}
    		
    		} 
    		
    		} // Scroll check
 		
    		// finally, for all
  		for(WebElement e : listsElements)
  		{
//	          			System.out.println(e.getAttribute("Name"));
  			if (e.getAttribute("Name").equals(ItemName))
 				{ 
 				  return e;
 				}
  		}
  		// if element not found
  		return null;
  	}
      
      /**
       * Press the large increment button in a list
       * @param lstBox
       * @param ItemName
       * @throws InterruptedException
       */
      public void ScrollLargeIncrement(WebElement lstBox) throws InterruptedException
  	{
    	  // Scan for buttons first
          List<WebElement> scrollElements= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
          for(WebElement e : scrollElements)
          {         	 
        	  while(e.getAttribute("Name").equals("Forward by large amount"))
        	  {
        		String info =  e.getAttribute("ClickablePoint");
        		 if(info == null)
        			 return;
        		 e.click();
        	  }

          }
          
  	}
      
      
      /**
       * Choose tagging option for compound
       * @param compoundText - compound text
       * @param compoundE - the compound element
     * @throws AWTException 
     * @throws IOException 
     * @throws InterruptedException 
       */
	public void chooseCompoundTaggingOption(String compoundText,WebElement compoundE) throws AWTException, IOException, InterruptedException {
		
		// Get number of words in compound
		String[] splitter = compoundText.split(" ");
		int length = splitter.length;
		
		// Get the compound element position
    	String attachPlace = compoundE.getAttribute("ClickablePoint");
 		String[] XY = attachPlace.split(",");
 		Robot robot = new Robot();
 	    int XPoint = 0, YPoint = 0;
 	    XPoint = Integer.parseInt(XY[0]);
 	    YPoint = Integer.parseInt(XY[1]);
 	
 	    // Right click on the element and choose the compound option
        robot.mouseMove(XPoint, YPoint);
        robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
        
        //**************************************
        WebElement mainMenuText = desktopRoot.findElement(By.xpath("./*[contains(@ControlType,'ControlType.Menu')]"));
        takeElementScreenshot(mainMenuText);
		// Right click on the compound and read the tagging option values
		List<String> readMenuText = readTextFromImage();
		
        // Choose the suitable compound option
		compoundText = Setting.Language.equals("EN")? compoundText.replaceAll(" ", "_") : compoundText;
	    int optionIndex =-1;
	    System.out.println(readMenuText);
		for(int i=0;i<readMenuText.size();i++)
			if(readMenuText.get(i).contains(compoundText))
				{optionIndex = i;break;}
		System.out.println(optionIndex);
        // Get the menu values
  		WebElement mainMenu = desktopRoot.findElement(By.xpath("./*[contains(@ControlType,'ControlType.Menu')]"));
		List<WebElement> menu = mainMenu.findElements(By.xpath(".//*[contains(@ControlType,'ControlType.MenuItem')]"));
		for(int i=0;i<menu.size();i++)
		{
			WebElement e = menu.get(i);
			if(optionIndex == i)
				{e.click();break;}
		
		}
      	
      	for(int i=0;i<length;i++)
      	{
      	// Get the tagging options
      	List<WebElement> paneList= pn_readings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
      	innerloop:
        	for(WebElement e : paneList)
    		{
        			WebElement Buttons = e.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
        			System.out.println("---buttons elements: " + Buttons);
        			Buttons.click();
        			System.out.println("---button is clicked");
        			break innerloop;
        	}
      	}
      	
      	
}
	
	
    
      /**
       * Right click on element and choose the wanted menu option
       * @param wordElement - the word element
       * @param optionIndex - the wanted list option to click on
     * @throws AWTException 
       */
	public void chooseWordOptionFromMenu(WebElement wordElement,int optionIndex) throws AWTException {		
	
	// Get the compound element position
	String attachPlace = wordElement.getAttribute("ClickablePoint");
	String[] XY = attachPlace.split(",");
	Robot robot = new Robot();
    int XPoint = 0, YPoint = 0;
    XPoint = Integer.parseInt(XY[0]);
    YPoint = Integer.parseInt(XY[1]);

    // Right click on the element and choose the compound option
    robot.mouseMove(XPoint, YPoint);
    robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
      
    // Get the menu values
	WebElement mainMenu = desktopRoot.findElement(By.xpath("./*[contains(@ControlType,'ControlType.Menu')]"));
	List<WebElement> menu = mainMenu.findElements(By.xpath(".//*[contains(@ControlType,'ControlType.MenuItem')]"));
	for(int i=0;i<menu.size();i++)
	{
		WebElement e = menu.get(i);
		if(optionIndex == i)
			{e.click();return;}
	
	}
	
	// Option not found
	System.out.println("Option not found!");
        		assertTrue(false);
	        }

	  			
 
	              /**
       * Get colors legend displayed values
       * @throws InterruptedException
       */
      public List<String> getColorsLegendDisplayedValues() throws InterruptedException
  	{
    	  // Get all text elements
          List<WebElement> textElements= pn_legend.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
          ArrayList<String> values = new ArrayList<String>();
          for(WebElement e : textElements)
          {         	 
        	  // Add only the displayed values - without maximizing the window
        		String info =  e.getAttribute("ClickablePoint");
        		 if(info == null)
        		 	{}
        		 else
        			 values.add(e.getAttribute("Name"));
          }
          
          return values;
    }
          
      
     
      /**
       * Get all grammar indexes from grammar_indices.txt file
       * @throws InterruptedException
     * @throws IOException 
       */
      public List<String> getAllGrammarIndexesFromFile() throws InterruptedException, IOException
   	{
     	 List<String> grammar_indexes = new ArrayList<String>();
     	 File file = new File(Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt");
		 BufferedReader reader = new BufferedReader(new FileReader(file));
		 String line;
			while((line = reader.readLine()) != null) {
	   		     line = line.trim();
	   		    String[] splitted = line.split("	");
	   		    grammar_indexes.add(splitted[0]); 		   		       
	   		}
      
	   		reader.close();
	   		return grammar_indexes;
	   }
  	
      
      /**
       * Get used grammar indexes from corpus.txt file
       * @throws InterruptedException
       * @throws FileNotFoundException 
       */
      public List<String> getGrammarIndexesFromCorpus() throws InterruptedException, FileNotFoundException
   	{
     	 Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt"), "UTF-8");
     	 List<String> grammarIndexes = new ArrayList<String>();
	   		while(in.hasNextLine()) 
	   		{
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");
	   		
					try { 
						
						for (int i = 15; i < splitted.length; i++)
						{
							String grammar_index = splitted[i].split("֏")[1].split("¡")[0];
							if(!grammarIndexes.contains(grammar_index))
								grammarIndexes.add(grammar_index);
						}
						
								
						} catch (Exception e) {
//										System.out.println(e);
								}
					   
			}		   		
	   		in.close();		   		
			return grammarIndexes;
   	}
      
 /**
  * Get sentence values from the corpus.txt file by number
  * @return
 * @throws IOException 
  */
  public  HashMap<String, String> getSentenceDropdownsListsValuesFromFileById(String sentenceNumber) throws IOException {
	  
	  InputStream file = new FileInputStream(Setting.AppPath + "EN\\Data\\corpus.txt");
  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
  HashMap<String, String> info = new HashMap<>();
  String line;
   	while((line = reader.readLine()) != null)
   	{
   		    line = line.trim();
   		    String[] splitted = line.split("	");   		

   		 try {		   		  
   		    if(sentenceNumber.trim().equals(splitted[0]))
   		    {
   		    	info.put("audience", splitted[4].replaceAll("_", " "));
   		    	info.put("social status",splitted[5].replaceAll("_", " "));
   		    	info.put("sentence type",splitted[6].replaceAll("_", " "));
   		    	info.put("complexity", splitted[7].replaceAll("_", " "));
   		    	info.put("formality", splitted[8].replaceAll("_", " "));
   		    	info.put("speech act", splitted[9]);
   		    	info.put("medium", splitted[10].replaceAll("_", " "));
   		    	info.put("lingo", splitted[11].replaceAll("_", " "));
   		    	info.put("mood", splitted[12]);
   		    	info.put("dialect", splitted[13].replaceAll("_", " "));
			   		   
			   		    	reader.close();
			   		    	return info;
			   		    }
			   		 }
				    catch(ArrayIndexOutOfBoundsException e )
			   		    {  	}
			   		    	
			     }
	
			   		reader.close();
					return info;
		  }
  

		  /**
   * Backup file from 'source' path to 'target' path
   * @param source
   * @param dest
 * @throws InterruptedException 
 * @throws IOException 
   */
  public boolean backupFile(String source, String dest) throws InterruptedException, IOException
{	 
	  
	  		@SuppressWarnings("resource")
		FileChannel inChannel = new
        FileInputStream(source).getChannel();
        @SuppressWarnings("resource")
			FileChannel outChannel = new
	            FileOutputStream(dest).getChannel();
	        try {
	            inChannel.transferTo(0, inChannel.size(),
	                    outChannel);
	        } 
	        catch (IOException e) {
	            throw e;
	        }
	        finally {
	            if (inChannel != null) inChannel.close();
	            if (outChannel != null) outChannel.close();
	        }
	  Path sourcePath = Paths.get(source);
	  Path destPath = Paths.get(dest);
	  boolean backupSuccess = Files.readAllLines(sourcePath).equals(Files.readAllLines(destPath));
	  return backupSuccess;
}
  
  /**
  * Check if dictionary key exists in sync_map.txt file
  * @return
 * @throws IOException 
  */
  public boolean checkIfFoundInSyncMap(String dictionaryKey) throws IOException {
	  
	  InputStream file = new FileInputStream(Setting.AppPath + Setting.Language+"\\Data\\sync_map.txt");
	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	  String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		  if(line.contains(dictionaryKey))
	   		  {
	   			  reader.close();
	   			  return true;
	   		  }
	   		    
	   	 }

	   		reader.close();
			return false;
  }
  
  /**
   * Get tagged dictionary key's from corpus that have more than one representation
   * @throws IOException
   */
  public  List<String> getTaggedDictionaryKeysInCorpus() throws IOException {
	  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	  List<String> DictionaryRecords = DictionaryTab.getDictionaryKeysMultiReps();
	  List<String> records = new ArrayList<String>();
	  String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
   		    if(splitted[2].equals("fully_tagged"))
   		    {
   			for (int i = 15; i < splitted.length; i++) {
				String word = splitted[i].split("֏")[0];
				if(DictionaryRecords.contains(word.toLowerCase()))
    				{
						String version = splitted[i].split("¿")[1].split("\\|")[0];
						if(!records.contains(version))
						records.add(version);
    				}
   				}
   		    	
   		    }
   		    
	   	}
   		reader.close();
   		return records;
  } 
  

 
  /**
   * Get grammar index info from grammar_indices.txt file
   * @throws InterruptedException
   * @throws IOException 
   */
  public List<String> getGrammarIndexInfo(String indexNumber) throws InterruptedException, IOException
{
 	 List<String> info = new ArrayList<String>();
 	 File file = new File(Setting.AppPath +Setting.Language +"\\Data\\grammar_indices.txt");
	 BufferedReader reader = new BufferedReader(new FileReader(file));
	 String line;
		while((line = reader.readLine()) != null) {
   		     line = line.trim();
   		    String[] splitted = line.split("	");
   		    if(splitted[0].equals(indexNumber))
   		    {
   		    	for(int i=1;i<splitted.length;i++)
   		    		info.add(splitted[i]);
   		    	break;
   		    }
   		}
  
   		reader.close();
   		return info;
   }
  
  
  /**
   * Get record grammar index from corpus.txt file
   * @throws InterruptedException
   * @throws FileNotFoundException 
   */
  public String getRecordGrammarIndex(String sentence, String record) throws InterruptedException, FileNotFoundException
{
	  sentence = sentence.substring(6);
 	 Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\corpus.txt"), "UTF-8");
 	 String grammar_index ="";
   		while(in.hasNextLine()) 
   		{
   		    String line = in.nextLine().trim();
   		    String[] splitted = line.split("	");
   		    
   		 // To avoid the empty sentence
   		 if(!splitted[0].equals("14148"))
   		if(sentence.trim().equals(splitted[14].trim()))
   			{
				try { 
					
					for (int i = 15; i < splitted.length; i++)
					{
						if((splitted[i].split("֏")[0].equals(record)))		
						{
							 grammar_index = splitted[i].split("֏")[1].split("¡")[0];
							 in.close();		
							return grammar_index;
						}
					}
							
					} catch (Exception e) {
//											System.out.println(e);
							}   
   			}		
   		}
   		in.close();		   		
		return grammar_index;
}
  
  /**
   * Get tagged dictionary key's from corpus that have more than one form
   * @throws IOException
   */
  public  List<String> getTaggedDictionaryKeysInCorpusForms() throws IOException {
	  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	  List<String> DictionaryRecords = DictionaryTab.getDictionaryKeysMultiForms();
	  List<String> recordsWithForms = new ArrayList<String>();
	  String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
   		    if(splitted[2].equals("fully_tagged"))
   		    {
   				// Get sentence words tagging 	
   		    	for (int i = 15; i < splitted.length; i++)
				{
   		    		String tagging = splitted[i];
   		    		if(splitted[i].contains("¿"))
   		    		{
	   		    		String dictionaryWithForm = splitted[i].split("¿")[1];;
	   		    		for(int j=0;j<DictionaryRecords.size();j++)
	   					{
	   		    			// Add dictionary key with its form that are tagged in corpus
//	   						if(tagging.contains(DictionaryRecords.get(j)) && !recordsWithForms.contains((dictionaryWithForm)))
	   		    			if(tagging.contains(DictionaryRecords.get(j)))
	   							recordsWithForms.add(dictionaryWithForm);
	   					}
   		    		}
				}
 
   		    }
   		    
	   	}
	   	
	   	// Get the clean dictionary keys 
	   	List<String> records = new ArrayList<String>();
	   	for(int i=0;i<recordsWithForms.size();i++)
	   	{
	   		String cleanDictionary = recordsWithForms.get(i).split("\\|")[0];
//	   		if(!records.contains(cleanDictionary))
	   			records.add(cleanDictionary);
	   	}	
	   	
	    // Get dictionary keys that have more than one representation
	   	List<String> finalRecords = new ArrayList<String>();
			  for(int i=0;i<records.size();i++)
			  {
				int occurrences = Collections.frequency(records, records.get(i));
				if(occurrences>1)
					if(Setting.Language.equals("EN"))
							{
								if(onlyCharacters(records.get(i).toLowerCase()))
								if(!finalRecords.contains(records.get(i).toLowerCase()))
									finalRecords.add(records.get(i).toLowerCase());
							}
					else
						finalRecords.add(records.get(i));
			  }
		
	
   		reader.close();
   		return finalRecords;
  } 
  
  
  /**
   * Check if the sentence contains any compound
   * @throws IOException
   */
  public boolean containsCompound(String sentence) throws IOException {
	sentence = sentence.substring(6);
	InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
 		 // To avoid the empty sentence
	   		 if(!splitted[0].equals("14148"))
   		    if(splitted[14].equals(sentence))
   		    {		   		    
   		    	try { 
					
					for (int i = 15; i < splitted.length; i++)
					{
						// Symbol exists when the word is tagged as part of compound
						if((splitted[i].contains("¯"))) {		
							reader.close();
							return true;
						}
					}
							
					} catch (Exception e) {
//											System.out.println(e);
							}   
   		    reader.close();
	    	return false;
   		   }
   		    
	   	}
   		
	   	reader.close();
	   	return false;
  } 
  
  
  
  /**
   * Check if the sentence contains specific compound
   * @throws IOException
   */
  public boolean containsCompound(String sentence,String compoundKey) throws IOException {
	sentence = sentence.substring(6);
	InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
 		 // To avoid the empty sentence
	   		 if(!splitted[0].equals("14148"))
   		    if(splitted[14].equals(sentence))
   		    {		   		    
   		    	try { 
					
					for (int i = 15; i < splitted.length; i++)
					{
						// Symbol exists when the word is tagged as part of compound
						if((splitted[i].contains("¯"))) {	
							String getCompound =  splitted[i].split("¯")[1];
							if(getCompound.equals(compoundKey)) {
							reader.close();
							return true;}
						}
					}
							
					} catch (Exception e) {
//											System.out.println(e);
							}   
   		    reader.close();
	    	return false;
   		   }
   		    
	   	}
   		
	   	reader.close();
	   	return false;
  } 
  
  
  /**
   * Get record/dictionary key index number from file
   * @throws IOException
   */
  public int getSentenceRecordIndex(String sentenceNumber,String dictionaryKey) throws IOException {
	InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
 		 // To avoid the empty sentence
	   		 if(!splitted[0].equals("14148"))
   		    if(splitted[0].equals(sentenceNumber))
   		    {		   		    
   		    	try { 
   		    		
   		    		int index = 0;
					
					for (int i = 15; i < splitted.length; i++)
					{
					
						String getDictionary = splitted[i].split("¿")[1].split("\\|")[0];
						if(getDictionary.equals(dictionaryKey)) {
							reader.close();
							return index;
						}
						index++;
					}
					break;
							
					} catch (Exception e) {
//											System.out.println(e);
							}   
   		    	
   		    	// Not found
	    
   		   }    
	   
	   	}
	 reader.close();
	 return -1;
  } 
  
  
  /**
   * Check if the dictionary key is part of compound or contraction
   * @throws IOException
   */
  public boolean isDictionaryKeyPartofCompoundContraction(String sentenceNumber,String dictionaryKey) throws IOException {

	InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
	BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	String line;
	   	while((line = reader.readLine()) != null)
	   	{
	   		line = line.trim();
   		    String[] splitted = line.split("	"); 
   		    if(splitted[0].equals(sentenceNumber))
   		    {		   		    
   		    	try { 
					
					for (int i = 15; i < splitted.length; i++)
					{
						// Symbol exists when the word is tagged as part of compound
						if((splitted[i].contains("¯")) && splitted[i].contains(dictionaryKey))		
							return true;
						// Symbol exists when the word is tagged as part of contraction
						if((splitted[i].contains("§")) && splitted[i].contains(dictionaryKey))		
							return true;
						//*******************
						if(splitted[i].contains("'") && splitted[i].contains(dictionaryKey))	
							return true;
					}
							
					} catch (Exception e) {
//												System.out.println(e);
										}   
   		    			reader.close();	
		   		    	return false;
			   		   }
			   		    
   				   	}
	   				reader.close();
   				   	return false;
   			  } //end method
  
  
	  /**
	   * Get tagged compounds from corpus
	   * @throws IOException
	   */
	  public  List<String> getTaggedDictionaryKeysInCorpusCompound() throws IOException {
		  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
		  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
//		  List<String> DictionaryRecords = DictionaryTab.getDictionaryKeysMultiReps();
		  List<String> records = new ArrayList<String>();
		  String line;
		   	while((line = reader.readLine()) != null)
		   	{
		   		line = line.trim();
	   		    String[] splitted = line.split("	"); 
	   		    if(splitted[2].equals("fully_tagged"))
	   		    {
	   			for (int i = 15; i < splitted.length; i++) {
	   				
	   				if(Setting.Language.equals("EN"))
	   				{
		   				if(splitted[i].contains("¯") )
		   				{
	//					String word = splitted[i].split("֏")[0];
	//					if(DictionaryRecords.contains(word.toLowerCase()))
		    				{
								String compound = splitted[i].split("¯")[1];
								if(!records.contains(compound))
								records.add(compound);
		    				}
		   				}
	   				}
	   				
	   				else
		   				if(Setting.Language.equals("HE"))
		   					if(splitted[i].contains("¯") && !splitted[i].contains("plural") && !splitted[i].contains("past"))
		   					{
								String compound = splitted[i].split("¯")[1];
								if(!records.contains(compound))
								records.add(compound);
		   					}
	   		    }
	   			
		   	}
		   }
	   		reader.close();
	   		return records;
	  } // end method
   
	  
	  /**
	   * Get the index of the first part of comopound in corpus.txt file
	   * @throws IOException
	   */
	  public int getSentenceCompouundRecordIndex(String sentenceNumber,String compoundKey) throws IOException {
		InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
		BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
		String line;
		   	while((line = reader.readLine()) != null)
		   	{
		   		line = line.trim();
	   		    String[] splitted = line.split("	"); 
	 		 // To avoid the empty sentence
	   		    if(splitted[0].equals(sentenceNumber))
	   		    {		   	
	   		    	
	   		    	int index = 0;
	   		    	for (int i = 15; i < splitted.length; i++)
	   		    	{
	   		    		if(splitted[i].contains("¯"))
	   		    				{
		
									String getCompound =  splitted[i].split("¯")[1];
									if(getCompound.equals(compoundKey)) {
										reader.close();
										return index;
									}
								}
										
	   		    	index++;
	   		    	}

				}
	   		  }
		   	
		   reader.close();
		   return -1;
	  } // end method
	  
	  /**
	   * Check if dictionary key exists in sync_map.txt file
	   * @return
	  * @throws IOException 
	   */
	   public boolean checkIfFoundInSyncMap(String oldValue,String newValue) throws IOException {
	 	  
	 	  InputStream file = new FileInputStream(Setting.AppPath + Setting.Language+"\\Data\\sync_map.txt");
	 	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	 	  String line;
	 	   	while((line = reader.readLine()) != null)
	 	   	{
	 	   		String[] splitted = line.split("	");
	 	   		if(splitted[0].contains(oldValue) && splitted[1].contains(newValue)) {
	 	   			reader.close();
	 	   			return true;
	 	   		}
	 	    }

	 	   		reader.close();
	 			return false;
	   }
	   
	   
	   /**
	    * gets the image path and returns the string in the image
	    * @param path
	    */
	   public List<String> readTextFromImage()
	   {
		   // Read the text in the image
			BytePointer outText;
			
	        TessBaseAPI api = new TessBaseAPI();
	        // Initialize tesseract-ocr with English, without specifying tessdata path
	        if(Setting.Language.equals("EN"))
	        {
		        if (api.Init("tessdata", "ENG") != 0) {
		            System.err.println("Could not initialize tesseract.");
		            System.exit(1);
		        }
	        
	        }
	        else
	        {
	        	  if (api.Init("tessdata", "HEB") != 0) {
			            System.err.println("Could not initialize tesseract.");
			            System.exit(1);
			        }
	        }

	        // Open input image with leptonica library
	        PIX image = pixRead(Setting.AppPath+"tempScreenshot.png");
	        api.SetImage(image);
	        // Get OCR result
	        outText = api.GetUTF8Text();
	        String textOutput = outText.getString();
	        List<String> imageText = new ArrayList<String>();
	        String[] splitText = textOutput.split("\\n");
	        for(int i=0;i<splitText.length;i++)
	        	if(!splitText[i].equals(""))
	        		imageText.add(splitText[i]);
	        
//	        System.out.println(textOutput);

	        // Destroy used object and release memory
	        api.End();
	        outText.deallocate();
	        pixDestroy(image);
	        
	        api.close();
	        return imageText;
	   }
	   
	   public void takeElementScreenshot(WebElement element) throws IOException, AWTException, InterruptedException
	   {
		   
		   	Thread.sleep(1000);
  			 // Get entire page screenshot
  	        File screenshot = ((TakesScreenshot)DriverContext._Driver).getScreenshotAs(OutputType.FILE);
  	        BufferedImage  fullImg = ImageIO.read(screenshot);

  	        // Get the location of element on the page
		    String menuePlace1 = element.getAttribute("BoundingRectangle");
		    
			String[] valuesSplit = menuePlace1.split(",");
		    int X, Y, Height, Width = 0;
			X = Integer.parseInt(valuesSplit[0]);
		    Y = Integer.parseInt(valuesSplit[1]);
		    Height = Integer.parseInt(valuesSplit[2]);
		    Width = Integer.parseInt(valuesSplit[3]);
		    
  	        // Crop the entire page screenshot to get only element screenshot
  	        BufferedImage eleScreenshot= fullImg.getSubimage(X, Y,
  	            Height, Width);
  	        ImageIO.write(eleScreenshot, "png", screenshot);

  	        // Copy the element screenshot to disk
  	        File screenshotLocation = new File(Setting.AppPath+"tempScreenshot.png");
  	        FileUtils.copyFile(screenshot, screenshotLocation);
	   }
	   
	   
		  /**
		   * Get tagged compounds from corpus for Hebrew 
		   * @throws IOException
		   */
		  public   List<ArrayList<String>> hebrewCompoundsInCorpus() throws IOException {
			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\corpus.txt"));
			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
//			  List<String> DictionaryRecords = DictionaryTab.getDictionaryKeysMultiReps();
			  List<ArrayList<String>> compoundWithWords = new ArrayList<ArrayList<String>>();  
			  String line;
			   	while((line = reader.readLine()) != null)
			   	{
			   		line = line.trim();
		   		    String[] splitted = line.split("	"); 
		   		    if(splitted[2].equals("fully_tagged"))
		   		    {
		   			for (int i = 15; i < splitted.length; i++) {
			   				if(splitted[i].contains("¯"))
			   					{
			   						String compound = splitted[i].split("¯")[1];
									String compoundWords ="";
									int j;
									innerloop:
									for(j=i;j<splitted.length;j++)
									{
										String word = splitted[j].split("֏")[0];
										compoundWords += word+" ";		
										try {
											if(!splitted[j+1].contains("¯"))
												break innerloop;
										} catch (Exception e) {
											break innerloop;
										}
										
									}
									compoundWords = compoundWords.trim();
									i=j;

									ArrayList<String> temp = new ArrayList<String>();
									temp.add(compound);
									temp.add(compoundWords);
									if(!compoundWithWords.contains(temp))
										compoundWithWords.add(temp);
									
									//************
//									if(compoundWithWords.size()==65)
//										System.out.println();
			   					}
		   		       }
		   			
			    	}
			   }
		   		reader.close();
		   		return compoundWithWords;
		  } // end method
	          
		  

		  public String getDictionaryKeyFromFile(String sentenceNumber,int wordIndex) throws InterruptedException, FileNotFoundException
		 {
		  	 String info = "";
		 	 int count = 0;
		 	 Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\corpus.txt"), "UTF-8");
		 	 while(in.hasNextLine()) {
		 	    String line = in.nextLine().trim();
		 	    String[] splitted = line.split("	");
		 	
		 	    try { 
		 	    		if(sentenceNumber.equals(splitted[0]))
		 	    	{
		 	    			for (int i = 15; i < splitted.length; i++) {
		 	    				if( count == wordIndex )
		 	    					info = splitted[i];  
		 	    				count++;
		 						}
		 	    	}
		 	    			
		 	    	} catch (Exception e) {
		 	    		System.out.println(e);
		 	    		}	
		 	    }
		 	String dictionaryKey = "";
		 	try {
		 	 dictionaryKey = info.split("¿")[1].split("\\|")[0];		
		 	
		 	}catch (Exception e) {
		 		// Exception if the word is not tagged
		 		dictionaryKey = "Not tagged";
		 		}
		    		
		 		in.close();
		    	return dictionaryKey;
		 }
} // Class end
	  
