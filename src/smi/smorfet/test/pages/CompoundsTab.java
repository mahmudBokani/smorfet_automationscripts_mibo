package smi.smorfet.test.pages;


import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.collect.Lists;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;


/**
 * Define elements and methods for the Homepage
 *
 */
public class CompoundsTab extends BasePage {
	
		// Dictionary whole frame tab
		@FindBy(how=How.ID,using ="TitleBar")
		public WebElement TitleBar;
		
		// Intro window
		@FindBy(how=How.NAME,using ="Attachment")
		public WebElement WindowAttachment;
		
		// Message when press new button twice 
		@FindBy(how=How.NAME,using ="Warning: bad key:")
		public WebElement newTwice;
		
		// Cancel button on the bad key warning
		@FindBy(how=How.NAME,using ="Cancel")
		public WebElement cancelBadKey;
		
		// Cancel button on the bad key warning
		@FindBy(how=How.NAME,using ="Close")
		public WebElement closeBadKey;
		
		// Dictionary tab 
		@FindBy(how=How.NAME,using ="Dictionary")
		public WebElement dictionaryTab;
		
		// Abort button in menu intro window
		@FindBy(how=How.NAME,using ="Abort")
		public WebElement btnAbort;
		
		// Compounds tab name
		@FindBy(how=How.NAME,using ="Compounds")
		public WebElement CompoundsTab;
		
		// compounds tab whole frame
		@FindBy(how=How.ID,using ="tc_TabControl")
		public WebElement tabcompounds;
		
		// Origin dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_origin")
		public WebElement dd_Origin;
		
		// Acronym dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_acronym")
		public WebElement dd_Acronym;
		
		// Style dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_style")
		public WebElement dd_Style;
		
		// Register dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_register")
		public WebElement dd_Register;
		
		// Frequency dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_frequency")
		public WebElement dd_frequency;
		
		// Compound Type dropdown 
		@FindBy(how=How.ID,using ="dd_compounds_type")
		public WebElement dd_CompoundsType;
			
		// New button
		@FindBy(how=How.ID,using ="bt_new")
		public WebElement btnNew;
		
		// Save button
		@FindBy(how=How.ID,using ="bt_save")
		public WebElement btnSave;
		
		// "Cancel" button on the confirmation message for the Save button
		@FindBy(how=How.NAME,using ="Cancel")
		public WebElement btnCancelSave;
		
		// "Cancel" button on the confirmation message for the Mass button
		@FindBy(how=How.NAME,using ="cancel")
		public WebElement btnCancelMass;
		
		// To be dropdown on the confirmation message for the Mass button
		@FindBy(how=How.ID,using ="dd_utility_value")
		public WebElement btnToBeMass;
		
	    // General Popup window
		@FindBy(how=How.ID,using ="panel1")
		public WebElement generalPopUp;
		
		// "Yes" button on the confirmation message for the Save button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement btnOKSave;
		
		// "YES" in the confirmation message for the new button
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement btnYes;
		
		// "No" in the confirmation message for the new button
		@FindBy(how=How.NAME,using ="No")
		public WebElement btnNoNew;
		
		// Key form text field
		@FindBy(how=How.ID,using ="tb_compounds_key_form")
		public WebElement txtKeyForm;
		
		// Language combobox
		@FindBy(how=How.ID,using ="dd_compounds_language")
		public WebElement cmpLanguage;
	
		// POS combobox
		@FindBy(how=How.ID,using ="dd_compounds_POS")
		public WebElement cmpPOS;
		
		// Origin Combobox
		@FindBy(how=How.ID,using ="dd_compounds_origin")
		public WebElement cmpOrigin;
	
		// Style Combobox
		@FindBy(how=How.ID,using ="dd_compounds_style")
		public WebElement cmpStyle;
		
		// First dropdown for search: dd_condition_field
		@FindBy(how=How.ID,using ="dd_condition_field")
		public WebElement cmpCondition_field;
		
		// Second dropdown for search: dd_condition_operation
		@FindBy(how=How.ID,using ="dd_condition_operation")
		public WebElement cmpCondition_operation;
		
		// The text field for search :dd_condition_value
		@FindBy(how=How.ID,using ="dd_condition_value")
		public WebElement cmpCondition_value;
		
		// Dropdown after the search text field "all records" dd_all_or_next
		@FindBy(how=How.ID,using ="dd_all_or_next")
		public WebElement cmpCondition_all_next;
		
		// Retrieve button
		@FindBy(how=How.ID,using ="bt_retrieve_same")
		public WebElement btnRetrieve_same;
		
	 	// Acronym Combobox, choose value
		@FindBy(how=How.ID,using ="dd_compounds_acronym")
		public WebElement cmpAcronym;
	
	    // Register Combobox, choose value
		@FindBy(how=How.ID,using ="dd_compounds_register")
		public WebElement cmpRegister;
	
		// Frequency Combobox, choose value
		@FindBy(how=How.ID,using ="dd_compounds_frequency")
		public WebElement cmpFrequency;
	
		// Compounds type dropdown dd_compounds_type
		@FindBy(how=How.ID,using ="dd_compounds_type")
		public WebElement cmpType;
		
		// Compounds records list
		@FindBy(how=How.ID,using ="lb_compounds_retrieved_records")
		public WebElement lst_records;
	
		// Frames list
		@FindBy(how=How.ID,using ="lb_compounds_frames")
		public WebElement lst_frames;
		
		// Window too many many retrieved records
		@FindBy(how=How.NAME,using ="Too many retrieved records")
		public WebElement window_too_many_records;
		
		// Retrieved records label
		@FindBy(how=How.ID,using ="ll_compounds_retrieved_records")
		public WebElement ll_compounds_retrieved_records;
		
		// Window too many many retrieved records, OK button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement window_too_many_records_ok;
		
		// Lexical relations list
		@FindBy(how=How.ID,using ="lb_compounds_lexical_relations")
		public WebElement lst_Lexical_relations;
		
		// Modified On field
		@FindBy(how=How.ID,using ="tb_compounds_modified_on")
		public WebElement modified_on;
		
		// Semantic Field list
		@FindBy(how=How.ID,using ="lb_compounds_semantic_fields")
		public WebElement lb_compounds_semantic_fields;
		
		// Register list
		@FindBy(how=How.ID,using ="dd_compounds_register")
		public WebElement lst_Regester;
		
		// Acronym list
		@FindBy(how=How.ID,using ="dd_compounds_acronym")
		public WebElement lst_Acronym;
		
		// Semantic groups list
		@FindBy(how=How.ID,using ="lb_compounds_semantic_groups")
		public WebElement lst_SemanticGroups;
		
		// Language list
		@FindBy(how=How.ID,using ="dd_compounds_language")
		public WebElement lst_Language;
		
		// Part of speech list
		@FindBy(how=How.ID,using ="dd_compounds_POS")
		public WebElement lst_POS;
		
		// Frequency list
		@FindBy(how=How.ID,using ="dd_compounds_frequency")
		public WebElement lst_frequency;
		
		// Definition textbox
		@FindBy(how=How.ID,using ="tb_compounds_definition")
		public WebElement lst_Definition;
		
		// Semantic relations box
		@FindBy(how=How.ID,using ="lb_compounds_semantic_relations")
		public WebElement lst_SemanticRelations;
		
	   // Version field
		@FindBy(how=How.ID,using ="tb_compounds_key_version")
		public WebElement Field_Version;
		
		// Style dropdown
		@FindBy(how=How.ID,using ="dd_compounds_style")
		public WebElement Style_DD;
		
		// Etymology text field
		@FindBy(how=How.ID,using ="tb_compounds_etymology")
		public WebElement Etymology_Text;
		
		// Forms list 
		@FindBy(how=How.ID,using ="lb_compounds_words")
		public WebElement Forms_list;
		
		// Spellings list 
		@FindBy(how=How.ID,using ="lb_compounds_spellings")
		public WebElement Spellings_list;
		
		// Volume POS dropdown  
		@FindBy(how=How.ID,using ="dd_compounds_volume")
		public WebElement cmpVolumePOS;
		
		// Empty value popup message 
		@FindBy(how=How.NAME,using ="Value empty")
		public WebElement Empty_Popup;
		
		// Empty popup text : Value is empty! Please enter value 
		@FindBy(how=How.ID,using ="65535")
		public WebElement Empty_Popup_text;
		  
		// Left arrow in the footer bt_prev 
		@FindBy(how=How.ID,using ="bt_prev")
		public WebElement Previous_button;
		
		// Right arrow in the footer bt_next 
		@FindBy(how=How.ID,using ="bt_next")
		public WebElement Next_button;
		
		// Duplicate button
		@FindBy(how=How.ID,using ="bt_duplicate")
		public WebElement bt_duplicate;
		
		// Duplicate confirmation message - NO button
		@FindBy(how=How.NAME,using ="No")
		public WebElement btnNoDuplicate;
		
		// Duplicate confirmation message - Yes button
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement btnYesDuplicate;
		
		// Revert button
		@FindBy(how=How.ID,using ="bt_revert")
		public WebElement bt_revert;
		
		// Revert file  confirmation message
		@FindBy(how=How.NAME,using ="Revert File")
		public WebElement revertMessage;
		
		// Revert file  confirmation message - OK button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement btnOk;
		
		// Revert file  confirmation message - Cancel button
		@FindBy(how=How.NAME,using ="Cancel")
		public WebElement btnCancelRevert;
		
		// Merge button
		@FindBy(how=How.ID,using ="bt_merge_files")
		public WebElement  btn_merge_files;
		
		// Merge popup: Value dropdown 
		@FindBy(how=How.ID,using ="dd_utility_value")
		public WebElement  cmpPopupValue;
		
		// Merge popup: Cancel button 
		@FindBy(how=How.ID,using ="bt_utility_cancel")
		public WebElement  btnpopupCancelWindow;
		
		// Merge dropdown dd_utility_value
		@FindBy(how=How.ID,using ="dd_utility_value")
		public WebElement  dd_Merge;
		
		// Merge button in the popup
		@FindBy(how=How.ID,using ="bt_utility_accept")
		public WebElement  btn_popup_Merge;
		
		// The message   when merge end successfully 
		@FindBy(how=How.NAME,using ="file EN/Data/Compounds for merge.txt\rwas successfully converted to include all the last dictionary changes.\rthe converted file is saved as EN/Data/Compounds for merge.txt_converted")
		public WebElement TXTMergedSuccess;
		
		// The message   when merge end successfully 
		@FindBy(how=How.NAME,using ="file HE/Data/Compounds for merge.txt\rwas successfully converted to include all the last dictionary changes.\rthe converted file is saved as HE/Data/Compounds for merge.txt_converted")
		public WebElement HETXTMergedSuccess;
		
		// Close button in the merge successfully 
		@FindBy(how=How.NAME,using ="Close")
		public WebElement btn_Close;
		
		// Merge ended successfully 
		// Merge warning message  Merge warning
		@FindBy(how=How.NAME,using ="Merging records ended successfuly")
		public WebElement mergeEnded;
		
		// Merge warning message  Merge warning
		@FindBy(how=How.NAME,using ="Merge warning")
		public WebElement mergeWarning;
		
		// Merge warning message Yes button : Yes
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement mergeWarningYes;
		
		// Merge warning message No button :
		@FindBy(how=How.NAME,using ="No")
		public WebElement mergeWarningNo;
		
		// Merge warning message Yes button : Yes
		@FindBy(how=How.NAME,using ="Cancel")
		public WebElement mergeWarningCancel;
		
		// Mass attribute button   
		@FindBy(how=How.ID,using ="bt_mass_attribute")
		public WebElement  bt_mass_attribute;
		
		// Mass attribute popup : semantic groups dropdown  
		@FindBy(how=How.ID,using ="dd_utility_field")
		public WebElement  dd_SemanticGroups;
		
		// Mass attribute popup : Value dropdown
		@FindBy(how=How.ID,using ="dd_utility_value")
		public WebElement  dd_ToBe;
		
		// Mass attribute popup : Change button
		@FindBy(how=How.ID,using ="bt_utility_accept")
		public WebElement  bt_Change;
		
		// Mass attribute dropdown button : DropDown
		@FindBy(how=How.ID,using ="DropDown")
		public WebElement  dd_open;
		
		// Delete button   
		@FindBy(how=How.ID,using ="bt_delete")
		public WebElement  bt_delete;
		
		// Yes button on the delete confirmation message 
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement yesDelete;
		
		// Delete : record successfully remove message - OK button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement btnOKremoved;
		
		// No button on the delete confirmation message 
		@FindBy(how=How.NAME,using ="No")
		public WebElement noDelete;
		
		// Attach button bt_attach
		@FindBy(how=How.ID,using ="bt_attach")
		public WebElement  bt_attach;
		
		// Attach :confirmation message - OK button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement btnOKPopupWindow;
		
		// Spellings list box 
		@FindBy(how=How.ID,using ="lb_compounds_spellings")
		public WebElement  lb_Compounds_spellings;
		
		// Semantic relation Stress Dropdown
		@FindBy(how=How.NAME,using ="Stress")
		public WebElement cb_Stress;
		
		// Semantic relation noun combo box 
		@FindBy(how=How.NAME,using ="n[noun]")
		public WebElement cb_noun;
		
		// "Yes" button on the key field message
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement yesKeyFieldMessage;
		
		// Look in Corpus button 
		@FindBy(how=How.ID,using ="bt_look_in_corpus")
		public WebElement  btnLookCorpus;
		
		// Tagging compounds: first POS dropdown  Derived
		@FindBy(how=How.NAME,using ="Derived")
		public WebElement taggingPOS;
		
		// Tagging compounds: first POS dropdown  Derived
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement lookPopupYes;
		
		// Number of the tagged compounds lb_tagged
		@FindBy(how=How.ID,using ="lb_tagged")
		public WebElement  numberOfTaggedCompounds;
		
		// Look in corpus popup when corpus match found : "
		@FindBy(how=How.NAME,using ="Corpus match found: To copy selection press Yes, to continue looking press No.")
		public WebElement matchCorpusFound;
		
		// The text from the message:  Illegal POS selected!\r\rValue will be reverted
		@FindBy(how=How.NAME,using ="Illegal POS selected!\r\rValue will be reverted")
		public WebElement illegalText;
		
		// Illegal POS message - OK button
		@FindBy(how=How.NAME,using ="OK")
		public WebElement btnOKIllegal;
		
		// The upper arrow for scroll bar ID= SmallDecrement For Part of speech 
		@FindBy(how=How.NAME,using ="Back by small amount")
		public WebElement  scrollUpArrow;
		
		// The bottom arrow for scroll bar ID= SmallDecrement 
		@FindBy(how=How.NAME,using ="Forward by small amount")
		public WebElement  scrollDownArrow;
		
		// Warning: similar key exists message     
		@FindBy(how=How.NAME,using ="Warning: similar key exists:")
		public WebElement  similarKeyMessage;
		
		// Yes button on the similar key exists message     
		@FindBy(how=How.NAME,using ="Yes")
		public WebElement  yesSimilarKeyMessage;
		
		// No button on the similar key exists message     
		@FindBy(how=How.NAME,using ="No")
		public WebElement  noSimilarKeyMessage;
		
		//Cancel button on the similar key exists message     
		@FindBy(how=How.NAME,using ="Cancel")
		public WebElement  cancelSimilarKeyMessage;
	
	   // Derived pane
	   @FindBy(how=How.NAME,using ="Derived")
	   public WebElement pane_Derived;
	   
	   // Compounds Manager Message:" when edit the key form
	   @FindBy(how=How.NAME,using ="Compounds Manager Message:")
	   public WebElement changeKeyMsg;
	   
	   // Yes button on the Compounds Manager Message:" when edit the key form
	   @FindBy(how=How.NAME,using ="Yes")
	   public WebElement yesEditKey;
	   
	   // No button on the Compounds Manager Message:" when edit the key form
	   @FindBy(how=How.NAME,using ="No")
	   public WebElement noEditKey;
	   
	   // Text on the Compounds Manager Message:" when edit the key form
	   @FindBy(how=How.NAME,using ="You are about to change a key field. Is this ok?")
	   public WebElement textEditKey;
	   
	   // Text on the  Message:" Form not in right format
	   @FindBy(how=How.NAME,using ="Form not in right format\r\rYou entered only one word in the form. This should be a dictionary record!")
	   public WebElement notRightFormatText;
	   
	   // OK button on the  Message:" Form not in right format
	   @FindBy(how=How.NAME,using ="OK")
	   public WebElement notRightFormatOK;
	   
	   // Smorfet main window
	   @FindBy(how=How.ID,using ="SMI_Root_GUI")
	   public WebElement SMI_Root_GUI;
	   
	   // Scroll Large
	   @FindBy(how=How.ID,using ="LargeIncrement")
	   public WebElement LargeIncrement;
	   
	   // Mass window pane
	   @FindBy(how=How.ID,using ="panel1")
	   public WebElement panel1;
	   
	   // Mass Dictionary Conversion popup
	   @FindBy(how=How.NAME,using ="Mass Dictionary Conversion")
	   public WebElement massConversionPopup;
	   
	   // Yes button on Mass Dictionary Conversion popup
	   @FindBy(how=How.NAME,using ="Yes")
	   public WebElement massConversionYes;
	   
	   // No button
	   @FindBy(how=How.NAME,using ="No")
	   public WebElement No;
	
	   // Attribute editor
	   @FindBy(how=How.ID,using ="Attribute_Editor")
	   public WebElement Attribute_Editor;
	   
	   // Dictionary Manager Message: 
	   @FindBy(how=How.NAME,using ="Close")
	   public WebElement XClose;
	   
	   // No button Mass Dictionary Conversion popup
	   @FindBy(how=How.NAME,using ="Mass Dictionary Conversion")
	   public WebElement massConversionNo;
	   
	   // Mass "change its" dropdown
	   @FindBy(how=How.ID,using ="dd_utility_field")
	   public WebElement massChangeItsDD;
			
	   // Mass "to be" dropdown
	   @FindBy(how=How.ID,using ="dd_utility_value")
	   public WebElement massToBeDD;
			
	   // Mass "Dictionary Attributing Message" dialog - No button
	   @FindBy(how=How.NAME,using ="No")
	   public WebElement massNoBtn;
			
	   // Mass "Dictionary Attributing Message" dialog - Yes button
	   @FindBy(how=How.NAME,using ="Yes")
	   public WebElement massYesBtn;
		
	   // Attach popup window
	   @FindBy(how=How.NAME,using ="Attachment Message:")
	   public WebElement AttachPopup;
	   
	   // Uppercase/lowercase button
	   @FindBy(how=How.ID,using="bt_condition_uppercase")
	   public WebElement bt_condition_uppercase;
		
	   // Record status field value
	   @FindBy(how=How.ID,using="dd_compounds_status")
	   public WebElement dd_compounds_status;
		
		
	   public enum posEnd{punctuation, quantifier, question, suffix, title, verb, verb_particle};
	   
	   public enum POS{
		   accusative_word,adadjective,adverb,classifier,conjunction,demonstrative, 
		   existential,foreign_word,infinitive,modal,negation,number,phrase_ends, 
		   prefix,pronoun,quantifier,suffix,verb,adjective,article, 
		   clause_opener,copula,diacritic,foreign_script,genetive_word,interjection, 
		   name,noun,phrase_begins,possessive,preposition,punctuation,question,title,verb_particle
		  }
		
		
		public enum semantic_relation {
			   unknown, none, synonym, hyponym, hyponym_is, antonym, member_of, 
			   part_of, substance_of, verb_group, member, part, substance, 
			   similar_to, entailment, topic, region, usage, hypernym, 
			   hypernym_is, attribute, cause, topic_is, region_is, usage_is, 
			   see_also
			  }
			  
		public enum lexical_relation {
		   unknown, male_form, female_form, single_form, plural_form, 
		   alt_plural_form, dual_form, comparative, superlative, 
		   capability_adjective, relation_adjective, quality_adjective, 
		   lack_adjective, from_adjective, state_substantive, 
		   capability_substantive, quality_substantive, agent_form, 
		   action_substantive, position_substantive, condition_substantive, 
		   doctrine_substantive ,specialist_substantive, 
		   Affiliation_substantive, apocopation, apocopated_from, 
		   negative_form, verbal, from_noun ,derived_form, verb_extension, 
		   extended_from, adverbial, from_preposition, from_verb, 
		   diminutive_form, occupation_form, shop_form, comparative_of, 
		   superlative_of, capability_adjective_of, relation_adjective_of, 
		   quality_adjective_of, lack_adjective_of, from_adjective_of, 
		   state_substantive_of, capability_substantive_of, 
		   quality_substantive_of, agent_form_of, action_substantive_of, 
		   position_substantive_of, condition_substantive_of, 
		   doctrine_substantive_of, specialist_substantive_of, 
		   Affiliation_substantive_of, negative_form_of, verbal_of, 
		   verb_extension_of, adverbial_of, from_preposition_of, 
		   from_verb_of, diminutive_form_of, occupation_form_of, 
		   shop_form_of
		  }
	
		public enum Compounds_tags{
			   POS, Lemma, Form_Type, Conjunction, Relative, Preposition,
			   Definite, Possessive, Female, Plural, Cases, Tenses, Negative, 
			   Derived, Stress
			  }
		
		
		
	    /**
		 * Get random Hebrew text 
	     * @param length - enter the length of the text
		 */
	        public String RandomHEString(int length) {
	         char[] chars = "×�×‘×’×“×”×•×–×—×˜×™×›×œ×ž×Ÿ×¡×¢×¤×¦×§×¨×©×ª".toCharArray();
	         StringBuilder sb = new StringBuilder();
	         Random random = new Random();
	         for (int i = 0; i < length; i++) {
	             char c = chars[random.nextInt(chars.length)];
	             sb.append(c);
	         }
	         String output = sb.toString();
	         return output;
	        
	        }
		
				
    /**
	 * Get random text 
     * @param length - enter the length of the text
	 */
        public String RandomString(int length) {
         char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
         StringBuilder sb = new StringBuilder();
         Random random = new Random();
         for (int i = 0; i < length; i++) {
             char c = chars[random.nextInt(chars.length)];
             sb.append(c);
         }
         String output = sb.toString();
         return output;
        
        }
        
        // # Type in Key Form
        public String keyFormTypeRandom() {
			String random_input = RandomString(5);
			this.txtKeyForm.sendKeys(random_input);
			return random_input;
		}

       
        // find all elements in dd_condition_field
        // later try to make it globally
        //private List<String> getCompoboxValues(WebElement lstBox)
        public List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
    	{
        	//lstBox.click();
        	Thread.sleep(1000);
    		//get all children
    		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//        	try {
//        		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    		//System.out.println(itemlist.size());
    		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			itemNames.add(e.getAttribute("Name"));
    		    //itemNames.add(e.getAttribute(Name));
    		}
    		// Delete unknown value, it is not usable
    		if (itemNames.contains("unknown"))
    			itemNames.remove("unknown");
    		return itemNames;
    	}
        
        /**
         * Get chosen value in a dropdown
         * @param lstBox
         * @return
         * @throws InterruptedException
         */
        public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
    	{
        	
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			boolean isSelected = e.isSelected();
    			if (isSelected)
    				itemNames.add(e.getAttribute("Name"));
    		}
    		return itemNames;
    	
    	}
        
//        /**
//         * Choose value from dropdown that is LISTITEM
//          @param lstBox
//          @param ItemName
//          @throws InterruptedException
//         */
//        public void chooseValueInDropdown(WebElement lstBox, String ItemName) throws InterruptedException
//    	{
//        	lstBox.click();
//        	Thread.sleep(1000);
//        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
////    		System.out.println(itemlist.size());
//    		
//        	// Part of Speach case, use the scroll if needed
//        	if (lstBox == CurrentPage.As(CompoundsTab.class).cmpPOS) {
//        		String checkPOS = null;
//        		if (ItemName.contains(" "))
//        			checkPOS = ItemName.replace(" ", "_");
//        		
//        		Boolean atEndOfList = false;
//        		  for (posEnd pos : posEnd.values()) {
//        		        if (pos.name().equals(checkPOS)) {
//        		        	atEndOfList = true;
//        		        	break;
//        		        }
//        		    }
//        		  
//        	// Move the scroll
//        	if (atEndOfList)
//        		CurrentPage.As(CompoundsTab.class).LargeIncrement.click();
//        	}
//
////        	ArrayList<String> itemNames= new ArrayList<String>();
//    		for(WebElement e : itemlist)
//    		{
//    			//System.out.println(e.getAttribute("Name"));
////    			e.getAttribute("Name");
//
//    			if (e.getAttribute("Name").equals(ItemName))
//				{
//				  e.click();
//				  return;
//				}
//    		}
//    	}
//  
//        
//            window = driver.find_element_by_id('lb_compounds_retrieved_records')
//
//            records_list_box = driver.find_element_by_id('lb_compounds_retrieved_records')
//            all_records_in_listbox = records_list_box.find_elements_by_xpath(
//                "./*[contains(@ControlType, 'ControlType.ListItem')]")
//            records = []
//
//            for ii in all_records_in_listbox:
//                records.append(ii.get_attribute('Name'))

           
           /**
            * Choose POS value
            * @return
            */
           public String POS_Select() {
			return null;
//          #  POS compounds values
//          pos = {"Action name": '{e}', "accusative word": '{w}', "adadjective": '{d}', "adjective": '{a}',
//                 "adverb": '{r}',
//                 "article": '{t}', "classifier": '{l}', "Clause opener": '{s}', "Conjunction": '{c}', "copula": '{u}',
//                 "demonstrative": '{z}', "diacritic": '{D}', "existential": '{C}', "foreign script": '{L}',
//                 "foreign word": '{S}',
//                 "genetive word": '{F}', "infinitive": '{i}', "  nterjection": '{j}', "modal": '{m}', "name": '{x}',
//                 "negation": '{g}', "noun": '{n}', "number": '{N}', "phrase begins": '{B}', "phrase ends": '{E}',
//                 "possessive": '{V}', "prefix": '{f}', "preposition": '{p}', "pronoun": '{o}'}
//
//          # pos = {"Action name": '{e}', "accusative word": '{w}', "adadjective": '{d}', "adjective": '{a}', "adverb": '{r}',
//          #        "article": '{t}', "classifier": '{l}', "Clause opener": '{s}', "Conjunction": '{c}', "copula": '{u}',
//          #        "demonstrative": '{z}', "diacritic": '{D}', "existential": '{C}', "foreign script": '{L}', "foreign word":'{S}',
//          #        "genetive word": '{F}', "infinitive": '{i}', "  nterjection": '{j}', "modal": '{m}', "name": '{x}',
//          #        "negation": '{g}', "noun": '{n}', "number": '{N}', "phrase begins": '{B}', "phrase ends": '{E}',
//          #        "possessive": '{V}', "prefix": '{f}', "preposition": '{p}', "pronoun": '{o}', "punctuation": '{P}',
//          #        "quantifier": '{q}', "question": '{Q}', "suffix": '{U}', "title": '{l}', "verb": '{v}', "verb particle": '{A}'}
//
//          key, value = random.choice(list(pos.items()))
//          print ('key: {0}, value: {1}'.format(key, value))
//          driver.find_element_by_id('dd_compounds_POS').click()
//          driver.find_element_by_name(key).click()
//          return value
        	
        }
        
           /**
            * Get clean records names from the compounds.txt files
            * @return
            * @throws FileNotFoundException
            */
        public List<String> getRecordsFromcompounds() throws FileNotFoundException {
		   			
		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\compounds.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		    String[] removeLeft = splitted[0].split("}");
		   		    String[] removeRight = removeLeft[1].split("~");
		   		    // remove spaces in the beginning 
//		   		    removeRight[0]=  removeRight[0].replaceAll(" ", "");
		   		    removeRight[0] = removeRight[0].replace("_", " ");
		   		    removeRight[0] = removeRight[0].replace("-", " ");
		   		    lines.add(removeRight[0]);
		   		}
		   		
		   		in.close();
				return lines;
        }
        
        /**
         * Get the frames for specified record name
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public String getFramesOfRecordFromcompounds(String record) throws FileNotFoundException {
   			
	   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\compounds.txt"), "UTF-8");
//	   		List<String> lines = new ArrayList<>();
	   		while(in.hasNextLine()) {
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");

	   		    if (record.equals(splitted[0])) {
	   		    	in.close();
	   		    	return splitted[10].replace("_", " ");
	   		    }
	   		}
	   		
	   		in.close();
			return "";
    }
        
        public String getOnlyRecordName(String fullRecord) {
        	String[] removeLeft = fullRecord.split("}");
    		String[] removeRight = removeLeft[1].split("~");
    		return removeRight[0].replace("_", " ");
		}
        
        /**
         * Get clean records names from the Compounds.txt files that have
         * Compounds lexical relations or Compounds semantic relations
         * @return
         * @throws FileNotFoundException
         */
        public Map<String, String> availableRelationsForCompounds(String relationType, int amount) throws FileNotFoundException {
        
        Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Compounds.txt"), "UTF-8");
//        List<String> lines = new ArrayList<>();
        Map<String,String> records_relations = new HashMap<String,String>();
        
        while(in.hasNextLine()) {
            String line = in.nextLine().trim();
            //String[] splitted = line.split("			");
            String[] splitted = line.split("	");

//            String chosen = "";
            String fetch = "";
            try {
            	 //if (splitted[1].isEmpty()) continue;
            	for (String part : splitted) {
            		if (part == "" || !part.contains("¬")) continue;
            		
            		if (relationType == "semantic")
            		for (semantic_relation semantic_relation : semantic_relation.values()) {
            		    if (part.contains(semantic_relation+"")) {
            		    	fetch = part;
            		    	break;}
            		}
            		else if (relationType == "lexical")
                		for (lexical_relation lexical_relation : lexical_relation.values()) {
                		    if (part.contains(lexical_relation+"")) {
                		    	fetch = part;
                		    	break;}
                		}
            		if (fetch != "") break;
				}
            	
            	//if (splitted[1].isEmpty()) continue;
            	if (fetch == "") continue;
            	 String[] splittedRelationValues = fetch.split("¦");
            	 for (String semantic : splittedRelationValues) {
//					String[] cleanValueLeft = semantic.split("}");
//					String[] cleanValueRight = cleanValueLeft[1].split("~");
            		 
            		 // Skip if key is null
            		 if (semantic.split("¬")[0].equals("null"))
            			 continue;
            		 
            		 String part0 = "";
            		 if (relationType == "semantic")
            			 part0 = semanticRelationKeysConverter(semantic.split("¬")[0]);
            			 else if (relationType == "lexical")
            				 part0 = lexialRelationKeysConverter(semantic.split("¬")[0]);
            		 
            		 String cleanValueRight = semantic.split("¬")[1];
//            		 System.out.println(cleanValueRight);
//            		 String[] koko = cleanValueRight.split("\\{");
//            		 System.out.println(cleanValueRight.split("\\{"));
//            		 System.out.println(cleanValueRight.split("\\{")[0]);
            		 String part1 = cleanValueRight.split("\\{")[0];
            		 String part2 = cleanValueRight.split("\\{")[1].split("\\}")[0];
            		 String part3 = cleanValueRight.split("\\{")[1].split("\\}")[1];
            		 // Replace part 1
            		 if (part1.equals("English")) part1 = "EN";
            		 // Replace part 2
            		 part2 = POSConverter(part2);
            		 // Put together again
            		 cleanValueRight = part0 + "¬" +part1 + "{" + part2 + "}" + part3;
            		 
            		 
					if(cleanValueRight.contains("_") || cleanValueRight.contains("-")){
						String[] recordName = splitted[0].split("	");
//						String[] recordNameLeftClean = recordName[0].split("}");
//						String[] recordNameRightClean = recordNameLeftClean[1].split("~");
//						if(!lines.contains(recordNameRightClean[0]))
//							lines.add(recordNameRightClean[0]); // get clean compound record from splitted 0
						records_relations.put(recordName[0], cleanValueRight);
//						if(!lines.contains(recordName[0]))
//							lines.add(recordName[0]); // get clean compound record from splitted 0
					}
					
				}
	     } catch (Exception e) {
	     }
	        }
	
	        in.close();
	//     return lines;
	        
//	        // Copy the records that have relations to each other
//	        Map<String,String> records_relations_with_link_back = new HashMap<String,String>();
//	        for (String key : records_relations.keySet()) {
////	            System.out.println("key: " + key);
////	        	key = getOnlyRecordName(key).replace(" ", "_");
//	        	for (String value : records_relations.values()) {
////	        		value = value.split("ï¿½")[1];
////	        		System.out.println("value: " + value);
//	        	    if (key.equals(value.split("ï¿½")[1])) {
//	        	    	records_relations_with_link_back.put(key, value);
//	        	    	amount --;
//	        	    	if (amount <= 0)
//	        	    		return records_relations_with_link_back;
//	        	    		
////	        	    	break;
//	        	    }
//	        	    	
//	        	}
//	        }
	       
	        // Copy the records that have relations to each other
	        Map<String,String> records_relations_with_link_back = new HashMap<String,String>();
	        
	        // Check the first pair to search for related info
	        for(Map.Entry<String, String> entry1 : records_relations.entrySet()) {
	            String key1 = entry1.getKey();
	            String value1 = entry1.getValue();
	            
	            // Search for related info to the first pair
	            for(Map.Entry<String, String> entry2 : records_relations.entrySet()) {
	            	String key2 = entry2.getKey();
//		            String value2 = entry1.getValue();
		            
		            if (key2.equals(value1.split("¬")[1])) {
	        	    	records_relations_with_link_back.put(key1, value1);
	        	    	amount --;
	        	    	if (amount <= 0)
	        	    		return records_relations_with_link_back;
	        	    		
//	        	    	break;
	        	    }
	            }
	        }
	        
	        return records_relations_with_link_back;
	        
         }
        
        /**
         * Convert lexical relation keys
         * @param POSInput
         * @return
         */
        public String lexialRelationKeysConverter(String keyInput) {
//       	 POSInput = POSInput.replace(" ", "_");
       	 
         // Put manually the values here, later fetch from DB
         Map<String,String> keys = new HashMap<String,String>();
         keys.put("unknown", "XXX"); keys.put("male_form", "MAL"); keys.put("female_form", "FEM"); keys.put("single_form", "SIN");
         keys.put("plural_form", "PLU"); keys.put("alt_plural_form", "APL"); keys.put("dual_form", "DUA"); keys.put("comparative", "CMP");
         keys.put("superlative", "SUP"); keys.put("capability_adjective", "CPB"); keys.put("relation_adjective", "REL"); keys.put("quality_adjective", "QUA");
         keys.put("lack_adjective", "LCK"); keys.put("from_adjective", "FAJ"); keys.put("state_substantive", "STS"); keys.put("capability_substantive", "CPS");
         keys.put("quality_substantive", "QUS"); keys.put("agent_form", "AGN"); keys.put("action_substantive", "ACS"); keys.put("position_substantive", "POS");
         keys.put("condition_substantive", "COS"); keys.put("doctrine_substantive", "DOS"); keys.put("specialist_substantive", "SPS"); keys.put("Affiliation_substantive", "AFS");
         keys.put("apocopation", "APO"); keys.put("apocopated_from", "APF"); keys.put("negative_form", "NEG"); keys.put("verbal", "VBL");
         keys.put("from_noun", "FNN"); keys.put("derived_form", "DRF"); keys.put("verb_extension", "VEX"); keys.put("extended_from", "EXF");
         keys.put("adverbial", "ADL"); keys.put("from_preposition", "FPR"); keys.put("from_verb", "FVB"); keys.put("diminutive_form", "DIM");
         keys.put("occupation_form", "OCC"); keys.put("shop_form", "SHP"); keys.put("comparative_of", "CMO"); keys.put("superlative_of", "SPO");
         keys.put("capability_adjective_of", "CAO"); keys.put("relation_adjective_of", "RAO"); keys.put("quality_adjective_of", "QAO"); keys.put("lack_adjective_of", "LAO");
         keys.put("from_adjective_of", "FAO"); keys.put("state_substantive_of", "STN"); keys.put("capability_substantive_of", "CAN"); keys.put("quality_substantive_of", "QUN");
         keys.put("agent_form_of", "AFO"); keys.put("action_substantive_of", "ACN"); keys.put("position_substantive_of", "PON"); keys.put("condition_substantive_of", "CON");
         keys.put("doctrine_substantive_of", "DON"); keys.put("specialist_substantive_of", "SPN"); keys.put("Affiliation_substantive_of", "AFN"); keys.put("negative_form_of", "NFO");
         keys.put("verbal_of", "VLO"); keys.put("verb_extension_of", "VEO"); keys.put("adverbial_of", "ALO"); keys.put("from_preposition_of", "FPO");
         keys.put("from_verb_of", "FVO"); keys.put("diminutive_form_of", "DFF"); keys.put("occupation_form_of", "OFF"); keys.put("shop_form_of", "SFO");
         keys.put("root_family", "SRO");

         return keys.get(keyInput);
        }
        
        /**
         * Convert semantic relation keys
         * @param POSInput
         * @return
         */
        public String semanticRelationKeysConverter(String keyInput) {
//       	 POSInput = POSInput.replace(" ", "_");
       	 
         // Put manually the values here, later fetch from DB
         Map<String,String> keys = new HashMap<String,String>();
         
         keys.put("unknown", "XXX"); keys.put("none", "NON"); keys.put("synonym", "SYN"); keys.put("hyponym", "HPO");
         keys.put("hyponym_is", "HOI"); keys.put("antonym", "ANT"); keys.put("member_of", "MMO"); keys.put("part_of", "PTO");
         keys.put("substance_of", "SBO"); keys.put("verb_group", "VBG"); keys.put("member", "MEM"); keys.put("part", "PRT");
         keys.put("substance", "SUB"); keys.put("similar_to", "SIM"); keys.put("entailment", "ENT"); keys.put("topic", "TOP");
         keys.put("region", "REG"); keys.put("usage", "USG"); keys.put("hypernym", "HPR"); keys.put("hypernym_is", "HRI");
         keys.put("attribute", "ATT"); keys.put("cause", "CAU"); keys.put("topic_is", "TPI"); keys.put("region_is", "RGI");
         keys.put("usage_is", "UGI"); keys.put("see_also", "SEE");

         return keys.get(keyInput);
        } 

        /**
         * Get clean records names from the compounds.txt files
         * @return
         * @throws FileNotFoundException
         */
     public String getRecordsInfoFromcompounds(String searchedRecord) throws FileNotFoundException {
		   			
		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language +"\\Data\\compounds.txt"), "UTF-8");
		   		//List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");

		   		    String[] removeLeft = splitted[0].split("}");
		   		    String[] removeRight = removeLeft[1].split("~");
		   		// removeRight[0] = removeRight[0].replace("_", " ");
		   		// removeRight[0] = removeRight[0].replace("-", " ");
//		   		    lines.add(removeRight[0]);
		   		    if (removeRight[0].equals(searchedRecord)) {
		   		    	in.close();
		   		    	return line;
		   		    }
		   		}
		   		
		   		in.close();
				return "";
     }
         
     
		 /**
		  * Get regular records names from the compounds.txt files
		  * @return
		  * @throws FileNotFoundException
		  */
		  public String getRegularRecordsInfoFromcompounds(String searchedRecord) throws FileNotFoundException {
				   			
				   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+ "\\Data\\compounds.txt"), "UTF-8");
			   		//List<String> lines = new ArrayList<>();
			   		while(in.hasNextLine()) {
			   		    String line = in.nextLine().trim();
			   		    String[] splitted = line.split("	");
		
//			   		    String[] removeLeft = splitted[0].split("}");
//			   		    String[] removeRight = removeLeft[1].split("~");
			   		// removeRight[0] = removeRight[0].replace("_", " ");
			   		// removeRight[0] = removeRight[0].replace("-", " ");
		//		   		    lines.add(removeRight[0]);
			   		    if (splitted[0].equals(searchedRecord)) {
			   		    	in.close();
			   		    	return line;
			   		    }
			   		}
			   		
			   		in.close();
					return "";
		  }
     
     
             /**
              * Choose value in the Tagging area comboboxes
              * @param dropdown
              * @param Column
              * @param value
              * @throws InterruptedException
              * @throws AWTException
              */
             public void chooseTaggingValue(String dropdown, int Column, String value) throws InterruptedException, AWTException
          {
              dropdown = dropdown.replace(" ", "_");

              int chosenButton = Compounds_tags.valueOf(dropdown).ordinal() + (15 * Column) ;
//              int size = Compounds_tags.values().length;
              
              // Click on the button to open the dropdown
              List<WebElement> itemlistButton= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
              int count = 0;
              
              for(WebElement b : itemlistButton)
           { // buttons
            if (count == chosenButton) 
            {
             try {
              b.click();
          } catch (Exception e) {
           System.out.println("not clickable");
          }
            }
            count ++;
           } // end of buttons
              
              // Choose value in the dropdown
              count = 0;
              List<WebElement> itemlist= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
           for(WebElement e : itemlist)
           { 
            if (count == chosenButton) {
                   List<WebElement> itemlist1= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
                   for(WebElement f : itemlist1) {
                    if (f.getAttribute("Name").equals(value)) {
                  System.out.println("IN" + e.getAttribute("Name"));
                  f.click();
                 }
                   }
            }
           count++;
          } // close for
          } // close function         
     

   

             /**
	          * Convert POS name to POS code
	          * @param POSInput
	          * @return
	          */
	         public String POSConverter(String POSInput) {
	        	 POSInput = POSInput.replace(" ", "_");
	        	 
	          // Put manually the values here, later fetch from DB
	          Map<String,String> POS = new HashMap<String,String>();
	          POS.put("accusative_word", "W");  POS.put("clause_predicate", "e"); 
	          POS.put("adadjective", "d"); POS.put("adjective", "a"); 
	          POS.put("adverb", "r"); POS.put("article", "t"); 
	          POS.put("classifier", "I"); POS.put("clause_opener", "s"); 
	          POS.put("conjunction", "c"); POS.put("copula", "u"); 
	          POS.put("demonstrative", "z"); POS.put("diacritic", "D"); 
	          POS.put("existential", "C"); POS.put("foreign_script", "L"); 
	          POS.put("foreign_word", "S"); POS.put("genitive_word", "F"); 
	          POS.put("infinitive", "i"); POS.put("interjection", "j"); 
	          POS.put("modal", "m"); POS.put("name", "x"); 
	          POS.put("negation", "g"); POS.put("noun", "n"); 
	          POS.put("number", "N"); POS.put("phrase_begins", "B"); 
	          POS.put("phrase_ends", "E"); POS.put("possessive", "V"); 
	          POS.put("prefix", "f"); POS.put("preposition", "p"); 
	          POS.put("pronoun", "o"); POS.put("punctuation", "P"); 
	          POS.put("quantifier", "q"); POS.put("question", "Q"); 
	          POS.put("suffix", "U"); POS.put("title", "l"); 
	          POS.put("verb", "v"); POS.put("verb_particle", "A");
	          
	          return POS.get(POSInput);
	         } 

	         //this function for getting values from the tagging comboboxes:
	         /**
	                  * Get values from the Tagging area comboboxes
	                  * @param dropdown
	                  * @param Column
	                  * @param value
	                  * @throws InterruptedException
	                  * @throws AWTException
	                  */
	                 public List<String> getTaggingValues(String dropdown, int Column) throws InterruptedException, AWTException
	              {
	                  dropdown = dropdown.replace(" ", "_");

	                  int chosenButton = Compounds_tags.valueOf(dropdown).ordinal() + (15 * Column) ;
	                  
	                  // Click on the button to open the dropdown
//	                  List<WebElement> itemlistButton= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
	                  int count = 0;
	                  ArrayList<String> itemNames= new ArrayList<String>();
	               
	                  // Get values from the dropdown
	                  List<WebElement> itemlist= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
	               for(WebElement e : itemlist)
	               { 
	                if (count == chosenButton) {
	                       List<WebElement> itemlist1= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
	                       for(WebElement f : itemlist1) {
	                        itemNames.add(f.getAttribute("Name"));
	                       }
	                }
	               count++;
	              } // close for
	               
	               return itemNames;
	              } // close function
	  
 

	                 /**
	                  * Fetch the text and title of popup windows
	                  * @param test
	                  * @param logger
	                  * @param titleToCheck
	                  * @param textToCheck
	                  * @return
	                  */
	                 public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
	              {
	                  List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
	                  List<WebElement> text = null;
	                  List<WebElement> title = null;
	                  
	                  boolean titleIn = false;
	                  boolean textIn = false;
	                  
	                  // Check the main app for popup windows
	               for(WebElement e : windows)
	               { 
	                text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));         
	                      title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
	                  }
	               
	               // Fetch the found title
	               for(WebElement e : title)
	               { 
	                LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
	                if (e.getAttribute("Name").contains(titleToCheck))
	                 titleIn = true;
	                  }
	               
	               // Fetch the found title      
	               for(WebElement e : text)
	               { 
	                LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
	                if (e.getAttribute("Name").contains(textToCheck))
	                 textIn = true;
	                  }
	               
	               LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
	               
	                  if (titleIn && textIn)
	                   return true;
	                  else return false;
	              } // close function
     
	         
// this function does check that there are no popup windows:
	 /**
	         * Check that no Popup window is open
	         * @return
	         */
	        public boolean checkNoPopupWindow()
	     {
	         List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
	         
	         if (windows.size() == 0)
	          return true;
	         else return false;
	     }
	         
	        
	        /**
	         * Get the selected value from the Tagging area comboboxes
	         * @param dropdown
	         * @param Column
	         * @param value
	         * @throws InterruptedException
	         * @throws AWTException
	         */
	        public String getSelectedTaggingValue(String dropdown, int Column) throws InterruptedException, AWTException
	     {
	          dropdown = dropdown.replace(" ", "_");

	             int chosenButton = Compounds_tags.valueOf(dropdown).ordinal() + (15 * Column) ;
	             
	             // Click on the button to open the dropdown
//	             List<WebElement> itemlistButton= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
	             int count = 0;
	          
	             // Check which value is selected
	             List<WebElement> itemlist= pane_Derived.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
	          for(WebElement e : itemlist)
	          {
//	           System.out.println(e.getAttribute("Name"));
	           if (count == chosenButton) {
	            if (dropdown.equals("Lemma")) {
	             List<WebElement> itemlist1= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
//	                   itemlist1.remove(0);
	                   for(WebElement f : itemlist1) {
	                       return f.getAttribute("Name");              
	                   } 
	            }
	            else {
	                  List<WebElement> itemlist1= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
	                  itemlist1.remove(0);
	                  for(WebElement f : itemlist1) {
//	                   System.out.println(f.getAttribute("Name"));
	                      boolean isSelected = f.isSelected();
	                      if (isSelected) return f.getAttribute("Name");              
	                  }
	            } // close else
	           } // close if count
	          count++;
	         } // close for
	          
	          return null;
	     } // close function
	         
	        
	        
	        
	        /**
	         * Get POS that is valid with the current one
	         * @param currentPOS
	         * @return
	         */
	        public List<String> checkPOSValidity(String currentPOS) {
	         List<String> POSSvailable = convertPOSEnumToList();
	         List<String> POS2Delete = new ArrayList<String>();
	         POSSvailable.remove("pronoun");
	         POSSvailable.remove("modal");

	         // 1
	         if (currentPOS.equals("verb")) {
	        	 for(String POS : POSSvailable) {
	     if (!POS.equals("copula")) POS2Delete.add(POS);	 
	    }
	        	 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear();
	         }
	         // 2
	         if (!currentPOS.equals("verb")) {
	          POSSvailable.remove("copula");
	         }
	         // 3
	         if (currentPOS.equals("copula")) {
	        	 for(String POS : POSSvailable){
	     if (!POS.equals("verb")) POS2Delete.add(POS);	
	    }
	        	 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear();
	         }
	         // 4
	         if (!currentPOS.equals("copula")) {
	          POSSvailable.remove("verb");
	         }
	         // 5
	         if (currentPOS.equals("noun")) {
	        	 for(String POS : POSSvailable)
	        	 {
	        		  if (!POS.equals("title") && !POS.equals("name") && !POS.equals("demonstrative")) 
	        			  POS2Delete.add(POS);
	        	 }
	    		 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear();
	         }
	         // 6
	         if (currentPOS.equals("name")) {
	        	 for(String POS : POSSvailable) {
	     if (!POS.equals("title") && !POS.equals("noun")  && !POS.equals("demonstrative")) POS2Delete.add(POS);
	    }
	    		 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear();
	         }
	         // 7
	         if (currentPOS.equals("title")) {
	        	 for(String POS : POSSvailable) {
	     if (!POS.equals("name") && !POS.equals("noun")  && !POS.equals("demonstrative")) POS2Delete.add(POS);
	    }
	    		 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear();
	         }
	         // 8
	         if (currentPOS.equals("demonstrative")) {
	        	 for(String POS : POSSvailable) {
	     if (!POS.equals("title") && !POS.equals("noun")  && !POS.equals("name")) POS2Delete.add(POS);
	    }
	      		 POSSvailable.removeAll(POS2Delete);
	        	 POS2Delete.clear(); 
	         }
	         // 9
	         if (!currentPOS.equals( "noun") && !currentPOS.equals("name") && !currentPOS.equals("title")) {
	          POSSvailable.remove("demonstrative");
	         }
	         // 10
	         if (!currentPOS.equals("title") && !currentPOS.equals("name") && !currentPOS.equals("demonstrative")) {
	          POSSvailable.remove("noun");
	         }
	         // 11
	         if (!currentPOS.equals("demonstrative") && !currentPOS.equals("title") && !currentPOS.equals("noun")) {
	          POSSvailable.remove("name");
	         }
	         // 12
	         if (!currentPOS.equals("demonstrative") && !currentPOS.equals("name") && !currentPOS.equals("noun")) {
	          POSSvailable.remove("title");
	         }
	         // 13
	         if (currentPOS.equals("pronoun")) return null;
	         // 14
	         if (currentPOS.equals("modal")) return null;
	         
	         POSSvailable.remove(currentPOS);
	         
	         return POSSvailable;
	        }
	        
    public static List<String>  convertPOSEnumToList(){
       List<String> POSList=new ArrayList<String>();
       for (POS pos : POS.values()) {
        POSList.add( pos.name());
       }
       return POSList;
    }

	        
	        /**
	         * Choose values in mass window
	          @param field
	          @param value
	         */
	        public String chooseValuesInMassWindow(String field, String value) {
	        	// Click on the button to open the dropdown
	              List<WebElement> comboBoxes = panel1.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));

	              // Choose the field
	              // Click the dropdown button
	              List<WebElement> dropdownButton = comboBoxes.get(1).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
	              dropdownButton.get(0).click();
	              List<WebElement> itemlist= comboBoxes.get(1).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

		      		for(WebElement e : itemlist)
		      		{
		      			e.getAttribute("Name");
		      		    
		      			if (e.getAttribute("Name").equals(field))
		  				{
		  				  e.click();
		  				}
		      		}
	              
	                // Choose the value
		      		dropdownButton = comboBoxes.get(0).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
		      		dropdownButton.get(0).click();
		      		itemlist = comboBoxes.get(0).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
		      		
		      		// get list of all the available values
		      		List<String> allValues = new ArrayList<String>();
		      		for(WebElement e : itemlist)
		      			allValues.add(e.getAttribute("Name"));
		      		
		      		// if random value is requested
		      		if (value.equals("random")) {
		      			Random randomizer = new Random();
		      			value = allValues.get(randomizer.nextInt(allValues.size()));
		      		}
		      		
		      		// Check the distance of the needed value
		      		int place = allValues.indexOf(value);
		      		if (place > 30)
		      			place += place / 30;
		      		int moves = place / 30;
		      		for (int i = 0; i < moves; i++) 
		      			LargeIncrement.click();
					
//		      		// Origin
//		      		if (field.equals("origin") && value.equals("Foreign"))
//		      			LargeIncrement.click();
//		      		
//		      		// Semantic groups
//		      		if (field.equals("semantic groups"))// && value.equals("Foreign"))
//		      			LargeIncrement.click();
		      		
//		      		value = value.replace(" ", "");
		      		for(WebElement e : itemlist)
		      		{
		      		 //   System.out.println("name: " + e.getAttribute("Name"));
//		      		  if (e.getAttribute("Name").contains("birth"))
//		  				{
//		      			System.out.println(e.getAttribute("Name"));
//		      			System.out.println(value);
//		      			boolean equal = e.getAttribute("Name").equals(value);
//		  				}
		      			if (e.getAttribute("Name").equals(value))
		  				{
		  				  e.click();
		  				}
		      		}
//		      		System.out.println("mibo");
		      		
		      		// Click the Change button in the mass attribute popup
		    		CurrentPage.As(CompoundsTab.class).bt_Change.click();
		    		return value;
		      	}
	        
	        

	        // Clean the text within DucomentControl like Definition and Etymology
	        public void cleanDucomentControl(WebElement ducomentBox) throws AWTException {
				ducomentBox.click();
				String notEmptyYet = "text";
	    
	    		while (!notEmptyYet.isEmpty()) {
	    			notEmptyYet = ducomentBox.getText();
	    		//System.out.println(notEmptyYet);
	    			ducomentBox.sendKeys(Keys.CLEAR);
	    	
	    		}
			}
	        
	        /**
			  * Get clean tagged records set
			  * @return
	         * @throws IOException 
			  */
			  public  List<String> getTaggedWordsFromFile() throws IOException {
				  
				  File file = new File(Setting.AppPath + Setting.Language + "\\Data\\Compounds.txt");
				  BufferedReader reader = new BufferedReader(new FileReader(file));
				  String line;
				  List<String> taggedWords = new ArrayList<String>();
				   	while((line = reader.readLine()) != null)
				   	{
				   		    line = line.trim();
				   		    String[] splitted = line.split("	");
				   		    
				   		    if (splitted[1].equals("completed")) 
				   		    {
				   		    	// clean record
				   		    	String splittRecord = splitted[0].split("}")[1];
				   		    	String cleanRecord = splittRecord.split("~")[0].replace("_"," ");
				   		    	// get records that starts with letter
				   		    	if(Character.isLetter(cleanRecord.charAt(0)))
				   		    		taggedWords.add(cleanRecord);    
				   		    }
				     }
		
				   		reader.close();
						return taggedWords;
			  }
			  
			  /**
		         * Get records names with version zero from the compounds.txt file
		         * @return
		         * @throws IOException 
		         */
		         public List<String> getRecordsFromWithVersionZero() throws IOException {
		        	 
		        		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Compounds.txt"), "UTF-8");
				   		List<String> lines = new ArrayList<>();
				   		while(in.hasNextLine()) {
				   		    String line = in.nextLine().trim();
		 		   		    String[] splitted = line.split("	");
		 		   		    String[] removeLeft = splitted[0].split("}",2);
		 		   		    String[] removeRight = removeLeft[1].split("~");
		 		   		    if(removeRight[1].equals("0"))
		 		   		    	lines.add(removeRight[0]);
		 		   		}
		 		   		
		 		   		in.close();
		 				return lines;
		         }
		         
				
		         /**
		          * Get all frames for specified record name
		          * @param record
		          * @return
		          * @throws FileNotFoundException
		          */
		         public List<String> getAllFramesOfRecordFromcompounds(String record) throws FileNotFoundException {
		    			
		 	   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\compounds.txt"), "UTF-8");
		 	   		List<String> frames = new ArrayList<>();
		 	   		while(in.hasNextLine()) {
		 	   			
		 	   		    String line = in.nextLine().trim();
		 	   		    String[] splitted = line.split("	");

		 	   		    if (record.equals(splitted[0])) 
		 	   		    {
		 	   		    	String[] recordFrames = splitted[10].split("¦");
		 	   		    	for(int i=0;i<recordFrames.length	;i++)
		 	   		    		frames.add(recordFrames[i].replace("_", " "));
		 	   		    in.close();
			 			return frames;
		 	   		    }

		 	   		}
		 	   		
		 	   		in.close();
		 			return frames;
		     }
		         
		         
         
         /**
		  * Get undefined compounds from Compounds.txt files
		  * @param size - the wanted compounds set size
		  * @return
         * @throws IOException 
		  */
		  public  List<String> getUndefinedCompoundsSet(int size) throws IOException {
			  
			  File file = new File(Setting.AppPath + Setting.Language + "\\Data\\Compounds.txt");
			  BufferedReader reader = new BufferedReader(new FileReader(file));
			  List<String> undefiendCompounds= new ArrayList<String>();
			  String line;
			  int count = 0;
			   	while((line = reader.readLine()) != null && count<size)
			   	{
			   		    line = line.trim();
			   		    String[] splitted = line.split("	");
			   		    // Get compounds with "unknown" tagged
			   		    if(splitted[1].equals("unknown"))
			   		    {
			   		    	String cleanCompound = splitted[0].split("\\}")[1].split("~")[0];
			   		    	cleanCompound = cleanCompound.replace("_", " ");
			   		    	undefiendCompounds.add(cleanCompound);
			   		    	count++;	
			   		    }    
			   	}
			   	
			   	reader.close();
			   	return undefiendCompounds;
		  }
		  
	         /**
	          * Search in compounds
	          * @param test
	          * @param logger
	       	* @param firstDropdownValue
	       	* @param conditionDropdownValue
	       	* @param searchWord
	       	* @param filterDropdownValue
	          * @return
	       * @throws InterruptedException 
	          */
	         public void searchInCompound(ExtentTest test,Logger logger,String firstDropdownValue,String conditionDropdownValue,String searchWord,String filterDropdownValue) throws InterruptedException
	     	{
	        	 CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
	      	
	     		// Choose value from the first search dropdown
	     		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_field,firstDropdownValue);
	     		LogUltility.log(test, logger, firstDropdownValue+" is chosen from the first search dropdown");

	     		// Choose value from the condition dropdown
	     		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_operation, conditionDropdownValue);
	     		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

	     		//Type in the search field
	     		CompoundsTab.cmpCondition_value.sendKeys(searchWord);
	     		LogUltility.log(test, logger, "Type in the search field:"+searchWord);
	     		
	     		// Change the last filter value, in case it was changed by other TC
	     		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,filterDropdownValue);
	     		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
	     		
	     		// Click the Retrieve button
	     		CompoundsTab.btnRetrieve_same.click();
	     		LogUltility.log(test, logger, "Click the Retreive button");
	     		
	     	} // close function
	     	
	         
	         /**
	          * Check if the case sensitivity/A button is active
	          * @return
	          */
	         public boolean isUppercaseActive()
	         {
	        	 // If it is active (displayed as "A") then the Name attribute will be set to A otherwise to a
	        	 if(bt_condition_uppercase.getAttribute("Name").equals("A"))
	        		 return true;
	        	 return false;
	         }
	         
	         
	         /**
	          * Get all completed compounds with their dictionary records/lemma from file
	          * @param record
	          * @return
	          * @throws FileNotFoundException
	          */
	         public HashMap<String, List<String>> getAllCompoundsLemma() throws FileNotFoundException {
	    			
	 	   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\compounds.txt"), "UTF-8");
	 	   	HashMap<String, List<String>> compoundsWithLemma = new HashMap<String,List<String>>();
//	 	   		List<String> compoundWithLemma = new ArrayList<>();
	 	   		
	 	   		while(in.hasNextLine()) {
	 	   			
	 	   		    String line = in.nextLine().trim();
	 	   		    String[] splitted = line.split("	");

	 	   		    if (splitted[1].equals("completed")) 
	 	   		    	{
	 	   		    		
	 	   		    		String lemmaInfo = splitted[splitted.length-1];
	 	   		    		String[] splittedInfo = lemmaInfo.split("¦");
	 	   		    		ArrayList<String> lemmas = new ArrayList<String>();
	 	   		    		for(int i=0;i<splittedInfo.length;i++)
	 	   		    		{
	 	   		    			String dictionaryRecord = splittedInfo[i].split("\\|")[0];
	 	   		    			lemmas.add(dictionaryRecord);
	 	   		    		}
	 	   		    		// compound name and lemmas
	 	   		    	compoundsWithLemma.put(splitted[0], lemmas);
	 	   		    	}
	 	   		}
	 	   		
	 	   		in.close();
	 			return compoundsWithLemma;
	     }
	         
	         
	         /**
	            * Get clean records names from the compounds.txt files with completed status
	            * @return
	            * @throws FileNotFoundException
	            */
	        public List<String> getRecordsFromCompoundsWithCompletedStatus() throws FileNotFoundException {
			   			
			   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\compounds.txt"), "UTF-8");
			   		List<String> lines = new ArrayList<>();
			   		while(in.hasNextLine()) {
			   		    String line = in.nextLine().trim();
			   		    String[] splitted = line.split("	");
			   		    String[] removeLeft = splitted[0].split("}");
			   		    if(splitted[1].equals("completed")) 
			   		    {
				   		    String[] removeRight = removeLeft[1].split("~");
				   		    // remove spaces in the beginning 
	//			   		    removeRight[0]=  removeRight[0].replaceAll(" ", "");
				   		    removeRight[0] = removeRight[0].replace("_", " ");
				   		    removeRight[0] = removeRight[0].replace("-", " ");
				   		    lines.add(removeRight[0]);
			   		    }
			   		}
			   		
			   		in.close();
					return lines;
	        }
	        
	        /**
	         * Edit semantic relation value
	         * @param dropdown
	         * @param Column
	         * @param value
	         * @throws InterruptedException
	         * @throws AWTException
	         */
	        public void editSemanticRelation(ExtentTest test, Logger logger) throws InterruptedException, AWTException
	    	{
	        	LogUltility.log(test, logger, "Start of edit semantic relations function");
	        	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	        	
	        	// Get all semantic relations
	        	List<String> listedSR =  getSemanticRelationValues();
	        	LogUltility.log(test, logger, "Current semantic relations list: " + listedSR);
	        	
	        	// Choose random semantic relation
	        	Random randomizer = new Random();
	    		String randomRelation= listedSR.get(randomizer.nextInt(listedSR.size()));
	    		LogUltility.log(test, logger, "Random Chosen relation to edit: " + randomRelation);
	    		
	        	// Choose the semantic relation to be modified
	        	Actions shiftClick = new Actions(DriverContext._Driver);
	    		// Get the semantic relation values
	        	List<WebElement> relationsList= lst_SemanticRelations.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
	        	for(WebElement f : relationsList)
	    		{
	    			if (f.getAttribute("Name").equals(randomRelation)) 
	    			{
	    				try {
//	    					f.click();
	    					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
	    					break;
						} catch (Exception e) {
							System.out.println("not clickable");
						}
	    			}
	    		} // end of for
	        	
	        	//-------------------------------
	        	String randomKey = "";
	        	
	    		// Choose the key to be modified
	        	// Get all the available comboboxes in the app
	        	List<WebElement> comboboxesList= CompoundsTab.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ComboBox')]"));
	        	comboboxesList = Lists.reverse(comboboxesList);
	        	
	        	// Get the pos value
	        	String posValue = randomRelation.split("\\{")[1].split("\\}")[0];
	        	String posToChange = posValue+"["+DictionaryTab.POSCodeConverter(posValue)+"]";

	        	// Choose values from the 3 dropdowns
	        	int dropdownNumber = 1;
	        	for(WebElement f : comboboxesList)
	    		{ // buttons
	        		if(f.getAttribute("Name").equals("Stress") || f.getAttribute("Name").equals(posToChange) || f.getAttribute("Name").equals(""))
    				try {
//    					f.click();
//    					WebElement dropdownbtn = f.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
//    					dropdownbtn.click();
    					List<String> keysList = getValuesFromApp(f);
    					List<String> newKeysList = new ArrayList<String>();
//    					keysList.remove(posToChange);
       					if(dropdownNumber==2)
    					{
       						// Work only with the available POS
    						List<String> availablePos = getValuesFromApp(cmpVolumePOS);
    						for(int i=0;i<keysList.size();i++)
    							for(int j=0;j<availablePos.size();j++)
    								if(keysList.get(i).contains(availablePos.get(j)))
    									newKeysList.add(keysList.get(i));
    						randomKey = newKeysList.get(randomizer.nextInt(newKeysList.size()));
    					}
       					else
    					randomKey = keysList.get(randomizer.nextInt(keysList.size()));
    					
    					// Choose random new key
    					LogUltility.log(test, logger, "Enter random new key: " + randomKey);
    					
    		    		CommonFunctions.chooseValueInDropdown(f, randomKey);
    		    		dropdownNumber++;
					} catch (Exception f1) {
						System.out.println("not clickable");
					}
	    		} // end of outside for	
	        	
	        	
	        	// Get the add - green and remove - red buttons
	        	List<WebElement> buttons= CompoundsTab.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
	    		// Click the add/green button
	        	buttons.get(buttons.size()-1).click();
	        	LogUltility.log(test, logger, "Click the green add button");
	        	
	    	} // close function
	        
	        /**
	         * Get semantic relation values from app 
	         * @throws InterruptedException
	         */
	        public List<String> getSemanticRelationValues() throws InterruptedException
	    	{
	        	// Get the semantic relations values from app
	        	List<String> semanticRelationList = getValuesFromApp(lst_SemanticRelations);
	    		return semanticRelationList;
	    	}
	        
	        // find number of elements in dropdown/list
	        public List<String> getValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
	    	{
	    		//get all children
	        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

	        	ArrayList<String> itemNames= new ArrayList<String>();
	        	
	        	int i = 0;
	    		for(WebElement e : itemlist)
	    		{
	    			itemNames.add(e.getAttribute("Name"));
	    			i++;
	    			if (i>amount) break;
	    		}
	    		return itemNames;
	    	}
	        
	        /**
	         * Edit lexical relation value
	         * @throws InterruptedException
	         * @throws AWTException
	         */
	        public void editLexicalcRelation(ExtentTest test, Logger logger) throws InterruptedException, AWTException
	    	{
	        	LogUltility.log(test, logger, "Start of edit lexical relations function");
	        	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	        	
	        	// Get all semantic relations
	        	List<String> listedLR =  getLexicalRelationValues();
	        	LogUltility.log(test, logger, "Current lexical relations list: " + listedLR);
	        	
	        	// Choose random lexical relation
	        	Random randomizer = new Random();
	    		String randomRelation= listedLR.get(randomizer.nextInt(listedLR.size()));
	    		LogUltility.log(test, logger, "Random Chosen relation to edit: " + randomRelation);
	    		
	        	// Choose the semantic relation to be modified
	        	Actions shiftClick = new Actions(DriverContext._Driver);
	    		// Get the semantic relation values
	        	List<WebElement> relationsList= lst_Lexical_relations.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
	        	for(WebElement f : relationsList)
	    		{
	    			if (f.getAttribute("Name").equals(randomRelation)) 
	    			{
	    				try {
//	    					f.click();
	    					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
	    					break;
						} catch (Exception e) {
							System.out.println("not clickable");
						}
	    			}
	    		} // end of for
	        	
	        	//-------------------------------
	        	String randomKey = "";
	        	
	    		// Choose the key to be modified
	        	// Get all the available comboboxes in the app
	        	List<WebElement> comboboxesList= CompoundsTab.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ComboBox')]"));
	        	comboboxesList = Lists.reverse(comboboxesList);
	        	
	        	// Get the pos value
	        	String posValue = randomRelation.split("\\{")[1].split("\\}")[0];
	        	String posToChange = posValue+"["+DictionaryTab.POSCodeConverter(posValue)+"]";

	        	// Choose values from the 3 dropdowns
	        	int dropdownNumber = 1;
	        	for(WebElement f : comboboxesList)
	    		{ // buttons
	        		if(f.getAttribute("Name").equals("Stress") || f.getAttribute("Name").equals(posToChange) || f.getAttribute("Name").equals(""))
    				try {
//    					f.click();
//    					WebElement dropdownbtn = f.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
//    					dropdownbtn.click();
    					List<String> keysList = getValuesFromApp(f);
    					List<String> newKeysList = new ArrayList<String>();
//    					keysList.remove(posToChange);
       					if(dropdownNumber==2)
    					{
       						// Work only with the available POS
    						List<String> availablePos = getValuesFromApp(cmpVolumePOS);
    						for(int i=0;i<keysList.size();i++)
    							for(int j=0;j<availablePos.size();j++)
    								if(keysList.get(i).contains(availablePos.get(j)))
    									newKeysList.add(keysList.get(i));
    						randomKey = newKeysList.get(randomizer.nextInt(newKeysList.size()));
    					}
       					else
    					randomKey = keysList.get(randomizer.nextInt(keysList.size()));
    					
    					// Choose random new key
    					LogUltility.log(test, logger, "Enter random new key: " + randomKey);
    					
    		    		CommonFunctions.chooseValueInDropdown(f, randomKey);
    		    		dropdownNumber++;
					} catch (Exception f1) {
						System.out.println("not clickable");
					}
	    		} // end of outside for	
	        	
	        	
	        	// Get the add - green and remove - red buttons
	        	List<WebElement> buttons= CompoundsTab.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
	    		// Click the add/green button
	        	buttons.get(buttons.size()-1).click();
	        	Thread.sleep(1000);
	        	LogUltility.log(test, logger, "Click the green add button");
	        	
	    	} // close function
	        
	        /**
	         * Get lexical relation values from app 
	         * @throws InterruptedException
	         */
	        public List<String> getLexicalRelationValues() throws InterruptedException
	    	{
	        	// Get the lexical relations values from app
	        	List<String> lexicalRelationList = getValuesFromApp(lst_Lexical_relations);
	    		return lexicalRelationList;
	    	}
	        
	        
	        /**
	         * Check if compound key exists in compounds_sync_map.txt file
	         * @return
	        * @throws IOException 
	         */
	         public boolean checkIfFoundInCompoundSyncMap(String compoundKey) throws IOException {
	       	  
	       	  InputStream file = new FileInputStream(Setting.AppPath +Setting.Language +"\\Data\\compounds_sync_map.txt");
	       	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	       	  String line;
	       	   	while((line = reader.readLine()) != null)
	       	   	{
	       	   		  if(line.contains(compoundKey))
	       	   		  {
	       	   			  reader.close();
	       	   			  return true;
	       	   		  }
	       	   		    
	       	   	 }

	       	   		reader.close();
	       			return false;
	         }
	         
	         
	   	     /**
    		  * Get compound keys with frames from Compounds.txt file
    		  * @return
             * @throws IOException 
    		  */
    		  public  List<String> getCompoundKeysWithFrames() throws IOException {
    			  
    			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Compounds.txt"));
    			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
    			  List<String> keys = new ArrayList<String>();
    			  String line;
    			   	while((line = reader.readLine()) != null)
    			   	{
    			   		    line = line.trim();
    			   		    String[] splitted = line.split("	");
    			   		    // Check that there is frame value
    			   		    if(!splitted[10].isEmpty())
    			   		    	keys.add(splitted[0]);
    			   	}

    			   		reader.close();
    					return keys;
    		  }
    		  
    		  
    		  
    		  /**
    		   * Check if compound key exists in compounds_sync_map.txt file
    		   * @return
    		  * @throws IOException 
    		   */
    		   public boolean checkIfFoundInCompoundSyncMap(String oldValue,String newValue) throws IOException {
    		 	  
    		      InputStream file = new FileInputStream(Setting.AppPath + Setting.Language+"\\Data\\compounds_sync_map.txt");
    		 	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
    		 	  String line;
    		 	   	while((line = reader.readLine()) != null)
    		 	   	{
    		 	   		String[] splitted = line.split("	");
    		 	   		if(splitted[0].contains(oldValue) && splitted[1].contains(newValue)) {
	    		 	   		reader.close();
	    		 	   		return true;
    		 	   		}
    		 	    }

    		 	   		reader.close();
    		 			return false;
    		   }
    		          
	        
   	        /**
   	         * Get compound keys with 'verb' POS from compounds.txt file
   	         * @return
   	        * @throws IOException 
   	         */
   	         public List<String> getVerbCompounds() throws IOException {
   	       	  
   	       	  InputStream file = new FileInputStream(Setting.AppPath + Setting.Language+"\\Data\\Compounds.txt");
   	       	  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
   	       	  List<String> verbCompounds = new ArrayList<String>();
   	       	  String line;
   	       	   	while((line = reader.readLine()) != null)
   	       	   	{
   	       	   		String[] splitted = line.split("	");
   	       	   		if(splitted[1].equals("completed"))
   	       	   		{
   			   		    String[] removeLeft = splitted[0].split("\\}");
   			   		    String[] removeRight = removeLeft[0].split("\\{");
   			   		    if(removeRight[1].equals("v"))
   			   		    	verbCompounds.add(splitted[0]);
   			   		    	
   	       	   		}
   	       	   		    
   	       	   	 }

   	       	   		reader.close();
   	       	   		return verbCompounds;
   	       		
   	         }
   	         
}


