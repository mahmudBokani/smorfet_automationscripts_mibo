package smi.smorfet.test.pages;


import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class RulesTab extends BasePage {
	
	// Rules whole frame tab
	@FindBy(how=How.ID,using ="proximity_rules_layout")
	public WebElement rulesFrame;
	
	// Rule name tb_rule_name
	@FindBy(how=How.ID,using ="tb_rule_name")
	public WebElement ruleName;
	
	// Disambiguation rules tab 
	@FindBy(how=How.NAME,using ="disambiguation")
	public WebElement disambiguationTab;
	
	// Rules tab  Rules
	@FindBy(how=How.NAME,using ="Rules")
	public WebElement rulesTab;
	
	// Score values list Stress
	@FindBy(how=How.NAME,using ="Stress")
	public WebElement scoreList;
	
	// Record number box tb_record_number
	@FindBy(how=How.ID,using ="tb_record_number")
	public WebElement numberBox;
	
	// Score dropdown 
	@FindBy(how=How.ID,using ="cb_rule_sign")
	public WebElement ScoreDD;
	
	// Priority dropdown cb_rule_priority
	@FindBy(how=How.ID,using ="cb_rule_priority")
	public WebElement priorityDD;
	
	// Rule description textbox tb_rule_description
	@FindBy(how=How.ID,using ="tb_rule_description")
	public WebElement descriptionBox;
	
	// Save button
	@FindBy(how=How.ID,using ="bt_save")
	public WebElement btnSave;
	
	// New button
	@FindBy(how=How.ID,using ="bt_new")
	public WebElement btnNew;
	
	// Previous arrow button
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement bt_prev;
	
	// Left arrow in the footer bt_prev 
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement Previous_button;
	
	// Right arrow in the footer bt_next 
	@FindBy(how=How.ID,using ="bt_next")
	public WebElement Next_button;
	
	// Revert button
	@FindBy(how=How.ID,using ="bt_revert")
	public WebElement bt_revert;
	
	// Revert file popup Revert File
	@FindBy(how=How.NAME,using ="Revert File")
	public WebElement revertFileMsg;
	
	// OK button on the Revert file popup 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement revertFileMsgOK;
	
	// Cancel button on the Revert file popup 
	@FindBy(how=How.NAME,using ="Cancel")
	public WebElement revertFileMsgCancel;
	
	// Delete button
	@FindBy(how=How.ID,using ="bt_delete")
	public WebElement bt_delete;
	
	// Delete rule popup 
	@FindBy(how=How.NAME,using ="disambiguation Rule Delete warning:")
	public WebElement deleteRulePopup;
	
	// Yes button on the Delete rule popup
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement yesDeleteRule_bt;
	
	// No button on the Delete rule popup
	@FindBy(how=How.NAME,using ="No")
	public WebElement noDeleteRule_bt;
	
	// All button 
	@FindBy(how=How.ID,using ="bt_all_on")
	public WebElement allButton;
	
	// None button : bt_all_off
	@FindBy(how=How.ID,using ="bt_all_off")
	public WebElement noneButton;
	
	// Sort by dropdow dd_sort_mode
	@FindBy(how=How.ID,using ="dd_sort_mode")
	public WebElement sortDD;
	
	// Disambiguation Rules list 
	@FindBy(how=How.ID,using ="clb_disambiguation_rules")
	public WebElement disambiguationRulesList;
	
	// Right arrow for rules SmallIncrement
	@FindBy(how=How.ID,using ="SmallIncrement")
	public WebElement rightArrow;
	
	// Left arrow for rules SmallIncrement
	@FindBy(how=How.ID,using ="SmallDecrement")
	public WebElement leftArrow;
	
	// Rules edit area p_rule_pattern
	@FindBy(how=How.ID,using ="p_rule_pattern")
	public WebElement rulesEditArea;
	
	// Test me button Test Me!
	@FindBy(how=How.NAME,using ="Test Me!")
	public WebElement testMe;
	
	// Rule's name
	@FindBy(how=How.NAME,using ="Rule's name")
	public WebElement ruleNameMsg;
	
	// First rule in the list 
	@FindBy(how=How.NAME,using ="002 : {V} / {x, o} _ {n, I, x}")
	public WebElement firstRule;
	
	// Second rule in the list 
	@FindBy(how=How.NAME,using ="008 : * EN{u}be~2 before {p, a}")
	public WebElement secondRule;
	
	// Last rule in the list 
	@FindBy(how=How.NAME,using ="282 : {n} after {C} {u}")
	public WebElement lastRule;
	
	// Saving Disambiguation rule dialog : Saving disambiguation rules
	@FindBy(how=How.NAME,using ="Saving disambiguation rules")
	public WebElement savingDisRuleDialog;
	
	// OK button in Saving Disambiguation rule dialog 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement OKSavingDisRuleDialog;
	
	// Close button in Saving Disambiguation rule dialog 
	@FindBy(how=How.NAME,using ="Close")
	public WebElement closeSavingDisRuleDialog;
	
	// Rules input area
	@FindBy(how=How.ID,using ="p_rule_pattern")
	public WebElement p_rule_pattern;
	
	// Yes button on the new rule popup
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement newRuleYesbtn;
	
	// Or button in the second rule
	@FindBy(how=How.NAME,using ="or")
	public WebElement orbtn;
	
	// OK button in the empty DD message 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement emptyDDOKbtn;
	
	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Word in position dropdown Mandatory
	@FindBy(how=How.NAME,using ="Mandatory")
	public WebElement wordInPosition;
	
	// Add button +
	@FindBy(how=How.NAME,using ="+")
	public WebElement addbutton;
	
	// Normalization rules tab
	@FindBy(how=How.NAME,using ="normalization")
	public WebElement normalizationTab;
	
	// Normalization rules list clb_normalization_rules
	@FindBy(how=How.ID,using ="clb_normalization_rules")
	public WebElement normalizationRulesList;
	
	// Rule display layout
	@FindBy(how=How.ID,using ="rule_display_layout")
	public WebElement rule_display_layout;
	
	// Proximity rules layout
	@FindBy(how=How.ID,using ="proximity_rules_layout")
	public WebElement proximity_rules_layout;

	// Cancel button on the rules name  popup
	@FindBy(how=How.NAME,using ="Cancel")
	public WebElement ruleNameCancelbtn;
	
	// Rules output area
	@FindBy(how=How.ID,using ="p_rule_response")
	public WebElement p_rule_response;
	
	// Transcription tab
	@FindBy(how=How.NAME,using ="transcription")
	public WebElement transcription;
	
	// Cannot remove last condition popup OK button 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement cannotRemoveLastConditionpopupOK_btn;

	// Overlapping Condition Lists popup OK button 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement overlappingOK_btn;

	// Condition popup OK button 
	@FindBy(how=How.NAME,using ="OK")
	public WebElement conditionpopupOK_btn;
	
	// Prosody rules list clb_prosody_rules
	@FindBy(how=How.ID,using= "clb_prosody_rules")
	public WebElement prosodyRulesList;
	
	// Prosody tab
	@FindBy(how=How.NAME,using ="prosody")
	public WebElement prosodyTab;

	// Transcription rules list clb_transcription_rules
	@FindBy(how=How.ID,using="clb_transcription_rules")
	public WebElement transcriptionRulesList;

	// Transcription tab
	@FindBy(how=How.NAME,using ="transcription")
	public WebElement transcriptionTab;

	// Left window tabs
	@FindBy(how=How.ID,using="tc_proximity")
	public WebElement tc_proximity;

	// Empty dropdown popup One of the drop downs is empty!. Correct that before you save!"
	@FindBy(how=How.NAME,using="One of the drop downs is empty!. Correct that before you save!")
	public WebElement emptyDropdownPopup;
	
	// Ok button for the empty dropdown popup OK
	@FindBy(how=How.NAME,using="OK")
	public WebElement okEmptyDropdown;
	
	// Monitor tab
	@FindBy(how=How.NAME,using ="Monitor")
	public WebElement tabMonitor;

	// Rules list
	@FindBy(how=How.ID,using= "clb_normalization_rules")
	public WebElement clb_normalization_rules;
	
	// Backward button for tabs 
	@FindBy(how=How.NAME,using ="Backward")
	public WebElement btnBackward;
	
    /**
	 * Get random text 
     * @param length - enter the length of the text
	 */
        public String RandomString(int length) {
        	   StringBuilder sb = new StringBuilder();
        	   Random random = new Random();
        	   char[] chars = null;
        	switch(Setting.Language)
        	{
	        	case "EN":
		        chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();break;
	        	case "HE":
	        	chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray();break;
        	}
        	
        	for (int i = 0; i < length; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
        	}
        	
         String output = sb.toString();
         return output;
        
        }
        
        // # Type in Key Form
        public String keyFormTypeRandom() {
			String random_input = RandomString(5);
			this.ruleName.sendKeys(random_input);
			return random_input;
		}

        // find all elements in dd_condition_field
        // later try to make it globally
        //private List<String> getCompoboxValues(WebElement lstBox)
        public List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
    	{
        	//lstBox.click();
        	Thread.sleep(1000);
    		//get all children
    		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//        	try {
//        		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    		//System.out.println(itemlist.size());
    		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			itemNames.add(e.getAttribute("Name"));
    		    //itemNames.add(e.getAttribute(Name));
    		}
    		
    		// Delete unknown value, it is not usable
    		if (itemNames.contains("unknown"))
    			itemNames.remove("unknown");
    		
    		return itemNames;
    	}
        
        // find number of elements in dropdown/list
        public List<String> getValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
    	{
    		//get all children
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
        	
        	int i = 0;
    		for(WebElement e : itemlist)
    		{
    			itemNames.add(e.getAttribute("Name"));
    			i++;
    			if (i>amount) break;
    		}
    		return itemNames;
    	}
        
        /**
         * Get chosen value in a dropdown
         * @param lstBox
         * @return
         * @throws InterruptedException
         */
        public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
    	{
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
//    			System.out.println(e.getAttribute("Name"));
    			boolean isSelected = e.isSelected();
    			if (isSelected)
    				itemNames.add(e.getAttribute("Name"));
    		}
    		return itemNames;
    	}
        
       
        /**
         * Fetch the text and title of popup windows within Smorfet
         * @param test
         * @param logger
         * @param titleToCheck
         * @param textToCheck
         * @return
         */
        public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
    	{
        	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
        	List<WebElement> text = null;
        	List<WebElement> title = null;
        	
        	boolean titleIn = false;
    	    boolean textIn = false;
    	 
    	    try {
	        	// Check the main app for popup windows
	    		for(WebElement e : windows)
	    		{ 
	    			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
	            	title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
	        	}
	    		
	    		// Fetch the found title
	    		for(WebElement e : title)
	    		{
	    			LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
	    			if (e.getAttribute("Name").contains(titleToCheck))
	    				titleIn = true;
	        	}
	    		
	    		// Fetch the found title    		
	    		for(WebElement e : text)
	    		{ 
	    			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
	    			if (e.getAttribute("Name").contains(textToCheck))
	    				textIn = true;
	        	}
    		
    	    }
    	    catch (Exception e) {
				// TODO: handle exception
			}
    		LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
    		
    	    if (titleIn && textIn)
    	    	return true;
    	    else return false;
    	} // close function
        
        
        /**
         * Get clean records names from the Rules.txt files
         * @return
         * @throws FileNotFoundException
         */
         public boolean getRecordsFromRules(String ruleNumber, String ruleName ,String  ruleFile) throws FileNotFoundException {
 		   			
 		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\"+ruleFile), "UTF-8");
// 		   		List<String> lines = new ArrayList<>();
 		   		while(in.hasNextLine()) {
 		   		    String line = in.nextLine().trim();
 		   		    String[] splitted = line.split("	");
 		   		    if (ruleNumber.equals(splitted[1]) && ruleName.equals(splitted[2])) {
 		   		    	in.close();
 		   		    	return true ;
 		   		    }
 		   		    	
 		   		
 		   		}
 		   		
 		   		in.close();
 				return false;
         }

        /**
         * Rules input area
         * -no parameters:
		 *	+ add pane
         * @throws InterruptedException
         */
        public void rulesInputArea() throws InterruptedException
    	{
        	WebElement newPaneButton = p_rule_pattern.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
        	newPaneButton.click();
    	}
        
        /**
         * Rules input area : add new pane and return the element
         * -no parameters:
		 *	+ add pane
		 *
         * @throws InterruptedException
         */
        public WebElement rulesInputAreaWebElement() throws InterruptedException
    	{
        	WebElement newPaneButton = p_rule_pattern.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
        	newPaneButton.click();
        	// Get the new pane element
        	List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
        	return panes.get(panes.size()-1);
    	}

      /**
      * Rules input area
      * -two parameters - pane number, element:
	  *	+ add condition
	  *	Mandatory
	  *	X rule left
      * @param value
      * @param pane
      * @throws InterruptedException
      */
     public void rulesInputArea(int paneNum, String element) throws InterruptedException
 	{
    	 
    	 List<WebElement> pane = new ArrayList<WebElement>();
    	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

    	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//    	 System.out.println("mibo");

    	 // Split the panes by the X at the start of each pane
    	 boolean firstCondition = false, found = false;
    	 for(WebElement e : allElements)
 		{
    		 try {
    	    		// check if starting a new pane
    			 	if (firstCondition) {
	    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
	    	    			found = true;
	    	    	firstCondition = false;
	    	    	}
    	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
    	    			firstCondition = true;
    	    		

    	    		if (!found) {
    	    			pane.add(e);
    	    		} else {
    	    			found = false;
    	    			panes.add(pane);
    	    			pane = new ArrayList<WebElement>();
    	    			pane.add(e);
    	    		}
			} catch (Exception e2) {
				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
			}

     	}
    	 
    	 // After finishing running add the lastest pane
    	 panes.add(pane);
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
    	 
     	// Choose operation
     	switch (element) {
         // Add new condition button
     	case "add condition":
     		for(WebElement e : chosenPane)
    		{
//     			System.out.println(e.getAttribute("Name"));
     			
    			if (e.getAttribute("Name").contains("+") && e.getAttribute("ControlType").equals("ControlType.Button")) {
    				e.click();
    				return;
    			}
        	}
             return;
         
         // Mandatory
         case "Mandatory":
    		for(WebElement e : chosenPane)
    		{
    			if (e.getAttribute("Name").contains("Mandatory")) {
    				e.click();
    				return;
    			}
        	}
         	return;
         	
            // Optional
            case "Optional":
       		for(WebElement e : chosenPane)
       		{
       			if (e.getAttribute("Name").contains("Optional")) {
       				e.click();
       				return;
       			}
           	}
            	return;
         	
         // Remove rule
         case "remove rule":
         	for(WebElement e : chosenPane)
    		{
    			if (e.getAttribute("Name").contains("X")) {
    				e.click();
    				return;
    			}
        	}
         	return;
     	}    
     }
     
     /**
      * Rules input area
	  *	Get Mandatory or Optional
      * @param value
      * @param pane
     * @return 
      * @throws InterruptedException
      */
     public boolean rulesInputAreaMandatoryOrOptional(int paneNum, String element) throws InterruptedException
 	{
    	 
    	 List<WebElement> pane = new ArrayList<WebElement>();
    	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

    	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

    	 // Split the panes by the X at the start of each pane
    	 boolean firstCondition = false, found = false;
    	 for(WebElement e : allElements)
 		{
    		 try {
    	    		// check if starting a new pane
    			 	if (firstCondition) {
	    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
	    	    			found = true;
	    	    	firstCondition = false;
	    	    	}
    	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
    	    			firstCondition = true;
    	    		

    	    		if (!found) {
    	    			pane.add(e);
    	    		} else {
    	    			found = false;
    	    			panes.add(pane);
    	    			pane = new ArrayList<WebElement>();
    	    			pane.add(e);
    	    		}
			} catch (Exception e2) {
				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
			}

     	}
    	 
    	 // After finishing running add the lastest pane
    	 panes.add(pane);
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
    	 
     	// Choose operation
     	switch (element) {
         
         // Mandatory
         case "Mandatory":
    		for(WebElement e : chosenPane)
    		{
    			if (e.getAttribute("Name").contains("Mandatory")) {
    				return true;
    			}
        	}
         	return false;
         	
            // Optional
            case "Optional":
       		for(WebElement e : chosenPane)
       		{
       			if (e.getAttribute("Name").contains("Optional")) {
       				e.click();
       				return true;
       			}
           	}
            	return false;
         	
     	}
		return false;    
     }
     
     /**
      * Rules input area
      * -three parameters - pane, condition, or button number
      * or button
      * @param value
      * @param pane
      * @throws InterruptedException
      */
     public void rulesInputArea(int paneNum, int conditionNum ,int orNum) throws InterruptedException
   	{

   	 List<WebElement> pane = new ArrayList<WebElement>();
    	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

    	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//    	 System.out.println("mibo");
    	 
    	 // Split the panes by the X at the start of each pane
    	 boolean firstCondition = false, found = false;
    	 for(WebElement e : allElements)
   		{
//    		 System.out.println(e.getAttribute("ControlType"));
    		 
    		 try {
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
    	    			firstCondition = true;
    	    		

    	    		if (!found) {
    	    			pane.add(e);
    	    		} else {
    	    			found = false;
    	    			panes.add(pane);
    	    			pane = new ArrayList<WebElement>();
    	    			pane.add(e);
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}
    	 
    	 // After finishing running add the lastest pane
    	 panes.add(pane);
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
     	
    	 List<WebElement> condition = new ArrayList<WebElement>();
    	 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();
   	 
    	 // Split rule to condition according to the "or" after "X" OR pane after "X"
    	 firstCondition = false; found = false;
//    	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
    	 for(WebElement e : chosenPane)
   		{
    		 try {
//    			 System.out.println(e.getAttribute("ControlType"));
//    		   	System.out.println(e.getAttribute("Name"));
    		   	
//    		   	System.out.println();
    			 
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
   	    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
   	    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
    	    			firstCondition = true;
    	    		
    			 	
    	    		if (!found) {
    	    			condition.add(e);
    	    		} else {
    	    			found = false;
    	    			condition.add(e);
    	    			conditions.add(condition);
    	    			condition = new ArrayList<WebElement>();
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}

    	 // After finishing running add the latest condition
    	conditions.add(condition);
    	 
    	// Chosen condition
    	if (paneNum > 0) conditionNum++;
    	List<WebElement> chosenCondition = conditions.get(conditionNum);
    	
    	// Get the last Or and put it first in the list
    		for (int i = chosenCondition.size() - 1 ; i >= 0 && conditionNum > 0 ; i--) {
    		    	if (chosenCondition.get(i).getAttribute("Name").equals("or") || chosenCondition.get(i).getAttribute("Name").equals("and"))
    		    	{
    		    		chosenCondition.add(0, chosenCondition.get(i));
    		    		chosenCondition.remove(i+1);
    		    	}
    		}
		for (int i = chosenCondition.size() - 1 ; i >= 0 && (conditionNum-1) > 0; i--) {
    	if (chosenCondition.get(i).getAttribute("Name").equals("or") || chosenCondition.get(i).getAttribute("Name").equals("and"))
    	{
    		chosenCondition.add(0, chosenCondition.get(i));
    		chosenCondition.remove(i+1);
    		break;
    	}
}

    	// Calculate and choose the right "or"
     	int place = 0;
     	for(WebElement e : chosenCondition)
   		{
//     		System.out.println(e.getAttribute("Name"));

   			if ((e.getAttribute("Name").equals("or") || e.getAttribute("Name").equals("and")) && place==orNum) {
   				e.click();
   				return;
   			} else if (e.getAttribute("Name").equals("or") || e.getAttribute("Name").equals("and"))
   					place++;
   		}
   	}
     
     /**
      * Rules input area - Delete Sub condition
      * Two parameters - pane, condition
      * @param value
      * @param pane
      * @throws InterruptedException
      */
     public void rulesInputAreaDeleteSubCondition(int paneNum, int conditionNum) throws InterruptedException
   	{

   	 List<WebElement> pane = new ArrayList<WebElement>();
    	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

    	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//    	 System.out.println("mibo");
    	 
    	 // Split the panes by the X at the start of each pane
    	 boolean firstCondition = false, found = false;
    	 for(WebElement e : allElements)
   		{
//    		 System.out.println(e.getAttribute("ControlType"));
    		 
    		 try {
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
    	    			firstCondition = true;
    	    		

    	    		if (!found) {
    	    			pane.add(e);
    	    		} else {
    	    			found = false;
    	    			panes.add(pane);
    	    			pane = new ArrayList<WebElement>();
    	    			pane.add(e);
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}
    	 
    	 // After finishing running add the lastest pane
    	 panes.add(pane);
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
     	
    	 List<WebElement> condition = new ArrayList<WebElement>();
    	 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();
   	 
    	 // Split rule to condition according to the "or" after "X" OR pane after "X"
    	 firstCondition = false; found = false;
//    	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
    	 for(WebElement e : chosenPane)
   		{
    		 try {
//    			 System.out.println(e.getAttribute("ControlType"));
//    		   	System.out.println(e.getAttribute("Name"));
    		   	
//    		   	System.out.println();
    			 
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
   	    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
   	    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
    	    			firstCondition = true;
    	    		
    			 	
    	    		if (!found) {
    	    			condition.add(e);
    	    		} else {
    	    			found = false;
    	    			condition.add(e);
    	    			conditions.add(condition);
    	    			condition = new ArrayList<WebElement>();
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}

    	 // After finishing running add the latest condition
    	conditions.add(condition);
    	 
    	// Chosen condition
    	if (paneNum > 0) conditionNum++;
    	List<WebElement> chosenCondition = conditions.get(conditionNum);

    	// Calculate and choose the right "or"
//     	int place = 0;
     	for(WebElement e : chosenCondition)
   		{
//     		System.out.println(e.getAttribute("Name"));

   			if ((e.getAttribute("Name").equals("X") && e.getAttribute("ControlType").equals("ControlType.Button"))) {
   				e.click();
   				return;
   			}
   		}
   	}
     
     /**
      * Rules input area
      * -three parameters - pane, condition, or button number
      * or button
      * @param value
      * @param pane
     * @return 
      * @throws InterruptedException
      */
     public String rulesInputAreaCheckOR(int paneNum, int conditionNum ,int orNum) throws InterruptedException
   	{

    	 List<WebElement> pane = new ArrayList<WebElement>();
    	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

    	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//    	 System.out.println("mibo");
    	 
    	 // Split the panes by the X at the start of each pane
    	 boolean firstCondition = false, found = false;
    	 for(WebElement e : allElements)
   		{
//    		 System.out.println(e.getAttribute("ControlType"));
    		 
    		 try {
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
    	    			firstCondition = true;
    	    		

    	    		if (!found) {
    	    			pane.add(e);
    	    		} else {
    	    			found = false;
    	    			panes.add(pane);
    	    			pane = new ArrayList<WebElement>();
    	    			pane.add(e);
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}
    	 
    	 // After finishing running add the lastest pane
    	 panes.add(pane);
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
     	
    	 List<WebElement> condition = new ArrayList<WebElement>();
   	 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();
   	 
    	 // Split rule to condition according to the "or" after "X" OR pane after "X"
    	 firstCondition = false; found = false;
//    	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
    	 for(WebElement e : chosenPane)
   		{
    		 try {
//    			 System.out.println(e.getAttribute("ControlType"));
//    		   	System.out.println(e.getAttribute("Name"));
    		   	
//    		   	System.out.println();
    			 
    	    		// check if starting a new pane
    			 	if (firstCondition) {
   	    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
   	    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
   	    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
   	    	    			found = true;
   	    	    	firstCondition = false;
   	    	    	}
    			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
    	    			firstCondition = true;
    	    		
    			 	
    	    		if (!found) {
    	    			condition.add(e);
    	    		} else {
    	    			found = false;
    	    			condition.add(e);
    	    			conditions.add(condition);
    	    			condition = new ArrayList<WebElement>();
    	    		}
   			} catch (Exception e2) {
   				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
   			}
     	}

    	 // After finishing running add the latest condition
    	conditions.add(condition);
    	 
    	// Chosen condition
    	if (paneNum > 0) conditionNum++;
    	List<WebElement> chosenCondition = conditions.get(conditionNum);
    	
//    	// Get the last Or and put it first in the list
//    		for (int i = chosenCondition.size() - 1 ; i >= 0 && conditionNum > 0 ; i--) {
//    		    	if (chosenCondition.get(i).getAttribute("Name").equals("or") || chosenCondition.get(i).getAttribute("Name").equals("and"))
//    		    	{
//    		    		chosenCondition.add(0, chosenCondition.get(i));
//    		    		chosenCondition.remove(i+1);
//    		    	}
//    		}
		for (int i = chosenCondition.size() - 1 ; i >= 0 && conditionNum > 0; i--) {
    	if (chosenCondition.get(i).getAttribute("Name").equals("or") || chosenCondition.get(i).getAttribute("Name").equals("and"))
    	{
    		chosenCondition.add(0, chosenCondition.get(i));
    		chosenCondition.remove(i+1);
    		break;
    	}
}

    	// Calculate and choose the right "or"
     	int place = 0;
     	for(WebElement e : chosenCondition)
   		{
//     		System.out.println(e.getAttribute("Name"));

   			if ((e.getAttribute("Name").equals("or") || e.getAttribute("Name").equals("and")) && place==orNum) {
   				return e.getAttribute("Name");
   			} else if (e.getAttribute("Name").equals("or") || e.getAttribute("Name").equals("and"))
   					place++;
   		}
		return null;
   	}
     
  /**
   * Rules input area
   * -four parameters:pane, condition, dropdown exist name, dropdown change to
   * combobxes
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public WebElement rulesInputArea(int paneNum, int conditionNum ,String element, String value) throws InterruptedException
	{

	     List<WebElement> pane = new ArrayList<WebElement>();
	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//	 	 System.out.println("mibo");
	 	 

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<WebElement>();
	 	    			pane.add(e);
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	  	
	 	 List<WebElement> condition = new ArrayList<WebElement>();
		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();

//		 System.out.println("---------------------------------");
//		 System.out.println("paneNum: " + paneNum);
		 
	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
	 	 firstCondition = false; found = false;
	 	 boolean comboboxFound = false;
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
		    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
		    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
	 	    			firstCondition = true;
	 	    		
	 			 	if (e.getAttribute("ControlType").equals("ControlType.ComboBox"))
	 			 		comboboxFound = true;
	 			 	
	 	    		if (!found) {
	 	    			condition.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			condition.add(e);
	 	    			
	 	    			// If condition include a combobox then add it else delete it
	 	    			if(comboboxFound)
	 	    				conditions.add(condition);
	 	    			condition = new ArrayList<WebElement>();
	 	    			comboboxFound = false;
	 	    			
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	// Add the latest condition
	 	if(comboboxFound)
	 		conditions.add(condition);
	 	// Chosen condition
//	 	List<WebElement> chosenCondition = conditions.get(conditionNum+1);
	 	List<WebElement> chosenCondition = conditions.get(conditionNum);
	 	
//	 	 System.out.println("---------------------------------");
//		 System.out.println("conditionNum: " + conditionNum);

	 	// Choose value in dropdown
	 	// or
	 	// Enter text in an empty dropdown
	 	if (element != "") {
	 		for(WebElement e : chosenCondition)
			{
//	 			 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));

		 		// System.out.println();
	 			
				if (e.getAttribute("Name").equals(element) && e.getAttribute("ControlType").equals("ControlType.ComboBox")) {
					CommonFunctions.chooseValueInDropdown(e, value);
					return e;
				} 
			}
	 	}
	 	return null;
	}
  
  /**
   * Rules input area
   * -four parameters:pane, dropdown change to
   * positions
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesInputArea(int paneNum, int value) throws InterruptedException
	{

	     List<WebElement> pane = new ArrayList<WebElement>();
	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//	 	 System.out.println("mibo");
	 	 

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<WebElement>();
	 	    			pane.add(e);
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	  	
//	 	 List<WebElement> condition = new ArrayList<WebElement>();
//		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();

//		 System.out.println("---------------------------------");
//		 System.out.println("paneNum: " + paneNum);
		 
	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
	 	 firstCondition = false; found = false;
//	 	 boolean comboboxFound = false;
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 
	    	    	if ((e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("Mandatory")) ||
    	    				(e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("Optional"))){{
    	    					CommonFunctions.chooseValueInDropdown(e, String.valueOf(value));
    	    					return;
    	    				}
	  	}
	
		} catch (Exception e2) {
			e2.printStackTrace();
		}


	}
	}

  /**
   * Rules input area
   * -four parameters: pane, condition, dropdown count, text to put
   * empty combo fields
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesInputArea(int paneNum, int conditionNum ,int count, String value) throws InterruptedException
	{
	  
	     List<WebElement> pane = new ArrayList<WebElement>();
	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<WebElement>();
	 	    			pane.add(e);
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	  	
	 	 List<WebElement> condition = new ArrayList<WebElement>();
		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();

//		 System.out.println("---------------------------------");
//		 System.out.println("paneNum: " + paneNum);
		 
	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
	 	 firstCondition = false; found = false;
	 	 boolean comboboxFound = false;
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
		    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
		    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
	 	    			firstCondition = true;
	 	    		
	 			 	if (e.getAttribute("ControlType").equals("ControlType.ComboBox"))
	 			 		comboboxFound = true;
	 			 	
	 	    		if (!found) {
	 	    			condition.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			condition.add(e);
	 	    			
	 	    			// If condition include a combobox then add it else delete it
	 	    			if(comboboxFound)
	 	    				conditions.add(condition);
	 	    			condition = new ArrayList<WebElement>();
	 	    			comboboxFound = false;
	 	    			
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	// Add the latest condition
	 	if(comboboxFound)
	 		conditions.add(condition);
	 	// Chosen condition
//	 	List<WebElement> chosenCondition = conditions.get(conditionNum+1);
	 	List<WebElement> chosenCondition = conditions.get(conditionNum);
	 	
//	 	 System.out.println("---------------------------------");
//		 System.out.println("conditionNum: " + conditionNum);

	 	int i = 0;
	 	// Enter text in an empty dropdown
	 		for(WebElement e : chosenCondition)
			{
//	 			 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));

		 		// System.out.println();
	 			if (!(e.getAttribute("Name").equals("Mandatory") || e.getAttribute("Name").equals("Optional")))
	 			{
				if (e.getAttribute("ControlType").equals("ControlType.Edit") && i==count) {
					// Does this combobox include an edit component
					e.sendKeys(value);
					return;
					
					} else if(e.getAttribute("ControlType").equals("ControlType.Edit")){
						i++;
					}
	 			}
			}
			
	}
  
  /**
   * Rules input area - Read info
   * One parameter: pane
   * positions
   * @param value
   * @param pane
 * @return 
   * @throws InterruptedException
   */
  public List<String> readRulesInputArea(int paneNum) throws InterruptedException
	{

	     List<WebElement> pane = new ArrayList<WebElement>();
	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//	 	 System.out.println("mibo");
	 	 

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<WebElement>();
	 	    			pane.add(e);
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	  	
//	 	 List<WebElement> condition = new ArrayList<WebElement>();
//		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();

//		 System.out.println("---------------------------------");
//		 System.out.println("paneNum: " + paneNum);
		 
	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
	 	 firstCondition = false; found = false;
//	 	 boolean comboboxFound = false;
	 	 
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 

	    	    	if ((e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("Mandatory")) ||
    	    				(e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("Optional"))){{
    	    					List<String> chosenValues = getChosenValueInDropdown(e);
    	    					return chosenValues;
    	    				}
	  	}
	
		} catch (Exception e2) {
			e2.printStackTrace();
		}


	}
		return null;
	}
  
 
  /**
   * Rules input area, check if element do exist
   * -four parameters:pane, condition, type of element, value in element
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public boolean rulesInputAreaIfExist(int paneNum, int conditionNum ,String type, String value) throws InterruptedException
	{

	     List<WebElement> pane = new ArrayList<WebElement>();
	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

//	 	 System.out.println("mibo");
	 	 

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<WebElement>();
	 	    			pane.add(e);
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the latest pane
	 	 panes.add(pane);
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	  	
	 	 List<WebElement> condition = new ArrayList<WebElement>();
		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();

//		 System.out.println("---------------------------------");
//		 System.out.println("paneNum: " + paneNum);
		 
	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
	 	 firstCondition = false; found = false;
	 	 boolean comboboxFound = false;
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 
	 	    		// check if starting a new condoition
	 			 	if (firstCondition) {
		    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
		    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
		    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
	 	    			firstCondition = true;
	 	    		
	 			 	if (e.getAttribute("ControlType").equals("ControlType.ComboBox"))
	 			 		comboboxFound = true;
	 			 	
	 	    		if (!found) {
	 	    			condition.add(e);
	 	    		} else {
	 	    			found = false;
	 	    			condition.add(e);
	 	    			
	 	    			// If condition include a combobox then add it else delete it
	 	    			if(comboboxFound)
	 	    				conditions.add(condition);
	 	    			condition = new ArrayList<WebElement>();
	 	    			comboboxFound = false;
	 	    			
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	// Add the latest condition
	 	if(comboboxFound)
	 		conditions.add(condition);
	 	// Chosen condition
//	 	List<WebElement> chosenCondition = conditions.get(conditionNum+1);
//	 	List<WebElement> chosenCondition = conditions.get(conditionNum);
	 	
//	 	 System.out.println("---------------------------------");
//		 System.out.println("conditionNum: " + conditionNum);

	 	// Check if element and value does exist
//	 	if (value != "") {
//	 		for(WebElement e : chosenCondition)
//			{
//	 			 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));
//
//		 		// System.out.println();
//	 			
//				if (e.getAttribute("Name").equals(value) && e.getAttribute("ControlType").equals("ControlType." + type )) {
//					return true;
//				} 
////			}
//	 	}
//	 	return false;
	 	
	 	
	 	for(WebElement e : chosenPane) {
	 		
//			 System.out.println(e.getAttribute("ControlType"));
//	 		 System.out.println(e.getAttribute("Name"));
	 		
	 		if (e.getAttribute("ControlType").equals("ControlType." + type)) {
	 		
	 		switch ("ControlType." + type) {
			case "ControlType.Button":
				if(e.getAttribute("Name").equals(value))
					return true;
					break;	

			case "ControlType.Text":
				if(e.getAttribute("Name").equals(value))			
					return true;
					break;
					
			case "ControlType.ComboBox":
				List<WebElement> listItems = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
				for (WebElement item : listItems)
					if(item.isSelected())
						if(item.getAttribute("Name").equals(value))
							return true;
				break;
				
			case "ControlType.Edit":
				 if(e.getText().equals(value))
					 return true;
				 break;
				
			}
	 		
	 		}
	 		
	 		} // for(WebElement e : chosenPane) { 
	 	
	 	return false;
	}
  
  /**
   * Rules input area, check if rule is deleted successfully
   * one parameter: rule number
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesInputAreaRemoveRule(ExtentTest test, Logger logger, int paneNum) throws InterruptedException
	{

	     List<String> pane = new ArrayList<String>();
	 	 List<List<String>> panes = new ArrayList<List<String>>();
	 	 
	 	 List<String> paneAfter = new ArrayList<String>();
	 	 List<List<String>> panesAfter = new ArrayList<List<String>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<String>();
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);

	 	 // Click the delete button for the chosen rule
	 	 rulesInputArea(paneNum, "remove rule");
	 	
	 	 // Check the status after deleting the selected pane
	 	 //	-check the rules number are lower by one
	 	 // -check other rules are still exist with the same components
	 	// Split the panes by the X at the start of each pane
	 	 allElements = p_rule_pattern.findElements(By.xpath(".//*"));
	 	 firstCondition = false; found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panesAfter.add(paneAfter);
	 	    			paneAfter = new ArrayList<String>();
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}

	 	 // After finishing running add the latest pane
	 	 panesAfter.add(paneAfter);

	 	 // Remove the removed pane for the before pane list
	 	 panes.remove(paneNum);

	 	 // Check the size difference
	 	 boolean checkSize = (panesAfter.size() == panes.size());
	 	 LogUltility.log(test, logger, "Size difference: " + checkSize);
	 	 assertTrue(checkSize);
	 	 
	 	 // Check that the rules components in pane are the same as in the paneAfter
	 	 // Remove last cell in both due"
//	 	ControlType.Button +, ControlType.Pane
//	 	ControlType.Button +, ControlType.Button +
	 	panes.remove(panes.size()-1);
	 	panesAfter.remove(panesAfter.size()-1);
	 	boolean rulesEqual = panes.equals(panesAfter);
	 	LogUltility.log(test, logger, "Is Rule " + paneNum + " deleted successfully: " + rulesEqual);
	 	assertTrue(rulesEqual);
	}
  
  /**
   * Rules input area, add and check if a new rule is added successfully
   * one parameter: rule number
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesInputAreaAddRuleAndCheck(ExtentTest test, Logger logger) throws InterruptedException
	{

	     List<String> pane = new ArrayList<String>();
	 	 List<List<String>> panes = new ArrayList<List<String>>();
	 	 
	 	 List<String> paneAfter = new ArrayList<String>();
	 	 List<List<String>> panesAfter = new ArrayList<List<String>>();

	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));

	 	 // Split the panes by the X at the start of each pane
	 	 boolean firstCondition = false, found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
	 	    			pane = new ArrayList<String>();
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);

	 	 // Add a new rule
	 	 rulesInputArea();
	 	
	 	 // Check the status after addeing the selected pane
	 	 //	-check the rules number is increased by one
	 	 // -check other rules are still exist with the same components
	 	// Split the panes by the X at the start of each pane
	 	 allElements = p_rule_pattern.findElements(By.xpath(".//*"));
	 	 firstCondition = false; found = false;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 	    		// check if starting a new pane
	 			 	if (firstCondition) {
		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		    	    			found = true;
		    	    	firstCondition = false;
		    	    	}
	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panesAfter.add(paneAfter);
	 	    			paneAfter = new ArrayList<String>();
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}

	 	 // After finishing running add the latest pane
	 	 panesAfter.add(paneAfter);

	 	// Check the size difference
	 	 boolean checkSize = (panesAfter.size() == (panes.size()+1));
	 	 LogUltility.log(test, logger, "Size difference: " + checkSize);
	 	 assertTrue(checkSize);
	 	 
//	 	 // Remove the new pane and check the rest are what we had before
//	 	panesAfter.remove(panesAfter.size()-1);
	 	 
	 	 // Remove the last element in each pane for as separation issue
	 	 for (int i = 0; i < panes.size(); i++) {
	 		 panes.get(i).remove(panes.get(i).size()-1);
	 		panesAfter.get(i).remove(panesAfter.get(i).size()-1);
			}
	 	 
	 	 // Compare all the previous panes without the new added one
	 	boolean equal = true;
	 	 for (int i = 0; i < panes.size(); i++) {
	 		equal = panes.get(i).equals(panesAfter.get(i));
	 		if (equal == false) break;
		}
	 	LogUltility.log(test, logger, "Rule is successfully added: " + equal);
	 	assertTrue(equal);
	 	 
	 	 // Check that the rules components in pane are the same as in the paneAfter (without the new one)
	 	 // Remove last cell in both due"
//	 	ControlType.Button +, ControlType.Pane
//	 	ControlType.Button +, ControlType.Button +
	 	
//	 	panes.remove(panes.size()-1);
//	 	panesAfter.remove(panesAfter.size()-1);
//	 	boolean rulesEqual = panes.equals(panesAfter);
//	 	LogUltility.log(test, logger, "Rule is successfully added");
//	 	assertTrue(rulesEqual);
	}
  
// still need to fix the last part in i = 0; if want to use this
//  /**
//   * Get type and content of specifc combobox/field by its location number
//   * -four parameters:pane, condition, type of element, value in element
//   * @param value
//   * @param pane
//   * @throws InterruptedException
//   */
//  public List<String> rulesInputAreaComboContain(int paneNum, int conditionNum ,int count) throws InterruptedException
//	{
//
//	     List<WebElement> pane = new ArrayList<WebElement>();
//	 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();
//
//	 	 List<WebElement> allElements = p_rule_pattern.findElements(By.xpath(".//*"));
//	 	 // Split the panes by the X at the start of each pane
//	 	 boolean firstCondition = false, found = false;
//	 	 for(WebElement e : allElements)
//			{
//	 		 try {
//	 	    		// check if starting a new pane
//	 			 	if (firstCondition) {
//		    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
//		    	    			found = true;
//		    	    	firstCondition = false;
//		    	    	}
//	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
//	 	    			firstCondition = true;
//	 	    		
//
//	 	    		if (!found) {
//	 	    			pane.add(e);
//	 	    		} else {
//	 	    			found = false;
//	 	    			panes.add(pane);
//	 	    			pane = new ArrayList<WebElement>();
//	 	    			pane.add(e);
//	 	    		}
//				} catch (Exception e2) {
//					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
//				}
//	  	}
//	 	 
//	 	 // After finishing running add the lastest pane
//	 	 panes.add(pane);
//	 	 
//	 	// Chosen pane
//	 	 List<WebElement> chosenPane = panes.get(paneNum);
//	  	
//	 	 List<WebElement> condition = new ArrayList<WebElement>();
//		 List<List<WebElement>> conditions = new ArrayList<List<WebElement>>();
//
////		 System.out.println("---------------------------------");
////		 System.out.println("paneNum: " + paneNum);
//		 
//	 	 // Split rule to condition according to the "or" after "X" OR pane after "X"
//	 	 firstCondition = false; found = false;
//	 	 boolean comboboxFound = false;
////	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
//	 	 for(WebElement e : chosenPane)
//			{
//	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
//	 			 
//	 	    		// check if starting a new condoition
//	 			 	if (firstCondition) {
//		    	    		if ((e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("or")) ||
//		    	    				(e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("and")) ||
//		    	    				e.getAttribute("ControlType").equals("ControlType.Pane"))
//		    	    			found = true;
//		    	    	firstCondition = false;
//		    	    	}
//	 			 	if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
//	 	    			firstCondition = true;
//	 	    		
//	 			 	if (e.getAttribute("ControlType").equals("ControlType.ComboBox"))
//	 			 		comboboxFound = true;
//	 			 	
//	 	    		if (!found) {
//	 	    			condition.add(e);
//	 	    		} else {
//	 	    			found = false;
//	 	    			condition.add(e);
//	 	    			
//	 	    			// If condition include a combobox then add it else delete it
//	 	    			if(comboboxFound)
//	 	    				conditions.add(condition);
//	 	    			condition = new ArrayList<WebElement>();
//	 	    			comboboxFound = false;
//	 	    			
//	 	    		}
//				} catch (Exception e2) {
//					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
//				}
//	  	}
//	 	 
//	 	 
//	 	// Add the latest condition
//	 	if(comboboxFound)
//	 		conditions.add(condition);
//	 	// Chosen condition
////	 	List<WebElement> chosenCondition = conditions.get(conditionNum+1);
//	 	List<WebElement> chosenCondition = conditions.get(conditionNum);
//	 	
////	 	 System.out.println("---------------------------------");
////		 System.out.println("conditionNum: " + conditionNum);
//
//	 	int i = 0;
//	 	// Check if element and value does exist
//	 		for(WebElement e : chosenCondition)
//			{
//	 			 System.out.println();
//				 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));
//		 		// System.out.println();
//	 			List<String> info = new ArrayList<String>();
////				if (e.getAttribute("ControlType").equals("ControlType.ComboBox") || e.getAttribute("ControlType").equals("ControlType.ComboBox")) {
//////					if (i == count) {
////						 
////						 System.out.println(i);
////						 System.out.println(e.getAttribute("ControlType"));
////				 		 System.out.println(e.getAttribute("Name"));
////				 		 
////						info.add(e.getAttribute("ControlType"));					
////						info.add(e.getAttribute("Name"));
////						return info;
//////					}
////						
////				}
//			}
//			return null;
//	 	
//	}
  
  /**
   * Rules output area
   * -no parameters:
	 *	+ add pane
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesOutputArea() throws InterruptedException
	{
  	WebElement newPaneButton = p_rule_response.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
  	newPaneButton.click();
	}

  	  /**
      * Rules output area
      * -one parameters - pane number:
	  *	X rule left
      * @param value
      * @param pane
      * @throws InterruptedException
      */
     public void rulesOutputAreaRemovePane(int paneNum) throws InterruptedException
 	{
    	 
    	 List<List<WebElement>> panes = getPanesForOutputRules();
    	 
    	// Chosen pane
    	 List<WebElement> chosenPane = panes.get(paneNum);
    	 
         	for(WebElement e : chosenPane)
    		{
//         		System.out.println(e.getAttribute("Name"));
         		
    			if (e.getAttribute("Name").contains("X")) {
    				e.click();
    				return;
    			}
        	}
         	return;  
     }


   /**
   * Rules output area
   * -four parameters:pane, condition, dropdown exist name, dropdown change to
   * combobxes
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public WebElement rulesOutputArea(int paneNum, String element, String value) throws InterruptedException
	{
	  
	  	List<List<WebElement>> panes = getPanesForOutputRules();
	  
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);


	 	// Choose value in dropdown
	 	// or
	 	// Enter text in an empty dropdown
	 	if (element != "") {
	 		for(WebElement e : chosenPane)
			{
//	 			 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));
		 		 boolean selectedIsFound = false;
//		 		 System.out.println(e.getAttribute("Selection"));
		 		
		 		// System.out.println();
				if (e.getAttribute("Name").equals("word in position") && e.getAttribute("ControlType").equals("ControlType.ComboBox")) {
					List<WebElement> listItems = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
					for (WebElement item : listItems) {
						try {
							if(item.isSelected())
								if(item.getAttribute("Name").equals(element)) {
									selectedIsFound = true;
									break;
								}
						} catch (Exception e2) {
							System.out.println(e2);
						}
						
								
					}
					if (selectedIsFound) {
						CommonFunctions.chooseValueInDropdown(e, value);
					return e;
					}
				} 
			}
	 	}
	 	return null;
	}
  
  /**
   * Rules output area
   * Two parameters: pane, value
   * positions
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesOutputArea(int paneNum, int value) throws InterruptedException
	{

	  	List<List<WebElement>> panes = getPanesForOutputRules();
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	 	 
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 

	    	    	if (e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("word in position")){{
	    	    		CommonFunctions.chooseValueInDropdown(e, String.valueOf(value));
    	    					return;
    	    				}
	  	}
	
		} catch (Exception e2) {
			e2.printStackTrace();
		}

	}
	}
  
  /**
   * Rules output area
   * -four parameters: pane, condition, dropdown count, text to put
   * empty combo fields
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesOutputArea(int paneNum ,int count, String value) throws InterruptedException
	{
	  
	  	List<List<WebElement>> panes = getPanesForOutputRules();
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);

	 	int i = -1;
	 	// Enter text in an empty dropdown
	 		for(WebElement e : chosenPane)
			{
//	 			 System.out.println(e.getAttribute("ControlType"));
//		 		 System.out.println(e.getAttribute("Name"));

//		 		 System.out.println();
//	 			if (!(e.getAttribute("Name").equals("word in position")))
//	 			{
				if (e.getAttribute("ControlType").equals("ControlType.Edit") && i==count) {
					// Does this combobox include an edit component
					e.sendKeys(value);
					return;
					
					} else if(e.getAttribute("ControlType").equals("ControlType.Edit")){
						i++;
					}
//	 			}
			}
			
	}

  /**
   * Rules output area - Read info
   * One parameter: pane
   * positions
   * @param value
   * @param pane
 * @return 
   * @throws InterruptedException
   */
  public List<String> readRulesOutputArea(int paneNum) throws InterruptedException
	{

	  	List<List<WebElement>> panes = getPanesForOutputRules();
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	 	 
//	 	 int getrid = 2; // In order to get rid of the first two parts of the rule before starting working with the conditions
	 	 for(WebElement e : chosenPane)
			{
	 		 try {
//	 			System.out.println(e.getAttribute("ControlType"));
//	 		   	System.out.println(e.getAttribute("Name"));
//	 		   	
//	 		   	System.out.println();
	 			 

	 			if (e.getAttribute("ControlType").equals("ControlType.ComboBox") && e.getAttribute("Name").equals("word in position")){{
    	    					List<String> chosenValues = getChosenValueInDropdown(e);
    	    					return chosenValues;
    	    				}
	  	}
	
		} catch (Exception e2) {
			e2.printStackTrace();
		}


	}
		return null;
	}
  	
  /**
   * Rules output area, check if element do exist
   * -four parameters:pane, type of element, value in element
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public boolean rulesOutputAreaIfExist(int paneNum, String type, String value) throws InterruptedException
	{

	  	List<List<WebElement>> panes = getPanesForOutputRules();
	 	 
	 	// Chosen pane
	 	 List<WebElement> chosenPane = panes.get(paneNum);
	 	
//	 	 System.out.println("---------------------------------");
//		 System.out.println("conditionNum: " + conditionNum);
	 	
//	 	for(WebElement e : chosenPane)
//		{
// 			 System.out.println(e.getAttribute("ControlType"));
//	 		 System.out.println(e.getAttribute("Name"));
////	 		 boolean selectedIsFound = false;
////	 		 System.out.println(e.getAttribute("Selection"));
//	 		
//	 		 System.out.println();
////			if (e.getAttribute("ControlType").equals("ControlType." + type )) {
//	 		if (e.getAttribute("Name").equals(value) && e.getAttribute("ControlType").equals("ControlType." + type )) {
//				List<WebElement> listItems = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
////				for (WebElement item : listItems) {
////					try {
////						if(item.isSelected())
////							if(item.getAttribute("Name").equals(value)) {
////								selectedIsFound = true;
////								break;
////							}
////					} catch (Exception e2) {
////						System.out.println(e2);
////					}
////					
////				
//					System.out.println();
////					if (item.getAttribute("Name").equals(value) && item.getAttribute("ControlType").equals("ControlType." + type )) {
//					if (item.getAttribute("Name").equals(value)) {
//						return true;
//					} 
////				}
////				if (selectedIsFound) {
////				chooseValueInDropdown(e, value);
////				return e;
////				}
//			}
//
//		}
	 		
//	 	System.out.println();
	 	for(WebElement e : chosenPane) {
	 		 
	 		if (e.getAttribute("ControlType").equals("ControlType." + type)) {
	 		
	 		switch ("ControlType." + type) {
			case "ControlType.Button":
				if(e.getAttribute("Name").equals(value))
					return true;
					break;	

			case "ControlType.Text":
				if(e.getAttribute("Name").equals(value))			
					return true;
					break;
					
			case "ControlType.ComboBox":
				List<WebElement> listItems = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
				for (WebElement item : listItems)
					if(item.isSelected())
						if(item.getAttribute("Name").equals(value))
							return true;
				break;
				
			case "ControlType.Edit":
				 if(e.getText().equals(value))
					 return true;
				 break;
				
			}
	 		
	 		}
	 		
	 		} // for(WebElement e : chosenPane) { 
	 	
	 	return false;
	}
  
  /**
   * Rules input area, check if rule is deleted successfully
   * one parameter: rule number
   * @param value
   * @param pane
   * @throws InterruptedException
   */
  public void rulesOutputAreaRemoveRule(ExtentTest test, Logger logger, int paneNum) throws InterruptedException
	{

	     List<String> pane = new ArrayList<String>();
	 	 List<List<String>> panes = new ArrayList<List<String>>();
	 	 
	 	 List<String> paneAfter = new ArrayList<String>();
	 	 List<List<String>> panesAfter = new ArrayList<List<String>>();

	 	 List<WebElement> allElements = p_rule_response.findElements(By.xpath(".//*"));
	 	 
	 	// Split the panes by the X at the start of each pane
	 	 boolean found = false;
	 	 boolean skipFirst = true;
	 	 for(WebElement e : allElements)
			{
	 		 try {
	 			
//	 			System.out.println(e.getAttribute("ControlType"));
//	 			System.out.println(e.getAttribute("Name"));
	 			
	 	    		// check if starting a new pane
//	 			 	if (firstCondition) {
	 			if (e.getAttribute("ControlType").equals("ControlType.Pane") && e.getAttribute("Name").equals(""))
		 				if (skipFirst)
		 					skipFirst = false;
		 				else found = true;
//		    	    	firstCondition = false;
//		    	    	}
//	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
//	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panes.add(pane);
//	 	    			System.out.println("*******************");
	 	    			pane = new ArrayList<String>();
	 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}
	 	 
	 	 // After finishing running add the lastest pane
	 	 panes.add(pane);

	 	 // Click the delete button for the chosen rule
	 	 rulesOutputAreaRemovePane(paneNum);
	 	
	 	 // Check the status after deleting the selected pane
	 	 //	-check the rules number are lower by one
	 	 // -check other rules are still exist with the same components
	 	// Split the panes by the X at the start of each pane
	 	 allElements = p_rule_response.findElements(By.xpath(".//*"));
	 	 found = false;
	 	 skipFirst = true;
	 	for(WebElement e : allElements)
		{
 		 try {
 			
// 			System.out.println(e.getAttribute("ControlType"));
// 			System.out.println(e.getAttribute("Name"));
 			
 	    		// check if starting a new pane
// 			 	if (firstCondition) {
 			if (e.getAttribute("ControlType").equals("ControlType.Pane") && e.getAttribute("Name").equals(""))
	 				if (skipFirst)
	 					skipFirst = false;
	 				else found = true;
//	    	    	firstCondition = false;
//	    	    	}
// 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
// 	    			firstCondition = true;

 	    		if (!found) {
 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
 	    		} else {
 	    			found = false;
 	    			panesAfter.add(paneAfter);
// 	    			System.out.println("*******************");
 	    			paneAfter = new ArrayList<String>();
 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
 	    		}
			} catch (Exception e2) {
				System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
			}
  	}

	 	 // After finishing running add the latest pane
	 	 panesAfter.add(paneAfter);

	 	 // Remove the removed pane for the before pane list
	 	 panes.remove(paneNum);
	 	 
	 	 // Check that the rules components in pane are the same as in the paneAfter
	 	 // Remove last cell in both due"
//	 	ControlType.Button +, ControlType.Pane
//	 	ControlType.Button +, ControlType.Button +
	 	// remove ControlType.Button + if exist
	 	List<String> lastPane = null;
	 	String lastCell = null;
	 	 if (!panes.isEmpty()) {
	 		lastPane = panes.get(panes.size()-1);
	 		lastCell = lastPane.get(lastPane.size()-1); 
		 	if (lastCell.equals("ControlType.Button +")) {
		 		if ((panes.size())!=0) {
		 			lastPane.remove(lastPane.size()-1);
		 		}
		 			else
		 				lastPane.remove(lastPane.size()-1);
		 	}
	 	 }
	 	if (!panesAfter.isEmpty()) {
	 		lastPane = panesAfter.get(panesAfter.size()-1);
	 		lastCell = lastPane.get(lastPane.size()-1); 
		 	if (lastCell.equals("ControlType.Button +"))
		 		if ((panesAfter.size())!=0)
		 			lastPane.remove(lastPane.size()-1);
		 			else
		 				lastPane.remove(lastPane.size()-1);
	 	}
//	 	panes.remove(panes.size()-1);
//	 	panesAfter.remove(panesAfter.size()-1);
	 	 
	 	 // Check the size difference
	 	 boolean checkSize;
	 	 if (!panesAfter.isEmpty() && !panes.isEmpty()) {
		 	 checkSize = (panesAfter.size() == panes.size());
		 	 LogUltility.log(test, logger, "Size difference: " + checkSize);
		 	 assertTrue(checkSize);
	 	 }
	 	//--------------------
//	 	boolean rulesEqual0 = panes.get(0).equals(panesAfter.get(0));
//	 	System.out.println("panes.get(0).size(): " + panes.get(0).size());
//	 	System.out.println("panesAfter.get(0).size(): " + panesAfter.get(0).size());
//	 	for (int i = 0; i < panes.get(0).size(); i++) {
//	 		System.out.println("1: " + panes.get(0).get(i).getAttribute("Name"));
//	 		System.out.println("2: " + panesAfter.get(0).get(i).getAttribute("Name"));
//		}
//	 	
//	 	boolean rulesEqual1 = panes.get(1).equals(panesAfter.get(1));
//	 	System.out.println("panes.get(1).size(): " + panes.get(1).size());
//	 	System.out.println("panesAfter.get(1).size(): " + panesAfter.get(1).size());
//	 	for (int i = 0; i < panes.get(1).size(); i++) {
//	 		System.out.println("1: " + panes.get(1).get(i).getAttribute("Name"));
//	 		System.out.println("2: " + panesAfter.get(1).get(i).getAttribute("Name"));
//		}
	 	//--------------------
	 	
	 	 // Remove empty lists in the main list
	 	panes.removeIf(p -> p.isEmpty());
	 	panesAfter.removeIf(p -> p.isEmpty());
	 	
	 	
	 	if (panesAfter.isEmpty() && panes.isEmpty()) {
	 		LogUltility.log(test, logger, "Is Pane " + paneNum + " deleted successfully: True");
	 		return;
	 	}
	 	
	 	boolean rulesEqual = panes.equals(panesAfter);
	 	LogUltility.log(test, logger, "Is Pane " + paneNum + " deleted successfully: " + rulesEqual);
	 	assertTrue(rulesEqual);
	}
  
	  /**
	   * Rules output area, add and check if a new rule is added successfully
	   * one parameter: rule number
	   * @param value
	   * @param pane
	   * @throws InterruptedException
	   */
	  public void rulesOutputAreaAddRuleAndCheck(ExtentTest test, Logger logger) throws InterruptedException
		{
	
		  List<String> pane = new ArrayList<String>();
		 	 List<List<String>> panes = new ArrayList<List<String>>();
		 	 
		 	 List<String> paneAfter = new ArrayList<String>();
		 	 List<List<String>> panesAfter = new ArrayList<List<String>>();

		 	 List<WebElement> allElements = p_rule_response.findElements(By.xpath(".//*"));
		 	 
		 	// Split the panes by the X at the start of each pane
		 	 boolean found = false;
		 	 boolean skipFirst = true;
		 	 for(WebElement e : allElements)
				{
		 		 try {
		 			
//		 			System.out.println(e.getAttribute("ControlType"));
//		 			System.out.println(e.getAttribute("Name"));
		 			
		 	    		// check if starting a new pane
//		 			 	if (firstCondition) {
		 			if (e.getAttribute("ControlType").equals("ControlType.Pane") && e.getAttribute("Name").equals(""))
			 				if (skipFirst)
			 					skipFirst = false;
			 				else found = true;
//			    	    	firstCondition = false;
//			    	    	}
//		 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
//		 	    			firstCondition = true;
		 	    		

		 	    		if (!found) {
		 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
		 	    		} else {
		 	    			found = false;
		 	    			panes.add(pane);
//		 	    			System.out.println("*******************");
		 	    			pane = new ArrayList<String>();
		 	    			pane.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
		 	    		}
					} catch (Exception e2) {
						System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
					}
		  	}
		 	 
		 	 // After finishing running add the lastest pane
		 	 panes.add(pane);

		 	 // Add a new rule
		 	 rulesOutputArea();
		 	
		 	 // Check the status after deleting the selected pane
		 	 //	-check the rules number are lower by one
		 	 // -check other rules are still exist with the same components
		 	// Split the panes by the X at the start of each pane
		 	 allElements = p_rule_response.findElements(By.xpath(".//*"));
		 	 found = false;
		 	skipFirst = true;
		 	for(WebElement e : allElements)
			{
	 		 try {
	 			
//	 			System.out.println(e.getAttribute("ControlType"));
//	 			System.out.println(e.getAttribute("Name"));
	 			
	 	    		// check if starting a new pane
//	 			 	if (firstCondition) {
	 			if (e.getAttribute("ControlType").equals("ControlType.Pane") && e.getAttribute("Name").equals(""))
		 				if (skipFirst)
		 					skipFirst = false;
		 				else found = true;
//		    	    	firstCondition = false;
//		    	    	}
//	 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
//	 	    			firstCondition = true;
	 	    		

	 	    		if (!found) {
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		} else {
	 	    			found = false;
	 	    			panesAfter.add(paneAfter);
//	 	    			System.out.println("*******************");
	 	    			paneAfter = new ArrayList<String>();
	 	    			paneAfter.add(e.getAttribute("ControlType") + " "  +e.getAttribute("Name"));
	 	    		}
				} catch (Exception e2) {
					System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
				}
	  	}

		 	 // After finishing running add the latest pane
		 	 panesAfter.add(paneAfter);

		 	 // Check the size difference
		 	 boolean checkSize = (panesAfter.size() == (panes.size()+1));
		 	 LogUltility.log(test, logger, "Size difference: " + checkSize);
		 	 assertTrue(checkSize);
		 	 
		 	 // Check that the rules components in pane are the same as in the paneAfter
		 	 // Remove last cell in both due"
//		 	ControlType.Button +, ControlType.Pane
//		 	ControlType.Button +, ControlType.Button +
		 	panes.remove(panes.size()-1);
		 	panesAfter.remove(panesAfter.size()-1);
		 	
		 	//--------------------
//		 	boolean rulesEqual0 = panes.get(0).equals(panesAfter.get(0));
//		 	System.out.println("panes.get(0).size(): " + panes.get(0).size());
//		 	System.out.println("panesAfter.get(0).size(): " + panesAfter.get(0).size());
//		 	for (int i = 0; i < panes.get(0).size(); i++) {
//		 		System.out.println("1: " + panes.get(0).get(i).getAttribute("Name"));
//		 		System.out.println("2: " + panesAfter.get(0).get(i).getAttribute("Name"));
//			}
//		 	
//		 	boolean rulesEqual1 = panes.get(1).equals(panesAfter.get(1));
//		 	System.out.println("panes.get(1).size(): " + panes.get(1).size());
//		 	System.out.println("panesAfter.get(1).size(): " + panesAfter.get(1).size());
//		 	for (int i = 0; i < panes.get(1).size(); i++) {
//		 		System.out.println("1: " + panes.get(1).get(i).getAttribute("Name"));
//		 		System.out.println("2: " + panesAfter.get(1).get(i).getAttribute("Name"));
//			}
		 	//--------------------
		 	
		 // Compare all the previous panes without the new added one
		 	boolean equal = true;
		 	 for (int i = 0; i < panes.size(); i++) {
		 		equal = panes.get(i).equals(panesAfter.get(i));
		 		if (equal == false) break;
			}
		 	LogUltility.log(test, logger, "Rule is successfully added: " + equal);
		 	assertTrue(equal);
		}


	  /**
	   * Get records priority from the Rules.txt files
	   * @return
	   * @throws FileNotFoundException
	   */
	   public Hashtable<Integer, Integer> getRecordsPriorityFromRules(String  ruleFile) throws FileNotFoundException {
	 	   			
	 	   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\"+ruleFile), "UTF-8");
	 	   		Hashtable<Integer, Integer> rulesPriority = new Hashtable<Integer, Integer>();
	 	   		
	 	   		while(in.hasNextLine()) {
	 	   		    String line = in.nextLine().trim();
	 	   		    String[] splitted = line.split("	");  
	 	   		    int ruleNumber = Integer.parseInt(splitted[1]);
	 	   		    int priority = Integer.parseInt(splitted[4]);
	 	   		    rulesPriority.put(ruleNumber, priority);
	 	   		    }
	 	   		    
	 	   		in.close();
	 			return rulesPriority;
	   }
	   
	   /**
	    * click specific tab
	    * @param tab
	    * @throws InterruptedException
	    */
	   public void clickTabs(String tab) throws InterruptedException
	 	{
	   	List<WebElement> rulesTabs = tc_proximity.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TabItem')]"));
	   	for (WebElement tabIn : rulesTabs) {
//	   		System.out.println(tabIn.getAttribute("ControlType"));
// 		   	System.out.println(tabIn.getAttribute("Name"));
 		   	if (tabIn.getAttribute("Name").equals(tab))
 		   	tabIn.click();
		}
	 	}
	  
	   public Hashtable<String, String> checkboxMark(String file) throws InterruptedException, FileNotFoundException
	 	{
		   String ruleFile = null;
			   switch (file) {
					case "Dis":
						ruleFile = "disambiguation_rules";
						break;
					
					case "Nor":
						ruleFile = "normalization_rules";
						break;
											
					case "Pro":
						ruleFile = "prosody_rules";
						break;
						
					case "Trans":
						ruleFile = "transcription_rules";
						break;
					
					}
		    Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\"+ruleFile+".txt"), "UTF-8");
	   		Hashtable<String, String> rulesActive = new Hashtable<String, String>();
	   		
	   		while(in.hasNextLine()) {
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");
	   		    String ruleName = splitted[2];
	   		    String activeOrNot = splitted[0];
	   		    rulesActive.put(ruleName, activeOrNot);
	   		    }
	   		    
	   		in.close();
	   		
	   		return rulesActive;
	 	}
	   

	   public List<List<WebElement>> getPanesForOutputRules() {
		   List<WebElement> pane = new ArrayList<WebElement>();
		 	 List<List<WebElement>> panes = new ArrayList<List<WebElement>>();

		 	 List<WebElement> allElements = p_rule_response.findElements(By.xpath(".//*"));

//		 	 System.out.println("mibo");
		 	 

		 	 // Split the panes by the X at the start of each pane
		 	 boolean found = false;
		 	 boolean skipFirst = true;
		 	 for(WebElement e : allElements)
				{
		 		 try {
		 			
//		 			System.out.println(e.getAttribute("ControlType"));
//		 			System.out.println(e.getAttribute("Name"));
//		 			System.out.println();
		 	    		// check if starting a new pane
//		 			 	if (firstCondition) {
//			    	    		if (e.getAttribute("ControlType").equals("ControlType.Button") && e.getAttribute("Name").equals("X"))
		 			 			if (e.getAttribute("ControlType").equals("ControlType.Pane") && e.getAttribute("Name").equals(""))
		 			 				if (skipFirst)
		 			 					skipFirst = false;
		 			 				else found = true;
//			    	    	firstCondition = false;
//			    	    	}
//		 	    		if (e.getAttribute("ControlType").equals("ControlType.Pane"))
//		 	    			firstCondition = true;
		 	    		

		 	    		if (!found) {
		 	    			pane.add(e);
		 	    		} else {
		 	    			found = false;
		 	    			panes.add(pane);
//		 	    			System.out.println("*******************");
		 	    			pane = new ArrayList<WebElement>();
		 	    			pane.add(e);
		 	    		}
					} catch (Exception e2) {
						System.out.println("Bypassed by Try/catch: " + e.getAttribute("Name"));
					}
		  	}
		 	 
		 	 // After finishing running add the lastest pane
		 	 panes.add(pane);
		 	 
			return panes;
	}
	   
	   
	   
       /**
			  * Get records with proximity rules from the disambiguation_rules.txt file and exists in Dictionary.txt file
			  * @return
	         * @throws IOException 
			  */
			  public  List<String> getDisambiguationRlues() throws IOException {
				  
				  // Disambiguation file
				  File DisFile = new File(Setting.AppPath + Setting.Language+"\\Data\\disambiguation_rules.txt");	  
				  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
				  DictionaryTab dictionaryTab = new DictionaryTab();
				  List<String> records = new ArrayList<>();
				  String line;
				   	while((line = DisReader.readLine()) != null)
				   	{
				   		    line = line.trim();
				   		    String[] splitted = line.split("	");
				   		    String format = Setting.Language.equals("EN")? "EN{": "HE{" ;
				   		    if(splitted[2].contains(format))
				   		    {
				   		    	for(int i=0;i<splitted.length;i++)
				   		    		if(splitted[i].contains("dictionary_key"))
				   		    		{
				   		    			String record = splitted[i].split("dictionary_key")[1].substring(1);
				   		    			
				   		    			// Check if the record is found in Dictionary file
				   		    			if( ! (dictionaryTab.getRegularRecordsInfoFromDictionary(record).size() == 0 ))
				   		    				records.add(record);
				   		    		}
				   		    }    	
				     }
				   	
				   	DisReader.close();
				   	return records;
			  }
	   
			  
//			  /**
//			 * Get records with proximity rules from the disambiguation_rules.txt file and exists in Dictionary.txt file
//			 * @return
//		      * @throws IOException 
//			 */
//			 public  void getc() throws IOException {
//					  
//					  // Disambiguation file
					  File DisFile = new File(Setting.AppPath +Setting.Language +"\\Data\\disambiguation_rules.txt");	  
//					  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
//					
////					  List<Pair<String,HashMap<Integer,ArrayList<String>>>> info = new ArrayList<Pair<String,HashMap<Integer,ArrayList<String>>>>();
//					  
//					  String line;
//					   	while((line = DisReader.readLine()) != null)
//					   	{
//					   		   line = line.trim();
//					   		   String[] splitted = line.split("	");
//					   		    // Get sentences with more than two panes
//					   		  int numOfPanes = Integer.parseInt(splitted[6]);
//					   		  int i=0;
//					   		  if(numOfPanes >=2)
//					   		  {
//					   			String sentenceName = splitted[2];
//					   			String val = splitted[7+i];
//					   			String splitSubConditions[] = val.split("||")[1].split("|");
//					   			int subConNum = splitSubConditions.length;
//					   			while( subConNum-- > 0)
//					   			{
//					   				int j = 0;
//					   				val = val.split("||")[1].split("|")[j];
//					   				String dropdown = null;
//						   			String dropdownValue = null;				   			
//
//						   			if(val.contains("="))
//						   			{
//						   				dropdown = val.split("=")[0].replaceAll("_", " ");
//						   				dropdownValue =  val.split("=")[1].replaceAll("_", " ");
//						   				
//							   			if(val.contains(","))
//							   			{
//							   				int len = val.split(",").length;
//							   			}
//							   			
//						   			
//						   			}
//						   			if(val.contains("!="))
//						   			{
//						   				dropdown = val.split("!=")[0].replaceAll("_", " ");
//						   				dropdownValue =  val.split("!=")[1].replaceAll("_", " ");
//						   			}
//						   			if(val.contains("!>"))
//						   			{
//						   				dropdown = val.split("!>")[0].replaceAll("_", " ");
//						   				dropdownValue =  val.split("!>")[1].replaceAll("_", " ");
//						   			}
//						   			if(val.contains(">"))
//						   			{
//						   				dropdown = val.split(">")[0].replaceAll("_", " ");
//						   				dropdownValue =  val.split(">")[1].replaceAll("_", " ");
//						   			}
//						   			
//					   			}
//					   		
//					   		  }
//					    }
//					   	
//					   	DisReader.close();
//				  }
			 
			 
	  /**
		 * Get disambiguation rules with more than one condition from disambiguation_rules.txt files
		 * @return
	      * @throws IOException 
		 */
		 public  List<String> getDisRulesWithMultiConditions() throws IOException {
				  
				  // Disambiguation file
			  File DisFile = new File(Setting.AppPath+Setting.Language + "\\Data\\disambiguation_rules.txt");	  
			  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
			  List<String> disRules = new ArrayList<String>();
			  String line;
			 	while((line = DisReader.readLine()) != null)
			 		{
			 		 line = line.trim();
			   		   String[] splitted = line.split("	");
			   		   
			   		    // Get sentences with more than two panes/conditions
			   		   	int numOfPanes = Integer.parseInt(splitted[6]);
			   		   	if(numOfPanes>1)
			   		   	{
			   		   		int ruleNumber = Integer.parseInt(splitted[1]);
			   		   		String ruleNum = ruleNumber < 10 ? "00"+splitted[1] : ruleNumber>9 && ruleNumber <100 ? "0"+splitted[1] :splitted[1] ; 
			   		   		String ruleName = ruleNum+" : "+splitted[2];
			   		   		disRules.add(ruleName);   		
			   		   	}
			   		}
			 	
			   DisReader.close();
			   return disRules;
		 }
		   
		 
		 
		/**
		 *  Get number of panes/conditions of a rule from disambiguation_rules.txt files
		 * @param ruleNumber - the wanted rule number
		 * @return
		 * @throws IOException
		 */
			 public  int getDisRuleNumberOfConditions(String ruleNumber) throws IOException {
					  
				// Disambiguation file
				  File DisFile = new File(Setting.AppPath+Setting.Language + "\\Data\\disambiguation_rules.txt");	    
				  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
				  String line;
				  
				  // Since leading zeros are not represented in the file, we should eliminate them to be able to compare the rule numbers
				   ruleNumber = String.valueOf(Integer.parseInt(ruleNumber));
				 	while((line = DisReader.readLine()) != null)
				 		{
				 		 line = line.trim();
				   		   String[] splitted = line.split("	");
				   		   if(splitted[1].equals(ruleNumber))
				   		   {
				   		 	int numOfPanes = Integer.parseInt(splitted[6]);
				   		    DisReader.close();
				   		 	return numOfPanes;
				   		   }  
				   		}
				 	
				   DisReader.close();
				   // not found
				   return -1;
			 }
			 
			 /**
			  * get disambiguation rules names which has more than one subcondition
			  * @return
			  * @throws IOException
			  */
			 public  List<String> getDisRulesWithMultiSubConditions() throws IOException {
					  
			  // Disambiguation file
			  File DisFile = new File(Setting.AppPath+Setting.Language + "\\Data\\disambiguation_rules.txt");	  
			  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
			  List<String> disRules = new ArrayList<String>();
			  String line;
			 	while((line = DisReader.readLine()) != null)
			 		{
			 		 line = line.trim();
			   		 String[] splitted = line.split("	");	   
			   		 // Get sentences with more than two sub conditions
			   		 boolean hasSubCondition = false;
			   		  for(int i=7;i<splitted.length;i++)
			   		   {
			   			  
			   			  String toCheck ="";
			   			  if(splitted[i].contains("optional"))
			   				  toCheck = splitted[i].substring(12, splitted[i].length());
			   			  else
			   				  toCheck = splitted[i].substring(4, splitted[i].length());
			   		    	// Check if each condition includes sub conditions , look for "|" character
			   		    	if(toCheck.contains("|"))
			   		    		hasSubCondition = true;
			   		   }
			   		    
			   		  if(hasSubCondition)
			   		   {
				   		   	int ruleNumber = Integer.parseInt(splitted[1]);
			   		   		String ruleNum = ruleNumber < 10 ? "00"+splitted[1] : ruleNumber>9 && ruleNumber <100 ? "0"+splitted[1] :splitted[1] ; 
			   		   		String ruleName = ruleNum+" : "+splitted[2];
			   		   		disRules.add(ruleName); 
			   		   }
		
			   		}
			 	
			   DisReader.close();
			   return disRules;
			 }
			 
			 
			 
			 
		 /**
		  * Get the pane indexes which has more than one sub condition
		  * @return
		  * @throws IOException
		  */
		 public  List<Integer> getDisRulePaneIndexesForSubCondition(String ruleNumber) throws IOException {
				  
		  // Disambiguation file
		  File DisFile = new File(Setting.AppPath+Setting.Language + "\\Data\\disambiguation_rules.txt");	  
		  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
		  ruleNumber = String.valueOf(Integer.parseInt(ruleNumber));
		  List<Integer> panes = new ArrayList<Integer>();
		  String line;
		 	while((line = DisReader.readLine()) != null)
		 		{
		 		 line = line.trim();
		   		 String[] splitted = line.split("	");	   
		   		 if(splitted[1].equals(ruleNumber))
		   		 {
		   			 for(int i=7;i<splitted.length;i++)
			   		   {
			   			  String toCheck ="";
			   			  if(splitted[i].contains("optional"))
			   				  toCheck = splitted[i].substring(12, splitted[i].length());
			   			  else
			   				  toCheck = splitted[i].substring(4, splitted[i].length());
			   		    	// Check if each condition includes sub conditions , look for "|" character
			   		    	if(toCheck.contains("|"))
			   		    		panes.add(i-7);
			   		   }
		   		 }
		   		 
		   		}
		 	
		   DisReader.close();
		   // Not found
		   return panes;
		 }
		 
		 
		  /**
			 * Get disambiguation rules with N conditions / Panes from disambiguation_rules.txt files
			 * @return
		      * @throws IOException 
			 */
			 public  List<String> getDisRulesWithMultiConditions(int amount) throws IOException {
					  
					  // Disambiguation file
				  File DisFile = new File(Setting.AppPath+Setting.Language + "\\Data\\disambiguation_rules.txt");	  
				  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
				  List<String> disRules = new ArrayList<String>();
				  String line;
				 	while((line = DisReader.readLine()) != null)
				 		{
				 		 line = line.trim();
				   		   String[] splitted = line.split("	");
				   		   
				   		    // Get sentences with N panes/conditions
				   		    int numOfPanes = Integer.parseInt(splitted[6]);
				   		   	if(numOfPanes == amount)
				   		   	{
				   		   		int ruleNumber = Integer.parseInt(splitted[1]);
				   		   		String ruleNum = ruleNumber < 10 ? "00"+splitted[1] : ruleNumber>9 && ruleNumber <100 ? "0"+splitted[1] :splitted[1] ; 
				   		   		String ruleName = ruleNum+" : "+splitted[2];
				   		   		disRules.add(ruleName);   		
				   		   	}
				   		}
				 	
				   DisReader.close();
				   return disRules;
			 }
		 
			 
			 /**
			  * gets the pane/conditions dropdown values
			  * @param paneNum
			  * @return a list of the dropdowns values from left to right
			  * @throws InterruptedException
			  */
			 public List<String> readRulesInputAreaValues(int paneNum) throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get wanted pane element
				 WebElement pane = panes.get(paneNum);
				 // Get all pane itemlist elements to get the selected values
		        List<WebElement> itemlist= pane.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
		        List<String> values = new ArrayList<String>();
		        
		        for(WebElement e : itemlist)
		        {
		        	if(e.isSelected())
		        		values.add(e.getAttribute("Name"));		
		        }
				 
				 return values;
			 }
			 
			 
			 
			 /**
			  * gets the pane/conditions dropdown values
			  * @param paneNum
			  * @return a list of the dropdowns values from left to right
			  * @throws InterruptedException
			  */
			 public List<String> readRulesInputAreaDropDownValues(int paneNum,int dropdownNum) throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get wanted pane element
				 WebElement pane = panes.get(paneNum);
				 // Get all pane itemlist elements to get the selected values
		        List<WebElement> itemlist= pane.findElements(By.xpath(".//*[contains(@ClassName, 'ComboLBox')]"));	        
		        WebElement list = itemlist.get(dropdownNum);
		        List<WebElement> listElements = list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));   
		        List<String> values = new ArrayList<String>();
		     
		        for(WebElement e : listElements)
		        {
		        	values.add(e.getAttribute("Name"));
		        }
				 
				 return values;
			 }


			 
			 /**
			  * get displayed rule number of panes/conditions
			  * @return the number of panes
			  * @throws InterruptedException
			  */
			 public int getDisplayedRulePanesAmount() throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				
				 return panes.size();
			 }
			 
			 
			 /**
			  * get displayed rule number of sub conditions  for specific pane
			  * @return the number of panes
			  * @throws InterruptedException
			  */
			 public int getSubConditionsAmountinPane(int paneIndex) throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get second layer of panes
				 List<WebElement> pane = panes.get(paneIndex).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 List<WebElement> temp = pane.get(1).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 
				 return temp.size();
			 }
			 
			 
			 
			 
			 /**
			  * Get the specific sub condition dropdown values
			  * @param paneIndex
			  * @param subIndex
			  * @return
			  * @throws InterruptedException
			  */
			 public List<String> readRulesInputAreaValues(int paneIndex,int subIndex) throws InterruptedException
			 {
					 
				 List<String> values = new ArrayList<String>();
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get WIP Value first
				 WebElement cobmoList = panes.get(paneIndex).findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
				 List<WebElement> wipList = cobmoList.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
				 
				  for(WebElement e : wipList)
			        {
			        	if(e.isSelected())
			        		values.add(e.getAttribute("Name"));		
			        }
				  
				 // Get second layer of panes
				 List<WebElement> pane = panes.get(paneIndex).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 List<WebElement> temp = pane.get(1).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 List<WebElement> subConditionList = temp.get(subIndex).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
	
			
		        
		        for(WebElement e : subConditionList)
		        {
		        	if(e.isSelected())
		        		values.add(e.getAttribute("Name"));		
		        }
				 
				 return values;

			}
			 
			 
			 /**
			  * get displayed output area panes/conditions
			  * @return the number of panes
			  * @throws InterruptedException
			  */
			 public int getDisplayedOutputPanesAmount() throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_response.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				
				 return panes.size();
			 }
			 
			 
			 
			 /**
			  * gets the output area pane/conditions dropdown values
			  * @param paneNum
			  * @return a list of the dropdowns values from left to right
			  * @throws InterruptedException
			  */
			 public List<String> readRulesOutputtAreaDropDownValues(int paneNum,int dropdownNum) throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_response.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get wanted pane element
				 WebElement pane = panes.get(paneNum);
				 // Get all pane itemlist elements to get the selected values
		        List<WebElement> itemlist= pane.findElements(By.xpath(".//*[contains(@ClassName, 'ComboLBox')]"));	        
		        WebElement list = itemlist.get(dropdownNum);
		        List<WebElement> listElements = list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));   
		        List<String> values = new ArrayList<String>();
		     
		        for(WebElement e : listElements)
		        {
		        	values.add(e.getAttribute("Name"));
		        }
				 
				 return values;
			 }

			 
			 /**
			  * gets the pane/conditions of output area dropdown values
			  * @param paneNum
			  * @return a list of the dropdowns values from left to right
			  * @throws InterruptedException
			  */
			 public List<String> readRulesOutputAreaValues(int paneNum) throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_response.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get wanted pane element
				 WebElement pane = panes.get(paneNum);
				 // Get all pane itemlist elements to get the selected values
		        List<WebElement> itemlist= pane.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
		        List<String> values = new ArrayList<String>();
     
		        for(WebElement e : itemlist)
		        {
		        	if(e.isSelected())
		        		values.add(e.getAttribute("Name"));		
		        }
		          
		        // Get the input field value
		        List<WebElement> panelist= pane.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
		        panelist.remove(0);
		        if(!panelist.isEmpty())
		        	values.add(panelist.get(0).getText());
		        
				 return values;
			 }
			 
			 /**
			  * Click on the 'or' or 'and' button between sub conditions to change its value
			  * @param paneIndex
			  * @param subIndex
			  * @throws InterruptedException
			  */
			 
			 public void clickbtnBetweenSubCon(int paneIndex,int subIndex) throws InterruptedException
			 {
					 
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 
				 List<WebElement> pane = panes.get(paneIndex).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 List<WebElement> temp = pane.get(1).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 List<WebElement> buttons = temp.get(subIndex).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Button')]"));
	
//				 for(WebElement e : buttons)
//					 System.out.println(e.getAttribute("Name"));
				 buttons.get(1).click();			 
			}
			 
			 
			 
	
			 public int getEdgePaneIndex() throws InterruptedException
			 {
				 // Get panes/conditions elements
				 List<WebElement> panes = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
				 
				 // Get selected WIP from each condition
				 WebElement comboBox;
				 List<Integer> values = new ArrayList<Integer>();
				 for(int i=0;i<panes.size();i++)
				 {
					  comboBox =  panes.get(i).findElement(By.xpath("./*[contains(@ControlType, 'ControlType.ComboBox')]"));
					  WebElement list= comboBox.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.List')]"));
					 List<WebElement> itemlist = list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));;
						for(WebElement e : itemlist)
				        	if(e.isSelected())
				        		values.add(Integer.parseInt(e.getAttribute("Name")));		
				 }
				 
				 // Get the index of the condition with max and min value
				 int maxWipValue = Collections.min(values);
				 int minWipValue = Collections.max(values);
				 int paneIndexMax=-1;
				 int paneIndexMin=-1;
				 for(int i = 0; i < values.size(); i++){
					    if(maxWipValue < values.get(i)){
					    	maxWipValue = values.get(i);
					    	paneIndexMax = i;
					    }
					    
					    if(minWipValue > values.get(i)){
					    	minWipValue = values.get(i);
					    	paneIndexMin = i;
					    }
			 }
			 
			 int paneIndex = maxWipValue == 0 ? paneIndexMin : paneIndexMax;
			 return paneIndex;
			 } 
			 
			 
			 
			 /**
			  * reset the vertical scroll in rules input area to the beginning (scroll up)
			  * @throws AWTException
			  */
			 public void scrollUpRulesInputArea() throws AWTException
			 {
				 // get scrolls
				 List<WebElement> scrolls = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ScrollBar')]"));
				 WebElement button = scrolls.get(scrolls.size()-1).findElement(By.id("LargeDecrement"));	 	 
				String attachPlace = leftArrow.getAttribute("ClickablePoint");
				String[] XY = attachPlace.split(",");
				Robot robot = new Robot();
			    int XPoint = Integer.parseInt(XY[0]), YPoint = Integer.parseInt(XY[1]);
		        robot.mouseMove(XPoint, YPoint-10);      
				button.click();
			 }
			 
			 
			 /**
			  * Scroll down in rules input area
			  * @throws AWTException
			  */
			 public void scrollDownRulesInputArea() throws AWTException
			 {
				 // get scrolls
				 List<WebElement> scrolls = p_rule_pattern.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ScrollBar')]"));
				 WebElement button = scrolls.get(scrolls.size()-1).findElement(By.id("LargeIncrement"));	 	 
				String attachPlace = rightArrow.getAttribute("ClickablePoint");
				String[] XY = attachPlace.split(",");
				Robot robot = new Robot();
			    int XPoint = Integer.parseInt(XY[0]), YPoint = Integer.parseInt(XY[1]);
		        robot.mouseMove(XPoint, YPoint+10);      
				button.click();
			 }
			 
			 
			 
			 /**
			  * reset the vertical scroll in rules output area to the beginning (scroll up)
			  * @throws AWTException
			  */
			 public void scrollUpRulesOutputArea() throws AWTException
			 {
				 // get scrolls
				 List<WebElement> scrolls = p_rule_response.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ScrollBar')]"));
				 WebElement button = scrolls.get(scrolls.size()-1).findElement(By.id("LargeDecrement"));	 	 
				String attachPlace = leftArrow.getAttribute("ClickablePoint");
				String[] XY = attachPlace.split(",");
				Robot robot = new Robot();
			    int XPoint = Integer.parseInt(XY[0]), YPoint = Integer.parseInt(XY[1]);
		        robot.mouseMove(XPoint, YPoint-10);      
				button.click();
			 }
			 
			 
			 /**
			  * Scroll down in rules output area
			  * @throws AWTException
			  */
			 public void scrollDownRulesOutputArea() throws AWTException
			 {
				 // get scrolls
				 List<WebElement> scrolls = p_rule_response.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.ScrollBar')]"));
				 WebElement button = scrolls.get(scrolls.size()-1).findElement(By.id("LargeIncrement"));	 	 
				String attachPlace = rightArrow.getAttribute("ClickablePoint");
				String[] XY = attachPlace.split(",");
				Robot robot = new Robot();
			    int XPoint = Integer.parseInt(XY[0]), YPoint = Integer.parseInt(XY[1]);
		        robot.mouseMove(XPoint, YPoint+10);      
				button.click();
			 }
			 
			 
			/**
			 *  Get the dictionary keys of the conditions that are attached to the rule number
			 * @param ruleNumber
			 * @return
			 * @throws IOException
			 */
			  public  List<String> getDisambiguationRlueConditionsKeys(String ruleNumber) throws IOException {
				  
				  // Disambiguation file
				  File DisFile = new File(Setting.AppPath + Setting.Language+"\\Data\\disambiguation_rules.txt");	  
				  BufferedReader DisReader = new BufferedReader(new FileReader(DisFile));
				  List<String> records = new ArrayList<>();
				  String line;
				  boolean finished = false;
				   	while((line = DisReader.readLine()) != null)
				   	{
				   		    line = line.trim();
				   		    String[] splitted = line.split("	");
				   		    if(ruleNumber.equals(splitted[1]))
				   		    {
				   		    	for(int i=7;i<splitted.length;i++)
				   		    		if(splitted[i].contains("dictionary_key"))
				   		    		{
				   		    			String record = splitted[i].split("dictionary_key")[1].substring(1);
				   		    			// Eliminate the leading '=' extra symbol
				   		    			if(record.charAt(0) == '=')
				   		    				record = record.substring(1);
				   		    			
				   		    			String splitRecord[] = record.split("֏");
				   		    			for(int j=0;j<splitRecord.length;j++)
				   		    				records.add(splitRecord[j]);
				   		    			
				   		    			finished = true;
				   		    		}
				   		    }    
				   		    if(finished) break;
				     }
				   	
				   	DisReader.close();
				   	return records;
			  }
			  
			 
}// Class end