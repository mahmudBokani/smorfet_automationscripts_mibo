package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Dictinoary_Search_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Dictionary tab - Search");
	
		// Logger
		logger = LogManager.getLogger(DictionaryTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Dictionary tab - Search");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true){
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
			
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Smorfet app focus
		try {
			CurrentPage = GetInstance(DictionaryTab.class);
//			CurrentPage.As(DictionaryTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Dictionary");
		} catch (Exception e) {
		}
		
	}

	/**
	 * DIC_TC--45:Verify get results when Search exist record in the file 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_45_Verify_get_results_when_Search_exist_record_in_the_file() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_45", "Verify get results when Search exist record in the file");
		
		// Get records from the dictionary file
		CurrentPage = GetInstance(DictionaryTab.class);
		List<String> recordsFromFile = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord.toLowerCase())) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
//	/**
//	 * DIC_TC--6:Verify that get only the match results - "Not equal" search criteria
//	 * @throws InterruptedException 
//	 * @throws FileNotFoundException 
//	 */
//	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
//	public void DIC_TC_6_Verify_that_get_only_the_match_results_Not_equal_search_criteris() throws InterruptedException, FileNotFoundException
//	{
//		test = extent.createTest("Dictionary Search Test - DIC_TC_6", "Verify that get only the match results - \"Not equal\" search criteris");
//		
//		// Get records from the dictionary file
//		CurrentPage = GetInstance(DictionaryTab.class);
//		List<String> recordsFromFile = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
//
//		// Focus on dictionary tab
//		CurrentPage = GetInstance(DictionaryTab.class);
//	//	CurrentPage.As(DictionaryTab.class).tabDictionary.click();
//		CommonFunctions.clickTabs("Dictionary");
//		
//		// Choose "form" from the first search dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//		
//		// Choose "not equal" from the condition dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "not equal");
//		LogUltility.log(test, logger, "not equal is chosen in the condition dropdown");
//		
//		//Type in the search field
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomrecord);
//		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
//		
//		// Click the Retreive button
//		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
//		
//		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
//		
//		// Retrieve records
//		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
//		
//		// Check that retrieved records does not contain the searched record
//		if (recordsList.isEmpty()) assertTrue(false);
//		
//		boolean equalResult = true;
//		for (String s : recordsList) {
//			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//		       if(s.toLowerCase().equals(randomrecord)) continue;
//		       	else {
//		       		equalResult = false;
//		           break;
//		       	}
//		    }
//		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + equalResult);
//		assertFalse(equalResult);
//		
//		LogUltility.log(test, logger, "Test Case PASSED");	
//	}
//	
	/**
	 * DIC_TC--66:Verify user can filter the search by �Frames�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_66_Verify_user_can_filter_the_search_by_Frames() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_66", "Verify user can filter the search by Frames");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "frames");
		LogUltility.log(test, logger, "frames is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String randomFrame = "clausal complement";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomFrame);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomFrame);
		LogUltility.log(test, logger, "chosen search value: " + randomFrame);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
//		//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
//		String assignedFrames = CurrentPage.As(DictionaryTab.class).getFramesOfRecordFromDictionary(randomrecord);
//		boolean exist = assignedFrames.contains(randomFrame);
//		LogUltility.log(test, logger, "Does it include the frame: " + exist);
//		assertTrue(exist);
		
		// Get the frames values
		List<String> originList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frames);
		boolean found = originList.contains(randomFrame);
		//boolean found = searchValue.equals(originList.get(0));
		LogUltility.log(test, logger, "Is the searched word found " + found);
		assertTrue(found);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--8:Verify that get only the match results - "Not Contain" search criteris
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_8_Verify_that_get_only_the_match_results_Not_Contain_search_criteris() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_8", "Verify that get only the match results - \"Not Contain\" search criteris");
		
		// Get records from the dictionary file
		CurrentPage = GetInstance(DictionaryTab.class);
		List<String> recordsFromFile = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "not contain" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "not contain");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do not contain searched word: " + equalResult);
		assertFalse(equalResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--7:Verify that get only the match results - "Equal" search criteris
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_7_Verify_that_get_only_the_match_results_Equal_search_criteris() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_7", "Verify that get only the match results - \"Equal\" search criteris");
		
		// Get records from the dictionary file
		CurrentPage = GetInstance(DictionaryTab.class);
		List<String> recordsFromFile = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).window_too_many_records_ok.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");

			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomrecord.toLowerCase())) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + equalResult);
		assertTrue(equalResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--2:Verify that get only the match results
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_2_Verify_that_get_only_the_match_results() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_2", "Verify that get only the match results");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String doesNotExistRecord = CurrentPage.As(DictionaryTab.class).RandomString(10);
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(doesNotExistRecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + doesNotExistRecord);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).window_too_many_records_ok.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check there are no retrieved records
		boolean result = recordsList.isEmpty();
		LogUltility.log(test, logger, "Is record list empty: " + result);
		assertTrue(result);
				
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--67:Verify user can filter the search of �Origin�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_67_Verify_user_can_filter_the_search_of_Origin() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_67", "Verify user can filter the search of �Origin�");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "origin" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "origin");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Choose a value from the third dropdown
		String searchValue = "French";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, searchValue);
		LogUltility.log(test, logger, "Chosen search value: " + searchValue);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).window_too_many_records_ok.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Only take the first 40 results in order to click on them
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		
		// Check there are no retrieved records
		boolean result = recordsList.isEmpty();
		LogUltility.log(test, logger, "Is record list empty: " + result);
		assertFalse(result);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chose value from the origin dropdown
		List<String> originList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpOrigin);
		boolean found = searchValue.equals(originList.get(0));
		LogUltility.log(test, logger, "Is the searched word found " + found);
		assertTrue(found);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--23: Left ARROW Check the two arrows in the footer
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_23_Left_ARROW_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	{
	
	test = extent.createTest("Dictionary Search Test - DIC_TC_23", "LEFT ARROW Check the two arrows in the footer");
	  
	  // Focus on dictionary tab
	  CurrentPage = GetInstance(DictionaryTab.class);
	 // CurrentPage.As(DictionaryTab.class).tabDictionary.click();
	  CommonFunctions.clickTabs("Dictionary");
	  
	  // Choose a value from Volume[POS] dropdown
	  String volumePOS = "adadjective";
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
	  LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
	  
	  // Retrieve records
	  List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
	  LogUltility.log(test, logger, "Records retreived: " + recordsList);
	  
	  // Choose random record from the middle of the displayed list
	  recordsList = recordsList.subList(2, 10 > recordsList.size() ? recordsList.size() : 10);

	  // Pick a random record
	  Random randomizer = new Random();
	  int recordIndex = randomizer.nextInt(recordsList.size());
	  String randomrecord = recordsList.get(recordIndex);
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
	  LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	  
	  // Get index of the chosen record from the array
	  LogUltility.log(test, logger, "Index of the chosen record: " + recordIndex);
	  
	  // Click the Right arrow
	  CurrentPage.As(DictionaryTab.class).Previous_button.click();
	  
	  // Get the chosen value from the records list
	  List<String> chosenRecords = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records);
	  LogUltility.log(test, logger, "the chosen value from the records list: " + chosenRecords);
	  
	  // Check that the retreived record after clicking the arrow is indeed the previous one in the list
	  String nextRecord = recordsList.get(--recordIndex);
	  LogUltility.log(test, logger, "The chosen record after press the Previous arrow: " + nextRecord);
	  
	  boolean found = chosenRecords.get(0).equals(nextRecord);
	  LogUltility.log(test, logger, "Does the Previous arrow moved to the previous record: " + found);
	  LogUltility.log(test, logger, "the chosen record after pressing previous arrow: "+nextRecord + " ,is the same as the record index: " + recordIndex);
	  assertTrue(found);
	    
	  LogUltility.log(test, logger, "Test Case PASSED");
	
	
	}
	
	/**
	  * DIC_TC--23: RIGHT ARROW Check the two arrows in the footer
	  * @throws InterruptedException 
	  * @throws FileNotFoundException 
	  */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void DIC_TC_23_RIGHT_ARROW_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	 {
	  test = extent.createTest("Dictionary Search Test - DIC_TC_23", "RIGHT ARROW Check the two arrows in the footer");

	  
		
		// Focus on dictionary tab
	  CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
	  CommonFunctions.clickTabs("Dictionary");
	  
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(1, 10 > recordsList.size() ? recordsList.size() : 10);

	  // Pick a random record
	  Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	  
	  // Get index of the chosen record from the array
	  int recordIndex = recordsList.indexOf(randomrecord);
	  LogUltility.log(test, logger, "record index : " + recordIndex);
	  
	  // Click the Right arrow
	  CurrentPage.As(DictionaryTab.class).bt_next.click();
	  
	  // Get the chosen value from the records list
	  List<String> chosenRecords = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records);
	  LogUltility.log(test, logger, "Choosen value after press the next button: " + chosenRecords);
	  // Check that the retreived record after clicking the arrow is indeed the next one in the list
	  String nextRecord = recordsList.get(++recordIndex);
	  LogUltility.log(test, logger, "Choosen record after pressing the next button : " + nextRecord);
	  int recordIndex2 = recordsList.indexOf(nextRecord);
	  LogUltility.log(test, logger, "record index after press next button : " + recordIndex2);
	  
	  boolean found = chosenRecords.get(0).equals(nextRecord);
	  LogUltility.log(test, logger, "Does the next arrow moved to the next record: " + found);
	  assertTrue(found);
	    
	  LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	  * DIC_TC--69 : Verify user can filter the search of �Lexical Relations"
	  * @throws InterruptedException 
	  * @throws FileNotFoundException 
	  */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void DIC_TC_69_Verify_user_can_filter_the_search_of_Lexical_Relations() throws InterruptedException, FileNotFoundException
	 {
	  test = extent.createTest("Dictionary Search Test - DIC_TC_69", "Verify user can filter the search of �Lexical Relations");
	  
	  // Focus on dictionary tab
	  CurrentPage = GetInstance(DictionaryTab.class);
	  //CurrentPage.As(DictionaryTab.class).tabDictionary.click();
	  CommonFunctions.clickTabs("Dictionary");
	  
	  // Choose "Lexical Relations" from the first search dropdown
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "lexical relations");
	  LogUltility.log(test, logger, "Lexical Relations is chosen in the first search dropdown");
	  
	  // Choose "contains" from the condition dropdown
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
	  LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	  
	  //Type in the search field  
		String randomLexicalRelation = null;
		if (Setting.Language.equals("EN"))
			randomLexicalRelation = "micro";
		else if (Setting.Language.equals("HE"))
			randomLexicalRelation = "נִבְעַת";
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomLexicalRelation);
		LogUltility.log(test, logger, "word is typed in the search field: "+ randomLexicalRelation);
	  
	  
	  
//	  String randomLexicalRelation = "ocean";
//	  //CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomLexicalRelation);
//	  CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomLexicalRelation);
//	  LogUltility.log(test, logger, "chosen search value: " + randomLexicalRelation);
	  
	  // Click the Retrieve button
	  CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
	  //Thread.sleep(1000);
	  //CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
	  LogUltility.log(test, logger, "Click the Retreive button");
	  
	  //Thread.sleep(2000);
	  // Retrieve records
	  List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
	  
	  // Check that retrieved records do contain the searched record
	  if (recordsList.isEmpty()) assertTrue(false);
	  
	  // Choose a random record from the record list
	  // Pick a random record
	  Random randomizer = new Random();
	  String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	  
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
	  LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	  
	  // Get the Lexical relations values
	  List<String> lexicalRelationsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_Lexical_relations);
	  
	  // Check that retrieved records do contain the searched record
	  if (lexicalRelationsList.isEmpty()) assertTrue(false);
	  
	  // Check that retrieved records do contain the searched record
	  if (lexicalRelationsList.isEmpty()) assertTrue(false);
	  
	  boolean containResult = true;
	  for (String s : lexicalRelationsList) {
	         if(s.contains(randomLexicalRelation)) continue;
	          else {
	           containResult = false;
	             break;
	          }
	      }
	  LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
	  assertTrue(containResult);
	  
	  LogUltility.log(test, logger, "Test Case PASSED"); 
	 }
	
	/**
	 * DIC_TC--23: LEFT ARROW Check the two arrows in the footer
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_23_LEFT_ARROW_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_23", "LEFT ARROW Check the two arrows in the footer");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(1, 10 > recordsList.size() ? recordsList.size() : 10);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get index of the chosen record from the array
		int recordIndex = recordsList.indexOf(randomrecord);
		LogUltility.log(test, logger, "Chosen record index: " + recordIndex);
		
//		int recordIndex1 = recordsList.indexOf("EN{d}awful~0");
//		LogUltility.log(test, logger, "Chosen record index:EN{d}awful~0: " + recordIndex1);
//		LogUltility.log(test, logger, "Direct index:EN{d}awful~0: " + recordsList.indexOf("EN{d}awful~0"));
//		
//		int recordIndex2 = recordsList.indexOf("EN{d}bit~0");
//		LogUltility.log(test, logger, "Chosen record index:EN{d}bit~0: " + recordIndex2);
//		
//		LogUltility.log(test, logger, "List Size: " + recordsList.size());
//		
//		for (String record : recordsList) {
//			LogUltility.log(test, logger, "record: " + record + ", index: " + recordsList.indexOf(record));
//		}
		
		// Click the Left arrow
		CurrentPage.As(DictionaryTab.class).bt_prev.click();
		
		// Get the chosen value from the records list
		List<String> chosenRecords = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that the retreived record after clicking the arrow is indeed the next one in the list
		String nextRecord = recordsList.get(--recordIndex);
		
		boolean found = chosenRecords.get(0).equals(nextRecord);
		LogUltility.log(test, logger, "Does the next arrow moved to the previous record: " + found);
		assertTrue(found);
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--70:Verify user can filter the search of �Modified on�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_70_Verify_user_can_filter_the_search_of_Modified_on() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_70", "Verify user can filter the search of �Modified on�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Modified on" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "modified on");
		LogUltility.log(test, logger, "Modified on is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		  //Type in the search field  
			String randomModifiedOn = null;
			if (Setting.Language.equals("EN"))
				randomModifiedOn = "26/2018";
			else if (Setting.Language.equals("HE"))
				randomModifiedOn = "3/12/2018";
		
		
		
		//String randomModifiedOn = "3/12/2018";
		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomLexicalRelation);
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomModifiedOn);
		LogUltility.log(test, logger, "chosen search value: " + randomModifiedOn);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the modified_on value
//		List<String> modifiedOnList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).modified_on);
		String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		
		//verify that the the modified on match the serached one
		boolean sameModifiedOn = modifiedOnList.contains(randomModifiedOn);
		assertTrue(sameModifiedOn);
		
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched modified On: " + sameModifiedOn);
		assertTrue(sameModifiedOn);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--71:Verify user can filter the search of �Semantic Fields�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_71_Verify_user_can_filter_the_search_of_Semantic_Fields() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_71", "Verify user can filter the search of �Semantic Fields�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "semantic fields" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "semantic fields");
		LogUltility.log(test, logger, "Lexical fields is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomLexicalField = "banking";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomLexicalField);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomLexicalField);
		LogUltility.log(test, logger, "chosen search value: " + randomLexicalField);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen lexical fields list value
		List<String> chosenlexicalFieldsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_Lexical_Fields);
		LogUltility.log(test, logger, "chosen list: " + chosenlexicalFieldsList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		//verify that the the Lexical field match the searched one
		boolean sameLexicalField = chosenlexicalFieldsList.contains(randomLexicalField);
		assertTrue(sameLexicalField);
		
		LogUltility.log(test, logger, "Check that the retrieved lexical fields list do include the searched lexical field: " + sameLexicalField);
		assertTrue(sameLexicalField);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--72:Verify user can filter the search of �Register�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_72_Verify_user_can_filter_the_search_of_Register() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_72", "Verify user can filter the search of �Register�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Register" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "Register");
		LogUltility.log(test, logger, "Register is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomRegister = "slang";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomRegister);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomRegister);
		LogUltility.log(test, logger, "chosen search value: " + randomRegister);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Register list value
		List<String> chosenRegisterList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_Regester);
		LogUltility.log(test, logger, "chosen list: " + chosenRegisterList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		//verify that the Register match the searched one
		boolean sameRegister = chosenRegisterList.contains(randomRegister);
		assertTrue(sameRegister);
		
		LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched Register: " + sameRegister);
		assertTrue(sameRegister);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--73:Verify user can filter the search of �Acronym�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_73_Verify_user_can_filter_the_search_of_Acronym() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_73", "Verify user can filter the search of �Acronym�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Acronym" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "acronym");
		LogUltility.log(test, logger, "Acronym is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomAcronym = "fusion";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomAcronym);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomAcronym);
		LogUltility.log(test, logger, "chosen search value: " + randomAcronym);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Acronym list value
		List<String> chosenAcronymList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_Acronym);
		LogUltility.log(test, logger, "chosen list: " + chosenAcronymList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		//verify that the Acronym match the searched one
		boolean sameAcronym = chosenAcronymList.contains(randomAcronym);
		assertTrue(sameAcronym);
		
		LogUltility.log(test, logger, "Check that the retrieved Acronym list do include the searched Acronym: " + sameAcronym);
		assertTrue(sameAcronym);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--65:Verify user can filter the search of �Semantic Groups"
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_65_Verify_user_can_filter_the_search_of_Semantic_Groups() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_65", "Verify user can filter the search of �Semantic Groups�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Semantic Groups" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "semantic groups");
		LogUltility.log(test, logger, "semantic groups is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomSemanticGroups = "v desire";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomSemanticGroups);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomSemanticGroups);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic groups list value
		List<String> chosenSemanticGroupsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosenSemanticGroupsList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
		LogUltility.log(test, logger, "Check that the retrieved Semantic groups list do include the searched Semantic groups: " + randomSemanticGroups1);
		
		//verify that the Register match the searched one
		boolean sameSemanticGroups = chosenSemanticGroupsList.contains(randomSemanticGroups1);
		assertTrue(sameSemanticGroups);
		
		LogUltility.log(test, logger, "Check that the retrieved Semantic Groups list do include the searched Semantic Groups: " + sameSemanticGroups);
		assertTrue(sameSemanticGroups);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--161:Verify user can filter records with Language filter
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_161_Verify_user_can_filter_records_with_Language_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_161", "Verify user can filter records with Language filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "language" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "language");
		LogUltility.log(test, logger, "Language is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		//Type in the search field  
				String randomLanguage = null;
				if (Setting.Language.equals("EN"))
					randomLanguage = "Yiddish";
				else if (Setting.Language.equals("HE"))
					randomLanguage = "Hebrew";
//				CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomLanguage);
//				LogUltility.log(test, logger, "word is typed in the search field: "+ randomLanguage);
		
				//Select Value from the dropdown 
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomLanguage);
				//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
				LogUltility.log(test, logger, "chosen search value: " + randomLanguage);
		
//		String randomLanguage = "Hebrew";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomLanguage);
//		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
//		LogUltility.log(test, logger, "chosen search value: " + randomLanguage);
		
//		// Click the Retrieve button
//		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//		//Thread.sleep(1000);
//		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
				
//      Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");		
		
		
		// check if the "too many results" popup appear 
		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen language list value
		List<String> chosenLanguage = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_Language);
		LogUltility.log(test, logger, "chosen list: " + chosenLanguage);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//		LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
		
		//verify that the language  match the searched one
		boolean sameLanguage = chosenLanguage.contains(randomLanguage);
		assertTrue(sameLanguage);
		
		LogUltility.log(test, logger, "Check that the retrieved Language list do include the searched Language: " + sameLanguage);
		assertTrue(sameLanguage);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--162:Verify user can filter records with POS filter
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_162_Verify_user_can_filter_records_with_POS_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_162", "Verify user can filter records with POS filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "POS" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "pos");
		LogUltility.log(test, logger, "POS is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomPOS = "conjunction";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomPOS);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomPOS);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen POS list value
		List<String> chosenPOS = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_POS);
		LogUltility.log(test, logger, "chosen list: " + chosenPOS);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//		LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
		
		//verify that the POS match the searched one
		boolean samePOS = chosenPOS.contains(randomPOS);
		assertTrue(samePOS);
		
		LogUltility.log(test, logger, "Check that the retrieved POS list do include the searched POS: " + samePOS);
		assertTrue(samePOS);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--164:Verify user can filter records with Frequency  filter
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_164_Verify_user_can_filter_records_with_Frequency_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_164", "Verify user can filter records with Frequency filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Frequency" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "frequency");
		LogUltility.log(test, logger, "Status is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomFrequency = "very common";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomFrequency);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomFrequency);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Frequency list value
		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//		LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
		
		//verify that the Frequency match the searched one
		boolean sameFrequency = chosenFrequency.contains(randomFrequency);
		assertTrue(sameFrequency);
		
		LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency: " + sameFrequency);
		assertTrue(sameFrequency);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--165:Verify user can filter records with Definition filter
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_165_Verify_user_can_filter_records_with_Definition_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_165", "Verify user can filter records with Definition filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Definition" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "definition");
		LogUltility.log(test, logger, "Definition is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		//Type in the search field  
		String randomDefinition = null;
		if (Setting.Language.equals("EN"))
			randomDefinition = "school";
		else if (Setting.Language.equals("HE"))
			randomDefinition = "עצם";
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomDefinition);
		LogUltility.log(test, logger, "word is typed in the search field: "+ randomDefinition);
		
		
		
		
//		String randomDefinition = "עצם";
//		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDefinition);
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomDefinition);
//		LogUltility.log(test, logger, "chosen search value: " + randomDefinition);
//		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
//		// Get the chosen Definition list value
//		List<String> chosenDefinition = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_Definition);
//		LogUltility.log(test, logger, "chosen list: " + chosenDefinition);
		String chosenDefinition = CurrentPage.As(DictionaryTab.class).lst_Definition.getText();
		LogUltility.log(test, logger, "Text from the Definition field: " + chosenDefinition);
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//		LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
		
		//verify that the Frequency match the searched one
		boolean sameDefinition = chosenDefinition.contains(randomDefinition);
		assertTrue(sameDefinition);
		LogUltility.log(test, logger, "the retrieved Definition text: "+chosenDefinition + " ,include the searched Definition: " + randomDefinition);
		LogUltility.log(test, logger, "Check that the retrieved Definitionlist do include the searched Definition: " + sameDefinition);
		assertTrue(sameDefinition);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--166:Verify user can filter the search of �Semantic Relation"
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_166_Verify_user_can_filter_the_search_of_Semantic_Relation() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_166", "Verify user can filter the search of �Semantic Relation�");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Semantic Relation" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "semantic relations");
		LogUltility.log(test, logger, "semantic Relation is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		//Type in the search field  
		String randomSemanticRelation = null;
		if (Setting.Language.equals("EN"))
			randomSemanticRelation = "truant";
		else if (Setting.Language.equals("HE"))
			randomSemanticRelation = "מַטְבֵּעַ";
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticRelation);
		LogUltility.log(test, logger, "word is typed in the search field: "+ randomSemanticRelation);
		
		
//		String randomSemanticRelation = "מַטְבֵּעַ";
//		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomSemanticRelation);
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticRelation);
//		LogUltility.log(test, logger, "chosen search value: " + randomSemanticRelation);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Relation list value
		List<String> chosenSemanticRelationList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "chosen semantic relation list: " + chosenSemanticRelationList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticRelation1 = "desire";
		//String[] randomSemanticRelation1 = randomSemanticRelation.split(" ");
		//String randomSemanticRelation1 = randomSemanticRelation.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved Semantic Relation list do include the searched Semantic Relation: " + randomSemanticRelation1);
		
		//verify that the Semantic Relation match the searched one
		//boolean sameSemanticRelation = chosenSemanticRelationList.contains(randomSemanticRelation);
		//assertTrue(sameSemanticRelation);
		boolean sameSemanticRelation = false;
		for (String value : chosenSemanticRelationList) {
			 sameSemanticRelation = value.contains(randomSemanticRelation);
			 if (sameSemanticRelation) break;
		}
		LogUltility.log(test, logger, "the retrieved Semantic Relation List: "+chosenSemanticRelationList + " ,include the searched Semantic Relation: " + randomSemanticRelation);
		LogUltility.log(test, logger, "Check that the retrieved Semantic Relation list do include the searched Semantic Relation: " + sameSemanticRelation);
		assertTrue(sameSemanticRelation);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--167:Verify user can filter records with version filter
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_167_Verify_user_can_filter_records_with_Version_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_167", "Verify user can filter records with version filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "version" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "version");
		LogUltility.log(test, logger, "version is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomVersion = "2";
		
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
		LogUltility.log(test, logger, "chosen search value: " + randomVersion);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen version list value
	     String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();

		//verify that the version match the searched one
	    boolean sameversion = versionValue.equalsIgnoreCase(randomVersion);
		LogUltility.log(test, logger, "the retrieved version List: "+versionValue + " ,include the searched version: " + randomVersion);
		LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + sameversion);
		assertTrue(sameversion);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--168:Verify user can filter records with style filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_168_Verify_user_can_filter_records_with_style_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_168", "Verify user can filter records with style filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "style" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "style");
		LogUltility.log(test, logger, "Style is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomStyle = "neutral";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomStyle);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
		LogUltility.log(test, logger, "chosen search value: " + randomStyle);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Check if the "too many results" popup appear
		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Style list values
		List<String> chosenStyleList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Style_DD);
		LogUltility.log(test, logger, "chosen Style list: " + chosenStyleList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the Style match the searched one
	    // boolean sameStyle = versionValue.equalsIgnoreCase(randomStyle);
	     
	     boolean sameStyle = chosenStyleList.contains(randomStyle);
			assertTrue(sameStyle);
	     
		LogUltility.log(test, logger, "the retrieved Style List: "+chosenStyleList + " ,include the searched Style: " + randomStyle);
		LogUltility.log(test, logger, "Check that the retrieved Style list do include the searched Style: " + sameStyle);
		assertTrue(sameStyle);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	

	/**
	 * DIC_TC--169:Verify user can filter records with Definiteness filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_169_Verify_user_can_filter_records_with_Definiteness_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_169", "Verify user can filter records with Definiteness filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Definiteness" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "definiteness");
		LogUltility.log(test, logger, "Definiteness is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomDefiniteness = null;
		if (Setting.Language.equals("EN"))
			randomDefiniteness = "only definite";
		else if (Setting.Language.equals("HE"))
			randomDefiniteness = "only indefinite";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomDefiniteness);
//		LogUltility.log(test, logger, "word is typed in the search field: "+ randomDefiniteness);
		//Select Value from the dropdown 
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDefiniteness);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomDefiniteness);
	
		
		
		
//		String randomDefiniteness = "only indefinite";
////		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDefiniteness);
//		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
//		LogUltility.log(test, logger, "chosen search value: " + randomDefiniteness);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Definiteness list values
		List<String> chosenDefinitenessList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Definiteness_DD);
		LogUltility.log(test, logger, "chosen Definiteness list: " + chosenDefinitenessList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the Definiteness match the searched one
	     
	     boolean sameDefiniteness = chosenDefinitenessList.contains(randomDefiniteness);
			assertTrue(sameDefiniteness);
	     
		LogUltility.log(test, logger, "the retrieved Style List: "+chosenDefinitenessList + " ,include the searched Definiteness: " + randomDefiniteness);
		LogUltility.log(test, logger, "Check that the retrieved Definiteness list do include the searched Definiteness: " + sameDefiniteness);
		assertTrue(sameDefiniteness);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	
	/**
	 * DIC_TC--170:Verify user can filter records with Declinability filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_170_Verify_user_can_filter_records_with_Declinability_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_170", "Verify user can filter records with Declinability filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Declinability" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "declinability");
		LogUltility.log(test, logger, "Declinability is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomDeclinability = null;
		if (Setting.Language.equals("EN"))
			randomDeclinability = "unknown";
		else if (Setting.Language.equals("HE"))
			randomDeclinability = "plural only";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomDeclinability);
//		LogUltility.log(test, logger, "word is typed in the search field: "+ randomDeclinability);
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDeclinability);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomDeclinability);
		
		
		
		
//		String randomDeclinability = "plural only";
////		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDeclinability);
//		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
//		LogUltility.log(test, logger, "chosen search value: " + randomDeclinability);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Declinability list values
		List<String> chosenDeclinabilityList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Declinability_DD);
		LogUltility.log(test, logger, "chosen Declinability list: " + chosenDeclinabilityList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the Declinability match the searched one
	     
	     boolean sameDeclinability = chosenDeclinabilityList.contains(randomDeclinability);
			assertTrue(sameDeclinability);
	     
		LogUltility.log(test, logger, "the retrieved Declinability List: "+chosenDeclinabilityList + " ,include the searched Declinability: " + randomDeclinability);
		LogUltility.log(test, logger, "Check that the retrieved Declinability list do include the searched Declinability: " + sameDeclinability);
		assertTrue(sameDeclinability);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--171:Verify user can filter records with Dependency  filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_171_Verify_user_can_filter_records_with_Dependency_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_171", "Verify user can filter records with Dependency  filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Dependency " from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "dependency");
		LogUltility.log(test, logger, "Dependency  is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomDependency = "attached from";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomDependency);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
		LogUltility.log(test, logger, "chosen search value: " + randomDependency);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Dependency list values
		List<String> chosenDependencyList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Dependency_DD);
		LogUltility.log(test, logger, "chosen Dependency list: " + chosenDependencyList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the Dependency match the searched one
	     
	     boolean sameDependency = chosenDependencyList.contains(randomDependency);
			assertTrue(sameDependency);
	     
		LogUltility.log(test, logger, "the retrieved Dependency List: "+chosenDependencyList + " ,include the searched Dependency: " + randomDependency);
		LogUltility.log(test, logger, "Check that the retrieved Dependency list do include the searched Dependency: " + sameDependency);
		assertTrue(sameDependency);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	

	/**
	 * DIC_TC--172:Verify user can filter records with Number filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_172_Verify_user_can_filter_records_with_Number_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_172", "Verify user can filter records with Number filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Number" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "number");
		LogUltility.log(test, logger, "Number is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomNumber = "plural";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomNumber);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
		LogUltility.log(test, logger, "chosen search value: " + randomNumber);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Number list values
		List<String> chosenNumberList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Number_DD);
		LogUltility.log(test, logger, "chosen Number list: " + chosenNumberList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the number match the searched one
	     boolean sameNumber = chosenNumberList.contains(randomNumber);
			assertTrue(sameNumber);
	     
		LogUltility.log(test, logger, "the retrieved number List: "+chosenNumberList + " ,include the searched number: " + randomNumber);
		LogUltility.log(test, logger, "Check that the retrieved number list do include the searched number: " + sameNumber);
		assertTrue(sameNumber);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--173:Verify user can filter records with Gender filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_173_Verify_user_can_filter_records_with_Gender_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Search Test - DIC_TC_173", "Verify user can filter records with Gender filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose "Gender" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "gender");
		LogUltility.log(test, logger, "Gender is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomGender = "male";
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomGender);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
		LogUltility.log(test, logger, "chosen search value: " + randomGender);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Gender list values
		List<String> chosenGenderList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Gender_DD);
		LogUltility.log(test, logger, "chosen Gender list: " + chosenGenderList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
		
		

		//Remove the first letter for the random searched value  before compare results
		//String randomversion1 = "desire";
		//String[] randomversion1 = randomversion.split(" ");
		//String randomversion1 = randomversion.substring(2);
		//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
		
		//verify that the Gender match the searched one
	     
	     boolean sameGender = chosenGenderList.contains(randomGender);
			assertTrue(sameGender);
	     
		LogUltility.log(test, logger, "the retrieved Gender List: "+chosenGenderList + " ,include the searched Gender: " + randomGender);
		LogUltility.log(test, logger, "Check that the retrieved Gender list do include the searched Gender: " + sameGender);
		assertTrue(sameGender);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	

	/**
		 * DIC_TC--174:Verify user can filter records with Etymology filter 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_174_Verify_user_can_filter_records_with_Etymology_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Dictionary Search Test - DIC_TC_174", "Verify user can filter records with Etymology filter");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(DictionaryTab.class);
			//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
			CommonFunctions.clickTabs("Dictionary");
			
			// Choose "Etymology" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "etymology");
			LogUltility.log(test, logger, "Etymology is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			//Select Value from the dropdown 
			String randomEtymology = null;
			if (Setting.Language.equals("EN"))
				randomEtymology = "male";
			else if (Setting.Language.equals("HE"))
				randomEtymology = "פרטי";
			CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomEtymology);
			LogUltility.log(test, logger, "word is typed in the search field: "+ randomEtymology);
			
			
			
			
//			String randomEtymology = "פרטי";
//			CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//			//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomEtymology);
//			//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//			CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomEtymology);
//			LogUltility.log(test, logger, "chosen search value: " + randomEtymology);
			
			// Click the Retrieve button
			CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the Etymology text field values
			//List<String> chosenEtymologyList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).Etymology_DD);
			//LogUltility.log(test, logger, "chosen Etymology list: " + chosenEtymologyList);
			String etymologValue = CurrentPage.As(DictionaryTab.class). Etymology_Text.getText();
		//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
			
			

			//Remove the first letter for the random searched value  before compare results
			//String randomversion1 = "desire";
			//String[] randomversion1 = randomversion.split(" ");
			//String randomversion1 = randomversion.substring(2);
			//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
			
			//verify that the Etymology match the searched one
		     
		     boolean sameEtymology = etymologValue.contains(randomEtymology);
				assertTrue(sameEtymology);
		     
			LogUltility.log(test, logger, "the retrieved Etymology List: "+etymologValue + " ,include the searched Etymology: " + randomEtymology);
			LogUltility.log(test, logger, "Check that the retrieved Etymology list do include the searched Etymology: " + sameEtymology);
			assertTrue(sameEtymology);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
	
		/**
			 * DIC_TC--175:Verify user can filter records with Forms filter 
			 * @throws InterruptedException 
			 * @throws FileNotFoundException 
			 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_175_Verify_user_can_filter_records_with_Forms_filter() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Dictionary Search Test - DIC_TC_175", "Verify user can filter records with Forms filter");
				
				// Focus on dictionary tab
				CurrentPage = GetInstance(DictionaryTab.class);
				//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
				CommonFunctions.clickTabs("Dictionary");
				
				// Choose "Forms" from the first search dropdown
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "forms");
				LogUltility.log(test, logger, "Forms is chosen in the first search dropdown");
				
				// Choose "has key" from the condition dropdown
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "has key");
				LogUltility.log(test, logger, " has key is chosen in the condition dropdown");
				
				//Select Value from the dropdown 
				String randomForms = "infinitive";
				//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomForms);
				//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
				//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomVersion);
				LogUltility.log(test, logger, "chosen search value: " + randomForms);
				
				// Click the Retrieve button
				CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
				LogUltility.log(test, logger, "Click the Retreive button");
				
//				boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//				if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
				
				// Retrieve records
				List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
				recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
				// Check that retrieved records do contain the searched record
				if (recordsList.isEmpty()) assertTrue(false);
				
				// Choose a random record from the record list
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
				
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// Get the Forms list values
				List<String> chosenFormsList = CurrentPage.As(DictionaryTab.class). getValuesFromApp (CurrentPage.As(DictionaryTab.class). Forms_list);
				LogUltility.log(test, logger, "chosen Forms list: " + chosenFormsList);
				//String EtymologValue = CurrentPage.As(DictionaryTab.class). Forms_Text.getText();
			//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
				
				

				//Remove the first letter for the random searched value  before compare results
				//String randomversion1 = "desire";
				//String[] randomversion1 = randomversion.split(" ");
				//String randomversion1 = randomversion.substring(2);
				//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
				
				//verify that the Forms match the searched one
			     
			     boolean sameForms = chosenFormsList.contains(randomForms);
					assertTrue(sameForms);
			     
				LogUltility.log(test, logger, "the retrieved Forms List: "+chosenFormsList + " ,include the searched Forms: " + randomForms);
				LogUltility.log(test, logger, "Check that the retrieved Forms list do include the searched Forms: " + sameForms);
				assertTrue(sameForms);
				
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
			
			
			/**
				 * DIC_TC--177:Verify user can filter records with Spellings filter 
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
				public void DIC_TC_177_Verify_user_can_filter_records_with_Spellings_filter() throws InterruptedException, FileNotFoundException
				{
					test = extent.createTest("Dictionary Search Test - DIC_TC_177", "Verify user can filter records with Spellings filter");
					
					// Focus on dictionary tab
					CurrentPage = GetInstance(DictionaryTab.class);
					//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
					CommonFunctions.clickTabs("Dictionary");
					
					// Choose "Spellings" from the first search dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "spellings");
					LogUltility.log(test, logger, "Spellings is chosen in the first search dropdown");
					
					// Choose "contains" from the condition dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
					LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
					
					//Select Value from the dropdown
					//Select Value from the dropdown 
					String randomSpellings = null;
					if (Setting.Language.equals("EN"))
						randomSpellings = "male";
					else if (Setting.Language.equals("HE"))
						randomSpellings = "ה";
					CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSpellings);
					LogUltility.log(test, logger, "word is typed in the search field: "+ randomSpellings);
					
					
					
					
//					String randomSpellings = "ה";
//					//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//					//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomSpellings);
//					CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//					CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSpellings);
//					LogUltility.log(test, logger, "chosen search value: " + randomSpellings);
					
					// Click the Retrieve button
					CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
					LogUltility.log(test, logger, "Click the Retreive button");
					
					// Retrieve records
					List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
					recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
					// Check that retrieved records do contain the searched record
					if (recordsList.isEmpty()) assertTrue(false);
					
					// Choose a random record from the record list
					// Pick a random record
					Random randomizer = new Random();
//					String randomrecord = recordsList.get(3);
					String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
					
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
					LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
					
					// Get the Spellings list values
					List<String> chosenSpellingsList = CurrentPage.As(DictionaryTab.class). getValuesFromApp (CurrentPage.As(DictionaryTab.class). Spellings_list);
					LogUltility.log(test, logger, "chosen Spellings list: " + chosenSpellingsList);



					 // Check that we have attach and attached to record names in the spellings list
					  List<String> spellingList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).Spellings_list);

					  List<String> spellings = new ArrayList<String>() ;
					  //remove the values and keep only the records
					  for (String value : spellingList) {
					   String[] split = value.split("~");
					   spellings.add(split[1]);
					  }
					  LogUltility.log(test, logger, "Values in spelling list: " + spellings);
					  
					  //remove the values and keep only the records
					  List<String> cleanSpellings = new ArrayList<String>();
					  for (String value : spellings) {
						  if (value.contains(")"))
					   {String[] split = value.split("\\(");
					   cleanSpellings.add(split[0]);}
						  else
							  cleanSpellings.add(value);
						  
					  }
				     
				     boolean sameSpellings = false;
				     for(int i=0;i<cleanSpellings.size();i++)
				    	 if(cleanSpellings.get(i).contains(randomSpellings))
				    		 sameSpellings = true;
						assertTrue(sameSpellings);
				     
					LogUltility.log(test, logger, "the retrieved Spellings List: "+cleanSpellings + " ,include the searched Spellings: " + randomSpellings);
					LogUltility.log(test, logger, "Check that the retrieved Spellings list do include the searched Spellings: " + sameSpellings);
					assertTrue(sameSpellings);
					
					LogUltility.log(test, logger, "Test Case PASSED");	
				}
				
				
				/**
				 * DIC_TC--178:Check volume[POS] dropdown list
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
				public void DIC_TC_178_Check_volume_POS_dropdown_list() throws InterruptedException, FileNotFoundException
				{
					test = extent.createTest("Dictionary Search Test - DIC_TC_178", "Check volume[POS] dropdown list");
					
					// Focus on dictionary tab
					CurrentPage = GetInstance(DictionaryTab.class);
					//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
					CommonFunctions.clickTabs("Dictionary");
					
					//Select Value from the Volume POS dropdown 
					String randomPOS = "conjunction";
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, randomPOS);
					//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
					LogUltility.log(test, logger, "chosen search value: " + randomPOS);
					
					// Retrieve records
					List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
					
					// Check that retrieved records do contain the searched record
					if (recordsList.isEmpty()) assertTrue(false);
					
					// Choose a random record from the record list
					// Pick a random record
					Random randomizer = new Random();
					String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
					
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
					LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
					
					// Get the chosen POS list value
					List<String> chosenPOS = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_POS);
					LogUltility.log(test, logger, "chosen list: " + chosenPOS);

					//verify that the POS match the searched one
					boolean samePOS = chosenPOS.contains(randomPOS);
					assertTrue(samePOS);
					LogUltility.log(test, logger, "the retrieved POS List: "+chosenPOS + " ,include the searched Spellings: " + randomPOS);
					LogUltility.log(test, logger, "Check that the retrieved POS list do include the searched POS: " + samePOS);
					assertTrue(samePOS);
					
					LogUltility.log(test, logger, "Test Case PASSED");	
				}

				
//				/**
//				 * DIC_TC--526:Check retrieving an empty search users get popup 
//				 * @throws InterruptedException 
//				 * @throws FileNotFoundException 
//				 */
//				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
//				public void DIC_TC_526_Check_retrieving_an_empty_search_users_get_popup() throws InterruptedException, FileNotFoundException
//				{
//					test = extent.createTest("Dictionary Search Test - DIC_TC_526", "Check retrieving an empty search users get popup");
//					
//					// Focus on dictionary tab
//					CurrentPage = GetInstance(DictionaryTab.class);
//					//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
//					CommonFunctions.clickTabs("Dictionary");
//					
//					// Verify that the search text field is empty
//					
//					boolean fieldIsEmpty = CurrentPage.As(DictionaryTab.class).cmpCondition_value.getText().isEmpty();
//					 if (fieldIsEmpty){
//						CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//						LogUltility.log(test, logger, "Click the Retreive button since field is empty");}
//					
//					    else 
//					        {
//					    	CurrentPage.As(DictionaryTab.class).cmpCondition_value.clear();
//					    	// Click the Retrieve button
//							CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//							LogUltility.log(test, logger, "Click the Retreive button after clear text field");
//					        }
//					
//					// Verify that the Empty value popup displayed
//					 boolean  emptyPopup = CurrentPage.As(DictionaryTab.class).Empty_Popup.isDisplayed();
//					 assertTrue(emptyPopup);	 
//							 			     
//					LogUltility.log(test, logger, "Empty value popu message appear: " + emptyPopup);
//					assertTrue(emptyPopup);
//					
//					//Get message text
//					String empty_text = CurrentPage.As(DictionaryTab.class).Empty_Popup_text.getAttribute("Name");
//					LogUltility.log(test, logger, "Empty value popu message text: " + empty_text);
//					assertEquals(empty_text, "Value is empty! Please enter value");
//					
//					// Press Ok 
//					CurrentPage.As(DictionaryTab.class).btnOk.click();
//					LogUltility.log(test, logger, "Press the Ok button");
//					
//					LogUltility.log(test, logger, "Test Case PASSED");	
//				}
//				
				
				/**
				 * DIC_TC--550:Check the user can select the first record after executing a search 
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
				public void DIC_TC_550_Check_the_user_can_select_the_first_record_after_executing_a_search() throws InterruptedException, FileNotFoundException
				{
					test = extent.createTest("Dictionary Search Test - DIC_TC_550", "Check the user can select the first record after executing a search");

					// Focus on dictionary tab
					CurrentPage = GetInstance(DictionaryTab.class);
					//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
					CommonFunctions.clickTabs("Dictionary");
					
					// Choose "Form" from the first search dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
					LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
					
					// Choose "contains" from the condition dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
					LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
					
					//Select Value from the dropdown 
					//Select Value from the dropdown 
					String randomForm = null;
					if (Setting.Language.equals("EN"))
						randomForm = "male";
					else if (Setting.Language.equals("HE"))
						randomForm = "הוּא";
					CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomForm);
					LogUltility.log(test, logger, "word is typed in the search field: "+ randomForm);
					
					
//					String randomForm = "הוא";
//					//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//					//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomForms);
//					//CurrentPage.As(DictionaryTab.class).cmpCondition_value.click();
//					CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomForm);
//					LogUltility.log(test, logger, "chosen search value: " + randomForm);
					
					// Click the Retrieve button
					CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
					LogUltility.log(test, logger, "Click the Retreive button");
					
//					boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//					if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).window_too_many_records_ok.click();
					
					// Retrieve records
					List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
					recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
					// Check that retrieved records do contain the searched record
					if (recordsList.isEmpty()) assertTrue(false);
					
					// Choose the first record from the record list
					//Random randomizer = new Random();
					//String randomrecord = recordsList.get(0);
					
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, recordsList.get(0));
					
					  List<String> spellings = new ArrayList<String>() ;
					  //remove the values and keep only the records
					  for (String value : recordsList) {
					   String[] split = value.split("~");
					   spellings.add(split[1]);
					  }
					
					LogUltility.log(test, logger, "Choose from record list: " + recordsList.get(0));
					
					// Get the Key Form field value
//					List<String> chosenFormsList = CurrentPage.As(DictionaryTab.class). getValuesFromApp (CurrentPage.As(DictionaryTab.class). Forms_list);
//					LogUltility.log(test, logger, "chosen Forms list: " + chosenFormsList);
					String formValue = CurrentPage.As(DictionaryTab.class). txtKeyForm.getText();
				//  String versionValue = CurrentPage.As(DictionaryTab.class).Field_Version.getText();
					LogUltility.log(test, logger, "the From the Key Form field is: "+formValue );
					
					//verify that the Forms match the searched one
					
//					String recordsList.get(0) = recordsList.get(0).split("~");
////					  List<String> spellings = new ArrayList<String>() ;
////					  //remove the values and keep only the records
////					  for (String value : spellingList) {
////					   String[] split = value.split("~");
////					   spellings.add(split[1]);
////					  }
//				     
				     boolean sameForms = formValue.equals(randomForm);
						assertTrue(sameForms);
				     
					LogUltility.log(test, logger, "the retrieved Forms List: "+formValue + " ,include the searched Forms: " + randomForm);
					LogUltility.log(test, logger, "Check that the retrieved Forms list do include the searched Forms: " + sameForms);
					assertTrue(sameForms);
					
					LogUltility.log(test, logger, "Test Case PASSED");	
				}
				
				/**
				 * DIC_TC--432:Click on empty record box should not give exception
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
				public void DIC_TC_432_Click_on_empty_record_box_should_not_give_exception() throws InterruptedException, FileNotFoundException
				{
					test = extent.createTest("Dictionary Search Test - DIC_TC_432", "Click on empty record box should not give exception");
					
					// Focus on dictionary tab
					CurrentPage = GetInstance(DictionaryTab.class);
				//	CurrentPage.As(DictionaryTab.class).tabDictionary.click();
					CommonFunctions.clickTabs("Dictionary");
					
					// Choose "form" from the first search dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
					LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
					// Choose "equal" from the condition dropdown
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
					LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
					
					// Change the last filter to "all records", in case it was changed by other TC
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_all_next, "all records");
					LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
							
					// Type in the search field
					CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys("asdfasdf");
					LogUltility.log(test, logger, "word is typed in the search field: " + "asdfasdf");
					
					// Click the Retrieve button
					CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
					LogUltility.log(test, logger, "Click the Retreive button");
					
					// Get retrieved records amount
					String amount = CurrentPage.As(DictionaryTab.class).ll_dictionary_retrieved_records.getAttribute("Name");
					LogUltility.log(test, logger, "Records amount: " + amount);
					
					String[] amountarr = amount.split(":");
					// Check the retrieved amount is 0
					boolean result = amountarr[1].contains("0");
					assertTrue(result);
					
					
//					// Retrieve records
//					List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
//					LogUltility.log(test, logger, "Records retreived: " + recordsList);
//					
//					// Check that records box is empty
//					boolean isEmpty = recordsList.isEmpty();
//					LogUltility.log(test, logger, "is records box empty: " + isEmpty);
//					assertTrue(isEmpty);
//					
//					// Click the empty records box
					
					
					LogUltility.log(test, logger, "Test Case PASSED");	
				}
				

				/**
				 * DIC_TC--3: Verify get results when Search exist words
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
				@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
				public void DIC_TC_3_Verify_get_results_when_Search_exist_words() throws InterruptedException, FileNotFoundException
				{
					test = extent.createTest("Dictionary Search Test - DIC_TC_3", "Verify get results when Search exist words");
					
					// Focus on dictionary tab
					CurrentPage = GetInstance(DictionaryTab.class);
					//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
					CommonFunctions.clickTabs("Dictionary");
					
					// Get available dictionary records from file 
					List<String> dictionaryRecords = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
					Random random = new Random();
					
					// Search for random record 
					String searchRecord = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));	
					CurrentPage.As(DictionaryTab.class).searchInDictionary(test, logger, "form", "contains", searchRecord, "all records");
					
					// Retrieved records
					List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
					LogUltility.log(test, logger, "Retrieved records: "+recordsList);	
					
					// Check if the searched record found in each retrieved result
					boolean found = true;
					for(int i=0;i<recordsList.size();i++)
						if(!recordsList.get(i).contains(searchRecord))
						{
							found = false;break;
						}
					
					LogUltility.log(test, logger, "The word exists in each retrieved record: "+found);	
					assertTrue(found);
					
					LogUltility.log(test, logger, "Test Case PASSED");	
				}
				
				

				@AfterMethod(alwaysRun = true)
			    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
			    {		
					
					int testcaseID = 0;
					if (enableReportTestlink == true)
						testcaseID=rpTestLink.GetTestCaseIDByName("Dictionary/Search",method.getName());
					
//					System.out.println("tryCount: " + tryCount);
//					System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
				
					if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
							&& Integer.parseInt(Setting.RetryFailed)!=0) {
						extent.removeTest(test);
						
				        // Close popups to get back to clean app
						 System.out.println("Test Case Failed");
						 CurrentPage = GetInstance(DictionaryTab.class);
						 if(Setting.closeEveryWindow.equals("true"))
							 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
					}
					else if(result.getStatus() == ITestResult.FAILURE)
					    {
						 	tryCount = 0;
					        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
					        test.fail(result.getThrowable());
					        String screenShotPath = Screenshot.captureScreenShot();
					        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
					        // Send result to testlink
					        if (enableReportTestlink == true){
					        	try {
					        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
					            	LogUltility.log(test, logger, "Report to testtlink: " + response);
								} catch (Exception e) {
									LogUltility.log(test, logger, "Testlink Error: " + e);
								}
					        	}
					        
					        // Close popups to get back to clean app
//					        if (Integer.parseInt(Setting.RetryFailed) != 0) {
					         System.out.println("Test Case Failed");
							 CurrentPage = GetInstance(DictionaryTab.class);
							 if(Setting.closeEveryWindow.equals("true"))
								 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//					        }
					    }
					    else if(result.getStatus() == ITestResult.SUCCESS)
					    {
					    	tryCount = 0;
					        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
					        // Send result to testlink
					        if (enableReportTestlink == true){
					        	try {
					        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
					            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					    		} catch (Exception e) {
					    			LogUltility.log(test, logger, "Testlink Error: " + e);
					    		}
					        	
					        	}
					    }
					    else
					    {
					        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
					        test.skip(result.getThrowable());
					    }
					
					    extent.flush();
					    
						// Count how many a test was processed
						tryCount++;
						}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}

}
