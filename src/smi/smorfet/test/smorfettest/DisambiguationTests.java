package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;
//import com.sun.jna.platform.win32.OaIdl.VARKIND;
//import com.sun.jna.platform.win32.WinUser.INPUT;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.DisambiguationTab;
import smi.smorfet.test.pages.LexiconTab;


/**
 * 
 * All tests for Disambiguation 
 *
 */
public class DisambiguationTests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Disambiguation tab");
	
		// Logger
		logger = LogManager.getLogger(DisambiguationTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Disambiguation tab");
		logger.info("Disambiguation tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(DisambiguationTab.class);
//		CurrentPage.As(DisambiguationTab.class).Start_Window.click();
//		CurrentPage.As(DisambiguationTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(DisambiguationTab.class);
//			CurrentPage.As(DisambiguationTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Disambiguation");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * DIC_TC--188:Verify user gets popup when all the weight fields are zero
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_188_Verify_user_gets_popup_when_all_the_weight_fields_are_zero() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--188", "Verify user gets popup when all the weight fields are zero");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "nabil", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Click the None button
		DisambiguationTab.btnNone.click();
		LogUltility.log(test, logger, "Click the None button");
		
		// Click the Update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click the Update button");
		
		// Verify that popup is displayed
		boolean result = DisambiguationTab.popupWindowMessage(test, logger, "Error in recalculation", "The sum of the weights needs to be a positive number!");
		LogUltility.log(test, logger, "Popup has been displayed: "+result);
		assertTrue(result);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	

	/**
	 * DIC_TC--50:verify default value for Threshold 90
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_50_verify_default_value_for_Threshold_90() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--50", "Verify default value for Threshold 90");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "merry", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = 	CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Generate number larger than 90    
		int thresholdValue = randomizer.nextInt(90)+90;

		// Set field value
		DisambiguationTab.setElementValue(DisambiguationTab.thresholdInput, thresholdValue);
		LogUltility.log(test, logger, "Insert new Threshold value: "+thresholdValue);
		
		// Click the Update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click the Update button");
		
		// Get field value after updating
		double thresholdValueAfterUpdate = DisambiguationTab.getElementValue(DisambiguationTab.thresholdInput);
		LogUltility.log(test, logger, "The Threshold value after updating: "+thresholdValueAfterUpdate);
	    
	    // It should be 90
	    boolean is90 = thresholdValueAfterUpdate == 90 ;
	    LogUltility.log(test, logger, "The Threshold value is set to 90:"+is90);
	    assertTrue(is90);


		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--34:Check the displayed sentence from corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_34_Check_the_displayed_sentence_from_corpus() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--34", "Check the displayed sentence from corpus");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);

		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "test", "all records");
		
		// Retrieve sentences
		List<String> sentencesList =  CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
	
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence from Corpus tab: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Get the displayed sentence
		String disSentence = DisambiguationTab.getDisplayedSentence();
		LogUltility.log(test, logger, "Displayed sentence in Disambiguation tab: "+disSentence);
		
		// Check if it is the same sentence
		randomSentence = randomSentence.substring(6);
		boolean same = disSentence.trim().equals(randomSentence);
		LogUltility.log(test, logger, "The chosen sentence in corpus is displayed in disambiguation: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--40:Check the footer arrows in disambiguation
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_40_Check_the_footer_arrows_in_disambiguation() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--40", "Check the footer arrows in disambiguation");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "test" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains",searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		int index = randomizer.nextInt(sentencesList.size());
		String randomSentence = sentencesList.get(index);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence from Corpus tab: " + randomSentence);
				
		// Get the previous and next sentences
		// If the first sentence were chosen, then the previous should be the last sentence in the list
		String prevSentence = index == 0 ? sentencesList.get(sentencesList.size()-1) :sentencesList.get(index-1);
		LogUltility.log(test, logger, "Previous sentence is: " + prevSentence);
		// If the last sentence were chosen, then the next should be the first sentence in the list
		String nextSentence = index == sentencesList.size()-1 ? sentencesList.get(0) : sentencesList.get(index+1);
		LogUltility.log(test, logger, "Next sentence is: " + nextSentence);
			
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Get the displayed sentence
		String disSentence = DisambiguationTab.getDisplayedSentence();
		LogUltility.log(test, logger, "Displayed sentence in Disambiguation tab: "+disSentence);
		
		// Check if it is the same sentence
		randomSentence = randomSentence.substring(6);
		boolean same = disSentence.trim().equals(randomSentence);
		LogUltility.log(test, logger, "The chosen sentence in corpus is displayed in disambiguation: "+same);
		assertTrue(same);
		
		// Click the previous button
		DisambiguationTab.bt_prev.click();
		LogUltility.log(test, logger, "Click the previous button");
		
		// Get the sentence
		String prevDisSentence = DisambiguationTab.getDisplayedSentence();
		LogUltility.log(test, logger, "Displayed sentence in Disambiguation tab: "+prevDisSentence);
		
		// Check if it is the same previous sentence
		prevSentence = prevSentence.substring(6);
		boolean samePrev = prevDisSentence.trim().equals(prevSentence);
		LogUltility.log(test, logger, "The chosen sentence in corpus is displayed in disambiguation: "+samePrev);
		assertTrue(samePrev);
		
		// Click the next button twice button
		DisambiguationTab.bt_next.click();
		DisambiguationTab.bt_next.click();
		LogUltility.log(test, logger, "Click the next button twice");
					
		// Get the sentence
		String nextDisSentence = DisambiguationTab.getDisplayedSentence();
		LogUltility.log(test, logger, "Displayed sentence in Disambiguation tab: "+nextDisSentence);
		
		// Check if it is the same next sentence
		nextSentence = nextSentence.substring(6);
		boolean sameNext = nextDisSentence.trim().equals(nextSentence);
		LogUltility.log(test, logger, "The chosen sentence in corpus is displayed in disambiguation: "+sameNext);
		assertTrue(sameNext);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--455:Check the edit word in corpus appears in disambiguation tab
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_455_Check_the_edit_word_in_corpus_appears_in_disambiguation_tab() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--455", "Check the edit word in corpus appears in disambiguation tab");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "test" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains",searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = 	CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence from Corpus tab: " + randomSentence);
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		
		// Choose random word 
		int	wordNumber = randomizer.nextInt(wordsInRecord.size());
		String	randomWord = wordsInRecord.get(wordNumber);
		// Click on word
		CorpusTab.clickSentencesRecord(randomWord, wordNumber);
		LogUltility.log(test, logger, "Click on: "+randomWord);
		
		// Click on the word's end
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Edit the word using backspace	
		LogUltility.log(test, logger, "Add text to the word");
		CorpusTab.keyType(test, logger, CorpusTab.RandomString(2));
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");	
		
		// Get sentence after updating
		randomSentence = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records).get(0);
		LogUltility.log(test, logger, "Sentence after updating: "+randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Get the displayed sentence
		String disSentence = DisambiguationTab.getDisplayedSentence();
		LogUltility.log(test, logger, "Displayed sentence in Disambiguation tab: "+disSentence);
		
		// Check if it is the same sentence
		randomSentence = randomSentence.substring(6);
		boolean same = disSentence.trim().equals(randomSentence);
		LogUltility.log(test, logger, "The chosen sentence in corpus is displayed in disambiguation: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--35: Check Values range in settings - part 1 positive bound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_35_Check_Values_range_in_settings_part1_positive_bound() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--35", " Check Values range in settings - part 1 positive bound ");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "merry", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Generate number larger than 100    
		int value = randomizer.nextInt(90)+100;
		LogUltility.log(test, logger, "Set field values to number greater than 100: "+value);
		
		// Set the value to all fields
		DisambiguationTab.setElementValue(DisambiguationTab.best_next_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.best_next_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_negative_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_positive_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.compound_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.compound_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n1score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n2score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_nearscore, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p1score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p2score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_samescore, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.context_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.context_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.spelling_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.spelling_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_weight, value);
		
		// get the value from all fields
		double best_next_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.best_next_score);
		double best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
		double proximity_negative_score_value = DisambiguationTab.getElementValue(DisambiguationTab.proximity_negative_score);
		double proximity_positive_score_value = DisambiguationTab.getElementValue(DisambiguationTab.proximity_positive_score);
		double proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
		double compound_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.compound_score);
		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
		double grammar_n1score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_n1score);
		double grammar_n2score_value = DisambiguationTab.getElementValue(DisambiguationTab.grammar_n2score);
		double grammar_nearscore_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_nearscore);
		double grammar_p1score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_p1score);
		double grammar_p2score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_p2score);
		double grammar_samescore_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_samescore);
		double grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
		double context_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_score);
		double context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
		double spelling_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_score);
		double spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
		double frequency_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_score);
		double frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		
		// Check that all field is set to the maxium value = 100
		boolean is100 = best_next_score_value == 100 && best_next_weight_value == 100 && proximity_negative_score_value == 100 && proximity_positive_score_value == 100
				&& proximity_weight_value == 100 && compound_score_value == 100 && compound_weight_value == 100 && grammar_n1score_value == 100 && grammar_n2score_value == 100
				&& grammar_nearscore_value == 100 && grammar_p1score_value == 100 && grammar_p2score_value == 100 && grammar_samescore_value == 100 && grammar_weight_value == 100
				&& context_score_value == 100 && context_weight_value == 100 && spelling_score_value == 100 && spelling_weight_value == 100 
				&& frequency_score_value == 100 && frequency_weight_value == 100;
		LogUltility.log(test, logger, "All values has been set to the default maximum value 100: "+is100);
		assertTrue(is100);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--35: Check Values range in settings - part 2 negative bound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_35_Check_Values_range_in_settings_part2_negative_bound() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--35", " Check Values range in settings - part 2 negative bound");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "merry", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Generate negative number   
		int value = randomizer.nextInt(90)-100;
		LogUltility.log(test, logger, "Set field values to negative number: "+value);
		
		// Set the value to all fields
		DisambiguationTab.setElementValue(DisambiguationTab.best_next_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.best_next_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_negative_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_positive_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.compound_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.compound_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n1score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n2score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_nearscore, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p1score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p2score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_samescore, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.context_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.context_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.spelling_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.spelling_weight, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_weight, value);
		
		// get the value from all fields
		double best_next_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.best_next_score);
		double best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
		double proximity_negative_score_value = DisambiguationTab.getElementValue(DisambiguationTab.proximity_negative_score);
		double proximity_positive_score_value = DisambiguationTab.getElementValue(DisambiguationTab.proximity_positive_score);
		double proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
		double compound_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.compound_score);
		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
		double grammar_n1score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_n1score);
		double grammar_n2score_value = DisambiguationTab.getElementValue(DisambiguationTab.grammar_n2score);
		double grammar_nearscore_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_nearscore);
		double grammar_p1score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_p1score);
		double grammar_p2score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_p2score);
		double grammar_samescore_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_samescore);
		double grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
		double context_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_score);
		double context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
		double spelling_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_score);
		double spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
		double frequency_score_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_score);
		double frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		
		// Check that all field is set to the minimum value = 0
		boolean isZero = best_next_score_value == 0 && best_next_weight_value == 0 && proximity_negative_score_value == 0 && proximity_positive_score_value == 0
				&& proximity_weight_value == 0 && compound_score_value == 0 && compound_weight_value == 0 && grammar_n1score_value == 0 && grammar_n2score_value == 0
				&& grammar_nearscore_value == 0 && grammar_p1score_value == 0 && grammar_p2score_value == 0 && grammar_samescore_value == 0 && grammar_weight_value == 0
				&& context_score_value == 0 && context_weight_value == 0 && spelling_score_value == 0 && spelling_weight_value == 0 
				&& frequency_score_value == 0 && frequency_weight_value == 0;
		LogUltility.log(test, logger, "All values has been set to the default minimum value 0: "+isZero);
		assertTrue(isZero);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--36:Verify weight fields calculations
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_36_Verify_weight_fields_calculations() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--36", "Verify weight fields calculations");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Generate random positive number
		int[] positiveValues = new int[4];
		for(int i=0;i<positiveValues.length;i++)
			positiveValues[i] = randomizer.nextInt(20);
		
		// Set the value to all weight fields
		
		DisambiguationTab.setElementValue(DisambiguationTab.ft_proximity, positiveValues[3]);
		LogUltility.log(test, logger, "Set frame ft_proximity to: "+positiveValues[3]);
		
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_field_proximity, positiveValues[0]);
		LogUltility.log(test, logger, "Set semantic field proximity to: "+positiveValues[0]);
		
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_group_proximity, positiveValues[1]);
		LogUltility.log(test, logger, "Set semantic group proximity to: "+positiveValues[1]);
		
		DisambiguationTab.setElementValue(DisambiguationTab.frame_proximity, positiveValues[2]);
		LogUltility.log(test, logger, "Set frame proximity to: "+positiveValues[2]);

//		V1.17
//		DisambiguationTab.setElementValue(DisambiguationTab.best_next_weight, positiveValues[0]);
//		LogUltility.log(test, logger, "Set best next weight to: "+positiveValues[0]);
//			
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_weight, positiveValues[1]);
//		LogUltility.log(test, logger, "Set proximity weight to: "+positiveValues[1]);	
//
//		DisambiguationTab.setElementValue(DisambiguationTab.compound_weight, positiveValues[2]);
//		LogUltility.log(test, logger, "Set compound weight to: "+positiveValues[2]);		
//	
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_weight, positiveValues[3]);
//		LogUltility.log(test, logger, "Set grammar weight to: "+positiveValues[3]);	
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.context_weight, positiveValues[4]);
//		LogUltility.log(test, logger, "Set context weight to: "+positiveValues[4]);
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.spelling_weight, positiveValues[5]);
//		LogUltility.log(test, logger, "Set context weight to: "+positiveValues[5]);
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.frequency_weight, positiveValues[6]);
//		LogUltility.log(test, logger, "Set frequency weight to: "+positiveValues[6]);
		
		// Click update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// get the value from all fields
		double ft_prox_value = DisambiguationTab.getElementValue(DisambiguationTab.ft_proximity);
		double semantic_field_value = DisambiguationTab.getElementValue(DisambiguationTab.semantic_field_proximity);
		double semantic_group_value = DisambiguationTab.getElementValue(DisambiguationTab.semantic_group_proximity);
		double frame_prox_value = DisambiguationTab.getElementValue(DisambiguationTab.frame_proximity);
		
//		V1.17
//		int best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
//		int proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
//		int compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
//		int grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
//		int context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
//		int spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
//		int frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		
		// Sum the weights
//		int weights_sum = best_next_weight_value+proximity_weight_value+compound_weight_value+grammar_weight_value+context_weight_value+spelling_weight_value+frequency_weight_value;
		double weights_sum = ft_prox_value+semantic_field_value+semantic_group_value+frame_prox_value;
		weights_sum = Math.floor(weights_sum);
		LogUltility.log(test, logger, "The weights field sum is: "+weights_sum);
		
		// Verify that the weights sum is less than 100
		boolean lessThan100 = weights_sum<=100;
		LogUltility.log(test, logger, "The weights sum is less than 100: "+lessThan100);
		assertTrue(lessThan100);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--42:Check the "None" button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_42_Check_the_None_button() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--42", "Check the \"None\" button");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "merry", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Click none button
		DisambiguationTab.btnNone.click();
		LogUltility.log(test, logger, "Click None button");
		
		// get the value from all weight fields
		double best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
		double proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
		double grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
		double context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
		double spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
		double frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		
		// The weight value of is set to zero
		boolean isZero = best_next_weight_value == 0 && proximity_weight_value == 0 && compound_weight_value == 0 && grammar_weight_value == 0 && context_weight_value == 0 && spelling_weight_value == 0 && frequency_weight_value == 0 ;
		LogUltility.log(test, logger, "The value of each weight is set to Zero: "+isZero);
		assertTrue(isZero);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--447:Check Score fields when recalculation
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_447_Check_Score_fields_when_recalculation() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--447", "Check Score fields when recalculation");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Set value of score field to zero
		int value = 0;
		DisambiguationTab.setElementValue(DisambiguationTab.compound_score_field, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score_field,value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_score_field,value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_score_field,value);
		
//		V1.17
//		DisambiguationTab.setElementValue(DisambiguationTab.best_next_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_negative_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_positive_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.compound_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n1score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n2score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_nearscore, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p1score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p2score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_samescore, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.context_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.spelling_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score, value);
		LogUltility.log(test, logger, "Set all score fields value to: "+value);
		
		// Click update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// get the value from all weight fields
		
		// get the value from all fields
		double ft_prox_value = DisambiguationTab.getElementValue(DisambiguationTab.ft_proximity);
		double semantic_field_value = DisambiguationTab.getElementValue(DisambiguationTab.semantic_field_proximity);
		double semantic_group_value = DisambiguationTab.getElementValue(DisambiguationTab.semantic_group_proximity);
		double frame_prox_value = DisambiguationTab.getElementValue(DisambiguationTab.frame_proximity);
				
//		V1.17
//		int best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
//		int proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
//		int compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
//		int grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
//		int context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
//		int spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
//		int frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		
		// Sum the weights
//		int weights_sum = best_next_weight_value+proximity_weight_value+compound_weight_value+grammar_weight_value+context_weight_value+spelling_weight_value+frequency_weight_value;
		double weights_sum = ft_prox_value+semantic_field_value+semantic_group_value+frame_prox_value;
		weights_sum  = Math.floor(weights_sum);
		LogUltility.log(test, logger, "The weights field sum is: "+weights_sum);
		
		// Verify that the weights sum is less than 100
		boolean lessThan100 = weights_sum<=100;
		LogUltility.log(test, logger, "The weights sum is less than 100: "+lessThan100);
		assertTrue(lessThan100);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--513:check setting all weights to 100 and all scores 0 then click updating
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_513_check_setting_all_weights_to_100_and_all_scores_0_then_click_updating() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--513", "Check setting all weights to 100 and all scores 0 then click updating");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Set value of score field to zero
		int value = 0;
//		DisambiguationTab.setElementValue(DisambiguationTab.best_next_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_negative_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_positive_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.compound_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n1score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_n2score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_nearscore, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p1score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_p2score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_samescore, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.context_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.spelling_score, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score, value);
		DisambiguationTab.setElementValue(DisambiguationTab.compound_score_field, value);
		DisambiguationTab.setElementValue(DisambiguationTab.frequency_score_field, value);
		DisambiguationTab.setElementValue(DisambiguationTab.proximity_score_field, value);
		DisambiguationTab.setElementValue(DisambiguationTab.grammar_score_field, value);

		LogUltility.log(test, logger, "Set all score fields value to: "+value);
		
		// Set value of weights to 100
		value = 100;
//		DisambiguationTab.setElementValue(DisambiguationTab.best_next_weight, value);	
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_weight, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.compound_weight, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_weight, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.context_weight, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.spelling_weight, value);
//		DisambiguationTab.setElementValue(DisambiguationTab.frequency_weight,value);
		DisambiguationTab.setElementValue(DisambiguationTab.ft_proximity,value);
		DisambiguationTab.setElementValue(DisambiguationTab.frame_proximity,value);
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_group_proximity,value);
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_field_proximity,value);
		
		LogUltility.log(test, logger, "Set all weights fields value to: "+value);
		
		// Click update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// get the value from all weight fields
//		double best_next_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.best_next_weight);
//		double proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
//		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
//		double grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
//		double context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
//		double spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
//		double frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
		double ft_prox_weight_value =  DisambiguationTab.getElementValue(DisambiguationTab.ft_proximity);
		double frame_prox_weight_value =  DisambiguationTab.getElementValue(DisambiguationTab.frame_proximity);
		double sem_group_weight_value =  DisambiguationTab.getElementValue(DisambiguationTab.semantic_group_proximity);
		double sem_filed_weight_value =  DisambiguationTab.getElementValue(DisambiguationTab.semantic_field_proximity);
		
		// Sum the weights
		double weights_sum = ft_prox_weight_value+frame_prox_weight_value+sem_group_weight_value+sem_filed_weight_value;
		weights_sum = Math.floor(weights_sum);
		LogUltility.log(test, logger, "The weights field sum is: "+weights_sum);
		
		// Verify that the weights sum is less than 100
		boolean lessThan100 = weights_sum<=100;
		LogUltility.log(test, logger, "The weights sum is less than 100: "+lessThan100);
		assertTrue(lessThan100);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--262:Check the impact of the weight field
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_262_Check_the_impact_of_the_weight_field() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		test = extent.createTest("Disambiguation Tests - DIC_TC--262", "Check the impact of the weight field");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
	
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// Set the value to all weight fields
		int value = 100;
//		DisambiguationTab.setElementValue(DisambiguationTab.proximity_weight, value);
//		LogUltility.log(test, logger, "Set proximity weight to: "+value);	
//
//		DisambiguationTab.setElementValue(DisambiguationTab.compound_weight, value);
//		LogUltility.log(test, logger, "Set compound weight to: "+value);		
//	
//		DisambiguationTab.setElementValue(DisambiguationTab.grammar_weight, value);
//		LogUltility.log(test, logger, "Set grammar weight to: "+value);	
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.context_weight, value);
//		LogUltility.log(test, logger, "Set context weight to: "+value);
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.spelling_weight, value);
//		LogUltility.log(test, logger, "Set context weight to: "+value);
//		
//		DisambiguationTab.setElementValue(DisambiguationTab.frequency_weight, value);
//		LogUltility.log(test, logger, "Set frequency weight to: "+value);
		
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_field_proximity, value);
		LogUltility.log(test, logger, "Set semantic field weight to: "+value);	
		
		DisambiguationTab.setElementValue(DisambiguationTab.semantic_group_proximity, value);
		LogUltility.log(test, logger, "Set semantic group weight to: "+value);	
		
		DisambiguationTab.setElementValue(DisambiguationTab.frame_proximity, value);
		LogUltility.log(test, logger, "Set frame proximity weight to: "+value);	
		
		DisambiguationTab.setElementValue(DisambiguationTab.ft_proximity, value);
		LogUltility.log(test, logger, "Set ft proximity weight to: "+value);

		// Click update button
		DisambiguationTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// get the value from all fields
//		double proximity_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.proximity_weight);
//		LogUltility.log(test, logger, "Get proximity weight value: "+proximity_weight_value);	
//		
//		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_weight);
//		LogUltility.log(test, logger, "Get compound weight value: "+compound_weight_value);		
//		
//		double grammar_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.grammar_weight);
//		LogUltility.log(test, logger, "Get grammar weight value: "+grammar_weight_value);		
//		
//		double context_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.context_weight);
//		LogUltility.log(test, logger, "Get context weight value: "+context_weight_value);	
//		
//		double spelling_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.spelling_weight);
//		LogUltility.log(test, logger, "Get spelling weight value: "+spelling_weight_value);	
//		
//		double frequency_weight_value = 	DisambiguationTab.getElementValue(DisambiguationTab.frequency_weight);
//		LogUltility.log(test, logger, "Get frequency weight value: "+frequency_weight_value);	
		
		double compound_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.compound_score_field);
		LogUltility.log(test, logger, "Get compound weight value: "+compound_weight_value);	
		
		double freq_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.frequency_score_field);
		LogUltility.log(test, logger, "Get frequency weight value: "+freq_weight_value);	
		
		double prox_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.proximity_score_field);
		LogUltility.log(test, logger, "Get proximity weight value: "+prox_weight_value);	
		
		double grammar_weight_value = DisambiguationTab.getElementValue(DisambiguationTab.grammar_score_field);
		LogUltility.log(test, logger, "Get grammar weight value: "+grammar_weight_value);	
		
		// Click previous and next buttons to sync
		DisambiguationTab.bt_next.click();
		DisambiguationTab.bt_prev.click();
		LogUltility.log(test, logger, "Click next + previous to sync changes");
		
		// Get the rules popup info
		HashMap<String, List<Double>> popupInfo = DisambiguationTab.getRulesPopupInfo();
		LogUltility.log(test, logger, "The popup info: "+popupInfo);
		
		// Check that the displayed weight info in popup matches the input fields value
//		int proximity_weightPopUp = popupInfo.get("Proximity").get(1);
//		int compound_weightPopUp = popupInfo.get("Compound").get(1);
//		int grammar_weightPopUp = popupInfo.get("Grammar").get(1);
//		int context_weightPopUp = popupInfo.get("Context").get(1);
//		int spelling_weightPopUp =  popupInfo.get("Spelling").get(1);
//		int frequency_weightPopup =popupInfo.get("Frequency").get(1);
		
		double compound_score = popupInfo.get("compound").get(1);
		double frequency_score = popupInfo.get("frequency").get(1);
		double proximity_score = popupInfo.get("proximity").get(1);
		double grammar_score = popupInfo.get("grammar").get(1);
		
		// Compare values
//		boolean equalValue = proximity_weight_value == proximity_weightPopUp;
//		LogUltility.log(test, logger, "The proximity weight are equal: "+equalValue);
//		assertTrue(equalValue);
//		
//		equalValue = compound_weight_value == compound_weightPopUp;
//		LogUltility.log(test, logger, "The compound weight are equal: "+equalValue);
//		assertTrue(equalValue);
//		
//		equalValue = grammar_weight_value == grammar_weightPopUp;
//		LogUltility.log(test, logger, "The grammar weight are equal: "+equalValue);
//		assertTrue(equalValue);
//		
//		equalValue = context_weight_value == context_weightPopUp;
//		LogUltility.log(test, logger, "The context weight are equal: "+equalValue);
//		assertTrue(equalValue);
//		
//		equalValue = spelling_weight_value == spelling_weightPopUp;
//		LogUltility.log(test, logger, "The spelling weight are equal: "+equalValue);
//		assertTrue(equalValue);
//		
//		equalValue = frequency_weight_value == frequency_weightPopup;
//		LogUltility.log(test, logger, "The frequency weight are equal: "+equalValue);
//		assertTrue(equalValue);
		
		boolean equalValue = compound_weight_value == compound_score;
		LogUltility.log(test, logger, "The compound is equal: "+equalValue);
		assertTrue(equalValue);
		
		equalValue = freq_weight_value == frequency_score;
		LogUltility.log(test, logger, "The frequency is equal: "+equalValue);
		assertTrue(equalValue);
		
		equalValue = prox_weight_value == proximity_score;
		LogUltility.log(test, logger, "The proximity is equal: "+equalValue);
		assertTrue(equalValue);
		
		equalValue = grammar_weight_value == grammar_score;
		LogUltility.log(test, logger, "The proximity is equal: "+equalValue);
		assertTrue(equalValue);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--189:Verify user gets info for each records from Dictionary in the Disambiguation
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_189_Verify_user_gets_info_for_each_records_from_Dictionary_in_the_Disambiguation() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		// FOR NOW CHECK ONLY THE FIRST WORD SINCE HORIZONTAL SCROLL ISN'T IMPLEMENTED IN DISAMBIGUATION TAB
		test = extent.createTest("Disambiguation Tests - DIC_TC--189", "Verify user gets info for each records from Dictionary in the Disambiguation");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Initialize dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Get sentence words
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click record to get the tagging option
		String record = recordsList.get(0);
		int recordNumber = 0;
		CorpusTab.clickSentencesRecord(record, recordNumber);
		LogUltility.log(test, logger, "Click on: "+record);
		
		// Get the currently chosen tag
		List<String> chosenTag = CorpusTab.getRecordTaggingFromCorpusFile(randomSentence, record);
		LogUltility.log(test, logger, "Current tagging: " + chosenTag);
		
		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
		int optionIndex = taggingOptions.indexOf(chosenTag);
		LogUltility.log(test, logger, "Get tagging option index: " + optionIndex );
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		 // Get tagging option popup info from disambiguation tab
		 HashMap<String, String> recordInfo = DisambiguationTab.getWordTaggingOptionInfo(record,recordNumber, optionIndex);
		 LogUltility.log(test, logger, "Selected tagging option from disambiguation tab: "+recordInfo);
		 
		 // Compare disambiguation and corpus info: POS,form type and key name
		 boolean sameKey = recordInfo.get("Key").contains(chosenTag.get(0));
		 LogUltility.log(test, logger, "Same Key: "+sameKey);
		 assertTrue(sameKey);
		
		 boolean samePOS = DictionaryTab.POSConverter(recordInfo.get("POS")).equals(chosenTag.get(1));
		 LogUltility.log(test, logger, "Same POS: "+samePOS);
		 assertTrue(samePOS);
		
		 boolean sameFormType = recordInfo.get("Form Type").equals(chosenTag.get(2));
		 LogUltility.log(test, logger, "Same Form Type: "+sameFormType);
		 assertTrue(sameFormType);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--187:Verify the record versions in the Disambiguation are the same as in the lexicon
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_187_Verify_the_record_versions_in_the_Disambiguation_are_the_same_as_in_the_lexicon() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		// FOR NOW CHECK ONLY THE FIRST WORD SINCE HORIZONTAL SCROLL ISN'T IMPLEMENTED IN DISAMBIGUATION TAB
		test = extent.createTest("Disambiguation Tests - DIC_TC--187", "Verify the record versions in the Disambiguation are the same as in the lexicon");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Initialize lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
//		String randomSentence = " 3909 בקשתך בנושא הסכמה לביטול שיעבוד פוליסת ביטוח החיים התקבלה ונמצאת בבדיקה .";
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Get sentence words
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click record to get the tagging option
		String record = recordsList.get(0);
		int recordNumber = 0;
		CorpusTab.clickSentencesRecord(record, recordNumber);
		LogUltility.log(test, logger, "Click on: "+record);
		
		// For hebrew
		if(Setting.Language.equals("HE"))
		{	
			// Click on the first part of the word, the part before the pipe
			WebElement place = CorpusTab.sentencesRecordElement(record, recordNumber);		
		 	String attachPlace = place.getAttribute("ClickablePoint");
			String[] XY = attachPlace.split(",");

			Robot robot = new Robot();
		    int XPoint = 0, YPoint = 0;
			
			XPoint = Integer.parseInt(XY[0])+20;
		    YPoint = Integer.parseInt(XY[1]);

	        robot.mouseMove(XPoint, YPoint);
	        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            
			// Eliminate the pipes from Hebrew word
			record = record.replace(" | ", "");
		}
		
		// Get the currently chosen tag
		List<String> chosenTag = CorpusTab.getRecordTaggingFromCorpusFile(randomSentence, record);
		LogUltility.log(test, logger, "Current tagging: " + chosenTag);
		
		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
		int optionIndex = taggingOptions.indexOf(chosenTag);
		LogUltility.log(test, logger, "Get tagging option index: " + optionIndex );
		
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		 // Get tagging option popup info from disambiguation tab
		 HashMap<String, String> recordInfo = DisambiguationTab.getWordTaggingOptionInfo(record,recordNumber, optionIndex);
		 LogUltility.log(test, logger, "Selected tagging option from disambiguation tab: "+recordInfo);
		 
		 // Navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
			
		// Search for the record
		LexiconTab.searchInLexicon(test, logger, "form","equal", chosenTag.get(0),"all records");
		
		// Get list values and choose one
		List<String> lexiconRecordsList = LexiconTab.getValuesFromApp(LexiconTab.lst_records);
		String lexiconRecord = lexiconRecordsList.get(randomizer.nextInt(lexiconRecordsList.size()));
		LexiconTab.chooseValueInDropdown(LexiconTab.lst_records, lexiconRecord);
		LogUltility.log(test, logger, "Choose record:"+ lexiconRecord);
		
		// Get the chosen value info
		 ArrayList<HashMap<String, String>> lexicalDisplayList = LexiconTab.lexiconDisplayTable();
		 boolean sameKey = false;
		 boolean samePOS = false;
		 boolean sameFormType = false;
		 for(int i=0;i<lexicalDisplayList.size();i++)
		 {
			 if(lexicalDisplayList.get(i).get("Dictionary Key").equals(recordInfo.get("Key")))
			 {
				 
				 sameKey=true;
				 samePOS = lexicalDisplayList.get(i).get("POS").equals((recordInfo.get("POS")));
				 sameFormType = lexicalDisplayList.get(i).get("Form Type").equals(recordInfo.get("Form Type"));
				 LogUltility.log(test, logger, "Lexicon matching data: "+lexicalDisplayList.get(i));
			 }
		 }
		 // Compare disambiguation and lexicon info: POS,form type and key name
	
		 
		 LogUltility.log(test, logger, "Same Key: "+sameKey);
		 assertTrue(sameKey);

		 LogUltility.log(test, logger, "Same POS: "+samePOS);
		 assertTrue(samePOS);
		
		 LogUltility.log(test, logger, "Same Form Type: "+sameFormType);
		 assertTrue(sameFormType);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--38:Check that disambiguation highlights your chosen word version from corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_38_Check_that_disambiguation_highlights_your_chosen_word_version_from_corpus() throws InterruptedException, AWTException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, UnsupportedFlavorException, IOException
	{
		// FOR NOW CHECK ONLY THE FIRST WORD SINCE HORIZONTAL SCROLL ISN'T IMPLEMENTED IN DISAMBIGUATION TAB
		test = extent.createTest("Disambiguation Tests - DIC_TC--38", "Check that disambiguation highlights your chosen word version from corpus");
		
		// Initialize disambiguation tab
		CurrentPage = GetInstance(DisambiguationTab.class);
		DisambiguationTab DisambiguationTab = CurrentPage.As(DisambiguationTab.class);
		
		// Initialize corpus tab
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Navigate to corpus
		CorpusTab.tabCorpus.click();
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		// Search to work with less sentences
		String searchedWord = Setting.Language.equals("EN")? "merry" : "בדיקה";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Get sentence words
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click record to get the tagging option
		String record = recordsList.get(0);
		int recordNumber = 0;
		CorpusTab.clickSentencesRecord(record, recordNumber);
		LogUltility.log(test, logger, "Click on: "+record);
		
		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();	
		
		//  choose option from the taggingOptions
		List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		System.out.println("choose random tagging option: " + randomTaggingOption);
		int optionIndex = taggingOptions.indexOf(randomTaggingOption);
		LogUltility.log(test, logger, "Get tagging option index: " + randomTaggingOption );
		
		// Choose tagging option
		try {
			CorpusTab.chooseTaggingOption(randomTaggingOption);
			LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
		} catch (Exception e) {
			System.out.println("exception: " + e);
		}
				
	
		// Navigate to disambiguation tab
		DisambiguationTab.tabDisambiguation.click();
		LogUltility.log(test, logger, "Navigate to disambiguation tab");
		
		// FOR HEBREW
		if(Setting.Language.equals("HE"))
		{	
			String displayedSentence = DisambiguationTab.getDisplayedSentence();
			record = displayedSentence.split(" ")[0];
		}
		
		// Check that the chosen version is highlighted with green/yellow
		WebElement tagOptionElement = DisambiguationTab.getWordTaggingOptionElement(record,recordNumber, optionIndex);
		String color = CommonFunctions.getColor(tagOptionElement);
		boolean isHighlighted = color.equals("Green2") || color.equals("Yellow2");
		LogUltility.log(test, logger, "The chosen word version is highlighted: "+isHighlighted+" ,The Color is: "+color);
		assertTrue(isHighlighted);
	
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Disambiguation",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
