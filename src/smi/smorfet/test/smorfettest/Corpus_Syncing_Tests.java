package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;
import smi.smorfet.test.pages.MainDisplayWindow;

/**
 * 
 * All tests for Corpus Syncing
 *
 */
public class Corpus_Syncing_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Corpus tab - Syncing");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite = Corpus Syncing Tests - Syncing");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(CorpusTab.class);
//			CurrentPage.As(CorpusTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Corpus");
			
			// Click the reset button
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			CorpusTab.bt_reset_search.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	/**
	 * DIC_TC--645:Syncing changes in semantics\frames with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_645_Syncing_changes_in_semantics_frames_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Corpus Tests - DIC_TC--645", "Syncing changes in semantics\\frames with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
	
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word
		
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		String searchedWord = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedWord,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedWord);
			if(!continueSearch)
				break;
		}
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
   		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedWord.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedWord);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
				
		// Get current word tagging
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanKey, "all records");
		
		// choose the wanted record
		LogUltility.log(test, logger, "Choose from the list: "+searchedWord);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedWord);
		
		// Get semantic fields values and remove the chosen ones in order to choose a new value
		List<String> availableSemanticFields = DictionaryTab.getValuesFromApp(DictionaryTab.lb_dictionary_semantic_fields);
		List<String> markedSemanticFields = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields);
		for(int i=0;i<markedSemanticFields.size();i++)
			availableSemanticFields.remove(markedSemanticFields.get(i));

		// Choose semantic field value
		String chosenSF = availableSemanticFields.get(randomizer.nextInt(availableSemanticFields.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields, chosenSF);
		LogUltility.log(test, logger, "Choose semantic field value: "+chosenSF);
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close the app
		LogUltility.log(test, logger, "Close smorfet");
		CommonFunctions.close_smorfet.click();
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedWord);
		LogUltility.log(test, logger, searchedWord+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
//		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
	
		// Rechoose the sentence and word
//		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get all tagging info
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		
		// Check if semantic field exists
		boolean updatedSFSuccess = false;
		String chosenSemanticFieldsAfterChange = "";
		for(int i=0;i<recordInfo.size();i++)
		{		
			if(recordInfo.get(i).get("Dictionary Key").contains(searchedWord))
			{
				chosenSemanticFieldsAfterChange = recordInfo.get(i).get("Semantic Fields").replaceAll(" ", "_");
				chosenSemanticFieldsAfterChange =chosenSemanticFieldsAfterChange.replaceAll("&","and");
				
				LogUltility.log(test, logger, "Chosen semantic field in dictionary: "+chosenSF+" , Corpus: "+chosenSemanticFieldsAfterChange);
				if(chosenSemanticFieldsAfterChange.contains(chosenSF))
					updatedSFSuccess = true;
			}
			
		
		}
		
		LogUltility.log(test, logger, "The semantic field were updated successfully: "+updatedSFSuccess);
		assertTrue(updatedSFSuccess);
		
		// end of step 9
		
		//Step 10
		// Click save
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
			
		// Step 11
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		List<String>  getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// semantic fields from grammar indices file
		String SFfromGIfile = getGrammarIndexInfo.get(7).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The semantic fields from grammar_indices.txt: "+SFfromGIfile);
		LogUltility.log(test, logger, "The semantic fields from corpus GUI: "+chosenSemanticFieldsAfterChange);
		boolean sameGI = SFfromGIfile.equals(chosenSemanticFieldsAfterChange);
		LogUltility.log(test, logger, "The semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	

	/**
	 * DIC_TC--647:Syncing tag-replacement for deleted record with external corpus and grammar files � leave
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_647_Syncing_tag_replacement_for_deleted_record_with_external_corpus_and_grammar_files_leave_empty() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--647", "Syncing tag-replacement for deleted record with external corpus and grammar files � leave empty");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);

		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
//		// Close the app
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word
		
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		String searchedWord = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedWord,"all records");
		
//		// Choose value from the first search dropdown
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "all filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
//   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedWord);
			if(!continueSearch)
				break;
		}
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
   		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedWord.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedWord);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get current word tagging
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanKey, "all records");
		
		// choose the wanted record
		LogUltility.log(test, logger, "Choose from the list: "+searchedWord);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedWord);
		
		String fullDictionaryName = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_records).get(0);
		
		// Click the delete button
		DictionaryTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		// Click the yes button
		DictionaryTab.btnYes.click();
		
		boolean popups = true;
		while (popups) {
			try {
				DictionaryTab.replacementWindow.isDisplayed();
				//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
				//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
				// Choose leave empty and click replace
				List<String> replaceList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
				LogUltility.log(test, logger, "Choose first value: Leave empty: " + replaceList);
				CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, replaceList.get(0));
				DictionaryTab.btnpopupAcceptWindow.click();
			} catch (Exception e) {
				popups = false;
			}
		}
		
		// Click ok
		DictionaryTab.btnOk.click();
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close the app
		LogUltility.log(test, logger, "Close smorfet");
		CommonFunctions.close_smorfet.click();
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedWord);
		LogUltility.log(test, logger, searchedWord+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
				
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get info from app
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		
		// check that the tagging option was deleted
		boolean deleteSuccess = true ;
		// It should not be available/displayed
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(fullDictionaryName))
				deleteSuccess = false;
		
		LogUltility.log(test, logger, "The record tagging option was deleted successfully: "+deleteSuccess);
		assertTrue(deleteSuccess);
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}
			
		// Get the currently chosen tag
		List<String> chosenTagAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagAfter);
		boolean noTag = chosenTagAfter.isEmpty();
		LogUltility.log(test, logger, "The word is not tagged after choosing 'Leave empty' option: " + noTag);
		assertTrue(noTag);

		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Check that there is no grammar index applied (empty value).	
		boolean noGrammarIndex = grammarIndex.equals("");
		LogUltility.log(test, logger, "There is no grammar index applied: "+noGrammarIndex);
		assertTrue(noGrammarIndex);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	


	/**
	 * DIC_TC--646:Syncing tag-replacement for deleted record with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_646_Syncing_tag_replacement_for_deleted_record_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Corpus Tests - DIC_TC--646", "Syncing tag-replacement for deleted record with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word
		
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
//		String searchedKey = "EN{n}king~0";
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continuouSearch;
		String randomRecord ;
		if(Setting.Language.equals("EN"))
		{
			do {
				// Choose random record
				 randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));	
				if(randomRecord.contains("'"))
					continuouSearch = true;
				else
				{
					continuouSearch = false;
					CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
					LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
				}
		   		
			}while(continuouSearch);
		}
		else
		{
			 randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));	
			 CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
			LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		}
			
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
				wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
//		String cleanKey  = "king";
//		int wordIndex = wordsInRecordLowerCase.indexOf(cleanKey.toLowerCase());
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(),searchedKey);
		// For Hebrew if word contains a pipe
//		if(wordIndex == -1 && Setting.Language.equals("HE"))
//			for(int i=0;i<wordsInRecord.size();i++)
//				if(wordsInRecord.get(i).contains(cleanKey))
//					{wordIndex = i; break;}
		
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));

		// Get current word tagging
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal", chosenTagging.get(0), "all records");
		
		// reassembel the wanted record
		String chosenDic = Setting.Language+"{"+chosenTagging.get(1)+"}"+chosenTagging.get(0);
		LogUltility.log(test, logger, "Choose from the list: "+chosenDic);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, chosenDic);
	
		// Click the delete button
		DictionaryTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		// Click the yes button
		DictionaryTab.btnYes.click();
		
		boolean popups = true;
		List<String> replacments = new ArrayList<String>();
		while (popups) {
			try {
				DictionaryTab.replacementWindow.isDisplayed();
				//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
				//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
				// Choose leave empty and click replace
				List<String> replaceList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
				LogUltility.log(test, logger, "Replacements list: " + replaceList);
				if(replaceList.size()>1)
					replaceList.remove("Leave empty");
				String replacement = replaceList.get(randomizer.nextInt(replaceList.size()));
				replacments.add(replacement);
				LogUltility.log(test, logger, "Choose replacment: " + replacement);
				CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, replacement);
				DictionaryTab.btnpopupAcceptWindow.click();
			} catch (Exception e) {
				popups = false;
			}
		}
		
		// Click ok
		DictionaryTab.btnOk.click();
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(chosenDic);
		LogUltility.log(test, logger, chosenDic+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		
		// check that the tagging option was deleted
		boolean deleteSuccess = true ;
		// It should not be available/displayed
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				deleteSuccess = false;
		LogUltility.log(test, logger, "The deleted tagging isn't displayed: "+deleteSuccess);
		assertTrue(deleteSuccess);

		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}
		
		// Get current word  after changing tagging
		List<String> chosenTaggingAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTaggingAfter);

		// reassembel the wanted record
		String taggingAfter = Setting.Language+"{"+chosenTaggingAfter.get(1)+"}"+chosenTaggingAfter.get(0);
		boolean changedSuccess = false ;
		for(int i=0;i<replacments.size();i++)
		if(replacments.get(i).contains(taggingAfter))
			{changedSuccess = true;break;}
		LogUltility.log(test, logger, "The replacemnts was completed successfully: "+changedSuccess);
		assertTrue(changedSuccess);
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Get current word tagging
		chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Convert POS code 
		String convertRecordPos = DictionaryTab.POSCodeConverter(chosenTagging.get(1));
		LogUltility.log(test, logger, "The POS in corpus: "+convertRecordPos+" ,The POS in grammar indices: "+getGrammarIndexInfo.get(1));
		boolean sameGI = convertRecordPos.equals(getGrammarIndexInfo.get(1));
		LogUltility.log(test, logger, "The POS are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--648:Syncing tag-replacement for deleted form with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_648_Syncing_tag_replacement_for_deleted_form_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Corpus Tests - DIC_TC--648", "Syncing tag-replacement for deleted record with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
//		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpusForms();
//		List<String> dictionaryKeys2 = CorpusTab.getTaggedDictionaryKeysInCorpusForms();
		// Get dictionary keys that have more than one representation and form type
//		dictionaryKeys.retainAll(dictionaryKeys2);
		
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
//		String searchedKey = "HE{l}תא\"ל~0";
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Add filter by form 
   		// Get the dictionary keys form, and remove the default form from the list
   		List<String> getCleanRecordForms = DictionaryTab.getCleanRecordForms(searchedKey);
 		LogUltility.log(test, logger, "Get clean forms: "+getCleanRecordForms);
   		getCleanRecordForms.remove(0);
   		
//		 firstDD = "form type";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
//   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");
//
//   		// Choose value from the condition dropdown
//   		conditionDropdownValue = "equal";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
//   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");
//
//   		// Choose form
   		String chosenForm = getCleanRecordForms.get(randomizer.nextInt(getCleanRecordForms.size()));
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenForm);
// 		LogUltility.log(test, logger, "Choose form value: "+chosenForm);
//   		
//   		// Change the last filter value
// 		filterDropdownValue = "add filter";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
//   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
//   		
//   		// Click the Retrieve button
//   		CorpusTab.btnRetrieve_same.click();
//   		LogUltility.log(test, logger, "Click the Retreive button");
	
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String  randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		// Select from corpus
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get the word tagged FORM 
		
		// Get the currently chosen tag
		List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		String taggedForm="";
		if(!chosenRecord.isEmpty())
			 taggedForm = chosenRecord.get(2);	
		LogUltility.log(test, logger, "Current form: " + taggedForm);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
	
		//**************************************
		DictionaryTab.removeForm(chosenForm.replaceAll(" ", "_"));
		LogUltility.log(test, logger, "Remove form: "+chosenForm);

		// Get replacements values
		List<String> replaceList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
		LogUltility.log(test, logger, "Replacements list: " + replaceList);
//		replaceList.remove("Leave empty");
		
		// Choose random replacement
		String replacement = replaceList.get(randomizer.nextInt(replaceList.size()));
		LogUltility.log(test, logger, "Choose replacment: " + replacement);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, replacement);
		DictionaryTab.btnpopupAcceptWindow.click();
		String replacementKey ="";
		if(!replacement.equals("Leave empty"))
		{
			replacementKey = replacement.split("\\|")[0];
			replacement = replacement.split("\\|")[1];
			
		}
		
//		// Click ok
//		DictionaryTab.btnOk.click();
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// check that the replacement was added
		if(!replacement.equals("Leave empty"))
		{
			boolean replaceSuccess = false ;
			// It should not be available/displayed
			for(int i=0;i<recordInfo.size();i++)
				if(recordInfo.get(i).get("Form Type").contains(replacement)  && recordInfo.get(i).get("Dictionary Key").equals(replacementKey))
					replaceSuccess = true;
			LogUltility.log(test, logger, "The replacment form exists: "+replaceSuccess);
			assertTrue(replaceSuccess);
			}
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Get current word form type
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Get form type from the grammar index info
		LogUltility.log(test, logger, "The Form Type in corpus: "+chosenTagging.get(2)+" ,The Form Type in grammar indices: "+getGrammarIndexInfo.get(2));
		boolean sameGI = chosenTagging.get(2).equals(getGrammarIndexInfo.get(2));
		LogUltility.log(test, logger, "The Form Types are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--649:Syncing tag-replacement for deleted form with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_649_syncing_tag_replacement_for_deleted_form_with_external_corpus_and_grammar_files_leave_empty() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Corpus Tests - DIC_TC--649", "Syncing tag-replacement for deleted form with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);

		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		List<String> dictionaryKeys2 = CorpusTab.getTaggedDictionaryKeysInCorpusForms();
		// Get dictionary keys that have more than one representation and form type
		dictionaryKeys.retainAll(dictionaryKeys2);
		
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continuouSearch;
		boolean hasCompound;
		String randomRecord ;
		do {
			// Choose random record
			 randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));	
			 hasCompound = CorpusTab.containsCompound(randomRecord);
			if(randomRecord.contains("'"))
				continuouSearch = true;
			else
				continuouSearch = false;		
	   		
		}while(continuouSearch || hasCompound);
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
//		int wordIndex = wordsInRecordLowerCase.indexOf(cleanKey);
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(),searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));

//		// Get info FORMS from app
//		ArrayList<HashMap<String, String>> keyCorpusInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
//		List<String> attachedFormsToCorpus = new ArrayList<String>();
//		for(int i=0;i<keyCorpusInfo.size();i++)
//			attachedFormsToCorpus.add(keyCorpusInfo.get(i).get("Form Type"));
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
	
		// Delete attached form
//		String attForm = attachedFormsToCorpus.get(randomizer.nextInt(attachedFormsToCorpus.size()));
		String attForm = DictionaryTab.removeForm();
		LogUltility.log(test, logger, "Remove form: "+attForm);
	
		// Choose random replacement
		String replacement = "Leave empty";
		LogUltility.log(test, logger, "Choose replacment: " + replacement);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, replacement);
		DictionaryTab.btnpopupAcceptWindow.click();
		
//		// Click ok
//		DictionaryTab.btnOk.click();
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// check that the form  was deleted
		boolean deleteSuccess = false ;
		// It should not be available/displayed
		for(int i=0;i<recordInfo.size();i++)
			if(!recordInfo.get(i).get("Form Type").equals(attForm) && recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				deleteSuccess = true;
		LogUltility.log(test, logger, "The deleted form isn't displayed: "+deleteSuccess);
		assertTrue(deleteSuccess);

		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}
//
//		List<String> taggingFromFileAfterDelete = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord, wordsInRecord.get(wordIndex));
//		boolean wordNotTaggedInFile = false;
//		if(taggingFromFileAfterDelete.isEmpty())
//			wordNotTaggedInFile = true;
//		LogUltility.log(test, logger, "The word is NOT tagged: "+wordNotTaggedInFile);
//		assertTrue(wordNotTaggedInFile);
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Get current word form type
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Get form type from the grammar index info
		LogUltility.log(test, logger, "The Form Type in corpus: "+chosenTagging.get(2)+" ,The Form Type in grammar indices: "+getGrammarIndexInfo.get(2));
		boolean sameGI = chosenTagging.get(2).equals(getGrammarIndexInfo.get(2));
		LogUltility.log(test, logger, "The Form Types are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
		
	
	/**
	 * DIC_TC--650:Syncing tag-replacement for deleted spelling with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_650_Syncing_tag_replacement_for_deleted_spelling_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--650", "Syncing tag-replacement for deleted spelling with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
				
		// Close smorfet and click No
		CommonFunctions.clickTabs("Dictionary");
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> multiSpellings = DictionaryTab.getDictionaryKeysWithMultiSpellings();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		// Intersection , gets dictionary keys that are tagged in corpus that has more than one spelling
		dictionaryKeys.retainAll(multiSpellings);

		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		HashMap<String, String> infoB4Delete = DictionaryTab.getRegularRecordsInfoFromDictionary(searchedKey);
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continuouSearch;
		boolean hasCompound;
		String randomRecord ;
		do {
			// Choose random record
			 randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));	
			 hasCompound = CorpusTab.containsCompound(randomRecord);
			if(randomRecord.contains("'"))
				continuouSearch = true;
			else
				continuouSearch = false;		
	   		
		}while(continuouSearch || hasCompound);
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
//		int wordIndex = wordsInRecordLowerCase.indexOf(cleanKey);
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
	
		// Get the spelling values and the value to delete
		// the spelling value to delete equals the dictionary key name
		List<String> spellingValues = DictionaryTab.getSpellinigValues();
		String attachedSpelling="" ;
		for(int i=0;i<spellingValues.size();i++)
			if(spellingValues.get(i).contains(cleanKey))
				attachedSpelling = spellingValues.get(i);
		
		// Delete the attached spelling
		LogUltility.log(test, logger, "Remove spelling: "+attachedSpelling);
		DictionaryTab.removeSpelling(attachedSpelling);
		
		// Get replacements values
		List<String> replaceList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
		LogUltility.log(test, logger, "Replacements list: " + replaceList);
//		replaceList.remove("Leave empty");
		
		// Choose random replacement
		String replacement = replaceList.get(randomizer.nextInt(replaceList.size()));
		LogUltility.log(test, logger, "Choose replacment: " + replacement);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, replacement);
		DictionaryTab.btnpopupAcceptWindow.click();
		if(!replacement.equals("Leave empty"))
			replacement = replacement.split("\\|")[1];
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Get info from file after removing the spelling
		HashMap<String, String> infoAfterDelete = DictionaryTab.getRegularRecordsInfoFromDictionary(searchedKey);
		LogUltility.log(test, logger, "The record spelling Before delete: "+infoB4Delete.get("spellings"));
		LogUltility.log(test, logger, "The record spelling After delete: "+infoAfterDelete.get("spellings"));
		
		// Get the spelling code and convert to full name
		String spellingCode = attachedSpelling.split("~")[0];
		String spellingName = DictionaryTab.SpellingCodeConverter(spellingCode);
		
		// Assembel the spelling to match the file structure
		String removedSpelling = spellingName+"~"+ attachedSpelling.split("~")[1];
		LogUltility.log(test, logger, "Check if the spelling has been deleted: "+removedSpelling);
		
		// Check that the removed spelling doesn't exist anymore
		boolean deleteSuccess =!infoAfterDelete.get("spellings").contains(removedSpelling);
		LogUltility.log(test, logger, "The spelling was removed from file: "+deleteSuccess);
		assertTrue(deleteSuccess);
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Get current word form type
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Get form type from the grammar index info
		LogUltility.log(test, logger, "The Form Type in corpus: "+chosenTagging.get(2)+" ,The Form Type in grammar indices: "+getGrammarIndexInfo.get(2));
		boolean sameGI = chosenTagging.get(2).equals(getGrammarIndexInfo.get(2));
		LogUltility.log(test, logger, "The Form Types are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--654:Syncing dictionary changes that result in new grammar indices with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_654_Syncing_dictionary_changes_that_result_in_new_grammar_indices_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Corpus Tests - DIC_TC--654", "Syncing dictionary changes that result in new grammar indices with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
				
		// Close smorfet and click No
		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
//		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
//		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
//		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
//		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
	
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar index file
//		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
//		backupStatus = CorpusTab.backupFile(giSource, giDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
//		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
//		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to Dictionary");
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Remove POS that has less than 3 semantic groups
		posList.remove("verb_particle");
		posList.remove("suffix");
		posList.remove("possessive");
		posList.remove("existential");
		posList.remove("demonstrative");
		posList.remove("article");
		
		// For Hebrew remove more categories
		if(Setting.Language.equals("HE"))
			{	posList.remove("accusative_word");	posList.remove("clause_predicate");	posList.remove("genitive_word");	}
		
		// Pick a random POS
		Random randomizer = new Random();
		int RandomIndex = randomizer.nextInt(posList.size());
		String randompos = posList.get(RandomIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Get dictionary list
		List<String> recordsList  = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from records list: " + randomRecord);
		String cleanRecordName = DictionaryTab.txtKeyForm.getText();
		
		// Unselect SG values
		List<String> semanticGroupValues = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
//		LogUltility.log(test, logger, "Selected semantic groups: " + chosenSG);
		CommonFunctions.unselectValuesFromList(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "Unselect semantic groups values ");
		
		// Choose 3 semantic group values
		LogUltility.log(test, logger, "Select 3 semantic group values");
		for(int i=0;i<3;i++)
		{
		String SemanticGroup = semanticGroupValues.get(randomizer.nextInt(semanticGroupValues.size()));
		semanticGroupValues.remove(SemanticGroup);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups, SemanticGroup);
		LogUltility.log(test, logger, "Select: "+SemanticGroup);
		}
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Should create a new grammar index, it will be the last value
		List<String> grammarIndexes = CorpusTab.getAllGrammarIndexesFromFile();
		String newGrammarIndex = grammarIndexes.get(grammarIndexes.size()-1);
		LogUltility.log(test, logger, "The new created grammar index: "+newGrammarIndex);
		LogUltility.log(test, logger, "The grammar index info: "+CorpusTab.getGrammarIndexInfo(newGrammarIndex));
	
		// STEP 4
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
//		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
//		backupStatus = CorpusTab.backupFile(giDest,giSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
//		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
//		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// STEP 5
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
			
		// Navigate to dictionary tab
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to Dictionary");
		
		// STEP 6
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// STEP 7
		// Get the grammar indices values after replacing files
		List<String> grammarIndexesAfter = CorpusTab.getAllGrammarIndexesFromFile();
		boolean giFound = grammarIndexesAfter.contains(newGrammarIndex);
		LogUltility.log(test, logger, "The grammar index: "+newGrammarIndex+", Found in file:"+giFound);
		assertTrue(giFound);
		
		// STEP 8
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
			
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Search for the grammar index value
		newGrammarIndex = newGrammarIndex.trim();
		LexiconTab.searchInLexicon(test, logger, "grammar index", "equal",newGrammarIndex, "all records");
		
		// Get list values
		List<String> lexiconRecords = LexiconTab.getValuesFromApp(LexiconTab.lst_records);
		LogUltility.log(test, logger, "Lexicon records list:"+lexiconRecords);
		
		// Check if it contains the dictionary key
		boolean sameRecord = lexiconRecords.contains(cleanRecordName);
		LogUltility.log(test, logger, "The record: "+cleanRecordName+", Exists in the list:"+sameRecord);	
		assertTrue(sameRecord);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--676:Synchronization between compounds and corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_676_Synchronization_between_compounds_and_corpus() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--676", "Synchronization between compounds and corpus");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);

//		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
//		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
//		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();

//		String searchedKey = compoundKeys.get(randomizer.nextInt(compoundKeys.size()));
		String searchedKey = "HE{a}יוצא_דופן~0";
		String  cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		String randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
				
//		// Get all available lemma values
		List<String> firstLemmas = CompoundsTab.getTaggingValues("Lemma", 0);
		LogUltility.log(test, logger, "Available first lemmas:"+firstLemmas);
		
		// Get all available lemma values
		List<String> secondLemmas = CompoundsTab.getTaggingValues("Lemma", 1);
		LogUltility.log(test, logger, "Available second lemmas:"+secondLemmas);
				
		// Change the lemma that has more than one option
		String chosenLemma;
		int lemmaIndex;
		if(firstLemmas.size()>2)
		{
			// Change the first lemma value
			String lemmaValue = CompoundsTab.getSelectedTaggingValue("Lemma",0);
			LogUltility.log(test, logger, "The lemma value is: "+lemmaValue);
			
			// Remove the selected option and the first empty option
			firstLemmas.remove(0);
			firstLemmas.remove(lemmaValue);
			
			// Select new value
			chosenLemma = firstLemmas.get(randomizer.nextInt(firstLemmas.size()));
			CompoundsTab.chooseTaggingValue("Lemma", 0, chosenLemma);
			LogUltility.log(test, logger, "The new chosen lemma is: "+chosenLemma);
			lemmaIndex = 0;
		}
			
		else
		{
			// Change the first lemma value
			String lemmaValue = CompoundsTab.getSelectedTaggingValue("Lemma",0);
			LogUltility.log(test, logger, "The lemma value is: "+lemmaValue);
			
			// Remove the selected option and the first empty option
			secondLemmas.remove(0);
			secondLemmas.remove(lemmaValue);
			
			// Select new value
			chosenLemma = secondLemmas.get(randomizer.nextInt(secondLemmas.size()));
			CompoundsTab.chooseTaggingValue("Lemma", 0, chosenLemma);
			LogUltility.log(test, logger, "The new chosen lemma is: "+chosenLemma);
			lemmaIndex = 1;
		}
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
//			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the tagged dictionary key after saving
//		System.out.println( wordsInRecord.get(wordIndex+lemmaIndex).replace(" | ", ""));
//		String taggedDictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord, wordsInRecord.get(wordIndex+lemmaIndex).replace(" | ", ""), wordIndex+lemmaIndex);
//   		LogUltility.log(test, logger, "The dictionary key from file after Saving: "+taggedDictionaryKey);
   		
   		// Check if the chosen lemma has been set in the file
//   		boolean changed = chosenLemma.contains(taggedDictionaryKey);
		String lemmaCleanKey = chosenLemma.split(" ")[0].trim();
		boolean changed = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,5).trim(), lemmaCleanKey);
   		LogUltility.log(test, logger, "The chosen lemma: "+chosenLemma+" , appears in the file:"+changed);
		assertTrue(changed);
	
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
//		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		

		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
				
		// Get the tagged dictionary key after saving
		String taggedDictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord, wordsInRecord.get(wordIndex+lemmaIndex), wordIndex+lemmaIndex);
   		LogUltility.log(test, logger, "The dictionary key from file after Saving: "+taggedDictionaryKey);
   		
   		// Check if the chosen lemma has been set in the file
   		changed = chosenLemma.contains(taggedDictionaryKey);
   		LogUltility.log(test, logger, "The chosen lemma: "+chosenLemma+" , appears in the file:"+changed);
		assertTrue(changed);
				
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--652:Syncing tag-replacement for changed spelling with external corpus and grammar files
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_652_Syncing_tag_replacement_for_changed_spelling_with_external_corpus_and_grammar_files() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--652", "Syncing tag-replacement for changed spelling with external corpus and grammar files");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
				
		// Close smorfet and click No
		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
//		List<String> multiSpellings = DictionaryTab.getDictionaryKeysWithMultiSpellings();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		// Intersection , gets dictionary keys that are tagged in corpus that has more than one spelling
//		dictionaryKeys.retainAll(multiSpellings);

		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get the currently chosen tag
		List<String> chosenTag = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " +chosenTag);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
		
		// Click on the word suitable form type
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.Forms_list, chosenTag.get(2));
		LogUltility.log(test, logger, "Click on form: "+chosenTag.get(2));
			
		// Get spelling values
		List<String> spellingValues = DictionaryTab.getValuesFromApp(DictionaryTab.Spellings_list);
		
		// Edit the first spelling value
		DictionaryTab.editSpelling(test, logger,spellingValues.get(0));
		
		boolean popups = true;
		String replacement="";
		while (popups) {
			try {
				popups = DictionaryTab.replacementWindow.isDisplayed();
				List<String> replaceList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
				
				// work without "leave empty"
				replaceList.remove(0);	
				replacement = replaceList.get(randomizer.nextInt(replaceList.size()));
				LogUltility.log(test, logger, "Choose  value: " + replacement);
				CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue,replacement);
				DictionaryTab.btnpopupAcceptWindow.click();
			} catch (Exception e) {
				popups = false;
			}
		}
			
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey,replacement);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

				
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get the currently chosen tag
		List<String> chosenTagAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagAfter);
		
		// Get the replacement form and compare it with the displayed one
		String replacementForm = replacement.split("\\|")[1];
		boolean same = replacementForm.equals(chosenTagAfter.get(2));
		LogUltility.log(test, logger, "The replacement form: " +replacementForm+", the displayed form: "+chosenTagAfter.get(2));
		LogUltility.log(test, logger, "The replacement has been applied successfully: " +same);
		assertTrue(same);
			
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Get current word form type
		List<String> chosenTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagging);
		
		// Get form type from the grammar index info
		LogUltility.log(test, logger, "The Form Type in corpus: "+chosenTagging.get(2)+" ,The Form Type in grammar indices: "+getGrammarIndexInfo.get(2));
		boolean sameGI = chosenTagging.get(2).equals(getGrammarIndexInfo.get(2));
		LogUltility.log(test, logger, "The Form Types are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--653:Syncing tag-replacement for changed spelling with external corpus and grammar files � leave empty
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_653_Syncing_tag_replacement_for_changed_spelling_with_external_corpus_and_grammar_files_leave_empty() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--653", "Syncing tag-replacement for changed spelling with external corpus and grammar files � leave empty");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
				
		// Close smorfet and click No
		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
//		List<String> multiSpellings = DictionaryTab.getDictionaryKeysWithMultiSpellings();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		// Intersection , gets dictionary keys that are tagged in corpus that has more than one spelling
//		dictionaryKeys.retainAll(multiSpellings);

		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get the currently chosen tag
		List<String> chosenTag = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " +chosenTag);
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
		
		// Click on the word suitable form type
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.Forms_list, chosenTag.get(2));
		LogUltility.log(test, logger, "Click on form: "+chosenTag.get(2));
			
		// Get spelling values
		List<String> spellingValues = DictionaryTab.getValuesFromApp(DictionaryTab.Spellings_list);
		
		// Edit the first spelling value
		DictionaryTab.editSpelling(test, logger,spellingValues.get(0));
		
		boolean popups = true;
		String replacement="";
		while (popups) {
			try {
				popups = DictionaryTab.replacementWindow.isDisplayed();		
				// choose "leave empty"
				replacement = "Leave empty";
				LogUltility.log(test, logger, "Choose  value: " + replacement);
				CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue,replacement);
				DictionaryTab.btnpopupAcceptWindow.click();
			} catch (Exception e) {
				popups = false;
			}
		}
			
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		if(replacement.equals("Leave empty"))
			replacement = "";
			
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get the currently chosen tag
		List<String> chosenTagAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord.substring(0, 5).trim(), wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagAfter);
		boolean noTag = chosenTagAfter.isEmpty();
		LogUltility.log(test, logger, "The word is not tagged after choosing 'Leave empty' option: " + noTag);
		assertTrue(noTag);

		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Check that there is no grammar index applied (empty value).
		
		boolean noGrammarIndex = grammarIndex.equals("");
		LogUltility.log(test, logger, "There is no grammar index applied: "+noGrammarIndex);
		assertTrue(noGrammarIndex);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Corpus/Syncing",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
