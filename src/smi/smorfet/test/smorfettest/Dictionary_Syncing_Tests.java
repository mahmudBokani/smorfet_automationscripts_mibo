package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;
import smi.smorfet.test.pages.MainDisplayWindow;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Dictionary_Syncing_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Dictionary tab - Syncing");
	
		// Logger
		logger = LogManager.getLogger(DictionaryTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Dictionary tab - Syncing");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(DictionaryTab.class);
//		CurrentPage.As(DictionaryTab.class).Start_Window.click();
//		CurrentPage.As(DictionaryTab.class).bt_abort.click();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(DictionaryTab.class);
//			CurrentPage.As(DictionaryTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Dictionary");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * DIC_TC--669:Syncing changes to grammar attributes with the lexicon: deleting a grammar attribute
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_669_Syncing_changes_to_grammar_attributes_with_the_lexicon_deleting_a_grammar_attribute() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--669", "Syncing changes to grammar attributes with the lexicon: deleting a grammar attribute");

		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
	
		// Focus on dictionary tab
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
//		// Choose a value from Volume[POS] dropdown
//		String volumePOS = "suffix";
//		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
//		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);
		
		// Get dictionary keys with grammar attributes
		List<String> keysWithGA = DictionaryTab.getDictionaryKeysWithGrammarAttribute();
		
		// Pick random dictionary key
		Random randomizer = new Random();
		String randomRecord = keysWithGA.get(randomizer.nextInt(keysWithGA.size())); 

		// Search for the record
		String cleanKey = randomRecord.split("}")[1].split("~")[0];
		DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanKey, "all records");

		// Pick the record
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);

		// Get selected form
		String selectedForm = CommonFunctions.getChosenValueInDropdown(DictionaryTab.Forms_list).get(0);
		LogUltility.log(test, logger, "The selected form is: " + selectedForm);
		
		// combine the record name and form 
		String recordwithForm = randomRecord+"|"+selectedForm;
		LogUltility.log(test, logger, "The record key and form combined: " + recordwithForm);
		
		// Get grammar attribute values from Lexicon.txt file before deleting
		String GAB4Delete = LexiconTab.getRecordGrammarAttributesFromFile(recordwithForm);
		
		// Get all the available grammar attribute values
		List<String> grammarAttributeValues = DictionaryTab.getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes: " + grammarAttributeValues);
		
		// Choose random grammar attribute value
		String randomAttribute = grammarAttributeValues.get(randomizer.nextInt(grammarAttributeValues.size()));
		DictionaryTab.chooseValueInDropdownText(DictionaryTab.pn_grammar_attributes, randomAttribute);
		LogUltility.log(test, logger, "Choose grammar attribute: " + randomAttribute);
		
		// Click the delete button
		DictionaryTab.bt_delete_grammar_attribute.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		// Navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to Lexicon tab");
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Get grammar attribute values from Lexicon.txt file after deleting
		String GAafterDelete = LexiconTab.getRecordGrammarAttributesFromFile(recordwithForm);
		LogUltility.log(test, logger, "grammar attributes from lexicon file BEFORE deleting: " + GAB4Delete);
		LogUltility.log(test, logger, "grammar attributes from lexicon file AFTER deleting: " + GAafterDelete);
		boolean different = !GAB4Delete.equals(GAafterDelete);
		LogUltility.log(test, logger, "The grammar attribute: "+randomAttribute+", has been deleted successfully:"+different);
		assertTrue(different);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	

	/**
	 * DIC_TC--670:Syncing changes to grammar attributes with the lexicon: adding a new grammar attribute
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_670_Syncing_changes_to_grammar_attributes_with_the_lexicon_adding_a_new_grammar_attribute() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--670", "Syncing changes to grammar attributes with the lexicon: adding a new grammar attribute");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Click dictionary Tab
//		DictionaryTab.dictionaryTab.click();
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Click the dictionary Tab");
		
		// Get text to use for search
		List<String> records = LexiconTab.getRecordsFromLexiconWithGA();
		LogUltility.log(test, logger, "Getting records from lexicon.txt which has grammar attributes");
		  
		// Pick random record
		Random random = new Random();
		String randomRecord = records.get(random.nextInt(records.size()));
		
		// Get record grammar attributes
		String GAfromFileBefore = LexiconTab.getRecordGrammarAttributesFromFile(randomRecord);
		LogUltility.log(test, logger, "The record grammar attributes from file BEFORE adding: "+GAfromFileBefore);
		
		// Clean record
		String[] removeLeft1 = randomRecord.split("}");
	    String[] removeRight1 = removeLeft1[1].split("~");
	    String cleanRandomRecord = removeRight1[0];
		LogUltility.log(test, logger, "Record to use: " + cleanRandomRecord);
		
		// Search in dictionary
		DictionaryTab.searchInDictionary(test,logger,"form","equal",cleanRandomRecord,"all records");
	
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		    
		// Choose the record
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose record from the list: " + randomRecord);
		
		// Add new GA
		// Click the blue add attribute button
		DictionaryTab.bt_add_grammar_attribute.click();
		LogUltility.log(test, logger, "Click add grammar attribute button");
		
		// Choose attribute from the dropdown
		String[] attribute = {"Attach To","Change To"};
		String randomAtt = attribute[random.nextInt(attribute.length)];

		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute, randomAtt);
		LogUltility.log(test, logger, "Choose from attribute list: " + randomAtt);
		
		// Get POS values
		List<String> posValuesAtt = DictionaryTab.getValuesFromApp(DictionaryTab.cb_attribute_4);
		List<String> chosenValues = DictionaryTab.getGrammarAttributeValues();
		
		// Make sure not to choose already chosen value
		List<String> alreadySelectedValues = new ArrayList<String>();
		for(int i=0;i<chosenValues.size();i++)
		{
			String splitter = ":";
			String value = chosenValues.get(i).split(splitter)[1];
			
			if(randomAtt.equals("Attach To"))
			{
				value = value.split(" \\(")[0];
				alreadySelectedValues.add(value);
				
			}
			else
				alreadySelectedValues.add(value);
		}
		
		posValuesAtt.removeAll(alreadySelectedValues);
		String randomPOSAtt = posValuesAtt.get(random.nextInt(posValuesAtt.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute_4, randomPOSAtt);
		LogUltility.log(test, logger, "Choose from POS list: " + randomPOSAtt);
		
		// Choose all
		DictionaryTab.bt_all.click();
		LogUltility.log(test, logger, "Click all button" );
		
		// Click confirm
		DictionaryTab.bt_confirm.click();
		LogUltility.log(test, logger, "Click confirm button" );
		
		// Click save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		
		// Navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to Lexicon");
		
		// Generate the lexicon file
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the ok button");
		CurrentPage.As(LexiconTab.class).bt_utility_accept.click();
		
		// For syncing
		try {
			CommonFunctions.chooseValueInDropdown(LexiconTab.cmpCondition_field,"form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		}catch (Exception e) {
			e.printStackTrace();
		}
	
		
		// Get record grammar attributes
		String GAfromFileAfter = LexiconTab.getRecordGrammarAttributesFromFile(randomRecord);
		LogUltility.log(test, logger, "The record grammar attributes from file AFTER adding: "+GAfromFileAfter);
		
		// Check that the deleted value isn't found in the file
		boolean added = !GAfromFileAfter.equals(GAfromFileBefore);
		LogUltility.log(test, logger, "The grammar attribute was added successfully: "+added);
		assertTrue(added);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--671:Syncing changes to grammar attributes with the lexicon: changing a grammar attribute
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_671_Syncing_changes_to_grammar_attributes_with_the_lexicon_changing_a_grammar_attribute() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--671", "Syncing changes to grammar attributes with the lexicon: changing a grammar attribute");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Click dictionary Tab
//		DictionaryTab.dictionaryTab.click();
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Click the dictionary Tab");
		
		// Get text to use for search
		List<String> records = LexiconTab.getRecordsFromLexiconWithGA();
		LogUltility.log(test, logger, "Getting records from lexicon.txt which has grammar attributes");
		  
		// Pick random record
		Random random = new Random();
		String randomRecord = records.get(random.nextInt(records.size()));
			
		// Clean record
		String[] removeLeft1 = randomRecord.split("}");
	    String[] removeRight1 = removeLeft1[1].split("~");
	    String cleanRandomRecord = removeRight1[0];
		LogUltility.log(test, logger, "Record to use: " + cleanRandomRecord);
		
		// Search in dictionary
		DictionaryTab.searchInDictionary(test,logger,"form","equal",cleanRandomRecord,"all records");
	
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		    
		// Choose the record
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose record from the list: " + randomRecord);
		
		// Get selected form
		String selectedForm = CommonFunctions.getChosenValueInDropdown(DictionaryTab.Forms_list).get(0);
		LogUltility.log(test, logger, "The selected form is: " + selectedForm);
		
		// combine the record name and form 
		String recordwithForm = randomRecord+"|"+selectedForm;
		LogUltility.log(test, logger, "The record key and form combined: " + recordwithForm);
		
		// Get record grammar attributes
		String GAfromFileBefore = LexiconTab.getRecordGrammarAttributesFromFile(recordwithForm);
		LogUltility.log(test, logger, "The record grammar attributes from file BEFORE adding: "+GAfromFileBefore);
		
		// Get all the available grammar attribute values
		List<String> grammarAttributeValues = DictionaryTab.getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes: " + grammarAttributeValues);
		
		// Work only with grammar attributes of "Attach_To" and "Change_To"
		List<String> grammarAttributeValesACto = new ArrayList<String>(); 
		for(int i=0;i<grammarAttributeValues.size();i++)
			if(grammarAttributeValues.get(i).contains("To"))
				grammarAttributeValesACto.add(grammarAttributeValues.get(i));
		
		// Double click to edit a random grammar attribute value
		String randomAttribute = grammarAttributeValesACto.get(random.nextInt(grammarAttributeValesACto.size()));
		DictionaryTab.editValueInGrammarAttributes(DictionaryTab.pn_grammar_attributes, randomAttribute);
		LogUltility.log(test, logger, "Edit grammar attribute: " + randomAttribute);
		
		// Modify the checkboxes
		DictionaryTab.modifyGrammarAttributeCheckboxes(test,logger);
	
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
		DictionaryTab.bt_confirm.click();
		
		// Click save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		
		// Navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to Lexicon");
		
		// Generate the lexicon file
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the ok button");
		CurrentPage.As(LexiconTab.class).bt_utility_accept.click();
		
		// For syncing
		try {
			CommonFunctions.chooseValueInDropdown(LexiconTab.cmpCondition_field,"form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		}catch (Exception e) {
			e.printStackTrace();
		}
	
		
		// Get record grammar attributes
		String GAfromFileAfter = LexiconTab.getRecordGrammarAttributesFromFile(recordwithForm);
		LogUltility.log(test, logger, "The record grammar attributes from file AFTER adding: "+GAfromFileAfter);
		
		// Check that the deleted value isn't found in the file
		boolean added = !GAfromFileAfter.equals(GAfromFileBefore);
		LogUltility.log(test, logger, "The grammar attribute was added successfully: "+added);
		assertTrue(added);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--734:Check sync between dictionary and internal corpus and external corpus when changing semantic fields
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_734_Check_sync_between_dictionary_and_internal_corpus_and_external_corpus_when_changing_semantic_fields() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Dictionary Tests - DIC_TC--734", "Check sync between dictionary and internal corpus and external corpus when changing semantic fields");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
	
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
//		String searchedKey = "EN{n}clerk~0";
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");

   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String  randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		// Select from corpus
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
	
		// Get semantic fields values and remove the chosen ones in order to choose a new value
		List<String> availableSemanticFields = DictionaryTab.getValuesFromApp(DictionaryTab.lb_dictionary_semantic_fields);
		List<String> markedSemanticFields = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields);
		for(int i=0;i<markedSemanticFields.size();i++)
			availableSemanticFields.remove(markedSemanticFields.get(i));

		// Choose semantic field value
		String chosenSF = availableSemanticFields.get(randomizer.nextInt(availableSemanticFields.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields, chosenSF);
		LogUltility.log(test, logger, "Choose semantic field value: "+chosenSF);
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
	
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		boolean SFdisplayed = false;
		String SFfromCorpus="";
		chosenSF=chosenSF.replaceAll("&", "and");
		chosenSF=chosenSF.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Fields").contains(chosenSF))
				{
					SFfromCorpus = recordInfo.get(i).get("Semantic Fields");
					SFdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic field is displayed: "+SFdisplayed);
		assertTrue(SFdisplayed);
			
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		String SFfromGIfile = getGrammarIndexInfo.get(7).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic fields from grammar_indices.txt: "+SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields from corpus GUI: "+SFfromCorpus);
		boolean sameGI = SFfromCorpus.equals(SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		 SFdisplayed = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Fields").contains(chosenSF))
				{
					SFfromCorpus = recordInfo.get(i).get("Semantic Fields");
					SFdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic field is displayed: "+SFdisplayed);
		assertTrue(SFdisplayed);
			
		// Check if the dictionary Key was added to Sync_map.txt file
		 foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		 grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		  getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		 SFfromGIfile = getGrammarIndexInfo.get(7).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic fields from grammar_indices.txt: "+SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields from corpus GUI: "+SFfromCorpus);
		 sameGI = SFfromCorpus.equals(SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--735:Check sync between dictionary and internal corpus and external corpus when changing semantic groups
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_735_Check_sync_between_dictionary_and_internal_corpus_and_external_corpus_when_changing_semantic_groups() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Dictionary Tests - DIC_TC--735", "Check sync between dictionary and internal corpus and external corpus when changing semantic groups");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");

   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String  randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		// Select from corpus
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
	
		// Get semantic groups values and remove the chosen ones in order to choose a new value
		List<String> availableSemanticGroups = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
		List<String> markedSemanticGroups = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
		availableSemanticGroups.removeAll(markedSemanticGroups);

		// Choose semantic groups value
		String chosenSG = availableSemanticGroups.get(randomizer.nextInt(availableSemanticGroups.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups, chosenSG);
		LogUltility.log(test, logger, "Choose semantic group value: "+chosenSG);
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
	
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		boolean SGdisplayed = false;
		String SGfromCorpus="";
		chosenSG=chosenSG.replaceAll("&", "and");
		chosenSG=chosenSG.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Group").contains(chosenSG))
				{
					SGfromCorpus = recordInfo.get(i).get("Semantic Group");
					SGdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic group is displayed: "+SGdisplayed);
		assertTrue(SGdisplayed);
			
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		String SGfromGIfile = getGrammarIndexInfo.get(6).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic groups from grammar_indices.txt: "+SGfromGIfile);
		LogUltility.log(test, logger, "The Semantic groups from corpus GUI: "+SGfromCorpus);
		boolean sameGI = SGfromGIfile.contains(SGfromCorpus);
		LogUltility.log(test, logger, "The Semantic groups are the same: "+sameGI);
		assertTrue(sameGI);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		 SGdisplayed = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Group").contains(chosenSG))
				{
					SGfromCorpus = recordInfo.get(i).get("Semantic Group");
					SGdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic group is displayed: "+SGdisplayed);
		assertTrue(SGdisplayed);
			
		// Check if the dictionary Key was added to Sync_map.txt file
		 foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		 grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		  getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		 SGfromGIfile = getGrammarIndexInfo.get(6).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic group from grammar_indices.txt: "+SGfromGIfile);
		LogUltility.log(test, logger, "The Semantic group from corpus GUI: "+SGfromCorpus);
		sameGI = SGfromGIfile.contains(SGfromCorpus);
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--736:Changing frames in a tagged record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_736_Changing_frames_in_a_tagged_record() throws InterruptedException, AWTException, IOException
	{
		// Done without using the grammar_indices
		test = extent.createTest("Dictionary Tests - DIC_TC--736", "Changing frames in a tagged record");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
//		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Focus on
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		Random randomizer = new Random();
		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
		List<String> dictionaryKeysWithFrames = DictionaryTab.getDictionaryKeysWithFrames();
		dictionaryKeys.retainAll(dictionaryKeysWithFrames);
		String searchedKey = dictionaryKeys.get(randomizer.nextInt(dictionaryKeys.size()));
		
		CorpusTab.searchInCorpus(test,logger,"dictionary key","equal",searchedKey,"all records");
	
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");

   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		boolean continueSearch;
		String  randomRecord="" ;
		
		// Get a sentence that the dictionary key is not part of compound or contraction
		for(int i=0;i<sentencesList.size();i++)
		{
			randomRecord = sentencesList.get(i);	
			continueSearch = CorpusTab.isDictionaryKeyPartofCompoundContraction(randomRecord.substring(0,6).trim(), searchedKey);
			if(!continueSearch)
				break;
		}
		
		// Select from corpus
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the word
		String cleanKey = searchedKey.split("\\}")[1].split("\\~")[0];
		int wordIndex = CorpusTab.getSentenceRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate to dictionary tab");
		
		// search for the word
		DictionaryTab.searchInDictionary(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchedKey);
		
		// Get frames values and remove the chosen ones in order to choose a new value
		List<String> availableFrames = DictionaryTab.getValuesFromApp(DictionaryTab.lst_frames);
		List<String> markedFrames = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_frames);
		for(int i=0;i<markedFrames.size();i++)
			availableFrames.remove(markedFrames.get(i));
		availableFrames.remove("no frame");

		// Choose new frame value
		String chosenFrame = availableFrames.get(randomizer.nextInt(availableFrames.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_frames, chosenFrame);
		LogUltility.log(test, logger, "Choose frame value: "+chosenFrame);
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new frame is displayed 
		boolean Framedisplayed = false;
		String FramesfromCorpus="";
//		chosenFrame=chosenFrame.replaceAll("&", "and");
		chosenFrame=chosenFrame.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Frames").contains(chosenFrame))
				{
					FramesfromCorpus = recordInfo.get(i).get("Frames");
					Framedisplayed = true;
				}
		LogUltility.log(test, logger, "The new Frame is displayed: "+Framedisplayed);
		assertTrue(Framedisplayed);
		
		// Check if the dictionary Key was added to Sync_map.txt file
		boolean foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		String FramesfromGIfile = getGrammarIndexInfo.get(4).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The frames from grammar_indices.txt: "+FramesfromGIfile);
		LogUltility.log(test, logger, "The frames from corpus GUI: "+FramesfromCorpus);
		boolean sameGI = FramesfromCorpus.equals(FramesfromGIfile);
		LogUltility.log(test, logger, "The Semantic groups are the same: "+sameGI);
		assertTrue(sameGI);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For syncing
		try {
			CommonFunctions.clickTabs("Corpus");
			} catch (Exception e) {
			e.printStackTrace();
					}

	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
		
		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		Framedisplayed = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Frames").contains(chosenFrame))
				{
					FramesfromCorpus = recordInfo.get(i).get("Frames");
					Framedisplayed = true;
				}
		LogUltility.log(test, logger, "The new frame is displayed: "+Framedisplayed);
		assertTrue(Framedisplayed);
			
		// Check if the dictionary Key was added to Sync_map.txt file
		 foundInSyncMap = CorpusTab.checkIfFoundInSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get the grammar index of the word
		 grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		  getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frames from grammar indices file
		FramesfromGIfile = getGrammarIndexInfo.get(4).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The frames grammar_indices.txt: "+FramesfromGIfile);
		LogUltility.log(test, logger, "The frames from corpus GUI: "+FramesfromCorpus);
		sameGI = FramesfromGIfile.equals(FramesfromCorpus);
		LogUltility.log(test, logger, "The frames are the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Dictionary/Syncing tests",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
