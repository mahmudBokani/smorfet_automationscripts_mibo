package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.DictionaryTab;


/**
 * 
 * All tests for compounds search
 *
 */
public class Compounds_Search_Tests_Adding_filter extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("compounds tab - Search");
	
		// Logger
		logger = LogManager.getLogger(CompoundsTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= compounds tab - Search");
		logger.info("compounds tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {if (Setting.ReportToTestlink.equals("true") && testlink == true){
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(CompoundsTab.class);
//			CurrentPage.As(CompoundsTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Compounds");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * DIC_TC--62:Verify user can add filter to the search
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_62_Verify_user_can_add_filter_to_the_search() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("compounds Search Test - DIC_TC_62", "Verify user can add filter to the search");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
				
		//Type in the search field
		String searchWord = "get";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		// Add the second word
		String searchWord2ND = "out";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--64 Verify user can change filter while searching 
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_64_Verify_user_can_change_filter_while_searching() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("compounds Search Test - DIC_TC_64", "Verify user can change filter while searching");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
				
		//Type in the search field
		String searchWord = "get";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word : "+searchWord +" , " + containResult);
		assertTrue(containResult);
		
		// Add the second word
		String searchWord2ND = "out";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: "+searchWord +" , and "+searchWord2ND +"," + containResult);
		assertTrue(containResult);
		
		// Change the second word
		searchWord2ND = "in";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "change filter");
		LogUltility.log(test, logger, "change filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: "+searchWord +" , and "+searchWord2ND +"," + containResult);
		assertTrue(containResult);

		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
	    {		
			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Compound/Search/Adding filter",method.getName());
			
//			System.out.println("tryCount: " + tryCount);
//			System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//			        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//			        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}

}
