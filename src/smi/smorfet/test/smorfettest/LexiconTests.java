package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;

/**
 * 
 * All tests for Lexicon search
 *
 */
public class LexiconTests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Lexicon tab");
	
		// Logger
		logger = LogManager.getLogger(LexiconTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Lexicon tab");
		logger.info("Lexicon tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(LexiconTab.class);
//		CurrentPage.As(LexiconTab.class).Start_Window.click();
//		CurrentPage.As(LexiconTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(LexiconTab.class);
//			CurrentPage.As(LexiconTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Lexicon");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
		 * DIC_TC--11:check the process of adding new word to the lexicon
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_11_check_the_process_of_adding_new_word_to_the_lexicon() throws InterruptedException, IOException, AWTException
		{
	   //Thread.sleep(5000);
		test = extent.createTest("Lexicon Search Tests - DIC_TC_11", "check the process of adding new word to the lexicon");
		
		//Press the dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).dictionaryTab.click();
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		
		CurrentPage.As(DictionaryTab.class).cmpPOS.click();
		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Press the save button   btnSave
		//Thread.sleep(5000);
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		LogUltility.log(test, logger, "Press the save button");
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Press the OK button in the Save popup");
		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived in the dictionary: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
	
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found in the dictionary? value should be true: " + equalResult);
		assertTrue(equalResult);
		
		// Navigate to Lexicon 
		
	//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
		LogUltility.log(test, logger, "Lexicon tab displayed ");
		
		// Choose "form" from the first search dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
	//			// Change the last filter to "all records", in case it was changed by other TC
	//			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Type in the search field
		String randomrecord =randomText;
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived in the lexicon: " + recordsList2);
				
		
		
		// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			boolean equalResult2 = true;
			for (String s : recordsList) {
				 String[] removeLeft = s.split("}");
		   		 String[] removeRight = removeLeft[1].split("~");
	
				//System.out.println("s: " + s + " -- recordsList: " + recordsList);
			       if(removeRight[0].toLowerCase().equals(randomText)) continue;
			       	else {
			       		equalResult2 = false;
			           break;
			       	}
			    }
			
			LogUltility.log(test, logger, "Is new record found in the lexicon? value should be true: " + equalResult2);
			assertTrue(equalResult2);
		
	
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
		 * DIC_TC--24:Check the record link in the "Dictionry key" column in Lexicon tab
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_24_Check_the_record_link_in_the_Dictionry_key_column_in_Lexicon_tab() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_24", "Check the record link in the \"Dictionry key\" column in Lexicon tab");
			
	//		// Get records from the Lexicon file
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	//		
	//		// Pick a random record
	//		Random randomizer = new Random();
	//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
	
			// Type in the search field
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="nabil";
			}
			else if (Setting.Language.equals("HE")) {
				firstDropdown = "presentations";
				randomrecord ="אוף";
			}
			
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//		// Change the last filter to "all records", in case it was changed by other TC
	//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
			
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			// Press the goto dictionary black button 
			CurrentPage.As(LexiconTab.class).bt_goto_dictionary.click();
			LogUltility.log(test, logger, "Click the black button to navigate to dictionary , and dictionary tab displayed and the required record selected");
			
			// In the dictionary tab verify that the frequency ins the same value as the searched one 
			
			
			// Get the key form value
			CurrentPage = GetInstance(DictionaryTab.class);
	//		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
	//		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//		//getChosenValueInDropdown
			String keyForm = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
			LogUltility.log(test, logger, "KeyForm value is: " + keyForm);
			
			//Verify that the keyForm from dictionary is the same as the word selected from Lexicon 
			boolean sameRecord = randomrecord1.contains(keyForm);
			assertTrue(sameRecord);
			LogUltility.log(test, logger, "Check that the lexicon word: " +randomrecord1 +" is the same as the dictionary word : "+ keyForm +"  and value should be True: " + sameRecord);
	
			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
	//				String randomSemanticGroups1 = randomSemanticGroups.substring(2);
	//				LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
	//		//verify that the Frequency match the searched one
	//		boolean sameFrequency = chosenFrequency.contains("very common");
	//		assertTrue(sameFrequency);
	//		
	//		LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency and value should be True: " + sameFrequency);
	//		assertTrue(sameFrequency);
			
			// Go back to the lexicon tab
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			LogUltility.log(test, logger, "Lexicon tab displayed again");
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--94:Verify arrows buttons are working
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_94_Verify_arrows_buttons_are_working() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_94", "Verify arrows buttons are working");
			
	//		// Get records from the Lexicon file
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	//		
	//		// Pick a random record
	//		Random randomizer = new Random();
	//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
			
			
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="nabil";
			}
			else if (Setting.Language.equals("HE")) {
				firstDropdown = "presentations";
				randomrecord ="אוף";
			}
			
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//		// Change the last filter to "all records", in case it was changed by other TC
	//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Type in the search field
//			String randomrecord ="nabil";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
			
			
			//Press the arrows
			// Click the Right arrow
			 CurrentPage.As(LexiconTab.class).bt_next.click();
			 CurrentPage.As(LexiconTab.class).bt_next.click();
			 LogUltility.log(test, logger, "Click the Next arrow");
			// Get the selected record in the results area after press the next button
			List<String> chosenRecordNext= CurrentPage.As(LexiconTab.class).getChosenValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "The selected Record after press the next arrow is: " + chosenRecordNext);
			 boolean nextRecord = !randomrecord1.equals(chosenRecordNext);
			 assertTrue(nextRecord);
			 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ randomrecord1 +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +nextRecord);
			// Click the Left arrow
			 CurrentPage.As(LexiconTab.class).Previous_button.click();
			 LogUltility.log(test, logger, "Click the Previous arrow");
			 //Get the selected record in the results area after press the Previous button
			 List<String> chosenRecordPrevious= CurrentPage.As(LexiconTab.class).getChosenValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records);
			 LogUltility.log(test, logger, "The selected Record after press the Previous arrow is: " + chosenRecordPrevious);
			 boolean previousRecord = !chosenRecordPrevious.equals(chosenRecordNext);
			 assertTrue(previousRecord);
			 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ chosenRecordPrevious +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +previousRecord);
			
						
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--159:Check the deleted record is removed from lexicon
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_159_Check_the_deleted_record_is_removed_from_lexicon() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_159", "Check the deleted record is removed from lexicon");
			
	//		// Get records from the Lexicon file
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	//		
	//		// Pick a random record
	//		Random randomizer = new Random();
	//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
			
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="an";
			}
			else if (Setting.Language.equals("HE")) {
//				firstDropdown = "presentations";
//				randomrecord ="אא";
				firstDropdown = "form";
				randomrecord ="אא";
				
			}
			
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//		// Change the last filter to "all records", in case it was changed by other TC
	//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Type in the search field
//			String randomrecord ="Mahmood";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			// Press the goto dictionary black button 
			CurrentPage.As(LexiconTab.class).bt_goto_dictionary.click();
			LogUltility.log(test, logger, "Click the black button to navigate to dictionary , and dictionary tab displayed and the required record selected");
			
			// In the dictionary tab verify that the frequency ins the same value as the searched one 
			
			
			// Get the key form value
			CurrentPage = GetInstance(DictionaryTab.class);
	//		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
	//		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//		//getChosenValueInDropdown
			String keyForm = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
			LogUltility.log(test, logger, "KeyForm value is: " + keyForm);
			
			//Verify that the keyForm from dictionary is the same as the word selected from Lexicon 
			boolean sameRecord = randomrecord1.equals(keyForm);
			assertTrue(sameRecord);
			LogUltility.log(test, logger, "Check that the lexicon word: " +randomrecord1 +" is the same as the dictionary word : "+ keyForm +"  and value should be True: " + sameRecord);
	
			//Press the delete button to delete the selected record
			
			// Click the delete button
			CurrentPage.As(DictionaryTab.class).bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			CurrentPage.As(DictionaryTab.class).btnYes.click();
			// Abort all the cancel popups
			boolean popups = true;
			while (popups) {
				try {
					CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.isDisplayed();
					//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
					//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
					// Choose leave empty and click replace
					CurrentPage.As(DictionaryTab.class).btnpopupAcceptWindow.click();
					List<String> replaceList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
					LogUltility.log(test, logger, "Choose first value: Leave empty: " + replaceList);
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, replaceList.get(0));
				} catch (Exception e) {
					popups = false;
				}
			}
			LogUltility.log(test, logger, "File is deleted");
			
			try {
				// try to get the text
				//String mibo = CurrentPage.As(DictionaryTab.class).text_removed_record_confirmation.getText();
				// Click OK on the confirmatin message
				CurrentPage.As(DictionaryTab.class).btnOk.click();
				// This is to close the popup for new bug (value is empty)
				CurrentPage.As(DictionaryTab.class).btnOk.click();
			} catch (Exception e) {
				LogUltility.log(test, logger, "Confirmation popup didn't appear");
			}
			
			// Retrieve the records list again and check that the deleted record does not exist
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(DictionaryTab.class);
			CurrentPage.As(DictionaryTab.class).tabDictionary.click();
			
			// Choose "form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//				// Change the last filter to "all records", in case it was changed by other TC
	//				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_all_next, "all records");
	//				LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Type in the search field
			String randomrecord3 ="Mahmood";
			CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomrecord3);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord3);
			
			// Click the Retrieve button
			CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			
			recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList);
			
			boolean doContain = recordsList.contains(randomrecord3);
			LogUltility.log(test, logger, "Does retrieved list : " +recordsList + " contain the removed record: " +randomrecord3+ " and value should be false"+ doContain);
			assertFalse(doContain);
	
			
			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
	//				String randomSemanticGroups1 = randomSemanticGroups.substring(2);
	//				LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
	//		//verify that the Frequency match the searched one
	//		boolean sameFrequency = chosenFrequency.contains("very common");
	//		assertTrue(sameFrequency);
	//		
	//		LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency and value should be True: " + sameFrequency);
	//		assertTrue(sameFrequency);
			
			// Go back to the lexicon tab
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			LogUltility.log(test, logger, "Lexicon tab displayed again");
			
	// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//				// Change the last filter to "all records", in case it was changed by other TC
	//				CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//				LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Type in the search field
			String randomrecord2 ="Mahmood";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord2);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord2);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button in the Lexicon tab");
			
			// Retrieve records
			List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived in the Lexicon after delete the record: " + recordsList2); 
			
			// Verify that the retrieved list doesn`t contain the deleted record in the lexicon
			boolean containsDeleted = ! recordsList2.contains(randomrecord2);
			assertTrue(containsDeleted);
			LogUltility.log(test, logger, "Records retreived list: " + recordsList2 + " doesn`t contain the deleted record :" +randomrecord2 + " and the value should be true : " +containsDeleted);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--212: Verify the blue box button in the Lexicon 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_212_Verify_the_blue_box_button_in_the_Lexicon() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_212", "Verify the blue box button in the Lexicon");
			
	//		// Get records from the Lexicon file
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	//		
	//		// Pick a random record
	//		Random randomizer = new Random();
	//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
			
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="nabil";
			}
			else if (Setting.Language.equals("HE")) {
				firstDropdown = "presentations";
				randomrecord ="איכות";
			}
			
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
			// Type in the search field
//			String randomrecord ="promise";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			// Press the goto corpus blue button 
			CurrentPage.As(LexiconTab.class).bt_goto_corpus.click();
			LogUltility.log(test, logger, "Click the black button to navigate to corpus , and corpus tab displayed and the required sentence selected");
			
			// Get the sentences from the corpus  
		     List<String> corpusRecords = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lb_corpus_records);
		 	LogUltility.log(test, logger, "Corpus sentences: " + corpusRecords);
			
			// Get the chosen sentence in the corpus to check that the sentence contain the selected word
	//		CurrentPage = GetInstance(DictionaryTab.class);
	//		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
	//		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//		//getChosenValueInDropdown
	//		
	//
	//		//Remove the first letter for the random searched value  before compare results
	//		//String randomSemanticGroups1 = "desire";
	//		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
	////				String randomSemanticGroups1 = randomSemanticGroups.substring(2);
	////				LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
			//verify that the Frequency match the searched one
		 	boolean corpusContainsword=true;
		 	for (String sentence : corpusRecords) {
		 		if (!sentence.contains(randomrecord))
		 			corpusContainsword=false;
			}
	
			LogUltility.log(test, logger, "the corpus contains the searched word? should be true:" + corpusContainsword);
			assertTrue(corpusContainsword);
			
			LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency and value should be True: " + corpusContainsword);
			assertTrue(corpusContainsword);
			
			// Go back to the lexicon tab
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			LogUltility.log(test, logger, "Lexicon tab displayed again");
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--213: Check Displaying a message when the blue box clicked and there are no results
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_213_Check_Displaying_a_message_when_the_blue_box_clicked_and_there_are_no_results() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_213", "Check Displaying a message when the blue box clicked and there are no results");
			
	//		// Get records from the Lexicon file
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	//		
	//		// Pick a random record
	//		Random randomizer = new Random();
	//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
	
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="Mohamad";
			}
			else if (Setting.Language.equals("HE")) {
				firstDropdown = "presentations";
				randomrecord ="אוף";
			}
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
			// Type in the search field
//			String randomrecord ="Mohamad";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
	
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			// Press the goto corpus blue button 
			CurrentPage.As(LexiconTab.class).bt_goto_corpus.click();
			LogUltility.log(test, logger, "Click the blue button to navigate to corpus , and corpus tab displayed and the required sentence selected");
			
			// Get the sentences from the corpus  
	//	     List<String> notIncorpusMessage = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).tb_message);
			 String notIncorpusMessage = CurrentPage.As(LexiconTab.class).tb_message.getText();
		 	LogUltility.log(test, logger, "Corpus sentences: " + notIncorpusMessage);
		 	
		 	// Verify that the message contain the words not in corpus
		 	String messagePart = "not in corpus";
		 	boolean messagecontain = notIncorpusMessage.contains(messagePart);
		 	LogUltility.log(test, logger, "Check that the message: " + notIncorpusMessage + " contain the part of the message: " + messagePart+ " and value should be true:" + messagecontain);
		 	assertTrue(messagecontain);
			
			// Get the chosen sentence in the corpus to check that the sentence contain the selected word
	//		CurrentPage = GetInstance(DictionaryTab.class);
	//		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
	//		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
	//		//getChosenValueInDropdown
	//		
	//
	//		//Remove the first letter for the random searched value  before compare results
	//		//String randomSemanticGroups1 = "desire";
	//		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
	////				String randomSemanticGroups1 = randomSemanticGroups.substring(2);
	////				LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
	//		//verify that the Frequency match the searched one
	//	 	boolean corpusContainsword=true;
	//	 	for (String sentence : corpusRecords) {
	//	 		if (!sentence.contains(randomrecord))
	//	 			corpusContainsword=false;
	//		}
	//
	//		LogUltility.log(test, logger, "the corpus contains the searched word? should be true:" + corpusContainsword);
	//		assertTrue(corpusContainsword);
	//		
	//		LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency and value should be True: " + corpusContainsword);
	//		assertTrue(corpusContainsword);
	//		
			// Go back to the lexicon tab
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			LogUltility.log(test, logger, "Lexicon tab displayed again");
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--248:Check the new added record are saved correctly in the database file
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_248_Check_the_new_added_record_are_saved_correctly_in_the_database_file() throws InterruptedException, IOException, AWTException
		{
			
		test = extent.createTest("Lexicon Tests - DIC_TC_248", "Check the new added record are saved correctly in the database file");
		
		//Press the dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).dictionaryTab.click();
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		
		CurrentPage.As(DictionaryTab.class).cmpPOS.click();
		
	//	// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
//		String randompos = "noun";
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Add form for hebrew dictionary keys
		if(Setting.Language.equals("HE"))
		{
			String formVal = CurrentPage.As(DictionaryTab.class).addRandomForm();	
			LogUltility.log(test, logger, "Adding new form: "+formVal);
		}
		
		// Press the save button   btnSave
		Thread.sleep(5000);
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		LogUltility.log(test, logger, "Press the save button");
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Press the OK button in the Save popup");
		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		// Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived in the dictionary: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
	
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found in the dictionary? value should be true: " + equalResult);
		assertTrue(equalResult);
		
		// Navigate to Lexicon 
		
		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
		LogUltility.log(test, logger, "Lexicon tab displayed ");
		
		// Choose "form" from the first search dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
	//			// Change the last filter to "all records", in case it was changed by other TC
	//			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Type in the search field
		String randomrecord =randomText;
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived in the lexicon: " + recordsList2);
				
		
		
		// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			boolean equalResult2 = true;
			for (String s : recordsList) {
				 String[] removeLeft = s.split("}");
		   		 String[] removeRight = removeLeft[1].split("~");
	
				//System.out.println("s: " + s + " -- recordsList: " + recordsList);
			       if(removeRight[0].toLowerCase().equals(randomText)) continue;
			       	else {
			       		equalResult2 = false;
			           break;
			       	}
			    }
			
			LogUltility.log(test, logger, "Is new record found in the lexicon? value should be true: " + equalResult2);
			assertTrue(equalResult2);
			
		// Click Generate button
		CurrentPage.As(LexiconTab.class).bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		CurrentPage.As(LexiconTab.class).bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		
		try {
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// Verify that the Lexicon file contains the record 
		List<String> afterAdd = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
		LogUltility.log(test, logger, "Check the Lexicon file after adding the record ");
		boolean fileContainsDeletedRecord = afterAdd.contains(randomText);
		assertTrue(fileContainsDeletedRecord);
		LogUltility.log(test, logger, "Lexicon File should not contain the deleted record, and value should be false :  " + recordsList2 );
		LogUltility.log(test, logger, "Test Case PASSED");	
		
	
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
		 * DIC_TC--250:Check the deleted record info are saved correctly in the database file
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_250_Check_the_deleted_record_info_are_saved_correctly_in_the_database_file() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_250", "Check the deleted record info are saved correctly in the database file");

			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Verify that the Lexicon file doesn`t contain the deleted record
//			String randomrecord ="Mahmood";		
			Random random = new Random();
			List<String> beforeDelete = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
			String randomrecord = beforeDelete.get(random.nextInt(beforeDelete.size()));
			LogUltility.log(test, logger, "Check the Lexicon file contain the record before deleting it ");
			boolean fileContainsRecord = beforeDelete.contains(randomrecord);
			LogUltility.log(test, logger, "Check the Lexicon file contain the record before deleting it and value should be True: " +fileContainsRecord);
			assertTrue(fileContainsRecord);
			
			// Clean record from Nikud
			randomrecord = CommonFunctions.cleanStringNikud(randomrecord);
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "presentations");
			LogUltility.log(test, logger, "presentations is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
					
			// Type in the search field
			
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList); 
			
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			// Press the goto dictionary black button 
			CurrentPage.As(LexiconTab.class).bt_goto_dictionary.click();
			LogUltility.log(test, logger, "Click the black button to navigate to dictionary , and dictionary tab displayed and the required record selected");
			
			// In the dictionary tab verify that the frequency ins the same value as the searched one 
			
			
			// Get the key form value
			CurrentPage = GetInstance(DictionaryTab.class);
	//		//getChosenValueInDropdown
			String keyForm = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
			LogUltility.log(test, logger, "KeyForm value is: " + keyForm);
			
			//Verify that the keyForm from dictionary is the same as the word selected from Lexicon 
			boolean sameRecord = CommonFunctions.cleanStringNikud(randomrecord1).contains(CommonFunctions.cleanStringNikud(keyForm));
			assertTrue(sameRecord);
			LogUltility.log(test, logger, "Check that the lexicon word: " +randomrecord1 +" is the same as the dictionary word : "+ keyForm +"  and value should be True: " + sameRecord);
	
			//Press the delete button to delete the selected record
			
			// Click the delete button
			CurrentPage.As(DictionaryTab.class).bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			CurrentPage.As(DictionaryTab.class).btnYes.click();
			// Abort all the cancel popups
			boolean popups = true;
			while (popups) {
				try {
					popups = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.isDisplayed();
					//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
					//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
					// Choose leave empty and click replace
					CurrentPage.As(DictionaryTab.class).btnpopupAcceptWindow.click();
					List<String> replaceList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
					LogUltility.log(test, logger, "Choose first value: Leave empty: " + replaceList);
					CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, replaceList.get(0));
				} catch (Exception e) {
					popups = false;
				}
			}
			LogUltility.log(test, logger, "File is deleted");
			
			try {
				// try to get the text
				//String mibo = CurrentPage.As(DictionaryTab.class).text_removed_record_confirmation.getText();
				// Click OK on the confirmatin message
				CurrentPage.As(DictionaryTab.class).btnOk.click();
				// This is to close the popup for new bug (value is empty)
				CurrentPage.As(DictionaryTab.class).btnOk.click();
			} catch (Exception e) {
				LogUltility.log(test, logger, "Confirmation popup didn't appear");
			}
			
			// Retrieve the records list again and check that the deleted record does not exist
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(DictionaryTab.class);
			CurrentPage.As(DictionaryTab.class).tabDictionary.click();
			
			// Press the save button   btnSave
			Thread.sleep(5000);
			CurrentPage.As(DictionaryTab.class).btnSave.click();
			LogUltility.log(test, logger, "Press the save button");
			CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
			LogUltility.log(test, logger, "Press the OK button in the Save popup");
			
			// For syncing
			// Choose "form" from the first search dropdown
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
						
			// Retrieve the lexicon file data and check that it doesn't contain the deleted record
			List<String> AfterDelete = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
			boolean deleted = !AfterDelete.contains(randomrecord);
			LogUltility.log(test, logger, "The record doesn't exists in lexicon anymore: "+deleted);
			assertTrue(deleted);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
		 * DIC_TC--305:Verify the info displayed per records are the same as in the dictionary
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_305_Verify_the_info_displayed_per_records_are_the_same_as_in_the_dictionary() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Lexicon Search Test - DIC_TC_305", "Verify the info displayed per records are the same as in the dictionary");
			
	////		// Get records from the Lexicon file
	////		CurrentPage = GetInstance(LexiconTab.class);
	////		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
	////		
	////		// Pick a random record
	////		Random randomizer = new Random();
	////		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
	//		
	//		
	//		
	//		
	//		
	//		//Press the dictionary tab
	//		CurrentPage = GetInstance(LexiconTab.class);
	//		CurrentPage.As(LexiconTab.class).dictionaryTab.click();
	//		
	//		CurrentPage = GetInstance(DictionaryTab.class);
	//		List<String> choosenPOS1 = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS);
	//
	//		LogUltility.log(test, logger, "Records retreived: " + choosenPOS1);
			
			
			String firstDropdown = null;
			String randomrecord = null;
			if (Setting.Language.equals("EN")) {
				firstDropdown = "form";
				randomrecord ="nabil";
			}
			else if (Setting.Language.equals("HE")) {
				firstDropdown = "presentations";
				randomrecord ="איכות";
			}
			
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			
			// Choose "form" from the first search dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, firstDropdown);
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
	//		// Change the last filter to "all records", in case it was changed by other TC
	//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
	//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Type in the search field
//			String randomrecord ="nabil";
			CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
			LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
			
			// Click the Retrieve button
			CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			boolean containResult = true;
			for (String s : recordsList) {
				//System.out.println("s: " + s + " -- recordsList: " + recordsList);
			       if(s.toLowerCase().contains(randomrecord)) continue;
			       	else {
			       		containResult = false;
			           break;
			       	}
			    }
			LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
			assertTrue(containResult);
			
			// select one of the results
			// Choose random record from the middle of the displayed list
			recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
					
			
			//Get the rows and columns values
			
			ArrayList<HashMap<String, String>> lexicalDisplayList = CurrentPage.As(LexiconTab.class).lexiconDisplayTable();
	
			String lexiconPOS =lexicalDisplayList.get(0).get("POS");
	
			//Number 0 above is the row, and POS is the column title
			LogUltility.log(test, logger, "POS from the Lexicon : " + lexiconPOS);
			LogUltility.log(test, logger, "All values from the Lexicon grid : " + lexicalDisplayList);
			
			// Press the goto dictionary black button 
			CurrentPage.As(LexiconTab.class).bt_goto_dictionary.click();
			LogUltility.log(test, logger, "Click the black button to navigate to dictionary, and dictionary tab displayed and the required record selected");
			
			// get the POS from the Dictionary tab
			CurrentPage = GetInstance(DictionaryTab.class);
			List<String> choosenPOS = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS);
			LogUltility.log(test, logger, "Choosen POS in the Dictionary : " + choosenPOS);
			
			// Verify that the chosen POS in the Lexicon is the same as in the Dictionary
			boolean samePOS= lexiconPOS.equals(choosenPOS.get(0));
			LogUltility.log(test, logger, "Choosen POS in the Dictionary : " + choosenPOS.get(0) + "  is the same one as in Lexicon : " +lexiconPOS + " and the value should be True :"+samePOS);
			
			// Go back to the lexicon tab
			//Press the Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).LexiconTab.click();
			
			// Focus on Lexicon tab
			CurrentPage = GetInstance(LexiconTab.class);
			CurrentPage.As(LexiconTab.class).tabLexicon.click();
			LogUltility.log(test, logger, "Lexicon tab displayed again");
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
	 * DIC_TC--605:Adding new records in dictionary are auto synchronized in lexicon
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_605_adding_new_records_in_dictionary_are_auto_synchronized_in_lexicon() throws InterruptedException, IOException, AWTException
	{
   //Thread.sleep(5000);
	test = extent.createTest("Lexicon Tests - DIC_TC_605", "Adding new records in dictionary are auto synchronized in lexicon");
	
	//Press the dictionary tab
	CurrentPage = GetInstance(DictionaryTab.class);
	CurrentPage.As(DictionaryTab.class).dictionaryTab.click();
	
	// Focus on dictionary tab
	CurrentPage = GetInstance(DictionaryTab.class);
	CurrentPage.As(DictionaryTab.class).tabDictionary.click();
	
	// Click the new button
	CurrentPage.As(DictionaryTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(DictionaryTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
	CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
	
	CurrentPage.As(DictionaryTab.class).cmpPOS.click();
	
	// Get random POS
	List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random POS
	Random randomizer = new Random();
	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Search for the new added record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "contains" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	// Type in the search field
	CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retreive button
	CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived in the dictionary: " + recordsList);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
	boolean equalResult = true;
	for (String s : recordsList) {
		 String[] removeLeft = s.split("}");
   		 String[] removeRight = removeLeft[1].split("~");

		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
	       	else {
	       		equalResult = false;
	           break;
	       	}
	    }
	
	LogUltility.log(test, logger, "Is new record found in the dictionary? value should be true: " + equalResult);
	assertTrue(equalResult);
	
	// Navigate to Lexicon 
	
//Press the Lexicon tab
	CurrentPage = GetInstance(LexiconTab.class);
	CurrentPage.As(LexiconTab.class).LexiconTab.click();
	
	// Focus on Lexicon tab
	CurrentPage = GetInstance(LexiconTab.class);
	CurrentPage.As(LexiconTab.class).tabLexicon.click();
	
	LogUltility.log(test, logger, "Lexicon tab displayed ");
	
	// Choose "form" from the first search dropdown
	CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "contains" from the condition dropdown
	CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
//			// Change the last filter to "all records", in case it was changed by other TC
//			CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
//			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
			
	// Type in the search field
	String randomrecord =randomText;
	CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retrieve button
	CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived in the lexicon: " + recordsList2);
			
	
	
	// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult2 = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");

			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult2 = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found in the lexicon? value should be true: " + equalResult2);
		assertTrue(equalResult2);
	

	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	/**
	 * DIC_TC--633:Verify acronyms are spelled by dots
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_633_Verify_acronyms_are_spelled_by_dots() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Lexicon Tests - DIC_TC_633", "Verify acronyms are spelled by dots");

		// Initialize dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		
		// Get clean dictionary records
		List<String> dictionaryRecords = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		LogUltility.log(test, logger, "Retrieving clean dictionary records from file");
		
		// Initialize lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Click on lexicon tab");
		
		// Search for random record
		Random random = new Random();
		String searchWord = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));
		CurrentPage.As(LexiconTab.class).searchInLexicon(test, logger, "presentations", "contains", searchWord, "all records");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
		// Choose random record
		String randomrecord = recordsList.get(random.nextInt(recordsList.size()));
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		//Get the rows and columns values
		ArrayList<HashMap<String, String>> lexicalDisplayList = CurrentPage.As(LexiconTab.class).lexiconDisplayTable();
		
		// Choose one of the rows options
		int chosenRow = random.nextInt(lexicalDisplayList.size());
		LogUltility.log(test, logger, "Choose random row: " + lexicalDisplayList.get(chosenRow));
		
		// Click the black square button to navigate to dictionary tab
		CurrentPage.As(LexiconTab.class).clickOnDisplayTableRow(chosenRow);
		LogUltility.log(test, logger, "Click on the black square in order to navigate to dictionary tab");
		
//		// Get the available spellings before adding new one
//		List<String> spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
//		LogUltility.log(test, logger, "Spelling list before adding: " + spellingsBeforeList);
		
		// Add new spelling value
		CurrentPage = GetInstance(DictionaryTab.class);
		String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(3);
		String key = "INI [initialism]";
		
		// Choose a key and value for the new transcription
		CurrentPage.As(DictionaryTab.class).addSpelling(key, randomValue);
		LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + key);

		// Navigate back to lexicon
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Click on lexicon tab");
		
		// Search for the same record again
		CurrentPage.As(LexiconTab.class).searchInLexicon(test, logger, "presentations", "contains", searchWord, "all records");
		
		// Choose same record
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		//Get the rows and columns values after adding new spelling
		ArrayList<HashMap<String, String>> lexicalDisplayListAfter = CurrentPage.As(LexiconTab.class).lexiconDisplayTable();
		
		// Get the presentations values
		String presentationsBefore = lexicalDisplayList.get(chosenRow).get("Presentations");
		String presentationsAfter = lexicalDisplayListAfter.get(chosenRow).get("Presentations");
		LogUltility.log(test, logger, "Presentations before adding: "+presentationsBefore+", Presenations after adding: "+presentationsAfter);
		
		// Check that the added spelling represented with dot
		boolean spellingWithDot = presentationsAfter.contains(randomValue+".");
		LogUltility.log(test, logger, "The new presentations contains the key value followed by dot: "+spellingWithDot);
		assertTrue(spellingWithDot);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--597:Check the merged records in lexicon
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_597_Check_the_merged_records_in_lexicon() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Lexicon Tests - DIC_TC--597", "Check the merged records in lexicon");

		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		CommonFunctions.clickTabs("Dictionary");
		
		// Generate merge file with minimum = 1 line , maximum = 5 lines
		Random random = new Random();
		int size = random.nextInt(5)+1;
		DictionaryTab.buildMergeFile(test, logger, size);
		
		// Click merge button
		DictionaryTab.bt_merge_files.click();
		LogUltility.log(test, logger, "click the merge button");
		
		// Get list values - should be one value for now
		List<String> filesList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
		LogUltility.log(test, logger, "Merge files list: "+filesList);
		
		// Choose the file
		LogUltility.log(test, logger, "Choose file: " +filesList.get(0));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue,filesList.get(0));
		
		// Click merge button
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click merge button in window");
		
		int sizeLoop = size;
		while(sizeLoop-- > 0)
		{	
			LogUltility.log(test, logger, "Click yes button to add the new record");
			DictionaryTab.btnYes.click();
		}
		
		// If merge warning still being displayed 
		String warningTitle = "Merge warning";
		boolean mergeWarning = DictionaryTab.popupWindowMessage(test, logger, warningTitle,"Click");
		if(mergeWarning)
			DictionaryTab.Cancel.click();
		
		// Get replacement window
//		boolean isPopupDisplayed = false;
//		try {
//			isPopupDisplayed =DictionaryTab.replacementWindow.isDisplayed();
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		if(isPopupDisplayed)
//		{
//			LogUltility.log(test, logger, "Click abort button");
//			DictionaryTab.bt_abort.click();
//			
//			LogUltility.log(test, logger, "Click cancel button");
//			DictionaryTab.Cancel.click();
//		}
		
		
		// Check that merge was completed successfully
//		Thread.sleep(2000);
		String textToCheck=	"Merging records ended successfuly";
		boolean mergeSuccess = DictionaryTab.popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "Merged successfully completed: "+mergeSuccess);
		assertTrue(mergeSuccess);
		DictionaryTab.btnOk.click();
		
		// Get clean records from the Dictionary_merge.txt file
		List<String> cleanMergeRecords = DictionaryTab.getRecordsFromDictionaryMerge();
		LogUltility.log(test, logger, "Get clean records from the merge file: "+cleanMergeRecords);
		
		// Navigate to lexicon tab
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Generate the lexicon file
		
		// Click Generate button
		LexiconTab.bt_generate_lexicon.click();
		LogUltility.log(test, logger, "Click the generate button");
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the green generate button");
		
		// wait for generate to finish
		try {
			LexiconTab.LexiconTab.click();
			LogUltility.log(test, logger, "wait for generate to finish");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Verify that the Lexicon file contains the record 
		List<String> afterAdd = LexiconTab.getRecordsFromLexicon();
		LogUltility.log(test, logger, "Check the Lexicon file after adding the record ");
		
		// Check that the records added to dictionary
		for(int i=0;i<size;i++)
		{
			boolean fileContainsRecords = afterAdd.contains(cleanMergeRecords.get(i));
			LogUltility.log(test, logger, "The record: "+cleanMergeRecords.get(i)+",exists in lexicon file: "+fileContainsRecords);	
			assertTrue(fileContainsRecords);
		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Lexicon",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
