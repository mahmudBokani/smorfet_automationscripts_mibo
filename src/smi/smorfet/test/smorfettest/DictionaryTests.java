package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import net.bytebuddy.agent.builder.AgentBuilder.InitializationStrategy.SelfInjection.Split;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;
import smi.smorfet.test.pages.MonitorTab;
import smi.smorfet.test.pages.RulesTab;


/**
 * 
 * All tests for Dictionary search
 *
 */
public class DictionaryTests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Dictionary tab");
	
		// Logger
		logger = LogManager.getLogger(DictionaryTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Dictionary tab");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(DictionaryTab.class);
//		CurrentPage.As(DictionaryTab.class).Start_Window.click();
//		CurrentPage.As(DictionaryTab.class).bt_abort.click();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(DictionaryTab.class);
//			CurrentPage.As(DictionaryTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Dictionary");
		} catch (Exception e) {
//			// e.printStackTrace();
		}
	}
	
	/**
	 * DIC_TC--17:Verify user can duplicate and revert successfully
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_17_Verify_user_can_duplicate_and_revert_successfully() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_17", "Verify user can duplicate and revert successfully");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Click the duplicate button
		CurrentPage.As(DictionaryTab.class).bt_duplicate.click();
		LogUltility.log(test, logger, "Click the duplicate button");
		
		// Click Yes on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		// Refresh the list and check that duplicated record is found
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
		
		// Increment the duplicated record and then search if it does exist in the records list
		String[] help = randomrecord.split("~");
		String duplicatedRecord =  help[0] + "~" + (Integer.parseInt(help[1])+1);
		LogUltility.log(test, logger, "DuplicatedRecord: " + duplicatedRecord);
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		boolean doesContain = recordsList.contains(duplicatedRecord);
		LogUltility.log(test, logger, "Does record list contain the duplicated record: " + doesContain);
		assertTrue(doesContain);
	
		// Click the revert button
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		LogUltility.log(test, logger, "Click the revert button");
		
		// Click Yes on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
//		CurrentPage.As(DictionaryTab.class).btnOk.click();
		
		// Refresh the list and check that duplicated record is removed
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
		
		// Get the records list and check that the duplicate record does not exist
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		LogUltility.log(test, logger, "check that the duplicate record does not exist");
		doesContain = recordsList.contains(duplicatedRecord);
		LogUltility.log(test, logger, "Does record list contain the duplicated record: " + doesContain);
		assertFalse(doesContain);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--87:Part one - Verify user can select Semantic Groups
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_87_Part_one_verify_user_can_change_select_Semantic_Groups() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_87", "Part one - Verify user can change/select Semantic Groups");
		
		// Focus on Dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
				
		// Choose "Semantic Groups" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "semantic groups");
		LogUltility.log(test, logger, "semantic groups is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomSemanticGroups = "v change";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomSemanticGroups);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomSemanticGroups);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Check if the Too many retrieved record message appear then press the OK button  
		
//		boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();

		// Retrieve records
		LogUltility.log(test, logger, "Before getting records list");
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Groups list value
		List<String> chosen1lexicalGroupsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen1lexicalGroupsList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		// Change or Select New Semantic Groups
		
		// Get all the values from the Semantic Groups list
		List<String> lst_SemanticGroups = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Semantic Groups list: " + lst_SemanticGroups);

		// Change or Select New Semantic Groups
		String semanticToAdd = "avoiding";
		Random randomfield = new Random();
		String randomselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups, semanticToAdd);
		LogUltility.log(test, logger, "Choose from record list: " + randomselect);
		
		// Click the Right arrow
		 CurrentPage.As(DictionaryTab.class).bt_next.click();
		  
		// Click the Left arrow
		 CurrentPage.As(DictionaryTab.class).Previous_button.click();
	
		// Get the chosen Semantic Groups list value after changing the selection 
		List<String> chosen2lexicalGroupsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen2lexicalGroupsList);
		
		
		
		//verify that the the Semantic Groups match the searched one
		boolean notSameFields = chosen1lexicalGroupsList.equals(chosen2lexicalGroupsList);
		assertFalse(notSameFields);
		LogUltility.log(test, logger, "Check that the retrieved Semantic group list do include the searched Semantic group: " + notSameFields);
		assertFalse(notSameFields);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--87:Part two - Verify user can select Semantic Groups
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_87_Part_two_verify_user_can_change_select_Semantic_Groups() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_87", "Part two - Verify user can change/select Semantic Groups");
		
		// Focus on Dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose "Semantic Groups" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "semantic groups");
		LogUltility.log(test, logger, "semantic groups is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomSemanticGroups = "v creation";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_value, randomSemanticGroups);
		//CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomSemanticGroups);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		//Check if the Too many retrieved record message appear then press the OK button  
//		try {
//			boolean isWindowDisplayed = CurrentPage.As(DictionaryTab.class).window_too_many_records.isDisplayed();
//			if (isWindowDisplayed) CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
//		} catch (Exception e) {
//			// e.printStackTrace();
//		}
		

		// Retrieve records
		LogUltility.log(test, logger, "Before getting records list");
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Groups list value
		List<String> chosen1lexicalGroupsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen1lexicalGroupsList);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		// Change or Select New Semantic Groups
		
		// Get all the values from the Semantic Groups list
		List<String> lst_SemanticGroups = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Records retreived: " + lst_SemanticGroups);
		
		// UnSelect the searched Semantic Groups by clicking it
		
		Random randomfield = new Random();
		String randomselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
		randomselect = "creation";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups, randomselect);
		LogUltility.log(test, logger, "Choose from record list: " + randomselect);
		
		// Click the Right arrow
		 CurrentPage.As(DictionaryTab.class).bt_next.click();
		  
		// Click the Left arrow
		 CurrentPage.As(DictionaryTab.class).Previous_button.click();
	
		// Get the chosen Semantic Groups list value after changing the selection 
		List<String> chosen2lexicalGroupsList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen2lexicalGroupsList);
		
		// Verify that the specific Semantic Groups was unselected
		boolean unselectGroup = !chosen2lexicalGroupsList.contains(randomselect);
		assertTrue(unselectGroup);
		LogUltility.log(test, logger, "Check that the retrieved Semantic group list do not include the searched Semantic group since was unselected: " + unselectGroup);
		//verify that the the Semantic Groups match the searched one
		boolean notSameFields = chosen1lexicalGroupsList.equals(chosen2lexicalGroupsList);
		assertFalse(notSameFields);
		LogUltility.log(test, logger, "Check that the retrieved Semantic group list do include the searched Semantic group: " + notSameFields);
		assertFalse(notSameFields);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--19:verify Dropdown reset after use in merge window
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_19_verify_Dropdown_reset_after_use_in_merge_window() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_19", "verify Dropdown reset after use in merge window");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Create a file for the dictionary to merge if it does not exist
		 File yourFile = new File(Setting.AppPath + Setting.Language + "\\Data\\" + "Dictionary1.txt");
//		File yourFile = new File((Setting.AppPath + Setting.Language + "\\Data\\Dictionary1.txt"), "UTF-8");
		yourFile.createNewFile(); // if file already exists will do nothing 
		LogUltility.log(test, logger, "File created for merge, in case it doesn`t exist");
		
		// Click on the merge button
		CurrentPage.As(DictionaryTab.class).btn_merge_files.click();
		LogUltility.log(test, logger, "Click the merge button");
		
//		// Create a file that for the dictionary to merge if it does not exist
//		File yourFile = new File(Setting.AppPath + Setting.Language + "\\Data\\" + "Dictionary1.txt");
//		yourFile.createNewFile(); // if file already exists will do nothing 
		
		// Retrieve records
		List<String> filesList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
		LogUltility.log(test, logger, "Files names retreived: " + filesList);
				
		// Choose a file in the merge dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, filesList.get(0));
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: Dictionary1.txt");
		
		// Click the cancel button in the merge popup
		CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
		LogUltility.log(test, logger, "Click cancel in the merge window");
		
		// Click the mass attribute button
		CurrentPage.As(DictionaryTab.class).bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the mass attribute button");
		
		// Check that no value is initially selected
		List<String> originList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
		boolean isEmpty =  originList.isEmpty();
		LogUltility.log(test, logger, "Is no value is selected in the dropdown: " + isEmpty);
		assertTrue(isEmpty);

		// Close the mass attribute window
		CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
		LogUltility.log(test, logger, "Click cancel in the mass attribute window, to close the popup");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--22:Verify the process of creating a new record in the dictionary
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_22_Verify_the_process_of_creating_a_new_record_in_the_dictionary() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_22", "Verify the process of creating a new record in the dictionary");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// enter a text into the form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);

		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "form is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");

			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found: " + equalResult);
		assertTrue(equalResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--43:Check the "D.com" button
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_43_Check_the_D_com_button() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_43", "Check the \"D.com\" button");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Remove all values from transcriptions box
		List<String> transcriptionsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lstTranscriptions);
		LogUltility.log(test, logger, "Transcriptions retreived: " + transcriptionsList);
		
		// Remove all the transcriptions values
		for (String transcription : transcriptionsList) {
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lstTranscriptions, transcription);
			// Click the delete button
			CurrentPage.As(DictionaryTab.class).bt_delete_transcription.click();
		}
		LogUltility.log(test, logger, "Remove all the transcriptions values");
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		
//		// Choose a POS
//		String POSValue = "verb";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, POSValue);
//		LogUltility.log(test, logger, "Choose from pos list: " + POSValue);
//		
//		
//		// Clear Definition, Etymology and Transcriptions 
//		CurrentPage.As(DictionaryTab.class).boxEtymology.clear();
//		CurrentPage.As(DictionaryTab.class).boxDefinition.clear();
//		CurrentPage.As(DictionaryTab.class).lstTranscriptions
//		LogUltility.log(test, logger, " Clear Definition, Etymology and Transcriptions");
//		
//		// Click the new button
//		CurrentPage.As(DictionaryTab.class).btnNew.click();
//		LogUltility.log(test, logger, "Click the new button");
//		
//		// Click yes to the confirmation button
//		CurrentPage.As(DictionaryTab.class).btnYes.click();
//		LogUltility.log(test, logger, "Click the Yes button");
//		
//		// Type a record name in the key form field
//		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
//		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
//		
//		// Get random POS
//		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
//		LogUltility.log(test, logger, "POS list: " + posList);
//		
//		// Pick a random pos
//		Random randomizer = new Random();
//		String randompos = posList.get(randomizer.nextInt(posList.size()));
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
//		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
//		
//		// Search for the new added record
//		// Choose "form" from the first search dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//		
//		// Choose "contains" from the condition dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
//		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//		
//		//Type in the search field
//		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
//		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
//		
//		// Click the Retreive button
//		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
//		
//		// Retrieve records
//		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
//		
//		// Check that retrieved records do contain the searched record
//		if (recordsList.isEmpty()) assertTrue(false);
//		
//		boolean equalResult = true;
//		for (String s : recordsList) {
//			 String[] removeLeft = s.split("}");
//	   		 String[] removeRight = removeLeft[1].split("~");
//
//			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
//		       	else {
//		       		equalResult = false;
//		           break;
//		       	}
//		    }
//		
//		LogUltility.log(test, logger, "Is new record found: " + equalResult);
//		assertTrue(equalResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--21:Verify user can delete a record from the database records
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_21_Verify_user_can_delete_a_record_from_the_database_records() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_21", "Verify user can delete a record from the database records");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);		

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Click the delete button
		CurrentPage.As(DictionaryTab.class).bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		// Click the yes button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		// Abort all the cancel popups
		boolean popups = true;
		while (popups) {
			try {
				CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.isDisplayed();
				//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
				//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
				// Choose leave empty and click replace
				CurrentPage.As(DictionaryTab.class).btnpopupAcceptWindow.click();
				List<String> replaceList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
				LogUltility.log(test, logger, "Choose first value: Leave empty: " + replaceList);
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, replaceList.get(0));
			} catch (Exception e) {
				popups = false;
			}
		}
		LogUltility.log(test, logger, "File is deleted");
		
		try {
			// try to get the text
			//String mibo = CurrentPage.As(DictionaryTab.class).text_removed_record_confirmation.getText();
			// Click OK on the confirmatin message
			CurrentPage.As(DictionaryTab.class).btnOk.click();
		} catch (Exception e) {
			LogUltility.log(test, logger, "Confirmation popup didn't appear");
		}
		
		// Retrieve the records list again and check that the deleted record does not exist
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		boolean doContain = recordsList.contains(randomrecord);
		LogUltility.log(test, logger, "Does list contain the removed record: " + doContain);
		assertFalse(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--61:Check cancel attach operation
	 * @throws InterruptedException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_61_Check_cancel_attach_operation() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_61", "Check cancel attach operation");
		
		if(Setting.Language.equals("HE")) return;
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
		
		// Reset attach color if it is yellow before starting the test
		String initialColor = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).bt_attach);
		LogUltility.log(test, logger, "Reset button, initial color is: " + initialColor);
//		Robot r = new Robot();
		if (initialColor.equals("Yellow")) {
			// Cancel the attach button, Shift click the attach button
//			r.keyPress(KeyEvent.VK_SHIFT);
//			CurrentPage.As(DictionaryTab.class).bt_attach.click();
//			r.keyRelease(KeyEvent.VK_SHIFT);
			Actions shiftClick = new Actions(DriverContext._Driver);
			shiftClick.keyDown(Keys.SHIFT).click(CurrentPage.As(DictionaryTab.class).bt_attach).keyUp(Keys.SHIFT).perform();
		}
		
		// Get color before Clicking the attach button
		String colorBefore = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).bt_attach);

		// Check that it is green
//		assertEquals(colorBefore.getRed(), 130);
//		assertEquals(colorBefore.getGreen(), 251);
//		assertEquals(colorBefore.getBlue(), 152);
		LogUltility.log(test, logger, "Color before: " + colorBefore);
		assertEquals(colorBefore, "Green");
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Get color after Clicking the attach button
		String colorAfter = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).bt_attach);
		
		// Check that it is yellow
//		assertEquals(colorAfter.getRed(), 219);
//		assertEquals(colorAfter.getGreen(), 255);
//		assertEquals(colorAfter.getBlue(), 0);
		LogUltility.log(test, logger, "Color after: " + colorAfter);
		assertEquals(colorAfter, "Yellow");
		
		// Cancel the attach button, Shift click the attach button
//		Robot r = new Robot();
//		r.keyPress(KeyEvent.VK_SHIFT);
//		CurrentPage.As(DictionaryTab.class).bt_attach.click();
//		r.keyRelease(KeyEvent.VK_SHIFT);
		Actions shiftClick = new Actions(DriverContext._Driver);
		shiftClick.keyDown(Keys.SHIFT).click(CurrentPage.As(DictionaryTab.class).bt_attach).keyUp(Keys.SHIFT).perform();
		
		LogUltility.log(test, logger, "Shift click the attach button");
		
		// Get color after canceling the attach button
		String colorAfterCancel = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).bt_attach);
		
		// Check that it is green
//		assertEquals(colorBefore.getRed(), 130);
//		assertEquals(colorBefore.getGreen(), 251);
//		assertEquals(colorBefore.getBlue(), 152);
		LogUltility.log(test, logger, "Color after cancel: " + colorAfterCancel);
		assertEquals(colorAfterCancel, "Green");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--74:Verify the process of attaching a record to another
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_74_Verify_the_procces_of_attaching_a_record_to_another() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_74", "Verify the procces of attaching a record to another");
		
		if(Setting.Language.equals("HE")) return;
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click OK on the confirmation window");
		
		// Choose another record to attach to
		String randomRecordSecond;
		do {
			randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		} while (randomRecordSecond.equals(randomRecord));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecordSecond);
		
		// Click the attach button again
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button again");
		
		// Choose a key value from the dropdown
		String keyValue = "standard 7"; 
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, keyValue);
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).bt_utility_accept.click();
		LogUltility.log(test, logger, "Click attach button in the popup");
//		 The above click does not work, trying another physical way
//		String attachPlace = CurrentPage.As(DictionaryTab.class).bt_utility_accept.getAttribute("ClickablePoint"); 
//		String[] XY = attachPlace.split(",");

//	      Robot robot = new Robot();
//        robot.mouseMove(Integer.parseInt(XY[0]),Integer.parseInt(XY[1]));
//        robot.mousePress(InputEvent.BUTTON1_MASK);
//        robot.mouseRelease(InputEvent.BUTTON1_MASK);
		
//		// Click OK on the confirmation popup
//		CurrentPage.As(DictionaryTab.class).btnOk.click();
//		LogUltility.log(test, logger, "Click OK on the Attached popup to close it");

		// Check that we have attach and attached to record names in the spellings list
		List<String> spellingList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lb_dictionary_spellings);
		//remove the values and keep only the records
		List<String> spellingListClean = new ArrayList<>();
		for (String value : spellingList) {
			String[] split = value.split("~");
//			spellingList.remove(value);
			spellingListClean.add(split[1]);
		}
		LogUltility.log(test, logger, "Values in spelling list: " + spellingListClean);
		
		String firstRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecord);
		String secondRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecordSecond);
		
		boolean bothExist = spellingListClean.contains(firstRecord) && spellingListClean.contains(secondRecord);
		LogUltility.log(test, logger, "Are first and second records exist in the spelling list box: " + bothExist);
		assertTrue(bothExist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--76:Check the attachment dropdown
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_76_Check_the_attachment_dropdown() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_76", "Check the attachment dropdown");
		
		if(Setting.Language.equals("HE")) return;
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
        
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click OK on the confirmation window");
		
		// Choose another record to attach to
		String randomRecordSecond;
		do {
			randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		} while (randomRecordSecond.equals(randomRecord));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecordSecond);
		
		// Click the attach button again
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button again");
		
		// Click the cancel button
		CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
		LogUltility.log(test, logger, "Click the cancel button");

//		// Check that attach window is closed
//		boolean isDiplayed = CurrentPage.As(DictionaryTab.class).windowAttachment.isDisplayed();
//		LogUltility.log(test, logger, "Is attachment window displayed: " + isDiplayed);
//		assertFalse(isDiplayed);
		
		// The result will check:
		// 1. the attached record is not included in the spelling box for the attached to
		// 2. the attach record still exist in the record list
		
		// For 1, Check that we have attach and attached to record names in the spellings list
		List<String> spellingList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lb_dictionary_spellings);
		//remove the values and keep only the records
		List<String> spellingListClean = new ArrayList<String>();
		for (String value : spellingList) {
			String[] split = value.split("~");
//			spellingList.remove(value);
			spellingListClean.add(split[1]);
		}
		LogUltility.log(test, logger, "Values in spelling list: " + spellingListClean);
		
		String firstRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecord);
		String secondRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecordSecond);
		
		boolean bothExist = !spellingListClean.contains(firstRecord) && spellingListClean.contains(secondRecord);
		LogUltility.log(test, logger, "Are first and second records exist in the spelling list box: " + bothExist);
		assertTrue(bothExist);
		
		// For 2, Check that  record still exist in the record list
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived again: " + recordsList);
		boolean stillExist = recordsList.contains(randomRecord);
		LogUltility.log(test, logger, "Is attach record still exist: " + stillExist);
		assertTrue(stillExist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--77:Check attaching a record to itself
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_77_Check_attaching_a_record_to_itself() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_77", "Check attaching a record to itself");
		
		if(Setting.Language.equals("HE")) return;
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click OK on the confirmation window");
		
		// Click the attach button again
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button again on the same record");
		
		// Check the system message
		boolean isDisplayed = CurrentPage.As(DictionaryTab.class).windowMessageAttachment.isDisplayed();
		LogUltility.log(test, logger, "Is attachment message window displayed: " + isDisplayed);
		assertTrue(isDisplayed);
		
		// Click the OK button
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
			
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--78:Verify user can create new Transcription in Transcriptions field
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_78_Verify_user_can_create_new_Transcription_in_Transcriptions_field() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_78", "Verify user can create new Transcription in Transcriptions field");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available transcriptions before adding new one
		List<String> transcriptionsBeforeList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
		LogUltility.log(test, logger, "Transcriptions list before adding: " + transcriptionsBeforeList);
		
		// Add text in the transcription edit field
		String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(5);
		String randomKey = "WLS [Welsh]";
		// Choose a key and value for the new transcription
		CurrentPage.As(DictionaryTab.class).addTranscriptions(randomKey, randomValue);
		LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + randomValue);

		// Get the available transcriptions after adding new one
		List<String> transcriptionsAfterList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
		LogUltility.log(test, logger, "Transcriptions list after adding: " + transcriptionsAfterList);
		
		// Check the after transcriptions list, it does have the previous values and the new added one
		// add the new transcription to the before transcriptions list and compare it the with after transcriptions list
		transcriptionsBeforeList.add(randomKey.substring(0, 3)+"~"+randomValue);
		
		boolean compare = transcriptionsAfterList.equals(transcriptionsBeforeList);
		LogUltility.log(test, logger, "Is a new transcription added successfully: " + compare);
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--79:Verify user can delete Transcriptions in the Transcriptions field
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_79_Verify_user_can_delete_Transcriptions_in_the_Transcriptions_field() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_79", "Verify user can delete Transcriptions in the Transcriptions field");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available transcriptions before adding new one
		List<String> transcriptionsBeforeList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
		LogUltility.log(test, logger, "Transcriptions list before adding: " + transcriptionsBeforeList);
		
		// Check if having only one transcription value then add a new one
		int transcriptions =  transcriptionsBeforeList.size();
		if (transcriptions <= 1) {
			// Add text in the transcription edit field
			String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(5);
			String randomKey = "WLS [Welsh]";
			// Choose a key and value for the new transcription
			CurrentPage.As(DictionaryTab.class).addTranscriptions(randomKey, randomValue);
			LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + randomValue);
			
			// Get the available transcriptions after adding a new one
			transcriptionsBeforeList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
			LogUltility.log(test, logger, "Transcriptions list after adding: " + transcriptionsBeforeList);
		}

		// Choose random transcription value and remove it
		String deletedTranscription = CurrentPage.As(DictionaryTab.class).removeTranscription();
		LogUltility.log(test, logger, "Deleted transcription: " + deletedTranscription);
		
		// Get the available transcriptions after adding new one
		List<String> transcriptionsAfterList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
		LogUltility.log(test, logger, "Transcriptions list after deleting: " + transcriptionsAfterList);
		
		// remove the delete transcription value from the before list
		transcriptionsBeforeList.remove(deletedTranscription);
		
		boolean compare = transcriptionsAfterList.equals(transcriptionsBeforeList);
		LogUltility.log(test, logger, "Is a transcription deleted successfully: " + compare);
		assertTrue(compare);
//		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--85:Verify user can create new Spelling in Spellings field
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_85_Verify_user_can_create_new_Spelling_in_Spellings_field() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_85", "Verify user can create new Spelling in Spellings field");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available spellings before adding new one
		List<String> spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list before adding: " + spellingsBeforeList);

		// Add new spelling value
		String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(5);
		String randomKey = "ACR [acronym]";
		// Choose a key and value for the new transcription
		CurrentPage.As(DictionaryTab.class).addSpelling(randomKey, randomValue);
		LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + randomValue);

		// Get the available spellings after adding new one
		List<String> spellingsAfterList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list after adding: " + spellingsAfterList);
		
		// Check the after spelling list, it does have the previous values and the new added one
		// add the new spelling to the before spelling list and compare it the with after spelling list
		spellingsBeforeList.add(randomKey.substring(0, 3)+"~"+randomValue);

		boolean compare = spellingsAfterList.equals(spellingsBeforeList);
		LogUltility.log(test, logger, "Is a new spelling added successfully: " + compare);
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--86:Verify user can delete Spelling in the Spellings field
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_86_Verify_user_can_delete_Spelling_in_the_Spellings_field() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_86", "Verify user can delete Spelling in the Spellings field");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available spellings before adding new one
		List<String> spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list before adding: " + spellingsBeforeList);
		
		// Check if having only one spelling value then add a new one
		int spellings =  spellingsBeforeList.size();
		if (spellings <= 1) {
			// Add new spelling value
			String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(5);
			String randomKey = "ACR [acronym]";
			// Choose a key and value for the new transcription
			CurrentPage.As(DictionaryTab.class).addSpelling(randomKey, randomValue);
			LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + randomValue);
			
			// Get the available spellings after adding a new one
			spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
			LogUltility.log(test, logger, "Spellings list after adding: " + spellingsBeforeList);
		}

		// Choose random spellings value and remove it
		String deletedSpelling = CurrentPage.As(DictionaryTab.class).removeSpelling();
		LogUltility.log(test, logger, "Deleted spelling: " + deletedSpelling);
		
		// Get the available spelling after adding new one
		List<String> spellingsAfterList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spellings list after deleting: " + spellingsAfterList);
		
		// remove the delete spelling value from the before list
		spellingsBeforeList.remove(deletedSpelling);
		
		boolean compare = spellingsAfterList.equals(spellingsBeforeList);
		LogUltility.log(test, logger, "Is a spellings deleted successfully: " + compare);
		assertTrue(compare);
//		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--95:check "W" button when nothing is found
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_95_check_W_button_when_nothing_is_found() throws InterruptedException, IOException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_95", "check \"W\" button when nothing is found");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);
		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random pos
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Click the W button
		CurrentPage.As(DictionaryTab.class).bt_search_wiktionary.click();
		String errorText = CurrentPage.As(DictionaryTab.class).window_TitleBar.getAttribute("Name");
		LogUltility.log(test, logger, "Popup window text: " + errorText);
		assertEquals(errorText, "Error: Cannot complete data from Internet");
		
		// close the info window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Revert changes
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		CurrentPage.As(DictionaryTab.class).revertFileMsgOK.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--96:Check "D.com" button when nothing is found
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_96_Check_D_com_button_when_nothing_is_found() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_96", "Check \"D.com\" button when nothing is found");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Click the D button
		CurrentPage.As(DictionaryTab.class).bt_search_dictionarycom.click();
		boolean errorPopup = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, "Error: Cannot complete data from Internet","Data completion from Internet can work only when the default form is selected!");
		assertTrue(errorPopup);
		
		// Close the info window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Revert changes
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		CurrentPage.As(DictionaryTab.class).revertFileMsgOK.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--105:Check the maximum records that could be hold in the Database records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_105_Check_the_maximum_records_that_could_be_hold_in_the_Database_records() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_105", "Check the maximum records that could be hold in the Database records");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String searchWord = Setting.Language.equals("HE")? "ּ" : "a";
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Check that too many retrieved records do popup is displayed
		boolean isDisplayed = CurrentPage.As(DictionaryTab.class).window_TooManyRetrievedRecords.isDisplayed();
		LogUltility.log(test, logger, "Is too many retrieved records poupus is displayed: " + isDisplayed);
		
		// Click the OK button to close the popup window
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click the OK button");
		
		// Check that the label do contain the max value 10000
		String recordsLablel = CurrentPage.As(DictionaryTab.class).ll_dictionary_retrieved_records.getAttribute("Name");
		LogUltility.log(test, logger, "Records label info: " + recordsLablel);
		
		// Count the records in the records list
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		int recordsNumber = recordsList.size();
		LogUltility.log(test, logger, "Number of records in the records list: " + recordsNumber);
		assertEquals(recordsNumber, 10000);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--233:Check the semantic relations between the records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_233_Check_the_semantic_relations_between_the_records() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_233", "Check the semantic relations between the records");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Get records that have semantic relation values
		List<String> recordsWithSemanticRelations = CurrentPage.As(DictionaryTab.class).getRecordsThatHaveSemanticRelationsFromDictionary();
		
		// Search for one of the records above, search for it and choose it
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsWithSemanticRelations.get(randomizer.nextInt(recordsWithSemanticRelations.size()));
		
		String cleanRecordForSearch = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomrecord);

		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(cleanRecordForSearch);
		LogUltility.log(test, logger, "word is typed in the search field: " + cleanRecordForSearch);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose a record from the record list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the values from the semantic relations box
		List<String> semanticRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "Values in semantic relation list: " + semanticRelatinsValues);
		
		// Click on random semantic relation value
		String randomSemanticRelation = semanticRelatinsValues.get(randomizer.nextInt(semanticRelatinsValues.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations, randomSemanticRelation);
		LogUltility.log(test, logger, "Choose from semantic relation list: " + randomSemanticRelation);
		
		// Now check that we have moved to the chosen list value as record
		String newRecord = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "Moved to new record: " + newRecord);
		
		boolean isIt = newRecord.equals(CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomSemanticRelation));
		LogUltility.log(test, logger, "Is the new reocrd the same one chosen in semantic field: " + isIt);
		assertTrue(isIt);
		
		// and check that we have the previous record in the new record semantic value
		// Get the semantic relations for the new record
		semanticRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations);
		List<String> cleanSemanticRelatinsValues = new ArrayList<>();
		for (String value : semanticRelatinsValues) {
			String[] values = value.split("¬");
			cleanSemanticRelatinsValues.add(values[1]);
		}
		LogUltility.log(test, logger, "Semantic relations of the second record: " + cleanSemanticRelatinsValues);
		boolean contains = cleanSemanticRelatinsValues.contains(randomrecord);
		LogUltility.log(test, logger, "randomrecord: " + randomrecord);
		LogUltility.log(test, logger, "Does the previous record is included in the new record semantic relations list: " + contains);
		assertTrue(contains);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--234:Check the lexical relations between the records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_234_Check_the_lexical_relations_between_the_records() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_234", "DIC_TC--234:Check the lexical relations between the records");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Get records that have lexical relation values
		List<String> recordsWithLexicalRelations = CurrentPage.As(DictionaryTab.class).getRecordsThatHaveLexicalRelationsFromDictionary();
		
		// Search for one of the records above, search for it and choose it
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsWithLexicalRelations.get(randomizer.nextInt(recordsWithLexicalRelations.size()));
		
		String cleanRecordForSearch = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomrecord);

		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(cleanRecordForSearch);
		LogUltility.log(test, logger, "word is typed in the search field: " + cleanRecordForSearch);
		
		// Click the Retrieve button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose a record from the record list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the values from the lexical relations box
		List<String> lexicalRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_LexicalRelations);
		LogUltility.log(test, logger, "Values in lexical relation list: " + lexicalRelatinsValues);
		
		// Click on random lexical relation value
		String randomLexicalRelation = lexicalRelatinsValues.get(randomizer.nextInt(lexicalRelatinsValues.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_LexicalRelations, randomLexicalRelation);
		LogUltility.log(test, logger, "Choose from lexical relation list: " + randomLexicalRelation);
		
		// Now check that we have moved to the chosen list value as record
		String newRecord = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "Moved to new record: " + newRecord);
		
		boolean isIt = newRecord.equals(CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomLexicalRelation));
		LogUltility.log(test, logger, "Is the new reocrd the same one chosen in semantic field: " + isIt);
		assertTrue(isIt);
		
		// and check that we have the previous record in the new record lexical value
		// Get the semantic relations for the new record
		lexicalRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_LexicalRelations);
		List<String> cleanLexicalRelatinsValues = new ArrayList<>();
		for (String value : lexicalRelatinsValues) {
			String[] values = value.split("¬");
			cleanLexicalRelatinsValues.add(values[1]);
		}
		LogUltility.log(test, logger, "Lexical relations of the second record: " + cleanLexicalRelatinsValues);
		boolean contains = cleanLexicalRelatinsValues.contains(randomrecord);
		LogUltility.log(test, logger, "randomrecord: " + randomrecord);
		LogUltility.log(test, logger, "Does the previous record is included in the new record semantic relations list: " + contains);
		assertTrue(contains);

		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 *  DIC_TC--244:Check the new added record info are saved correctly in the database file
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_244_Check_the_new_added_record_info_are_saved_correctly_in_the_database_file() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_244", "Check the new added record info are saved correctly in the database file");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);

		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);

		// Pick a random pos
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Click the save button
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");

			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found in records list box: " + equalResult);
		assertTrue(equalResult);
		
		// Get the records from the save file and check if the new record is added
		List<String> recordsFromDictionary = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		boolean isFound = recordsFromDictionary.contains(randomText);
		
		LogUltility.log(test, logger, "Is new record found in save files: " + isFound);
		assertTrue(isFound);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  DIC_TC--249:Check the deleted record info are saved correctly in the database file
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_249_Check_the_deleted_record_info_are_saved_correctly_in_the_database_file() throws InterruptedException, FileNotFoundException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_249", "Check the deleted record info are saved correctly in the database file");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Click the delete button
		CurrentPage.As(DictionaryTab.class).bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");

		// Click the yes button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		
		// Abort all the cancel popups
		boolean popups = true;
		while (popups) {
			try {
				CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.isDisplayed();
				//String firstLine = CurrentPage.As(DictionaryTab.class).popup_after_delete_first_line.getText();
				//CurrentPage.As(DictionaryTab.class).btnpopupCancelWindow.click();
				// Choose leave empty and click replace
				CurrentPage.As(DictionaryTab.class).btnpopupAcceptWindow.click();
				List<String> replaceList = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue);
				LogUltility.log(test, logger, "Choose first value: Leave empty: " + replaceList);
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, replaceList.get(0));
			} catch (Exception e) {
				popups = false;
			}
		}
		LogUltility.log(test, logger, "File is deleted");
//		Thread.sleep(3000);
		
		// Check the confirmation window
		String title = "";
		String text = "was successfully removed";
		boolean exist = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, title, text);
		LogUltility.log(test, logger, "Is popup displayed: " + exist);
//		try {
//			// try to get the text
//			//String mibo = CurrentPage.As(DictionaryTab.class).text_removed_record_confirmation.getText();
//			// Click OK on the confirmatin message
			CurrentPage.As(DictionaryTab.class).btnOk.click();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Confirmation popup didn't appear");
//		}
		
		// Click the save button
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
			
		try {
			CurrentPage.As(DictionaryTab.class).dictionaryTab.click();
			LogUltility.log(test, logger, "click to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}		
		
		// Retrieve the records list again and check that the deleted record does not exist
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		boolean doContain = recordsList.contains(randomrecord);
		LogUltility.log(test, logger, "Does list contain the removed record: " + doContain);
		assertFalse(doContain);

		// Get the records from the save file and check if the new record is added
		List<String> recordsFromDictionary = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		boolean isFound = recordsFromDictionary.contains(randomrecord);
		
		LogUltility.log(test, logger, "Is deleted record found in save files: " + isFound);
		assertFalse(isFound);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *   DIC_TC--259:Verify user can edit key form in Dictionary
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_259_Verify_user_can_edit_key_form_in_Dictionary() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_259", " DIC_TC--259:Verify user can edit key form in Dictionary");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Modify a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(3);
		String currentRecord = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(currentRecord+randomText);

		// Click the modify confirmation popup
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		
		// Refresh the list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Refresh value in volume POS dropdown: " + volumePOS);
		
		// Retrieve records
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived after record modify: " + recordsList);

		// Check if the record list does contain the modified record
		recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		boolean included = false;
		for (String record : recordsList) {
			included = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(record).equals(currentRecord+randomText);
			if (included == true) break;
		}
		LogUltility.log(test, logger, "Is modified record found: " + included);
		assertTrue(included);

		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--281:SemanticRelation Check result of attaching a record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_281_SemanticRelation_Check_result_of_attaching_a_record() throws InterruptedException, AWTException, FileNotFoundException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_281", "SemanticRelation Check result of attaching a record");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
				
		// Search for a record that have Semantic relation to another record
//		List<String> recordsWithSemanticRelation = CurrentPage.As(DictionaryTab.class).availableSemanticRelationsForDictionary();

//				// Choose a value from Volume[POS] dropdown
//				String volumePOS = "adadjective";
//				CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
//				LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click OK on the confirmation window");
		
		// Choose another record to attach to
		String randomRecordSecond;
		do {
			randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		} while (randomRecordSecond.equals(randomRecord));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecordSecond);
		
		// Click the attach button again
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button again");
		
		// Choose a key value from the dropdown
		String keyValue = "standard 1"; 
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, keyValue);
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).bt_utility_accept.click();
		LogUltility.log(test, logger, "Click OK on confirmation messages");
//				 The above click does not work, trying another physical way
//				String attachPlace = CurrentPage.As(DictionaryTab.class).bt_utility_accept.getAttribute("ClickablePoint"); 
//				String[] XY = attachPlace.split(",");

//			      Robot robot = new Robot();
//		        robot.mouseMove(Integer.parseInt(XY[0]),Integer.parseInt(XY[1]));
//		        robot.mousePress(InputEvent.BUTTON1_MASK);
//		        robot.mouseRelease(InputEvent.BUTTON1_MASK);

		// Check that we have attach and attached to record names in the spellings list
		List<String> spellingList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lb_dictionary_spellings);
		//remove the values and keep only the records
		for (String value : spellingList) {
			String[] split = value.split("~");
			spellingList.remove(value);
			spellingList.add(split[1]);
		}
		LogUltility.log(test, logger, "Values in spelling list: " + spellingList);
		
		String firstRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecord);
		String secondRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecordSecond);
		
		boolean bothExist = spellingList.contains(firstRecord) && spellingList.contains(secondRecord);
		LogUltility.log(test, logger, "Are first and second records exist in the spelling list box: " + bothExist);
		assertTrue(bothExist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--300:Check "D.com" button with no record being selected
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_300_Check_D_com_button_with_no_record_being_selected()
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_300", "Check \"D.com\" button with no record being selected");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Click the D button
		CurrentPage.As(DictionaryTab.class).bt_search_dictionarycom.click();
		LogUltility.log(test, logger, "Click D button");
		
		// Check the error message
		String errorMessage = CurrentPage.As(DictionaryTab.class).window_TitleBar.getAttribute("Name");
		LogUltility.log(test, logger, "Error message: " + errorMessage);
		assertEquals(errorMessage, "Error: Cannot complete data from Internet");
		
		// Close the info popup
//		CurrentPage.As(DictionaryTab.class).btnOk.click();
		
		// Close the warning popup
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		CurrentPage.As(DictionaryTab.class).Cancel.click();
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--301:Check "W" button with no record being selected
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_301_Check_W_button_with_no_record_being_selected()
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_301", "Check \"W\" button with no record being selected");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Click the D button
		CurrentPage.As(DictionaryTab.class).bt_search_wiktionary.click();
		LogUltility.log(test, logger, "Click W button");
		
		// Check the error message
		String errorMessage = CurrentPage.As(DictionaryTab.class).window_TitleBar.getAttribute("Name");
		LogUltility.log(test, logger, "Error message: " + errorMessage);
		assertEquals(errorMessage, "Error: Cannot complete data from Internet");
		
		// Close the warning popup
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--316:Verify deleted transcriptions are not restored automatically
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_316_Verify_deleted_transcriptions_are_not_restored_automatically() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_316", "Verify deleted transcriptions are not restored automatically");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Add transcription for the chosen record
		String randomValue = CurrentPage.As(DictionaryTab.class).RandomString(5);
		String randomKey = "YNG [Yinglish]";
		CurrentPage.As(DictionaryTab.class).addTranscriptions(randomKey,randomValue);
		LogUltility.log(test, logger, "Add new transcription: " + randomKey+randomValue);
		
		// Get the transcriptions list for the chosen record
		List<String> transcriptionsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lstTranscriptions);
		LogUltility.log(test, logger, "Transcriptions retreived: " + transcriptionsList);
		
		// Choose random transcription
		String randomTranscription = transcriptionsList.get(transcriptionsList.size()-1);
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lstTranscriptions, randomTranscription);
		LogUltility.log(test, logger, "Choose from transcription list: " + randomTranscription);
		
		// Click the delete button
		CurrentPage.As(DictionaryTab.class).bt_delete_transcription.click();
		LogUltility.log(test, logger, "Transcription deleted");

		// Click back the modified record to refresh
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose first record from record listto refresh: " + randomrecord);
		
		// Check that the deleted transcription does not exist
		// Get the transcriptions list for the chosen record
		transcriptionsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lstTranscriptions);
		LogUltility.log(test, logger, "Transcriptions retreived: " + transcriptionsList);
		
		boolean exist = transcriptionsList.contains(randomTranscription);
		LogUltility.log(test, logger, "Does the old transcription exist,value should be false: " + exist);
		assertFalse(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--347:Check the Definition is editable
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_347_Check_the_Definition_is_editable() throws InterruptedException, SQLException, FileNotFoundException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_347", "DIC_TC--347:Check the Definition is editable");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// remove the text in the definition text box
		CurrentPage.As(DictionaryTab.class).cleanDucomentControl(CurrentPage.As(DictionaryTab.class).boxDefinition);
		LogUltility.log(test, logger, "Delete everything from the Definition textbox");
		
		//Type in the Definition text field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(20);
		CurrentPage.As(DictionaryTab.class).boxDefinition.sendKeys(randomText);
		LogUltility.log(test, logger, "Text types in Definition box: " + randomText);

		// Click the save button
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Wait for the save to finish to check the save data
		// The below line is for wait purposes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		// Bring the definition for the modified record
		String definition = CurrentPage.As(DictionaryTab.class).getDefinitionForRecord(randomrecord);
		LogUltility.log(test, logger, "Definition from the saved file: " + definition);
		
		boolean isEqual = definition.equals(randomText);
		LogUltility.log(test, logger, "Is definition the same in file as saved: " + isEqual);
		assertTrue(isEqual);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--348:Check the Etymology is editable
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_348_Check_the_Etymology_is_editable() throws InterruptedException, SQLException, FileNotFoundException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_348", "Check the Etymology is editable");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// remove the text in the etymology text box
		CurrentPage.As(DictionaryTab.class).cleanDucomentControl(CurrentPage.As(DictionaryTab.class).boxEtymology);
		LogUltility.log(test, logger, "Delete everything from the Etymology textbox");
		
		//Type in the Etymology text field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(20);
		CurrentPage.As(DictionaryTab.class).boxEtymology.sendKeys(randomText);
		LogUltility.log(test, logger, "Text types in Etymolog box: " + randomText);

		// Click the save button
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Wait for the save to finish to check the save data
		// The below line is for wait purposes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		// Bring the definition for the modified record
		String etymology = CurrentPage.As(DictionaryTab.class).getEtymologyForRecord(randomrecord);
		LogUltility.log(test, logger, "Etymology from the saved file: " + etymology);
		
		boolean isEqual = etymology.equals(randomText);
		LogUltility.log(test, logger, "Is etymology the same in file as saved: " + isEqual);
		assertTrue(isEqual);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--362:Check the user cannot choose unknown value in the Part of Speech drop-down
	 * @throws InterruptedException 
	 * @throws SQLException
	 * @throws AWTException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_362_Check_the_user_cannot_choose_unknown_value_in_the_Part_of_Speech_drop_down() throws InterruptedException, SQLException, FileNotFoundException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_362", "Check the user cannot choose unknown value in the Part of Speech drop-down");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Choose the POS to unknown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, "unknown");
//		LogUltility.log(test, logger, "Choose value in POS dropdown: unknown");
		
		// Click POS dropdown then click the U button to choose unknown
		CurrentPage.As(DictionaryTab.class).cmpPOS.click();;
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_U);
		r.keyRelease(KeyEvent.VK_U);
		  
		// Check the popup message
//		boolean isDisplayed = CurrentPage.As(DictionaryTab.class).window_Illegal.isDisplayed();
//		LogUltility.log(test, logger, "Is illegal message displayed: " + isDisplayed);
//		assertTrue(isDisplayed);
		String title = "";
		String text = "Illegal POS selected";
		boolean displayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, title, text);
		LogUltility.log(test, logger, "Is popup displayed: " + displayed);
		assertTrue(displayed);
		
		// Close the pop up window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
//		// Close the info window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--405: Check clicking the new button several times then saving
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_405_Check_clicking_the_new_button_several_times_then_saving() throws InterruptedException, SQLException, FileNotFoundException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_405", "Check clicking the new button several times then saving");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");

		// Click the new button the second time
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");

		// Check popup message
		String title = "Warning: bad key:";
		String text = "The current key has an invalid value.";
		boolean exist = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, title, text);
		LogUltility.log(test, logger, "Is popup displayed: " + exist);
		assertTrue(exist);
		
		CurrentPage.As(DictionaryTab.class).Cancel.click();
    
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--514:check creating new records and using the arrows right after
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_514_check_creating_new_records_and_using_the_arrows_right_after() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_514", "check creating new records and using the arrows right after");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field  RandomString
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);
		
		
//		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
//		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random pos
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Click the right arrow button
		CurrentPage.As(DictionaryTab.class).bt_next.click();
		LogUltility.log(test, logger, "Click next button");
		
		// Click the left arrow button
		CurrentPage.As(DictionaryTab.class).bt_prev.click();
		LogUltility.log(test, logger, "Click back button");
		
		// Check that no Error window or exception
		boolean checkPopUps = CurrentPage.As(DictionaryTab.class).checkNoPopupWindow();
		LogUltility.log(test, logger, "Is there no exception/error window: " + checkPopUps);
		assertTrue(checkPopUps);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--523:check removing form types
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_523_check_removing_form_types() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - 523", "Check removing form types");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		 
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "copula";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available forms before adding new one
		List<String> formsBeforeList = CurrentPage.As(DictionaryTab.class).getFormsValues();
		LogUltility.log(test, logger, "Forms list before adding: " + formsBeforeList);
		
		// Check if having only one form value then add a new one
		int forms =  formsBeforeList.size();
		if (forms <= 1) {
			// Add new form value
			String addedForm = CurrentPage.As(DictionaryTab.class).addRandomForm();
			LogUltility.log(test, logger, "Added Form: " + addedForm);
			
			// Get the available forms after adding a new one
			formsBeforeList = CurrentPage.As(DictionaryTab.class).getFormsValues();
			LogUltility.log(test, logger, "Forms list after adding: " + formsBeforeList);
		}

		// Choose random form value OTHER THAN SIMPLE and remove it
		String deletedForm = CurrentPage.As(DictionaryTab.class).removeForm();
		LogUltility.log(test, logger, "Deleted form: " + deletedForm);
		
		// Close the replacy/abort popup window if displayed
		boolean checkPopUps = CurrentPage.As(DictionaryTab.class).checkNoPopupWindow();
		LogUltility.log(test, logger, "Is there no abort/replace window: " + checkPopUps);
		if (!checkPopUps)
			CurrentPage.As(DictionaryTab.class).btnpopupAcceptWindow.click();
		
		// Get the available forms after deleting
		List<String> formsAfterList = CurrentPage.As(DictionaryTab.class).getFormsValues();
		LogUltility.log(test, logger, "Forms list after deleting: " + formsAfterList);
		
		// Remove the delete form value from the before list
		formsBeforeList.remove(deletedForm);
		
		// Check the removing result
		boolean compare = formsAfterList.equals(formsBeforeList);
		LogUltility.log(test, logger, "Is a forms deleted successfully: " + compare);
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
		
	/**
	 * DIC_TC--524:check adding records with hyphen
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_524_check_adding_records_with_hyphen() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_524", "check adding records with hyphen");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
//		
//		//
//		String randomText="";
//		if (Setting.Language.equals("EN"))
//			randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
//		else if (Setting.Language.equals("HE"))
//			randomText= CurrentPage.As(DictionaryTab.class).RandomString(5);
//		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
//		LogUltility.log(test,logger,"Insert Key Form: "+randomText);
		
		
		
		// Type a record name in the key form field
		String randomText1=null,randomText2=null; 
	    randomText1 = CurrentPage.As(DictionaryTab.class).RandomString(5);
	    randomText2 = CurrentPage.As(DictionaryTab.class).RandomString(5);

		LogUltility.log(test, logger, "Record to enter: " + randomText1 + "-" + randomText2);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText1 + "-" + randomText2);
		
		// Click another element, for example definition
		CurrentPage.As(DictionaryTab.class).boxDefinition.click();
		
		// Check the warning popup info
		boolean popupCheck = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, "", "Form not in right format");
		LogUltility.log(test, logger, "warning popup appear: " + popupCheck);
		assertTrue(popupCheck);
		
		// Close the warning popup
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--599:Grammar Attributes - Check adding Grammar Attributes to non prefix, suffix or possessive words
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_599_Grammar_Attributes_Check_adding_Grammar_Attributes_to_non_prefix_suffix_or_possessive_words() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_599", "Grammar Attributes - Check adding Grammar Attributes to non prefix, suffix or possessive words");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
				
		// Choose a value from Volume[POS] dropdown
//		String volumePOS = "adadjective";
		List<String> posValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		Random random = new Random();
		posValues.remove("prefix");
		posValues.remove("suffix");
		posValues.remove("copula");
		String volumePOS = posValues.get(random.nextInt(posValues.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the add grammar attribute button");
		CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
		
		// Choose attribute from the dropdown
		String[] attribute = {"Attach To","Change To"};
		String randomAtt = attribute[randomizer.nextInt(attribute.length)];

		CommonFunctions.chooseValueInDropdown( CurrentPage.As(DictionaryTab.class).cb_attribute, randomAtt);
		LogUltility.log(test, logger, "Choose from attribute list: " + randomAtt);
				
		// Check the warning popup info
		boolean popupCheck = CurrentPage.As(DictionaryTab.class).popupWindowMessageInAttributeEditor(test, logger, "","These attributes are valid only when POS is prefix or suffix");
		LogUltility.log(test, logger, "warning popup appear: " + popupCheck);
		assertTrue(popupCheck);
		
		// Press ok button
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Press ok button to close the popup");
		
		CurrentPage.As(DictionaryTab.class).bt_cancel_attributes.click();
		LogUltility.log(test, logger, "Press cancel button to close the attribute editor window");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--601:Grammar Attributes - Check clicking the Confirm button without adding any values
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_601_Grammar_Attributes_Check_clicking_the_Confirm_button_without_adding_any_values() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_601", "Grammar Attributes - Check clicking the Confirm button without adding any values");
		 
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "prefix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);

		// Click the Grammar Attributes add button
		LogUltility.log(test, logger, "Click the add grammar attribute button");
		CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
		
		// Choose a value from attribute dropdown
		String attributeValue = "Attach To";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
		LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
		
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
		CurrentPage.As(DictionaryTab.class).bt_confirm.click();
		
		// Check the warning popup info
		boolean popupCheck = CurrentPage.As(DictionaryTab.class).popupWindowMessageInAttributeEditor(test, logger, "", "Please specify form types to apply to, or change the inclusion mode to 'ALL'");
		LogUltility.log(test, logger, "warning popup appear: " + popupCheck);
		assertTrue(popupCheck);
		
		
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--602:Grammar Attributes - Check deleting Grammar Attributes
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_602_Grammar_Attributes_Check_deleting_Grammar_Attributes() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_602", "Grammar Attributes - Check deleting Grammar Attributes");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "suffix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);

		// Get all the available grammar attribute values
		List<String> grammarAttributeValues = CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes: " + grammarAttributeValues);
		
		// Choose random grammar attribute value
		String randomAttribute = grammarAttributeValues.get(randomizer.nextInt(grammarAttributeValues.size()));
		CurrentPage.As(DictionaryTab.class).chooseValueInDropdownText(CurrentPage.As(DictionaryTab.class).pn_grammar_attributes, randomAttribute);
		LogUltility.log(test, logger, "Choose grammar attribute: " + randomAttribute);
		
		// Click the delete button
		CurrentPage.As(DictionaryTab.class).bt_delete_grammar_attribute.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		// Get all the available grammar attribute values after deletion
		List<String> grammarAttributeValuesAfter = CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes after deletiuon: " + grammarAttributeValuesAfter);
		
		// Compare the two lists
		grammarAttributeValues.remove(randomAttribute);
		boolean compare = grammarAttributeValues.equals(grammarAttributeValuesAfter);
		LogUltility.log(test, logger, "Grammar attribute deleted successfully: " + compare);
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--603:Grammar Attributes - Check editing Grammar Attributes values
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_603_Grammar_Attributes_Check_editing_Grammar_Attributes_values() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_603", "Grammar Attributes - Check editing Grammar Attributes values");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "suffix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);

		// Get all the available grammar attribute values
		List<String> grammarAttributeValues = CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes: " + grammarAttributeValues);
		
		// Double click to edit a random grammar attribute value
		String randomAttribute = grammarAttributeValues.get(randomizer.nextInt(grammarAttributeValues.size()));
		CurrentPage.As(DictionaryTab.class).editValueInGrammarAttributes(CurrentPage.As(DictionaryTab.class).pn_grammar_attributes, randomAttribute);
		LogUltility.log(test, logger, "Edit grammar attribute: " + randomAttribute);
		
		// Change the attribute dropdown
		String[] currentAttributeValue = randomAttribute.split(":");
		// Get values in attribute dropdown
		List<String> attributeValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cb_attribute);
		// Remove the current attribute from the list in order not to choose it
		attributeValues.remove(currentAttributeValue[0]);
		// Remove also Stress Attractiveness, it is not working yet
		attributeValues.remove("Stress Attractiveness");
		
		// Choose a value from attribute dropdown
		String attributeValue = attributeValues.get(randomizer.nextInt(attributeValues.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
		LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
		
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
		CurrentPage.As(DictionaryTab.class).bt_confirm.click();

		// Get all the available grammar attribute values after modifying
		List<String> grammarAttributeValuesAfter = CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes after modifying: " + grammarAttributeValuesAfter);	
		
		// Check that grammar attribute is updated successfully
//		grammarAttributeValues.remove(randomAttribute);
//		grammarAttributeValues.add(randomAttribute.replace(currentAttributeValue[0], attributeValue));
		
		// Sort lists
		Collections.sort(grammarAttributeValues);
		Collections.sort(grammarAttributeValuesAfter);
		
		boolean compare = grammarAttributeValues.equals(grammarAttributeValuesAfter);
		LogUltility.log(test, logger, "Grammar Attribute updated successfully: " + compare);

		
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--677:Check D.com button with a record that does not have POS
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_677_Check_D_com_button_with_a_record_that_does_not_have_POS() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_677", "Check D.com button with a record that does not have POS");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "Insert Key Form: "+randomText);
		
		// Click the D.com button
		CurrentPage.As(DictionaryTab.class).bt_search_dictionarycom.click();
		LogUltility.log(test, logger, "Click the D.Com button: ");
		
		// Check the error popup window
		String title = "Error: Cannot complete data from Internet";
		String text = "Data completion from Internet can work only when the default form is selected!";
		boolean warningPopupFound = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, title, text);
		LogUltility.log(test, logger, "Is warning popup found: " + warningPopupFound);
		assertTrue(warningPopupFound);
		
		// Close the warning window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		
		// Revert changes
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		CurrentPage.As(DictionaryTab.class).revertFileMsgOK.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--678:Check W button with a record that does not have POS
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_678_Check_W_button_with_a_record_that_does_not_have_POS() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_678", "Check W button with a record that does not have POS");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		
		// Click the W button
		CurrentPage.As(DictionaryTab.class).bt_search_wiktionary.click();
		LogUltility.log(test, logger, "Click the W button: ");
		
		// Check the error popup window
		String title = "Error: Cannot complete data from Internet";
		String text = "Data completion from Internet can work only when the default form is selected!";
		boolean warningPopupFound = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, title, text);
		LogUltility.log(test, logger, "Is warning popup found: " + warningPopupFound);
		assertTrue(warningPopupFound);
		
		// Close the warning window
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
				
		// Revert changes
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		CurrentPage.As(DictionaryTab.class).revertFileMsgOK.click();
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--679:Grammar Attributes - Check All button in the attribute editor
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_679_Grammar_Attributes_Check_All_button_in_the_attribute_editor() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_679", "Grammar Attributes - Check All button in the attribute editor");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "suffix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		String attributePOS = "name";
		
		// Get the available grammar attributes list
		List<String> grammarAttributeValues = CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		
		// In order to not add another grammar attribute with available name
		// Find the attribute
		String randomAttribute = null;
		for(int i=0;i<grammarAttributeValues.size();i++)
			if(grammarAttributeValues.get(i).contains(attributePOS))
				randomAttribute = grammarAttributeValues.get(i);
		
		// Remove the attribute with name
		if(randomAttribute!=null)
		{
			CurrentPage.As(DictionaryTab.class).chooseValueInDropdownText(CurrentPage.As(DictionaryTab.class).pn_grammar_attributes, randomAttribute);
			
			// Click the delete button
			CurrentPage.As(DictionaryTab.class).bt_delete_grammar_attribute.click();
			LogUltility.log(test, logger, "Click the delete button");
		}
		
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the add grammar attribute button");
		CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
		
		// Choose a value from attribute dropdown
		String attributeValue = "Attach To";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
		LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
		
		// Choose a value from attribute POS dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute_4, attributePOS);
		LogUltility.log(test, logger, "Choose value in attribute POS dropdown: " + attributePOS);
		
		// Click the All button
		CurrentPage.As(DictionaryTab.class).bt_all.click();
		
		// Get all available Form Types and selection status
		HashMap<String, Boolean> formTypes = CurrentPage.As(DictionaryTab.class).getAllFormsTypesFromAttributeEditorWithSelection();

		// Check all values are unselected
		boolean allselected = true;
		for (Map.Entry<String, Boolean> entry : formTypes.entrySet())
		{
			if (entry.getValue().equals("false")) {
				allselected = false;
				break;
			}
		}
		
		LogUltility.log(test, logger, "All values are unselected: " + allselected);
		assertTrue(allselected);
		
		// Close the grammar attribute window
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--680:Grammar Attributes - Check None button in the attribute editor
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_680_Grammar_Attributes_Check_None_button_in_the_attribute_editor() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_680", "Grammar Attributes - Check None button in the attribute editor");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "suffix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the add grammar attribute button");
		CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
		
		// Choose a value from attribute dropdown
		String attributeValue = "Attach To";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
		LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
		
		// Choose a value from attribute POS dropdown
		String attributePOS = "name";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute_4, attributePOS);
		LogUltility.log(test, logger, "Choose value in attribute POS dropdown: " + attributePOS);
		
		// Click the None button
		CurrentPage.As(DictionaryTab.class).bt_none.click();
		
		// Get all availabe Form Types and selection status
		HashMap<String, Boolean> formTypes = CurrentPage.As(DictionaryTab.class).getAllFormsTypesFromAttributeEditorWithSelection();

		// Check all values are unselected
		boolean allUnselected = true;
		for (Map.Entry<String, Boolean> entry : formTypes.entrySet())
		{
			System.out.println(entry.getValue());
			if (entry.getValue().equals("True")) {
				allUnselected = false;
				break;
			}
		}
		
		LogUltility.log(test, logger, "All values are unseledted: " + allUnselected);
		assertTrue(allUnselected);
		// Close the grammar attribute window
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}


	/**
	 * DIC_TC--53:Verify user can "Revert" changes Part 1 Textboxes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_53_Verify_user_can_Revert_changes_Part1_Textboxes() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_53", "Verify user can \"Revert\" changes Part 1 Textboxes");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Edit Key Form,Definition and Etymology 
		
		// Get key form value before changing
		String keyFormB4Change = DictionaryTab.txtKeyForm.getText();
		LogUltility.log(test, logger, "Key form before change: " + keyFormB4Change);
		
		// Change key form value
		String randomText = DictionaryTab.RandomString(3);
		DictionaryTab.txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "Edit Key form: " + randomText);
		
		// Click on definition textbox
		DictionaryTab.boxDefinition.click();
		
		// Click yes on popup
		DictionaryTab.btnYes.click();
		
		//Get definition value before changing
		String defB4Change = DictionaryTab.boxDefinition.getText();
		LogUltility.log(test, logger, "Click the yes button");
		
		LogUltility.log(test, logger, "Definition before change: " + defB4Change);
		
		// Change definition value
		randomText = DictionaryTab.RandomString(10);
		DictionaryTab.boxDefinition.sendKeys(randomText);
		LogUltility.log(test, logger, "Text typed in Definition box: " + randomText);
		
		// Get Etymology value before changing
		String etyB4Change = DictionaryTab.Etymology_Text.getText();
		LogUltility.log(test, logger, "Etymology before change: " + etyB4Change);

		// Change Etymology value 
		randomText = DictionaryTab.RandomString(10);
		DictionaryTab.Etymology_Text.sendKeys(randomText);
		LogUltility.log(test, logger, "Text typed in Etymology box: " + randomText);
				
		// Click the revert button
		DictionaryTab.bt_revert.click();
		LogUltility.log(test, logger, "Click the revert button");
		
		// Click Yes on the confirmation message
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
//		// Check the 'Error reading Dictionary' popup and key will be set to default
//		String titleToCheck = "Error reading Dictionary";
//		String textToCheck = "Key will be set to default";
//		boolean keyFormPopup = DictionaryTab.popupWindowMessage(test, logger, titleToCheck, textToCheck);
//		
//		// Click ok button
//		DictionaryTab.btnOk.click();
		
		// choose the record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
		
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get Values after revert
//		LogUltility.log(test, logger, "Key form set to default after revert: " + keyFormPopup);
		String defAfterRevert = DictionaryTab.boxDefinition.getText();
		LogUltility.log(test, logger, "Definition after revert " + defAfterRevert);
		String etyAfterRevert = DictionaryTab.Etymology_Text.getText();
		LogUltility.log(test, logger, "Etymology after revert " + etyAfterRevert);
		
		// Compare Values
		boolean sameDefinition = defB4Change.equals(defAfterRevert);
		LogUltility.log(test, logger, "Definition form is the same after revert: " + sameDefinition);
		boolean sameEtymology = etyB4Change.equals(etyAfterRevert);
		LogUltility.log(test, logger, "Etymology form is the same after revert: " + sameEtymology);
		assertTrue( sameDefinition && sameEtymology);
//		assertTrue(keyFormPopup && sameDefinition && sameEtymology);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--80:Verify user can create new form in Forms field
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_80_Verify_user_can_create_new_form_in_Forms_field() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--80", "Verify user can create new form in Forms field");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
	
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "title";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get available forms values
		List<String> formsValues = DictionaryTab.getFormsValues();
		LogUltility.log(test, logger, "Forms values: " + formsValues);
		
		// Remove form value to avoid trying to add to a list having all available values before
		if(formsValues.size() == 8)
			DictionaryTab.removeForm();
		LogUltility.log(test, logger, "Remove form value to avoid trying to add to a list having all available values before" );
		
		// Add new form
		DictionaryTab.tabDictionary.click();
		String newForm = DictionaryTab.addRandomForm();
		LogUltility.log(test, logger, "Add new Form: " + newForm);
		
		// Click the save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click the save button");
		
		// Choose record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		
		// Get the new forms list after saving
		List<String> formsValuesAfterSave = DictionaryTab.getFormsValues();
		LogUltility.log(test, logger, "Forms values after saving: " + formsValuesAfterSave);
		
		boolean isAdded = formsValuesAfterSave.contains(newForm);
		LogUltility.log(test, logger, newForm+ " is added to the forms list: " + isAdded);
		
		assertTrue(isAdded);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--81:Verify user can delete Forms from the Forms field
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_81_Verify_user_can_delete_Forms_from_the_Forms_field() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--81", "Verify user can delete Forms from the Forms field");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "title";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get available forms values
		List<String> formsValues = DictionaryTab.getFormsValues();
		LogUltility.log(test, logger, "Forms values: " + formsValues);
		
		// add form value to avoid trying to remove the one available value
		if(formsValues.size() == 1)
		{
			// Add new form
			String newForm = DictionaryTab.addRandomForm();
			LogUltility.log(test, logger, "Add new Form: " + newForm);
		}
		
	
		// Remove form value
		String removedForm = DictionaryTab.removeForm();
		LogUltility.log(test, logger, "Remove form: "+removedForm);
		
		// Click the save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click the save button");
		
		// Choose record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		
		// Get the new forms list after saving
		List<String> formsValuesAfterSave = DictionaryTab.getFormsValues();
		LogUltility.log(test, logger, "Forms values after saving: " + formsValuesAfterSave);
		
		boolean isDeleted = !formsValuesAfterSave.contains(removedForm);
		LogUltility.log(test, logger, removedForm+ " is removed from the forms list: " + isDeleted);
		
		assertTrue(isDeleted);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	
	
	/**
	 * DIC_TC--88:Verify user can change/select Semantic Fields
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_88_Verify_user_can_change_select_Semantic_Fields() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--88", "Verify user can change or select Semantic Fields");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		int randomIndex = randomizer.nextInt(recordsList.size());
		String randomrecord = recordsList.get(randomIndex);
		String randomrecord2 = recordsList.get(randomIndex-1);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get list selected values
		List<String> selectedValues = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields);
		LogUltility.log(test, logger, "Selected Semantic Fields values:  "+selectedValues);
		
		// Unselect values from the list 
		CommonFunctions.unselectValuesFromList(DictionaryTab.lb_dictionary_semantic_fields);
		LogUltility.log(test, logger, "Unselect values from the Semantic Fields list ");
		
		// Click second record to sync
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord2);
		
		// Click the first record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		
		// Select values from the list
		List<String> availableValues = DictionaryTab.getValuesFromApp(DictionaryTab.lb_dictionary_semantic_fields);
		
		// Eliminate former selected values
		availableValues.removeAll(selectedValues);
		String newSemanticField = availableValues.get(randomizer.nextInt(availableValues.size()));
			
		// Select new Value
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields, newSemanticField);
		LogUltility.log(test, logger, "Select new value: "+newSemanticField);
		
		// Click second record to sync
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord2);
		
		// Click the first record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		
		// Click the save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click the save button");
//		LogUltility.log(test, logger, "Selected semantic fields after saving the changes: "+selectedValuesAfterSave);
		LogUltility.log(test, logger, "Selected semantic fields before saving the changes: "+selectedValues);
		
				
		// Compare if changes made
		List<String> selectedValuesAfterSave = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields);
		boolean changesMade = selectedValuesAfterSave.contains(newSemanticField);
		LogUltility.log(test, logger, "Changes are set: "+changesMade);
		
		LogUltility.log(test, logger, "Selected semantic fields after saving the changes: "+selectedValuesAfterSave);
		LogUltility.log(test, logger, "Selected semantic fields before saving the changes: "+selectedValues);
		LogUltility.log(test, logger, "Semantic fields after saving the changes: "+selectedValuesAfterSave+"should contain semantic fields before saving the changes: " +selectedValues+ "and the value should be true: "+changesMade);
		assertTrue(changesMade);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--97:Check Cancel button in "Save" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_97_Check_Cancel_button_in_Save_popup() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--97", "Check Cancel button in \"Save\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
	
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		Random randomizer = new Random();
		List<String> volumePOSlist = DictionaryTab.getValuesFromApp(DictionaryTab.cmpVolumePOS);
		String volumePOS = volumePOSlist.get(randomizer.nextInt(volumePOSlist.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the definition before editing from file
		String definitionB4Cancel = DictionaryTab.getDefinitionForRecord(randomrecord);
		LogUltility.log(test, logger, "Definition from the saved file: " + definitionB4Cancel);
		
		// Edit definition text field
		String randomText = DictionaryTab.RandomString(20);
		DictionaryTab.boxDefinition.sendKeys(randomText);
		LogUltility.log(test, logger, "Text types in Definition box: " + randomText);
		
		// Click save button and cancel
		DictionaryTab.btnSave.click();
		LogUltility.log(test, logger, "Click the save button");
		DictionaryTab.Cancel.click();
		LogUltility.log(test, logger, "Click the cancel button");
		
		// Get definition from file after canceling
		String definitionAfterCancel = DictionaryTab.getDefinitionForRecord(randomrecord);
		LogUltility.log(test, logger, "Definition from the saved file after canceling: " + definitionAfterCancel);
		
		boolean noChange = definitionAfterCancel.equals(definitionB4Cancel);
		LogUltility.log(test, logger, "No change was made in the file: " + noChange);	
		assertTrue(noChange);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--149:Check adding all values to "Form"
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_149_Check_adding_all_values_to_Form() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--149", "Check adding all values to Form");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
//		// Revert to avoid the problem with the "add form" button
//		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//				
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Ok on the confirmation message");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "article";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get available forms values
		List<String> formsValues = DictionaryTab.getFormsValues();
		LogUltility.log(test, logger, "Forms values: " + formsValues);
		
		// Add new form
		CommonFunctions.clickTabs("Dictionary");
		DictionaryTab.bt_add_form.click();
		
//		Actions act=new Actions(DriverContext._Driver);
//		act.moveToElement(DriverContext._Driver.findElement(By.id("bt_add_form"))).click().build().perform();
		
//		boolean errorPopup = DictionaryTab.popupWindowMessage(test,logger,"Error creating new form","You cannot add another form to this record.\rAll the available form-types are occupied.");
//		if(!errorPopup)
//		{
//			DictionaryTab.boxDefinition.click();
//			DictionaryTab.bt_add_form.click();
//		}
		
		//********************
//		String attachPlace = CurrentPage.As(DictionaryTab.class).bt_add_form.getAttribute("ClickablePoint"); 
//		String[] XY = attachPlace.split(",");
//
//	    Robot robot = new Robot();
//        robot.mouseMove(Integer.parseInt(XY[0]),Integer.parseInt(XY[1]));
//        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
//        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        //*********************
		LogUltility.log(test, logger, "Click on the blue button to add new form");
		
		// Get the popup warning
		boolean errorPopup = DictionaryTab.popupWindowMessage(test,logger,"Error creating new form","You cannot add another form to this record.\rAll the available form-types are occupied.");
		
		// Compare expected and actual message
		LogUltility.log(test, logger, "error message is displayed: "+errorPopup);
		assertTrue(errorPopup);

		// press ok button
		DictionaryTab.btnOk.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--93:Check the "Mass Attribution" button - part1 in dropdowns
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_93_Check_the_Mass_Attribution_button_part1_in_dropdowns() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--93", "Check the Mass Attribution button - part1 in dropdowns");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click on the dictionary tab
		DictionaryTab.tabDictionary.click();	
		
		// Search in order to have less number of sentences
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
				
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
//		//Type in the search field
//		String searchWord = "ocean";
//		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
//		LogUltility.log(test, logger, "word is typed in the search field");
		
		String searchWord="";
		if (Setting.Language.equals("EN"))
			searchWord = "ocean";
		else if (Setting.Language.equals("HE"))
			searchWord= "את";
		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field:"+searchWord);
		
		
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Click the Retreive button
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Click on the mass button
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available dropdowns on the left in Corpus 
		String[] dropDownLists = {"origin","style","acronym","Register","frequency","dependency","number","gender","definiteness","declinability"};
		String chosenDD = dropDownLists[randomizer.nextInt(dropDownLists.length)];
		
		// Chosen dropdown webelement
		WebElement lstBox = null;
		switch(chosenDD)
		{
		case "origin":
			 lstBox = DictionaryTab.cmpOrigin;break;
		case "style":
			 lstBox = DictionaryTab.cmpStyle;break;
		case "acronym":
			 lstBox = DictionaryTab.cmpAcronym;break;
		case "Register":
			 lstBox = DictionaryTab.cmpRegister;break;
		case "frequency":
			 lstBox = DictionaryTab.cmpFrequency;break;
		case "dependency":
			 lstBox = DictionaryTab.cmpDependency;break;
		case "number":
			 lstBox = DictionaryTab.cmpNumber;break;
		case "gender":
			 lstBox = DictionaryTab.cmpGender;break;
		case "definiteness" :
			 lstBox = DictionaryTab.cmpDefiniteness;break;
		case "declinability" :
			lstBox = DictionaryTab.cmpDeclinability;
		}
		
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massChangeItsDD, chosenDD);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenDD);
		
		// Get value before change
		List<String> valueB4Change = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value before change in dropdown: "+valueB4Change);
				
		// Get record POS type code
		String posCode = randomrecord.split("}")[0];
		posCode = posCode.split("\\{")[1];
		
		// Choose second dropdown value  dd_utility_value
		List<String> dropdownValues = DictionaryTab.getValuesFromApp(DictionaryTab.massToBeDD);
		List<String> relevantDropdownValues = new ArrayList<String>();
		for(String str : dropdownValues)
				relevantDropdownValues.add(str);
		
		String chosenDD2 = relevantDropdownValues.get(randomizer.nextInt(relevantDropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		
		// Click on change
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button");
		
		try {
			DictionaryTab.tabDictionary.click();		
			LogUltility.log(test, logger, "Click Dictionary to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}
				
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get dropdown value
		List<String> valueAfterChange = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value after change in dropdown: "+valueAfterChange);
		
		boolean isChanged = valueAfterChange.get(0).equals(chosenDD2);
		LogUltility.log(test, logger, "The value is changed: "+isChanged);
		assertTrue(isChanged);
		
		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//				
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Ok on the confirmation message");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--93:Check the Mass Attribution button - part2 in marking fields with No
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_93_Check_the_Mass_Attribution_button_part2_in_marking_fields_with_No() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--93", "Check the Mass Attribution button - part2 in marking fields with No");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
				
		// Click on the Corpus tab
//		DictionaryTab.tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Search in order to have less number of sentences
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
						
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
						
//		//Type in the search field
//		String searchWord = "ocean";
//		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
//		LogUltility.log(test, logger, "word is typed in the search field");
		
		String searchWord="";
		if (Setting.Language.equals("EN"))
			searchWord = "ocean";
		else if (Setting.Language.equals("HE"))
			searchWord= "בית";
		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field:"+searchWord);
			
		// Change the last filter value, in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
						
		// Click the Retreive button
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		// Click on the mass button
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available marking fields on the left in Corpus 
		String[] fieldsLists = {"semantic groups","semantic fields"};
		String chosenMF = fieldsLists[randomizer.nextInt(fieldsLists.length)];

		// Chosen field webelement
		WebElement lstBox = null;
		switch(chosenMF)
		{
		case "semantic groups":
			 lstBox = DictionaryTab.lst_SemanticGroups;break;
		case "semantic fields":
			 lstBox = DictionaryTab.lb_dictionary_semantic_fields;
		}
		
		// Choose first dropdown value
		Thread.sleep(3000);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massChangeItsDD, chosenMF);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenMF);
		
		// Get values before change
		List<String> valueB4Change = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values before change: "+valueB4Change);
				
		// Get record POS type code
		String posCode = randomrecord.split("}")[0];
		posCode = posCode.split("\\{")[1];
		
		// Choose second dropdown value  dd_utility_value
	
		List<String> dropdownValues = DictionaryTab.getValuesFromApp(DictionaryTab.massToBeDD);
		List<String> relevantDropdownValues = new ArrayList<String>();
		if(chosenMF.equals("semantic groups"))
		{
			for(String str : dropdownValues)
				if(str.substring(0,1).equals(posCode))
					relevantDropdownValues.add(str);
		}
		else
			relevantDropdownValues = dropdownValues;
				
		String chosenDD2 = relevantDropdownValues.get(randomizer.nextInt(relevantDropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		chosenDD2 = chosenDD2.replace(" and "," & ");

		// Click on change
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button");
		
		try {
			DictionaryTab.tabDictionary.click();		
			LogUltility.log(test, logger, "Click Dictionary to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		try {
			// Click on NO
			DictionaryTab.massNoBtn.click();
			LogUltility.log(test, logger, "Click No");
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		try {
			boolean windowCon = DictionaryTab.popupWindowMessageOutside(test, logger, "You have unsaved changes in the corpus, would you like to save them? Click 'Yes' to save, click 'No' for the corpus to be overriden.");
			if(windowCon)
				DictionaryTab.No.click();
			}
			catch(Exception e)
				{}
		
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Get values after change
		List<String> valueAfterChange = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value after change in dropdown: "+valueAfterChange);
		
		if(chosenMF.equals("semantic groups")) chosenDD2 = chosenDD2.substring(2);
		boolean isChanged = valueAfterChange.get(0).equals(chosenDD2);
		LogUltility.log(test, logger, "The value is changed: "+isChanged);
		assertTrue(isChanged);
		
		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//				
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Ok on the confirmation message");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--93:Check the Mass Attribution button - part3 in marking fields with Yes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_93_Check_the_Mass_Attribution_button_part3_in_marking_fields_with_Yes() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--93", "Check the Mass Attribution button - part3 in marking fields with Yes");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
				
		// Click on the Corpus tab
		DictionaryTab.tabDictionary.click();		
		
		// Search in order to have less number of sentences
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "form is chosen in the first search dropdown");
						
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
						
//		//Type in the search field
//		String searchWord = "ocean";
//		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
//		LogUltility.log(test, logger, "word is typed in the search field");
		
		String searchWord="";
		if (Setting.Language.equals("EN"))
			searchWord = "ocean";
		else if (Setting.Language.equals("HE"))
			searchWord= "אתא";
		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field:"+searchWord);
		
		// Change the last filter value, in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
						
		// Click the Retreive button
//		Thread.sleep(3000);
		DictionaryTab.btnRetrieve_same.click();
//		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		// Click on the mass button
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available marking fields on the left in Corpus 
		String[] fieldsLists = {"semantic groups","semantic fields"};
		String chosenMF = fieldsLists[randomizer.nextInt(fieldsLists.length)];

		// Chosen field webelement
		WebElement lstBox = null;
		switch(chosenMF)
		{
		case "semantic groups":
			 lstBox = DictionaryTab.lst_SemanticGroups;break;
		case "semantic fields":
			 lstBox = DictionaryTab.lb_dictionary_semantic_fields;
		}
		
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massChangeItsDD, chosenMF);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenMF);
		
		// Get values before change
		List<String> valueB4Change = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values before change: "+valueB4Change);
		
		// Get record POS type code
		String posCode = randomrecord.split("}")[0];
		posCode = posCode.split("\\{")[1];
		
		// Choose second dropdown value  dd_utility_value
		List<String> dropdownValues = DictionaryTab.getValuesFromApp(DictionaryTab.massToBeDD);
		List<String> relevantDropdownValues = new ArrayList<String>();
		if(chosenMF.equals("semantic groups"))
		{
			for(String str : dropdownValues)
				if(str.substring(0,1).equals(posCode))
					relevantDropdownValues.add(str);
		}
		else
			relevantDropdownValues = dropdownValues;
				
		String chosenDD2 = relevantDropdownValues.get(randomizer.nextInt(relevantDropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		chosenDD2 = chosenDD2.replace(" and "," & ");
		
		// Click on change
		Thread.sleep(3000);
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button");
		
		try {
			DictionaryTab.tabDictionary.click();		
			LogUltility.log(test, logger, "Click Dictionary to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		// Click on Yes
		DictionaryTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click yes");
	
		try {
			Thread.sleep(3000);
			boolean windowCon = DictionaryTab.popupWindowMessageOutside(test, logger, "You have unsaved changes in the corpus, would you like to save them? Click 'Yes' to save, click 'No' for the corpus to be overriden.");
			if(windowCon)
				DictionaryTab.No.click();
			}
			catch(Exception e)
				{}
			
		try {
			CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		// Get values after change
		List<String> valueAfterChange = DictionaryTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values after change in dropdown: "+valueAfterChange);
		
		// Check if the new value is added alongside old values and old values still appear
		if(chosenMF.equals("semantic groups")) chosenDD2 = chosenDD2.substring(2);
		boolean isAdded =  valueAfterChange.contains(chosenDD2) && valueAfterChange.containsAll(valueB4Change);
		LogUltility.log(test, logger, "The value is added: "+isAdded);
		assertTrue(isAdded);
		
		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//						
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Ok on the confirmation message");
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--98:Check Cancel button in "New" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_98_Check_Cancel_button_in_New_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--98", "Check Cancel button in \"New\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Get the popup warning
		boolean errorPopup = DictionaryTab.popupWindowMessage(test,logger,"Dictionary Manager Message:","Adding new record to the dictionary. Are you sure?\r\rPlease note: the process will search for completing data online, but it will be only partial!\rIf you want to complete the full data, use the 'd-com' button");
				
		// Compare expected and actual message
		LogUltility.log(test, logger, "Dictionary Manager message is displayed: "+errorPopup);
		assertTrue(errorPopup);

		// press no button
		DictionaryTab.No.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--99:Check Cancel button in "Revert" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_99_Check_Cancel_button_in_Revert_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--99", "Check Cancel button in \"Revert\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.bt_revert.click();
		LogUltility.log(test, logger, "Click the revert button");
		
		// Get the popup warning
		boolean revertPopup = DictionaryTab.popupWindowMessage(test,logger,"Revert File","REVERT action will REMOVE ALL your changes so far (excluding rules and prosody)\r\rAre you sure?");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Revert File popup is displayed: "+revertPopup);
		assertTrue(revertPopup);

		// press no button
		DictionaryTab.Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--100:Check Cancel button in "Delete" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_100_Check_Cancel_button_in_Delete_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--100", "Check Cancel button in \"Delete\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		// Get the popup warning
		boolean deletePopup = DictionaryTab.popupWindowMessage(test,logger,"DictionaryForm_Version Manager Message:","You asked to delete this record from the dictionary. Are you sure?");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Dictionary Form popup is displayed: "+deletePopup);
		assertTrue(deletePopup);

		// press no button
		DictionaryTab.No.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--101:Check Cancel button in "Duplicate" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_101_Check_Cancel_button_in_Duplicate_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--101", "Check Cancel button in \"Duplicate\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.bt_duplicate.click();
		LogUltility.log(test, logger, "Click the duplicate button");
		
		// Get the popup warning
		boolean duplicatePopup = DictionaryTab.popupWindowMessage(test,logger,"DictionaryForm_Version Manager Message:","This dictionary Record will be duplicated. Are you sure?\r\r");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Dictionary Form popup is displayed: "+duplicatePopup);
		assertTrue(duplicatePopup);

		// press no button
		DictionaryTab.No.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--102:Check Cancel button in "Merge" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_102_Check_Cancel_button_in_Merge_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--102", "Check Cancel button in \"Merge\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.bt_merge_files.click();
		LogUltility.log(test, logger, "Click the merge button");
		
		// Get the popup warning
		boolean mergePopup = DictionaryTab.popupWindowMessage(test, logger,"the records of the Dictionary file ");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Merge popup is displayed: "+mergePopup);
		assertTrue(mergePopup);

		// press no button
		DictionaryTab.btnpopupCancelWindow.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--103:Check Cancel button in "Attach" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_103_Check_Cancel_button_in_Attach_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--103", "Check Cancel button in \"Attach\" pop up");
		
		if(Setting.Language.equals("HE")) return;

		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		int randomIndex = randomizer.nextInt(recordsList.size());
		String randomrecord = recordsList.get(randomIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records,randomrecord);
		
		// Click attach button
		DictionaryTab.bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");

		List<String> chosenRecord = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_records);
		
		// Get the popup warning
		boolean attachPopup = DictionaryTab.popupWindowMessage(test, logger,"Attachment Message:","This record's key "+chosenRecord.get(0)+" is now in the clipboard.\r\rNavigate to the record to which you want to attach it, and the click the 'attach' button again.\r\rIf you want to cancel this operation, shift-click the button.");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Attach popup is displayed: "+attachPopup);
		assertTrue(attachPopup);

		// press no button
		DictionaryTab.Cancel.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--104:Check Cancel button in "Mass Attribute" pop up
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_104_Check_Cancel_button_in_Mass_Attribute_popup() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--104", "Check Cancel button in \"Mass Attribute\" pop up");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Get the popup warning
		boolean massPopup = DictionaryTab.popupWindowMessageOutside(test, logger,"For each record in the retrieved\r records list:");
		// Compare expected and actual message
		LogUltility.log(test, logger, "Mass popup is displayed: "+massPopup);
		assertTrue(massPopup);

		// press no button
		DictionaryTab.btnpopupCancelWindow.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--193:Verify user can revert after duplicate a record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_193_Verify_user_can_revert_after_duplicate_a_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--193", "Verify user can revert after duplicate a record");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Search in order to have less number of sentences
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
				
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		//Type in the search field
		
		Random randomizer = new Random();
		int number = randomizer.nextInt(10);
		String searchWord = Integer.toString(number);
//		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
		if (Setting.Language.equals("EN"))
			searchWord = Integer.toString(number);
		else if (Setting.Language.equals("HE"))
			searchWord= "לאומ";
		DictionaryTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field:"+searchWord);
		
		
		
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "all records is chosen in the all/next dropdown");
   			
		// Click the retrieve button
   		Thread.sleep(3000);
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		DictionaryTab.btnRetrieve_same.click();
		
   		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose first record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);
		
		// Click Duplicate
		DictionaryTab.bt_duplicate.click();
		LogUltility.log(test, logger, "Click the duplicate button");
		
		// Click Yes
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// Click retrive again to update list
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button again");
		DictionaryTab.btnRetrieve_same.click();
		
		// Get records list
   		List<String>retrivedRecordsAfterDuplicate = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		String duplicateValue = retrivedRecordsAfterDuplicate.get(retrivedRecordsAfterDuplicate.size()-1);
   		LogUltility.log(test, logger, "Retrive the duplicated record: " + duplicateValue);
		
		// Click the revert button
		CurrentPage.As(DictionaryTab.class).bt_revert.click();
		LogUltility.log(test, logger, "Click the revert button");
		
		// Click Yes on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		// Get records list again
   		List<String>retrivedRecordsAfterRevert = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		LogUltility.log(test, logger, "Retrived records after revert: " + retrivedRecordsAfterRevert);
   		
   		// Check that the list after revert doesn't contain the duplicated value
   		boolean duplicateReverted = !retrivedRecordsAfterRevert.contains(duplicateValue);
   		LogUltility.log(test, logger, "Duplicated value reverted: " + duplicateReverted);
   		assertTrue(duplicateReverted);
   		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--215:Verify DICs create new version when user creating exist record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_215_Verify_DICs_create_new_version_when_user_creating_exist_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--215", "Verify DICs create new version when user creating exist record");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
				
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Not same as Part of Speech list values
		posList.remove("verb_particle");
		posList.remove("clause_opener");
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
   		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose random record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);	
		
		// Get clean record name
		String cleanRecord = chosenValue.split("}")[1];
		String recordName = cleanRecord.split("~")[0];
		String recordVersion = cleanRecord.split("~")[1];
		
		// Click new button
		DictionaryTab.btnNew.click();
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click new button ");
		
		// Insert name in key form
		DictionaryTab.txtKeyForm.sendKeys(recordName);
		LogUltility.log(test, logger, "Insert Key Form: "+recordName);
		
		// Choose same POS
		LogUltility.log(test, logger, "Choose same POS: "+randompos);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPOS, randompos);
			
		// Click yes button if warning message was displayed
		try {
			boolean popupDisplayed = DictionaryTab.popupWindowMessage(test,logger,"Warning: similar key exists:","It looks like there is already an entry with same language, p.o.s. and form.\r\rClick YES to proceed - a new version of the key will be created\rClick NO  to change the value of one of the key fields\rClick CANCEL to abort\r");
			if(popupDisplayed)
				DictionaryTab.btnYes.click();
			}
		catch(NullPointerException e)
			{}

		// Choose Volume POS again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		
		// Get records list after adding new value
   		List<String> retrivedRecordsAfterNew = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		String newVersionValue = retrivedRecordsAfterNew.get(retrivedRecordsAfterNew.size()-1);
   		LogUltility.log(test, logger, "New added value: " + newVersionValue);
   		
   		// Get new record version
		String cleanNewRecord = newVersionValue.split("}")[1];
		String newRecordVersion = cleanNewRecord.split("~")[1];
		boolean isDifferent = !recordVersion.equals(newRecordVersion);
		LogUltility.log(test, logger, "New added value version: " + recordVersion +" is different than old value version: "+recordVersion);
		LogUltility.log(test, logger, "Compare result: "+isDifferent);	
		assertTrue(isDifferent);
			
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--258:Verify user can make edits part 2 transcription
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_258_Verify_user_can_make_edits_part2_transcription() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--258", "Verify user can make edits part 2 transcription");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		posList.remove("phrase_ends");
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
   		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose random record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);	
		
		// Get Transcription list
		List<String> transList = DictionaryTab.getValuesFromApp(DictionaryTab.lstTranscriptions);
		LogUltility.log(test, logger, "Transcription list before editing: " + transList);
		
//		// Choose transcription value to edit
//		int transIndex = randomizer.nextInt(transList.size());
//		String chosenTrans = transList.get(transIndex);
//		CommonFunctions.chooseValueInDropdown(DictionaryTab.lstTranscriptions, chosenTrans);
//		LogUltility.log(test, logger, "Choose transcription: " + chosenTrans);
		
		// Edit transcription
//		String key = "PND";
		String value = DictionaryTab.RandomString(3);
		DictionaryTab.editTranscriptions(test,logger);
//		LogUltility.log(test, logger, "Insert new key: " + key+" value: "+value);
		
		// Get edited transcription list
		// Get Transcription list
		List<String> transEditedList = DictionaryTab.getValuesFromApp(DictionaryTab.lstTranscriptions);
		LogUltility.log(test, logger, "Transcription edited list: " + transEditedList);
		
		// Choose the edited transcription value
//		String chosenEditedTrans = transEditedList.get(transIndex);
//		LogUltility.log(test, logger, "Transcription edited value: " + chosenEditedTrans);
		
		// Compare transcription was modified
		boolean isChanged = !transList.equals(transEditedList);
		LogUltility.log(test, logger, "Transcription was edited: " + isChanged);
		assertTrue(isChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--216:Verify application mark the part of speech dropdown if the user created an exist record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_216_Verify_application_mark_the_part_of_speech_dropdown_if_the_user_created_an_exist_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--216", "Verify application mark the part of speech dropdown if the user created an exist record");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get records names with version zero
		List<String> records = DictionaryTab.getRecordsFromWithVersionZero();
		
		// Choose random record to search for
		Random randomizer = new Random();
		String searchValue = records.get(randomizer.nextInt(records.size()));
		
		// Search for value
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
				
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		//Type in the search field
		DictionaryTab.cmpCondition_value.sendKeys(searchValue);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Click the Retreive button
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);
		
		// Get record name from key form textbox
		String recordName = DictionaryTab.txtKeyForm.getText();
		
		// Get record POS 
		String posValue = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpPOS).get(0);
		
		// Click the new button
		DictionaryTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		DictionaryTab.txtKeyForm.sendKeys(recordName);
		LogUltility.log(test,logger,"Insert Key Form: "+recordName);
		
		// Choose POS
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPOS, posValue);
		LogUltility.log(test,logger,"Choose POS: "+posValue);
		
		boolean popupDisplayed = DictionaryTab.popupWindowMessage(test,logger,"Warning: similar key exists:","It looks like there is already an entry with same language, p.o.s. and form.\r\rClick YES to proceed - a new version of the key will be created\rClick NO  to change the value of one of the key fields\rClick CANCEL to abort\r");
		assertTrue(popupDisplayed);
		
		// Click No
		DictionaryTab.No.click();
		LogUltility.log(test,logger,"Click No");
		
		// Compare if highlighted
		String actualColor = CommonFunctions.getColor(DictionaryTab.cmpPOS);
		boolean colorMatch = actualColor.equals("Blue");
		LogUltility.log(test,logger,"The actual color: "+actualColor+" Matchs the expected: Blue");
		assertTrue(colorMatch);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--217:Verify user can cancel creating an exist record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_217_Verify_user_can_cancel_creating_an_exist_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--217", "Verify user can cancel creating an exist record");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get records names with version zero
		List<String> records = DictionaryTab.getRecordsFromWithVersionZero();
		
		// Choose random record to search for
		Random randomizer = new Random();
		String searchValue = records.get(randomizer.nextInt(records.size()));
		
		// Search for value
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
				
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		//Type in the search field
		DictionaryTab.cmpCondition_value.sendKeys(searchValue);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Click the Retreive button
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);
		
		// Get record name from key form textbox
		String recordName = DictionaryTab.txtKeyForm.getText();
		
		// Get record POS 
		String posValue = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpPOS).get(0);
		
		// Click the new button
		DictionaryTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		DictionaryTab.txtKeyForm.sendKeys(recordName);
		LogUltility.log(test,logger,"Insert Key Form: "+recordName);
		
		// Choose POS
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPOS, posValue);
		LogUltility.log(test,logger,"Choose POS: "+posValue);
		
		boolean popupDisplayed = DictionaryTab.popupWindowMessage(test,logger,"Warning: similar key exists:","It looks like there is already an entry with same language, p.o.s. and form.\r\rClick YES to proceed - a new version of the key will be created\rClick NO  to change the value of one of the key fields\rClick CANCEL to abort\r");
		assertTrue(popupDisplayed);
		
		// Click No
		DictionaryTab.Cancel.click();
		LogUltility.log(test,logger,"Click Cancel");
		
		// Compare if highlighted
		String chosenAfterCancel = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_records).get(0);
		boolean sameChosenValue = chosenAfterCancel.equals(chosenValue);
		LogUltility.log(test, logger, "Same record is chosen: "+sameChosenValue);
		assertTrue(sameChosenValue);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--242:Check adding a value to a listbox with the mass attribute part 1 No
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_242_Check_adding_a_value_to_a_listbox_with_the_mass_attribute_part1_No() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--242", "Check adding a value to a listbox with the mass attribute part 1 No");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		//String randompos = posList.get(randomizer.nextInt(posList.size()));
		String randompos = "adadjective";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
		// Get POS symbol
		String posSymbol = DictionaryTab.POSConverter(randompos);
		LogUltility.log(test, logger, "POS symbol: " + posSymbol);
		
		// Click the mass attribute button
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the mass attribute button");
		
		// Choose semantic groups
		String category = "semantic groups";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massChangeItsDD,category);
		LogUltility.log(test, logger, "Set change its dropdown to: "+category);
		
		// Get all values in to be dropdown
		List<String> massListValues = DictionaryTab.getValuesFromApp(DictionaryTab.massToBeDD);
		List<String> availableSGvalues = new ArrayList<String>();
		
		// Get relevant POS values
		for(int i=0;i<massListValues.size();i++)
			if(massListValues.get(i).charAt(0) == posSymbol.toLowerCase().charAt(0))
				availableSGvalues.add(massListValues.get(i));
		
		// Choose new semantic group value
		String chosenSG = availableSGvalues.get(randomizer.nextInt(availableSGvalues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massToBeDD,chosenSG);
		LogUltility.log(test, logger, "Set to be dropdown to: "+chosenSG);
		
		// Click change
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button ");
		
		// Click No button
		DictionaryTab.No.click();
		LogUltility.log(test, logger, "Click No button ");
		
		try {
			// Click again
			CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
			LogUltility.log(test, logger, "click to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}		
		
		try {
		boolean windowCon = DictionaryTab.popupWindowMessageOutside(test, logger, "You have unsaved changes in the corpus, would you like to save them? Click 'Yes' to save, click 'No' for the corpus to be overriden.");
		if(windowCon)
			DictionaryTab.No.click();
		}
		catch(Exception e)
			{}

		// Click again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "click to refresh");
					
		// Retrieve selected semantic groups
		List<String> selectedValues = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "Selected values: " + selectedValues);

		// Compare result
		boolean onlyOneSG = selectedValues.size() == 1;
		LogUltility.log(test, logger, "Only one semantic group value: " + onlyOneSG);
		assertTrue(onlyOneSG);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--242:Check adding a value to a listbox with the mass attribute part 2 Yes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_242_Check_adding_a_value_to_a_listbox_with_the_mass_attribute_part2_Yes() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--242", "Check adding a value to a listbox with the mass attribute part 2 Yes");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		//String randompos = posList.get(randomizer.nextInt(posList.size()));
		String randompos = "adadjective";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
		// Get POS symbol
		String posSymbol = DictionaryTab.POSConverter(randompos);
		LogUltility.log(test, logger, "POS symbol: " + posSymbol);
		
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Check mass effect on two records
		// Pick two random records
		int firstRandomIndex = randomizer.nextInt(recordsList.size());
		int secondRandomIndex;
		String randomRecord1 = recordsList.get(firstRandomIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord1);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord1);
		
		// unselect all values
		CommonFunctions.unselectValuesFromList(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "unselect values for the first record");
		
		// select semantic group
		String chosenSG = "downtoning";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups,chosenSG);
		LogUltility.log(test, logger, "select value for the first record: "+chosenSG);
		
		// Make sure to pick two different records
		do {secondRandomIndex = randomizer.nextInt(recordsList.size());}
			while(firstRandomIndex == secondRandomIndex);
		
		String randomRecord2 = recordsList.get(secondRandomIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord2);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord2);
		
		// unselect all values
		CommonFunctions.unselectValuesFromList(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "unselect values for the second record");
	
		// select semantic group
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups,chosenSG);
		LogUltility.log(test, logger, "select value for the second record: "+chosenSG);
		
		// For sync purpose
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord1);
		
		// Click save button
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		
		try {
			// Click again
			CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
			LogUltility.log(test, logger, "click to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}	
		
		// Click the mass attribute button
		DictionaryTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the mass attribute button");
		
		// Choose semantic groups
		String category = "semantic groups";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massChangeItsDD,category);
		LogUltility.log(test, logger, "Set change its dropdown to: "+category);
		
		// Get all values in to be dropdown
		List<String> massListValues = DictionaryTab.getValuesFromApp(DictionaryTab.massToBeDD);
		List<String> availableSGvalues = new ArrayList<String>();
		
		// Get relevant POS values
		for(int i=0;i<massListValues.size();i++)
			if(massListValues.get(i).charAt(0) == posSymbol.toLowerCase().charAt(0))
				if(!massListValues.get(i).contains(chosenSG))
					availableSGvalues.add(massListValues.get(i));
		
		// Choose new semantic group value
		String choseNewSG = availableSGvalues.get(randomizer.nextInt(availableSGvalues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.massToBeDD,choseNewSG);
		LogUltility.log(test, logger, "Set to be dropdown to: "+choseNewSG);
		
		// Click change
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button ");
		
		// Click yes button
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click Yes button ");
		
		try {
			// Click again
			CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
			LogUltility.log(test, logger, "click to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}		
		
		// Select first record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord1);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord1);
		
		// Retrieve selected semantic groups
		List<String> selectedValues1 = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "Selected values: " + selectedValues1);
		
		// select second record again
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord2);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord2);
		
		// Retrieve selected semantic groups
		List<String> selectedValues2 = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
		LogUltility.log(test, logger, "Selected values: " + selectedValues2);
		
		// Compare
		boolean foundInBoth = selectedValues1.contains(choseNewSG.substring(2)) && selectedValues2.contains(choseNewSG.substring(2));
		LogUltility.log(test, logger, "New semantic group value has been attatched for both records: " + foundInBoth);
		assertTrue(foundInBoth);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	



/**
 * DIC_TC--340:Check the semantic groups have multiple groups of values in each POS
 * @throws InterruptedException 
 * @throws AWTException 
 * @throws IllegalArgumentException 
 * @throws NoSuchFieldException 
 * @throws IllegalAccessException 
 * @throws SecurityException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_340_Check_the_semantic_groups_have_multiple_groups_of_values_in_each_POS() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
{
	test = extent.createTest("Dictionary Tests - DIC_TC--340", "Check the semantic groups have multiple groups of values in each POS");
	
	// Initialize current page
	CurrentPage = GetInstance(DictionaryTab.class);
	DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
	
	// Click Dictionary tab
	DictionaryTab.tabDictionary.click();
	
	// Get random POS
	List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random POS
//	Random randomizer = new Random();
//	int firstRandomIndex = randomizer.nextInt(posList.size());
//	int secondRandomIndex;
//	String randompos = posList.get(firstRandomIndex);
	String randompos= "adadjective";
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Make sure to pick two different records
//	do {secondRandomIndex = randomizer.nextInt(posList.size());}
//		while(firstRandomIndex == secondRandomIndex);
	
	// Get semantic groups list
	List<String> SGB4change = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
	LogUltility.log(test, logger, "Semantic group list: " + SGB4change);
	
	// Navigate for the new POS list
//	String newPOS = posList.get(secondRandomIndex);
	String newPOS = "adverb";
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, newPOS);
	LogUltility.log(test, logger, "Choose from pos list: " + newPOS);
	
	// Get semantic groups list
	List<String> SGAfterchange = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
	LogUltility.log(test, logger, "Semantic group list: " + SGAfterchange);
	
	// Verify that semantic groups list is different
	boolean isChanged = !SGAfterchange.equals(SGB4change);
	LogUltility.log(test, logger, "The semantic group has been changed accordingly: " + isChanged);
	assertTrue(isChanged);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}

	/**
	 * DIC_TC--258:Verify user can save changes in Dictionary part 1 dropdowns
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_258_Verify_user_can_save_changes_in_Dictionary_part1_dropdowns() throws InterruptedException, IOException
	{	
		test = extent.createTest("Dictionary Tests - DIC_TC--258", "Verify user can save changes in Dictionary part 1 dropdowns");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		//String randompos= "adadjective";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get record info from file
		HashMap<String, String> recordInfoB4Change = DictionaryTab.getRegularRecordsInfoFromDictionary(randomrecord);
		LogUltility.log(test, logger, "Record info from file: " + recordInfoB4Change);
		
		// get the dropdowns values before changing 
		String oldOrigin = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpOrigin).get(0);
		String oldFrequency = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpFrequency).get(0);
//		String oldPOS = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpPOS).get(0);
		String oldLanguage = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpLanguage).get(0);
		String oldStyle = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpStyle).get(0);
		String oldAcronym = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpAcronym).get(0);
		String oldRegister = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpRegister).get(0);
		String oldDependency = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpDependency).get(0);
		String oldRecordStatus = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpRecordStatus).get(0);
		String oldGender = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpGender).get(0);
		String oldNumber = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpNumber).get(0);
		String oldDefiniteness = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpDefiniteness).get(0);
		String oldDeclinability = DictionaryTab.getChosenValueInDropdown(DictionaryTab.cmpDeclinability).get(0);
		
		// Get available lists values
		List<String> originValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpOrigin);
		List<String> freqValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpFrequency);
		List<String> posValues = DictionaryTab.checkPOSValidity(randompos);
		List<String> languageValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpLanguage);
		List<String> styleValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpStyle);
		List<String> acronymValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpAcronym);
		List<String> registerValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpRegister);
		List<String> depValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpDependency);
		List<String> recordStatusValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpRecordStatus);
		List<String> genderValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpGender);
		List<String> numberValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpNumber);
		List<String> defValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpDefiniteness);
		List<String> decValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpDeclinability);
		
		// Remove old value
		originValues.remove(oldOrigin);
		freqValues.remove(oldFrequency);
		languageValues.remove(oldLanguage);
		styleValues.remove(oldStyle);
		acronymValues.remove(oldAcronym);
		registerValues.remove(oldRegister);
		depValues.remove(oldDependency);
		recordStatusValues.remove(oldRecordStatus);
		genderValues.remove(oldGender);
		numberValues.remove(oldNumber);
		defValues.remove(oldDefiniteness);
		decValues.remove(oldDeclinability);
		
		// Pick new random values
		Random random = new Random();
		String newOrigin = originValues.get(random.nextInt(originValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpOrigin, newOrigin);
		LogUltility.log(test, logger, "Choose new value: " + newOrigin);
		
		String newFreq = freqValues.get(random.nextInt(freqValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpFrequency, newFreq);
		LogUltility.log(test, logger, "Choose new value: " + newFreq);
		
		String newPOS = posValues.get(random.nextInt(posValues.size()));
		newPOS = newPOS.replace("_", " ");
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPOS,newPOS );
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Choose new value: " + newPOS);
		
		String newLanguage = languageValues.get(random.nextInt(languageValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpLanguage, newLanguage);
		LogUltility.log(test, logger, "Choose new value: " + newLanguage);
		
		String newStyle = styleValues.get(random.nextInt(styleValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpStyle, newStyle);
		LogUltility.log(test, logger, "Choose new value: " + newStyle);
		
		String newAcronym = acronymValues.get(random.nextInt(acronymValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpAcronym, newAcronym);
		LogUltility.log(test, logger, "Choose new value: " + newAcronym);
		
		String newRegister = registerValues.get(random.nextInt(registerValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpRegister, newRegister);
		LogUltility.log(test, logger, "Choose new value: " + newRegister);
		
		String newDependancy = depValues.get(random.nextInt(depValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpDependency, newDependancy);
		LogUltility.log(test, logger, "Choose new value: " + newDependancy);
		
		String newRecordStatus = recordStatusValues.get(random.nextInt(recordStatusValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpRecordStatus, newRecordStatus);
		LogUltility.log(test, logger, "Choose new value: " + newRecordStatus);
		
		String newGender = genderValues.get(random.nextInt(genderValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpGender, newGender);
		LogUltility.log(test, logger, "Choose new value: " + newGender);
		
		// Pick one of two number values {all,irrelevant}
		String newNumber = oldNumber.equals("all") ? "irrelevant" : "all";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpNumber, newNumber);
		LogUltility.log(test, logger, "Choose new value: " + newNumber);
		
		String newDefiniteness = defValues.get(random.nextInt(defValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpDefiniteness, newDefiniteness);
		LogUltility.log(test, logger, "Choose new value: " + newDefiniteness);
		
		String newDeclinability = decValues.get(random.nextInt(decValues.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpDeclinability, newDeclinability);
		LogUltility.log(test, logger, "Choose new value: " + newDeclinability);
		
		// Click save button
		DictionaryTab.btnSave.click();
		LogUltility.log(test, logger, "Click the save button");
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click OK ");
		
	
		try {
			// Click again
			CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
			LogUltility.log(test, logger, "click to refresh");
		} catch (Exception e) {
			// e.printStackTrace();
		}	
		
		
		// Get record info from file
		HashMap<String, String> recordInfoAfterChange = DictionaryTab.getRegularRecordsInfoFromDictionary(randomrecord);
		LogUltility.log(test, logger, "Record info from file: " + recordInfoAfterChange);
		
		// Compare dropdown values are changed 
		boolean allValuesChangedApp = true;
		boolean allValuesChangedFile = true;
		// App comparison
		if(oldRecordStatus.equals(newRecordStatus)) allValuesChangedApp=false;
		if(oldRegister.equals(newRegister)) allValuesChangedApp=false;
		if(oldAcronym.equals(newAcronym)) allValuesChangedApp=false;
		if(oldStyle.equals(newStyle)) allValuesChangedApp=false;
		if(oldOrigin.equals(newOrigin)) allValuesChangedApp=false;
		if(oldFrequency.equals(newFreq)) allValuesChangedApp=false;
		if(oldDefiniteness.equals(newDefiniteness)) allValuesChangedApp=false;
		if(oldDeclinability.equals(newDeclinability)) allValuesChangedApp=false;
		if(oldDependency.equals(newDependancy)) allValuesChangedApp=false;
		if(oldNumber.equals(newNumber)) allValuesChangedApp=false;
		if(oldGender.equals(newGender)) allValuesChangedApp=false;
		if(oldLanguage.equals(newLanguage)) allValuesChangedApp=false;
//		if(oldPOS.equals(newPOS)) allValuesChangedApp=false;
		LogUltility.log(test, logger, "All values has been changed in App: " + allValuesChangedApp);
		assertTrue(allValuesChangedApp);
		
		// File comparison
		if(recordInfoB4Change.get("record status").equals(recordInfoAfterChange.get("record status"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("register").equals(recordInfoAfterChange.get("register"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("acronym").equals(recordInfoAfterChange.get("acronym"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("style").equals(recordInfoAfterChange.get("style"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("origin").equals(recordInfoAfterChange.get("origin"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("frequency").equals(recordInfoAfterChange.get("frequency"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("definiteness").equals(recordInfoAfterChange.get("definiteness"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("declinability").equals(recordInfoAfterChange.get("declinability"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("dependency").equals(recordInfoAfterChange.get("dependency"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("number").equals(recordInfoAfterChange.get("number"))) allValuesChangedFile=false;
		if(recordInfoB4Change.get("gender").equals(recordInfoAfterChange.get("gender"))) allValuesChangedFile=false;
		
		LogUltility.log(test, logger, "All values has been changed in File: " + allValuesChangedFile);
		assertTrue(allValuesChangedFile);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}

	
	/**
	 *  DIC_TC--506:Check create a new record, generate lexicon then delete the new record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_506_Check_create_a_new_record_generate_lexicon_then_delete_the_new_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--506", "Check create a new record, generate lexicon then delete the new record");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
				
		// Initialize lexicon page
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Click Dictionary tab
		DictionaryTab.dictionaryTab.click();
		
		// Click the new button
		DictionaryTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = DictionaryTab.RandomString(5);
		DictionaryTab.txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Navigate to lexicon tab
		LexiconTab.LexiconTab.click();
		LogUltility.log(test, logger, "Navigate to Lexicon");
		
		//Type in the search field
		LexiconTab.cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click retrieve
		LexiconTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Click generate
		LexiconTab.bt_generate_lexicon.click();
		LexiconTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click the generate button");
		
		// Navigate back to dictionary
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Navigate back to Dictionary");
////			DictionaryTab.dictionaryTab.click();
//			
//			System.out.println("start");
////			DictionaryTab.dictionaryTab.click();
////			DictionaryTab.dictionaryTab.click();
//			System.out.println("mid");
//			LogUltility.log(test, logger, "Navigate to Dictionary");
////			
////			
//			Thread.sleep(3000);
////			CommonFunctions.chooseValueInDropdown(LexiconTab.cmpVolumePOS, "A");
//			System.out.println("choose A");
//			try {
//				CommonFunctions.clickTabs("Dictionary");
//			} catch (Exception e) {
//				// e.printStackTrace();
//			}
//			CommonFunctions.clickTabs("Dictionary");
//			System.out.println("click tab");
//			LogUltility.log(test, logger, "Navigate to Dictionary");
//		} catch (Exception e) {
//			System.out.println(e);
//		}	
		
		// Search for the record
		DictionaryTab.cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click retrieve
		LexiconTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// click on the record
		String chosenRecord = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records).get(0);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, chosenRecord);
		LogUltility.log(test, logger, "Choose the record");
		
		// Click delete
		DictionaryTab.bt_delete.click();
		LogUltility.log(test, logger, "Click delete button");
		
		// Click yes
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click Yes button");
		
		// Check that record deleted
		String textToCheck = "Record "+chosenRecord +" was successfully removed";
		boolean deleteWindow = DictionaryTab.popupWindowMessage(test, logger, "", textToCheck);
		assertTrue(deleteWindow);
		
		// Check if it's not found in file
		boolean deletedFromFile = DictionaryTab.getRegularRecordsInfoFromDictionary(chosenRecord).size() == 0;
		LogUltility.log(test, logger, "The record is not found in file: "+deletedFromFile);
		assertTrue(deletedFromFile);
		
		 DictionaryTab.btnOk.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--600:Grammar Attributes - Check adding Grammar Attributes to prefix, suffix
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_600_Grammar_Attributes_Check_adding_Grammar_Attributes_to_prefix_suffix() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC--600", "Grammar Attributes - Check adding Grammar Attributes to prefix, suffix");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		Random randomizer = new Random();
		String[] pos = {"suffix","prefix"};
		String randomPOS = pos[randomizer.nextInt(pos.length)];
		
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randomPOS);
		LogUltility.log(test, logger, "Choose from pos list: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record	
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Click the blue add attribute button
		DictionaryTab.bt_add_grammar_attribute.click();
		LogUltility.log(test, logger, "Click add grammar attribute button");
		
		// Choose attribute from the dropdown
		String[] attribute = {"Attach To","Change To"};
		String randomAtt = attribute[randomizer.nextInt(attribute.length)];

		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute, randomAtt);
		LogUltility.log(test, logger, "Choose from attribute list: " + randomAtt);
		
		// Get POS values
		List<String> posValuesAtt = DictionaryTab.getValuesFromApp(DictionaryTab.cb_attribute_4);
		List<String> chosenValues = DictionaryTab.getGrammarAttributeValues();
		
		// Make sure not to choose already chosen value
		List<String> alreadySelectedValues = new ArrayList<String>();
		for(int i=0;i<chosenValues.size();i++)
		{
			String splitter = ":";
			String value = chosenValues.get(i).split(splitter)[1];
			
			if(randomAtt.equals("Attach To"))
			{
				value = value.split(" \\(")[0];
				alreadySelectedValues.add(value);
				
			}
			else
				alreadySelectedValues.add(value);
		}
		
		posValuesAtt.removeAll(alreadySelectedValues);
		String randomPOSAtt = posValuesAtt.get(randomizer.nextInt(posValuesAtt.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute_4, randomPOSAtt);
		LogUltility.log(test, logger, "Choose from POS list: " + randomPOSAtt);
		
		// Choose all
		DictionaryTab.bt_all.click();
		LogUltility.log(test, logger, "Click all button" );
		
		// Click confirm
		DictionaryTab.bt_confirm.click();
		LogUltility.log(test, logger, "Click confirm button" );
		
		// Get grammar attributes list after adding
		List<String> posValuesAfterAdd = DictionaryTab.getGrammarAttributeValues();
		boolean isAdded = false;
		for(int i=0;i<posValuesAfterAdd.size();i++)
		{
			isAdded = posValuesAfterAdd.get(i).contains(randomPOSAtt);
			if(isAdded) break;
		}
		
	
		LogUltility.log(test, logger, "The grammar attribute has been added: " + isAdded);
		assertTrue(isAdded);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--604:Tagged record � Check the abort button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_604_Tagged_record_Check_the_abort_button() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--604", "Tagged record � Check the abort button");
		
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		CurrentPage = GetInstance(CompoundsTab.class);
		CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
		

		//getTaggingValues lemma 0
		// choose record then go to dicionary
		// Get tagged compounds from file
		List<String> taggedRecords = CompoundsTab.getTaggedWordsFromFile();
				
		// Click CompoundsTab 
		CompoundsTab.CompoundsTab.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		Random random = new Random();
		String searchWord = taggedRecords.get(random.nextInt(taggedRecords.size()));
		CompoundsTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word "+ searchWord+" is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
   		
		// Click the Retreive button
   		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose record
		List<String> values = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
		String value = values.get(random.nextInt(values.size()));
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records,value);
		LogUltility.log(test, logger, "Choose record: "+value);
		
		
		// Get tagged record from compounds
		String record = CompoundsTab.getTaggingValues("Lemma", 0).get(1);
		LogUltility.log(test, logger, "Get record from lemma: "+record);
		
		// Get clean record
		String choseRecord = record.split(" \\(")[0];
		String splitted[] = record.split("}");
		String cleanRecord = splitted[1].split("~")[0];
		
		// Navigate to Dictionary tab
		DictionaryTab.dictionaryTab.click();
		LogUltility.log(test, logger, "Navigate back to dictionary tab ");
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CompoundsTab.cmpCondition_value.sendKeys(cleanRecord);
		LogUltility.log(test, logger, "word"+ cleanRecord+" is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
   		
		// Click the Retreive button
   		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
				
		// Choose record
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records,choseRecord);
		LogUltility.log(test, logger, "Choose record: "+choseRecord);
					
		// click delete button
		DictionaryTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		// press yes button
		DictionaryTab.btnYes.click();
		LogUltility.log(test, logger, "Click yes button");
		
		String expected = "Should be either left empty or replaced by:";
		boolean popupWindow = DictionaryTab.popupWindowMessage(test, logger, expected);
		assertTrue(popupWindow);
		
		// Click abort
		DictionaryTab.btnpopupCancelWindow.click();
		LogUltility.log(test, logger, "Click abort button");
		
		// Check that the record wasn't deleted
		String chosenVal = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_records).get(0);
		
		boolean notDelete = DictionaryTab.getRegularRecordsInfoFromDictionary(choseRecord).size() > 0 && chosenVal.equals(choseRecord);
		LogUltility.log(test, logger, "The record still exists: "+notDelete);
		assertTrue(notDelete);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--614:Check the relevant proximity rules appear for the selected record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_614_Check_the_relevant_proximity_rules_appear_for_the_selected_record() throws InterruptedException, AWTException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--614", "Check the relevant proximity rules appear for the selected record");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		RulesTab rulesTab = GetInstance(RulesTab.class);

		// Focus on dictionary tab
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Focus on dictionary tab");
		
		// Get relevant records 	
		List<String> proximityRulesRecords = rulesTab.getDisambiguationRlues();
		String record;
		Random randomizer = new Random();
		LogUltility.log(test, logger, "Focus on dictionary tab");
		
		// clean record and search for it
		record = proximityRulesRecords.get(randomizer.nextInt(proximityRulesRecords.size()));
//		record = "HE{p}לְפַנ~0";
		String splitRecord = record.split("}")[1];
		String cleanRecord = splitRecord.split("~")[0];
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		DictionaryTab.cmpCondition_value.sendKeys(cleanRecord);
		LogUltility.log(test, logger, "word "+ cleanRecord+" is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
   		
		// Click the Retreive button
   		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose record from the dropdown	
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, record);
		LogUltility.log(test, logger, "Choose record: "+record);
		
		// Click on the green button in proximity rules
		DictionaryTab.bt_update_rules.click();
		LogUltility.log(test, logger, "Click on proximity rules green button");
		
		// Retrieve the list
		List<String> proximityRules = DictionaryTab.getValuesFromApp(DictionaryTab.proximityRulesList);
		LogUltility.log(test, logger, "Retrieved proximity rules: "+proximityRules);
		
		// Check if displayed rules are relevant and contains the record
//		for(int i=0;i<proximityRules.size();i++)
//			if( ! proximityRules.get(i).contains(record)) 		{	relevantRules=false;	break;}
		
		for(int i=0;i<proximityRules.size();i++)
		{
			boolean relevantRules = false;
			String ruleNumber = proximityRules.get(i).split(" : ")[0].substring(2).trim();
			List<String> ruleConditionKeys = rulesTab.getDisambiguationRlueConditionsKeys(ruleNumber);
			for(int j=0;j<ruleConditionKeys.size();j++)	
			{
				if(ruleConditionKeys.get(j).contains(record))
					{
						relevantRules=true;	
						LogUltility.log(test, logger, "Rule : "+proximityRules.get(i)+" ,Contains the record : "+relevantRules);
						break;
					}
			}
			if(!relevantRules)
			{
				LogUltility.log(test, logger, "Rule : "+proximityRules.get(i)+" ,Contains the record : "+relevantRules);
				assertTrue(relevantRules);
			}
		}
		
			
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--615 : Navigate to proximity rules from dictionary tab
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_615_Navigate_to_proximity_rules_from_dictionary_tab() throws InterruptedException, AWTException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--615", "Navigate to proximity rules from dictionary tab");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get relevant records 	
		RulesTab rulesTab = GetInstance(RulesTab.class);;
		List<String> proximityRulesRecords = rulesTab.getDisambiguationRlues();
		String record;
		Random randomizer = new Random();	
		
		// clean record and search for it
		record = proximityRulesRecords.get(randomizer.nextInt(proximityRulesRecords.size()));
		String splitRecord = record.split("}")[1];
		String cleanRecord = splitRecord.split("~")[0];
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		DictionaryTab.cmpCondition_value.sendKeys(cleanRecord);
		LogUltility.log(test, logger, "word "+ cleanRecord+" is typed in the search field");
		
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
   		
		// Click the Retreive button
   		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose record from the dropdown	
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, record);
		LogUltility.log(test, logger, "Choose record: "+record);
		
		// Click on the green button in proximity rules
		DictionaryTab.bt_update_rules.click();
		LogUltility.log(test, logger, "Click on proximity rules green button");
		
		// Retrieve the list
		List<String> proximityRules = DictionaryTab.getValuesFromApp(DictionaryTab.proximityRulesList);
		LogUltility.log(test, logger, "Retrieved proximity rules: "+proximityRules);
		
		// Choose value from the list
		String chosenProximityRule = proximityRules.get(randomizer.nextInt(proximityRules.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.proximityRulesList, chosenProximityRule);
		LogUltility.log(test, logger, "Click on: "+chosenProximityRule);
		
		// Eliminate the "D-" substring
		String cleanProximityRule = chosenProximityRule.split("D-")[1];
		LogUltility.log(test, logger, "Choosen value in proximity rules: "+cleanProximityRule);
		
		// Get chosen value from rules tab
		String chosenRuleFromRulesTab = rulesTab.getChosenValueInDropdown(rulesTab.disambiguationRulesList).get(0);
		LogUltility.log(test, logger, "Choosen value in rules tab after navigating: "+chosenRuleFromRulesTab);
		
		// Compare
		boolean correctNavigation = chosenRuleFromRulesTab.equals(cleanProximityRule);	
		LogUltility.log(test, logger, "Equal records, Navigation is working successfully: "+correctNavigation);
		assertTrue(correctNavigation);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--682:Create new record with different steps part 2
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_682_Create_new_record_with_different_steps_part2() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--682", "Create new record with different steps part 2");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
				
		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Type a record name in the key form field  
		String randomText="";
		randomText= CurrentPage.As(DictionaryTab.class).RandomString(5);
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);

		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "form is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Type in the search field
		CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");

			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found: " + equalResult);
		assertTrue(equalResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	
	/**
	 * DIC_TC--665:Check the display of frames
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_665_Check_the_display_of_frames() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--665", "Check the display of frames");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Compounds Tab
		DictionaryTab.dictionaryTab.click();
		LogUltility.log(test, logger, "Click the dictionary Tab ");
		
		// Search for value
		// Choose "frames" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "frames");
		LogUltility.log(test, logger, "frames is chosen in the first search dropdown");
				
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		// choose frame value
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_value, "mutual");
		LogUltility.log(test, logger, "Choose frame value");
		
		// Change the last filter value, in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
		LogUltility.log(test, logger, "all records is chosen in the all/next dropdown");
				
		// Click the Retreive button
		DictionaryTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get chosen frames value from App
		List<String> chosenAppFrames = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_frames);
		LogUltility.log(test, logger, "App chosen frames list: " + chosenAppFrames);

		// Get chosen frames value from file
		List<String> chosenFileFrames = DictionaryTab.getAllFramesOfRecordFromDictionary(randomrecord);
		LogUltility.log(test, logger, "File chosen frames list: " + chosenFileFrames);
		
		String[] chosenFileFramesSplitted  = chosenFileFrames.get(0).split("¦");
		LogUltility.log(test, logger, "File chosen frames list after splitted: " + chosenFileFramesSplitted);
		
		List<String> newlistaftersplit = Arrays.asList(chosenFileFramesSplitted);
		
		System.out.println(Arrays.asList(newlistaftersplit));
		// Compare lists
		boolean sameValues = chosenAppFrames.equals(newlistaftersplit);
		LogUltility.log(test, logger, "Same values appear in both App and file: " + sameValues);
		assertTrue(sameValues);
		
//		///----
//		for (int i = 0; i < chosenFileFramesSplitted.length; i++) {
//			newlistaftersplit.get(i);
//			chosenFileFramesSplitted.g
//		}
//		
//		//----
//		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}


	/**
	 * DIC_TC--12:Check the case sensitive  button in the search form - part1 button disabled
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_12_Check_the_case_sensetive_button_in_the_search_form_part1() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--12", "Check the case sensitive  button in the search form - part 1 button disabled");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Compounds Tab
		DictionaryTab.dictionaryTab.click();
		LogUltility.log(test, logger, "Click the dictionary Tab ");
		
		// Get text to use for search
		List<String> records = DictionaryTab.getRecordsFromDictionary();
		LogUltility.log(test, logger, "Getting clean records from dictionary.txt ");
		
		// Pick random record
		Random random = new Random();
		String randomRecord = records.get(random.nextInt(records.size()));
		LogUltility.log(test, logger, "Record to use: " + randomRecord);
		
		// Disable the case sensitivity button if needed
		if(DictionaryTab.isUppercaseActive())
		{
			LogUltility.log(test, logger, "Disable case sensitivity button ");
			DictionaryTab.bt_condition_uppercase.click();
		}
		
		// Search in dictionary
		DictionaryTab.searchInDictionary(test,logger,"form","equal",randomRecord,"all records");
	
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		    
		// Get clean records from the app list
		ArrayList<String> cleanAppRecords = new ArrayList<String>();
		for(int i=0;i<recordsList.size();i++)
		{
			String[] removeLeft = recordsList.get(i).split("}");
		    String[] removeRight = removeLeft[1].split("~");
		    cleanAppRecords.add(removeRight[0]);
		}
		
		
		// The case sensitivity button is set to 'a' ,it should ignorecase 
		boolean ignoringCase = true;
		for(int i=0;i<cleanAppRecords.size();i++)
			if(!randomRecord.equalsIgnoreCase(cleanAppRecords.get(i)))
				ignoringCase = false;

		assertTrue(ignoringCase);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--12:Check the case sensitive  button in the search form - part2 button enabled
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_12_Check_the_case_sensetive_button_in_the_search_form_part2() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--12", "Check the case sensitive  button in the search form - part 2 button enabled");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Compounds Tab
		DictionaryTab.dictionaryTab.click();
		LogUltility.log(test, logger, "Click the Compounds Tab ");
		
		// Get text to use for search
		List<String> records = DictionaryTab.getRecordsFromDictionary();
		LogUltility.log(test, logger, "Getting clean records from dictionary.txt ");
		
		// Pick random record
		Random random = new Random();
		String randomRecord = records.get(random.nextInt(records.size()));
		LogUltility.log(test, logger, "Record to use: " + randomRecord);
		
		// Enable the case sensitivity button if needed
		if(!DictionaryTab.isUppercaseActive())
		{
			LogUltility.log(test, logger, "Enable case sensitivity button ");
			DictionaryTab.bt_condition_uppercase.click();
		}
		
		// Search in dictionary
		DictionaryTab.searchInDictionary(test,logger,"form","equal",randomRecord,"all records");
	
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		    
		// Get clean records from the app list
		ArrayList<String> cleanAppRecords = new ArrayList<String>();
		for(int i=0;i<recordsList.size();i++)
		{
			String[] removeLeft = recordsList.get(i).split("}");
		    String[] removeRight = removeLeft[1].split("~");
		    cleanAppRecords.add(removeRight[0]);
		}
		
		
		// The case sensitivity button is set to 'A' ,it should not ignore case 
		boolean caseSensitive = true;
		for(int i=0;i<cleanAppRecords.size();i++)
			if(!randomRecord.equals(cleanAppRecords.get(i)))
				caseSensitive = false;

		assertTrue(caseSensitive);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--158:Check process of editing a record's spelling effects in lexicon
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_158_Check_process_of_editing_a_records_spelling_effects_in_lexicon() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_158", "Check process of editing a record's spelling effects in lexicon");
			
		// Initialize dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Initialize lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Get text to use for search
		List<String> records = DictionaryTab.getRecordsFromDictionary();
		LogUltility.log(test, logger, "Getting clean records from dictionary.txt ");
		
		// Pick random record
		Random random = new Random();
		String randomSearchRecord = records.get(random.nextInt(records.size()));
		LogUltility.log(test, logger, "Record to use: " + randomSearchRecord);
				
		// Navigate to lexicon
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Search for value
		LexiconTab.searchInLexicon(test, logger, "form", "equal", randomSearchRecord, "all records");

		// Retrieve records
		List<String> recordsList = LexiconTab.getValuesFromApp(LexiconTab.lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);		

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(LexiconTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get all displayed info
		ArrayList<HashMap<String, String>> lexicalDisplayList = LexiconTab.lexiconDisplayTable();

		// Choose one row and click on the black square to navigate to dictionary
		int index = random.nextInt(lexicalDisplayList.size());	
		LogUltility.log(test, logger, "Click on row number: " + index +" row info: "+lexicalDisplayList.get(index));
		LexiconTab.clickOnDisplayTableRow(index);
		
		// Get the available spellings before adding new one
		List<String> spellingsBeforeList = DictionaryTab.getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list before adding: " + spellingsBeforeList);
		
		// Check if having only one spelling value then add a new one
		int spellings =  spellingsBeforeList.size();
		if (spellings <= 1) {
			// Add new spelling value
			String randomValue = DictionaryTab.RandomString(5);
			// Get the keys list and remove the first unkown value ???
			DictionaryTab.bt_add_spelling.click();
			List<String> spellingsKeysList = DictionaryTab.getValuesFromApp(DictionaryTab.ListBox);
			DictionaryTab.bt_delete_spelling.click();
			spellingsKeysList.remove(0);
			String randomKey = spellingsKeysList.get(random.nextInt(spellingsKeysList.size())).trim();
//			String randomKey = "ACR [acronym]";
			// Choose a key and value for the new transcription
			DictionaryTab.addSpelling(randomKey, randomValue);
			LogUltility.log(test, logger, "Random value: " + randomValue + " - Random key: " + randomKey);
			
			// Get the available spellings after adding a new one
			spellingsBeforeList = DictionaryTab.getSpellinigValues();
			LogUltility.log(test, logger, "Spellings list after adding: " + spellingsBeforeList);
		}

		else {
			// Choose random spellings value and remove it
			String deletedSpelling = DictionaryTab.removeSpelling();
			LogUltility.log(test, logger, "Deleted spelling: " + deletedSpelling);
			
			// Get the available spelling after adding new one
			List<String> spellingsAfterList = DictionaryTab.getSpellinigValues();
			LogUltility.log(test, logger, "Spellings list after deleting: " + spellingsAfterList);
			}
		
		// Navigate back to lexicon
		CommonFunctions.clickTabs("Lexicon");
		LogUltility.log(test, logger, "Navigate to lexicon tab");
		
		// Search for value
		LexiconTab.searchInLexicon(test, logger, "form", "equal", randomSearchRecord, "all records");
		
		// Choose same record again
		CommonFunctions.chooseValueInDropdown(LexiconTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);

		// Get all displayed info after the change
		ArrayList<HashMap<String, String>> lexicalDisplayListAfter = LexiconTab.lexiconDisplayTable();
		
		// Verify that the presentation value has been changed
		LogUltility.log(test, logger, "Presentations after the change: "+lexicalDisplayListAfter.get(index).get("Presentations"));
		LogUltility.log(test, logger, "Presentations before the change: "+lexicalDisplayList.get(index).get("Presentations"));
		boolean changed = !lexicalDisplayListAfter.get(index).get("Presentations").equals(lexicalDisplayList.get(index).get("Presentations"));
		LogUltility.log(test, logger, "The change has been applied: "+changed);
		assertTrue(changed);
		
		// Go to dictionary and revert..
		CommonFunctions.clickTabs("Dictionary");
//		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//		
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Yes on the confirmation message");
				
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--163:Verify user can filter records with Status filter
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_163_Verify_user_can_filter_records_with_Status_filter() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC--163", "Verify user can filter records with Status filter");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click Dictionary Tab
		DictionaryTab.dictionaryTab.click();
		LogUltility.log(test, logger, "Click the Dictionary Tab ");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,20);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
 		// Get the status value dropdown values
 		List<String> statusValues = DictionaryTab.getValuesFromApp(DictionaryTab.cmpRecordStatus);
 		LogUltility.log(test, logger, "Status values from list: "+statusValues);
		
		// Get record status 
 		Random random = new Random();
		String status = statusValues.get(random.nextInt(statusValues.size()));
		
		LogUltility.log(test, logger, "We will change the status value for one record to:"+ status);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// change the selected record status  cmpRecordStatus
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpRecordStatus,status);
 		LogUltility.log(test, logger, "for the selected record : "+ randomrecord +" the status changed to :" +status);
 		
 		// Pick a random record
//		Random randomizer1 = new Random();
		String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord1);
		LogUltility.log(test, logger, "Choose from record list another record to manage tosave: " + randomrecord1);
		
//		// change the Declinability value to be able to save
//		String randomDeclinability = "regular";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDeclinability, randomDeclinability);
//		LogUltility.log(test, logger, "Change the Declinability to be able to save after that, new choosen value is :"+randomDeclinability);
		
 		//Save the changes
		// Click the save button
//		Thread.sleep(3000);
		CurrentPage.As(DictionaryTab.class).btnSave.click();
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Save the changes");
 		LogUltility.log(test, logger, "Start to search records with the required status");
 		
		// Set to status
// 		Thread.sleep(3000);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field,"status");
 		LogUltility.log(test, logger, "Set dropdown to : status");

		    
//		// Get record status 
// 		Random random = new Random();
//		String status = statusValues.get(random.nextInt(statusValues.size()));
		
		// Search in dictionary
		// Choose value from the first search dropdown
 		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field,"status");
 		LogUltility.log(test, logger, "Set dropdown to status");

 		// Choose value from the condition dropdown
 		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
 		LogUltility.log(test, logger, "Set dropdown to equal");

 		//Type in the search field
 		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_value, status);
 		LogUltility.log(test, logger, "Set dropdown to:"+status);
 		
 		// Change the last filter value, in case it was changed by other TC
 		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
 		LogUltility.log(test, logger, "Set dropdown to: all records ");
 		
 		// Click the Retrieve button
 		DictionaryTab.btnRetrieve_same.click();
 		LogUltility.log(test, logger, "Click the Retreive button");
		
		// If popup was displayed then close
		try {
			if(DictionaryTab.window_too_many_records.isDisplayed())
				DictionaryTab.btnOKPopupWindow.click();
		}
		catch(NoSuchElementException e)
		{
			
		}
		
		// Get records list from app
		List<String> recordsListAfterSearch = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsListAfterSearch);
				
		// Get all records with chosen status from file
		List<String> recordsWithStatusFromFile = DictionaryTab.getRecordsWithStatus(status);
		LogUltility.log(test, logger, "Records list with the chosens status form file: " + recordsWithStatusFromFile);
		
		// Check that all records has the chosen status
		boolean sameStatus = recordsWithStatusFromFile.containsAll(recordsListAfterSearch);
		LogUltility.log(test, logger, "All retrieved records has the same status: "+sameStatus);
		assertTrue(sameStatus);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--214:Verify deleting pionters between records when deleting a record - part1 lexical relations
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_214_Verify_deleting_pionters_between_records_when_deleting_a_record_part1_lexical_relations() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_214", "Verify deleting pionters between records when deleting a record - part1 lexical relations");
		
		if(Setting.Language.equals("HE"))
			return;
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Get records that have lexical relation values
		List<String> recordsWithLexicalRelations = CurrentPage.As(DictionaryTab.class).getRecordsThatHaveLexicalRelationsFromDictionary();
		
		// Search for one of the records above, search for it and choose it
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsWithLexicalRelations.get(randomizer.nextInt(recordsWithLexicalRelations.size()));
		
		String cleanRecordForSearch = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomrecord);
		// Search for record
		CurrentPage.As(DictionaryTab.class).searchInDictionary(test, logger, "form", "contains", cleanRecordForSearch, "all records");
		
		// Choose a record from the record list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the values from the lexical relations box
		List<String> lexicalRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_LexicalRelations);
		LogUltility.log(test, logger, "Values in lexical relation list: " + lexicalRelatinsValues);
		
		// Remove the chosen record
		LogUltility.log(test, logger, "Press the Delete button ");
		CurrentPage.As(DictionaryTab.class).bt_delete.click();
		LogUltility.log(test, logger, "Press Yes button in window ");
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Press ok");
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		
		// Search for one of the lexical values

		String randomLexicalRelation = lexicalRelatinsValues.get(randomizer.nextInt(lexicalRelatinsValues.size()));
		String[] splitted = randomLexicalRelation.split("¬");
		String newRecordName = splitted[1];
		String searchNewRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(newRecordName);
		CurrentPage.As(DictionaryTab.class).searchInDictionary(test, logger, "form", "contains", searchNewRecord, "all records");
				
		// Choose the new record from the list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, newRecordName);
		LogUltility.log(test, logger, "Choose from record list: " + newRecordName);
		
		// Get the lexical relations of the record and check that it doesn't contain the removed record
		// Get the values from the lexical relations box
		List<String> lexicalRelatinsValuesAfterDelete = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_LexicalRelations);
		LogUltility.log(test, logger, "Values in lexical relation list: " + lexicalRelatinsValuesAfterDelete);
		
		boolean notFound = !lexicalRelatinsValuesAfterDelete.contains(randomrecord);
		LogUltility.log(test, logger, "The lexical relation is NOT found: "+notFound);
		assertTrue(notFound);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--233:Check the semantic relations between the records - part2 semantic relations
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_214_Verify_deleting_pionters_between_records_when_deleting_a_record_part2_semantic_relations() throws InterruptedException, IOException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_214", "Verify deleting pionters between records when deleting a record");
		
		if(Setting.Language.equals("HE"))
			return;
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Get records that have semantic relation values
		List<String> recordsWithSemanticRelations = CurrentPage.As(DictionaryTab.class).getRecordsThatHaveSemanticRelationsFromDictionary();
		
		// Search for one of the records above, search for it and choose it
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsWithSemanticRelations.get(randomizer.nextInt(recordsWithSemanticRelations.size()));
		
		String cleanRecordForSearch = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomrecord);
		// Search for record
		CurrentPage.As(DictionaryTab.class).searchInDictionary(test, logger, "form", "contains", cleanRecordForSearch, "all records");
		
		// Choose a record from the record list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the values from the semantic relations box
		List<String> semanticRelatinsValues = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "Values in semantic relation list: " + semanticRelatinsValues);
		
		// Remove the chosen record
		LogUltility.log(test, logger, "Press the Delete button ");
		CurrentPage.As(DictionaryTab.class).bt_delete.click();
		LogUltility.log(test, logger, "Press Yes button in window ");
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		Thread.sleep(5000);
		LogUltility.log(test, logger, "Press ok");
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		
		// Search for one of the semantic values

		String randomSemanticRelation = semanticRelatinsValues.get(randomizer.nextInt(semanticRelatinsValues.size()));
		String[] splitted = randomSemanticRelation.split("¬");
		String newRecordName = splitted[1];
		String searchNewRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(newRecordName);
		CurrentPage.As(DictionaryTab.class).searchInDictionary(test, logger, "form", "contains", searchNewRecord, "all records");
				
		// Choose the new record from the list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, newRecordName);
		LogUltility.log(test, logger, "Choose from record list: " + newRecordName);
		
		// Get the semantic relations of the record and check that it doesn't contain the removed record
		// Get the values from the semantic relations box
		List<String> semanticRelatinsValuesAfterDelete = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "Values in semantic relation list: " + semanticRelatinsValuesAfterDelete);
		
		boolean notFound = !semanticRelatinsValuesAfterDelete.contains(randomrecord);
		LogUltility.log(test, logger, "The semantic relation is NOT found: "+notFound);
		assertTrue(notFound);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--616:Check the relevant grammar attributes displayed in the corpus
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_616_Check_the_relevant_grammar_attributes_displayed_in_the_corpus() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC_616", "Check the relevant grammar attributes displayed in the corpus");
		 
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "suffix";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//		String randomRecord = "EN{U}aholic~0";
		if(Setting.Language.equals("HE"))
			randomRecord = "HE{U}יִסְט~0";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get a clean record
		String cleanRecord= CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecord);
		LogUltility.log(test, logger, "clean record is: "+cleanRecord);
		
		// Get the grammar attribute values
		List<String> grammarAttValues =  CurrentPage.As(DictionaryTab.class).getGrammarAttributeValues();
		LogUltility.log(test, logger, "The grammar attribute values:"+grammarAttValues);
		
		// If it doesn't contain "Attach_To:noun" then attach it
		boolean alreadyAttached = false;
		for(int i=0;i<grammarAttValues.size();i++)
			if(grammarAttValues.get(i).contains("Attach_To: noun"))
				alreadyAttached = true;
		// If not attached to noun, then attach it
		if(!alreadyAttached)
		{
			// Click the Grammar Attributes add button
			LogUltility.log(test, logger, "Click the add grammar attribute button");
			CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
			
			// Choose a value from attribute dropdown
			String attributeValue = "Attach To";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
			LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
			
			// Choose a value from attribute POS dropdown
			String attributePOS = "noun";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute_4, attributePOS);
			LogUltility.log(test, logger, "Choose value in attribute POS dropdown: " + attributePOS);
			
			// Click the All button
			CurrentPage.As(DictionaryTab.class).bt_all.click();
			
			// Click the Grammar Attributes button
			LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
			CurrentPage.As(DictionaryTab.class).bt_confirm.click();
		}
		
		// If it doesn't contain "Change_To:noun" then attach it
		boolean alreadyChanged = false;
		for(int i=0;i<grammarAttValues.size();i++)
			if(grammarAttValues.get(i).contains("Change_To: noun"))
				alreadyChanged = true;
		// If not changed to noun, then change it
		if(!alreadyChanged)
		{
			// Click the Grammar Attributes add button
			LogUltility.log(test, logger, "Click the add grammar attribute button");
			CurrentPage.As(DictionaryTab.class).bt_add_grammar_attribute.click();
			
			// Choose a value from attribute dropdown
			String attributeValue = "Change To";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute, attributeValue);
			LogUltility.log(test, logger, "Choose value in attribute dropdown: " + attributeValue);
			
			// Choose a value from attribute POS dropdown
			String attributePOS = "noun";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cb_attribute_4, attributePOS);
			LogUltility.log(test, logger, "Choose value in attribute POS dropdown: " + attributePOS);
			
			// Click the All button
			CurrentPage.As(DictionaryTab.class).bt_all.click();
			
			// Click the Grammar Attributes button
			LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
			CurrentPage.As(DictionaryTab.class).bt_confirm.click();
		}
		
	
		// Go to monitor Tab
		CurrentPage = GetInstance(MonitorTab.class);
		CurrentPage.As(MonitorTab.class).tabMonitor.click();
		LogUltility.log(test, logger, "Navigate to Monitor tab");
		
		//Insert the sentence in the field
//		String sentenceToAdd = "the man is sleepaholic";
		String sentenceToAdd = Setting.Language.equals("EN")? "the man is sleep" :  "הכי קטנ";
		sentenceToAdd+=cleanRecord;
		CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
		
		//Click new button in footer
		LogUltility.log(test, logger, "Click New, Ok buttons");
		CurrentPage.As(MonitorTab.class).bt_new.click();
		//Confirm and click ok
		CurrentPage.As(MonitorTab.class).bt_ok.click();
		
		// Get the words from sentence separately
		CurrentPage = GetInstance(CorpusTab.class);
		List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		// Click on the composition
		CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(wordsInRecord.size()-1), wordsInRecord.size()-1);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordsInRecord.size()-1));
		
		// Get all tagging options
		List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
		
		// Random choose another option from the taggingOptions
		List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
		
		// Choose tagging option
		try {
			CurrentPage.As(CorpusTab.class).chooseTaggingOption(randomTaggingOption);
			LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
		} catch (Exception e) {
			System.out.println("exception: " + e);
		}
		
		String expectedMessage = "You have to select first the POS and Formtype of the composition In the purple line!";
		boolean popupDisplayed = CurrentPage.As(CorpusTab.class).popupWindowMessage(test, logger, "", expectedMessage);
		LogUltility.log(test, logger, "The popup for selecting POS and Formtype has been displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		LogUltility.log(test, logger, "Click the Ok button");
		CurrentPage.As(CorpusTab.class).bt_ok_new.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--630:Space Punctuation, defined record should stay the same and not split
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_630_Space_Punctuation_defined_record_should_stay_the_same_and_not_split() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--630", "Space Punctuation, defined record should stay the same and not split");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		// Click the new button
		CurrentPage.As(DictionaryTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(DictionaryTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		List<String> records = CurrentPage.As(DictionaryTab.class).getRecordsFromDictionary();
		Random random = new Random();
		String randomText = records.get(random.nextInt(records.size()))+"."+records.get(random.nextInt(records.size()))+".";
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test,logger,"Insert Key Form: "+randomText);
		
		// Get random POS
		List<String> posList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Navigate to monitor tab
		LogUltility.log(test, logger, "Navigate to monitor tab");
		CurrentPage = GetInstance(MonitorTab.class);
		CurrentPage.As(MonitorTab.class).tabMonitor.click();		
		
		// Click view tab
		CurrentPage.As(MonitorTab.class).viewTab.click();
		LogUltility.log(test, logger, "Click view tab");
		
		// Click show space punctuation
		if(!CurrentPage.As(MonitorTab.class).cbx_show_spacePunctuation.isSelected())
			CurrentPage.As(MonitorTab.class).cbx_show_spacePunctuation.click();
		LogUltility.log(test, logger, "Click the 'Show Space Punctuation' Checkbox");
		
		// Insert a sentence
		CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(randomText);
		LogUltility.log(test, logger, "Insert sentence: " + randomText);
		
		// Click the disambiguate button
		CurrentPage.As(MonitorTab.class).bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the disambiguate button");
		
		// Get text from the 'Space punctuation' text area
		String puncSentence = CurrentPage.As(MonitorTab.class).tb_space_punctuation.getText();
		LogUltility.log(test, logger, "Space punctuation sentence: " + puncSentence);
		
		// Verify that there is no spaces before each dot, so the record stays the same and not split
		// In Hebrew they should be spaced
		boolean noSpaces = Setting.Language.equals("EN") ? !puncSentence.contains(" .") : puncSentence.contains(" .") ;
		LogUltility.log(test, logger, "The record stays the same and no split has been applied: "+noSpaces);
		assertTrue(noSpaces);

		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--715:Create stress GA - displayed as -1 and get exception when try to edit it - Bug DIC-1174
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_715_Create_stress_GA_displayed_as_1_and_get_exception_when_try_to_edit_it_Bug_DIC_1174() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--715", "Create stress GA - displayed as -1 and get exception when try to edit it - Bug DIC-1174");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click dictionary Tab
//		DictionaryTab.dictionaryTab.click();
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Click the dictionary Tab");
		
		// Get random POS
		Random randomizer = new Random();
		String[] pos = {"suffix","prefix","possessive"};
		String randomPOS = pos[randomizer.nextInt(pos.length)];
//		String randomPOS = "prefix";
		
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randomPOS);
		LogUltility.log(test, logger, "Choose from pos list: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record		
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//		String randomrecord = "EN{f}e~0";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		// Add new GA
		// Click the blue add attribute button
		DictionaryTab.bt_add_grammar_attribute.click();
		LogUltility.log(test, logger, "Click add grammar attribute button");
		
		// Choose attribute from the dropdown
		String[] attribute = {"Stress Attractiveness"};
		Random random = new Random();
		String randomAtt = attribute[random.nextInt(attribute.length)];

		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute, randomAtt);
		LogUltility.log(test, logger, "Choose from attribute list: " + randomAtt);
		
		// Set to stress: Affix
		String randomPOSAtt = "Stress_on_Affix";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_2ndLine, randomPOSAtt);
		LogUltility.log(test, logger, "Choose from pattern list: " + randomPOSAtt);
		
		// Click confirm
		DictionaryTab.bt_confirm.click();
		LogUltility.log(test, logger, "Click confirm button" );
		
		// Get grammar attributes list after adding
		List<String> gaValuesAfter = DictionaryTab.getGrammarAttributeValues();
		boolean exists = gaValuesAfter.contains("Stress: Affix");
		LogUltility.log(test, logger, "The new Grammar attribute has been added successfully: "+exists);
		assertTrue(exists);
		
		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//		
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--717:Verify editing exist Grammar attribute for stress bug-DIC-1184
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_717_Verify_editing_exist_Grammar_attribute_for_stress_bug_DIC_1184() throws InterruptedException, IOException
	{
		// Add stress:affix for any chosen record then open the window and check if it was opened
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--717", "Verify editing exist Grammar attribute for stress bug-DIC-1184");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click dictionary Tab
//		DictionaryTab.dictionaryTab.click();
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Click the dictionary Tab");
		
		// Get random POS
		Random randomizer = new Random();
		String[] pos = {"suffix","prefix","possessive"};
		String randomPOS = pos[randomizer.nextInt(pos.length)];
//		String randomPOS = "prefix";
		
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randomPOS);
		LogUltility.log(test, logger, "Choose from pos list: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Pick a random record		
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//		String randomrecord = "EN{f}e~0";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
		// Add new GA
		// Click the blue add attribute button
		DictionaryTab.bt_add_grammar_attribute.click();
		LogUltility.log(test, logger, "Click add grammar attribute button");
		
		// Choose attribute from the dropdown
		String[] attribute = {"Stress Attractiveness"};
		Random random = new Random();
		String randomAtt = attribute[random.nextInt(attribute.length)];

		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_attribute, randomAtt);
		LogUltility.log(test, logger, "Choose from attribute list: " + randomAtt);
		
		// Set to stress: Affix
		String randomPOSAtt = "Stress_on_Affix";
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cb_2ndLine, randomPOSAtt);
		LogUltility.log(test, logger, "Choose from pattern list: " + randomPOSAtt);
		
		// Click confirm
		DictionaryTab.bt_confirm.click();
		LogUltility.log(test, logger, "Click confirm button" );
		
		// Get grammar attributes list after adding
		List<String> gaValuesAfter = DictionaryTab.getGrammarAttributeValues();
		boolean exists = gaValuesAfter.contains("Stress: Affix");
		LogUltility.log(test, logger, "The new Grammar attribute has been added successfully: "+exists);
		assertTrue(exists);
		
		// Reopen the window 
		DictionaryTab.editValueInGrammarAttributes(DictionaryTab.pn_grammar_attributes, "Stress: Affix");
		LogUltility.log(test, logger, "Edit grammar attribute: " + "Stress: Affix");
	
		// Check that it was opened without errors
		boolean opened = DictionaryTab.Attribute_Editor.isDisplayed();
		LogUltility.log(test, logger, "The window was opened successfully: "+opened);
		assertTrue(opened);
		
		// Close the window
		LogUltility.log(test, logger, "Close window");
		DictionaryTab.bt_cancel_attributes.click();
		
		// Click the revert button
//		DictionaryTab.bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//		
//		// Click Yes on the confirmation message
//		DictionaryTab.btnOk.click();
//		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--718:Verify deleting keyform then select another record bug DIC-1020
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_718_Verify_deleting_keyform_then_select_another_record_bug_DIC_1020() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--718", "Verify deleting keyform then select another record bug DIC-1020");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		List<String> pos = 	CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS,30);
		Random random = new Random();
		String volumePOS = pos.get(random.nextInt(pos.size()));
//		String volumePOS = "title";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in volume POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Modify a record name in the key form field
		String randomText = "";
		CurrentPage.As(DictionaryTab.class).txtKeyForm.sendKeys(randomText);
		
		// Pick second record for syncing
//		CurrentPage.As(DictionaryTab.class).Previous_button.click();
		CurrentPage.As(DictionaryTab.class).bt_next.click();
		
		// Check if 'warning bad key' popup displayed
		String titleToCheck = "Warning: bad key:";
		String textToCheck = "The current key has an invalid value.\r\rClick OK  to change the value of one of the key fields\rClick CANCEL to abort\r";
		boolean popupDisplayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, titleToCheck, textToCheck);
		LogUltility.log(test, logger, "The 'bad key' popup displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Close the window
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		LogUltility.log(test, logger, "Click the cancel button");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--719:Grammar Attributes - Check editing Grammar Attributes values are not being duplicated - Bug 1153
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_719_Grammar_Attributes_Check_editing_Grammar_Attributes_values_are_not_being_duplicated_Bug_1153() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--719", "Grammar Attributes - Check editing Grammar Attributes values are not being duplicated - Bug 1153");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		CurrentPage = GetInstance(LexiconTab.class);
		LexiconTab LexiconTab = CurrentPage.As(LexiconTab.class);
		
		// Click dictionary Tab
//		DictionaryTab.dictionaryTab.click();
		CommonFunctions.clickTabs("Dictionary");
		LogUltility.log(test, logger, "Click the dictionary Tab");
		
		// Get text to use for search
		List<String> records = LexiconTab.getRecordsFromLexiconWithGA();
		LogUltility.log(test, logger, "Getting records from lexicon.txt which has grammar attributes");
		  
		// Pick random record
		Random random = new Random();
		String randomRecord = records.get(random.nextInt(records.size()));
		
		// Clean record
		String[] removeLeft1 = randomRecord.split("}");
	    String[] removeRight1 = removeLeft1[1].split("~");
	    String cleanRandomRecord = removeRight1[0];
		LogUltility.log(test, logger, "Record to use: " + cleanRandomRecord);
		
		// Search in dictionary
		DictionaryTab.searchInDictionary(test,logger,"form","equal",cleanRandomRecord,"all records");
	
		// Get records list
		List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
		LogUltility.log(test, logger, "Records list: " + recordsList);
		    
		// Choose the record
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose record from the list: " + randomRecord);
		
		// Get all the available grammar attribute values
		List<String> grammarAttributeValues = DictionaryTab.getGrammarAttributeValues();
		LogUltility.log(test, logger, "Available grammar attributes BEFORE modifying: " + grammarAttributeValues);
		
		// Work only with grammar attributes of "Attach_To" and "Change_To"
		List<String> grammarAttributeValesACto = new ArrayList<String>(); 
		for(int i=0;i<grammarAttributeValues.size();i++)
			if(grammarAttributeValues.get(i).contains("To"))
				grammarAttributeValesACto.add(grammarAttributeValues.get(i));
		
		// Double click to edit a random grammar attribute value
		String randomAttribute = grammarAttributeValesACto.get(random.nextInt(grammarAttributeValesACto.size()));
		DictionaryTab.editValueInGrammarAttributes(DictionaryTab.pn_grammar_attributes, randomAttribute);
		LogUltility.log(test, logger, "Edit grammar attribute: " + randomAttribute);
		
		// Modify the checkboxes
		DictionaryTab.modifyGrammarAttributeCheckboxes(test,logger);
	
		// Click the Grammar Attributes button
		LogUltility.log(test, logger, "Click the confirm button in the attribute editor");
		DictionaryTab.bt_confirm.click();
		
		// Get grammar attribute list after modifying the record
		List<String> grammarAttributeValuesAfter = DictionaryTab.getGrammarAttributeValues();
		LogUltility.log(test, logger, "Grammar attributes after modifying:"+grammarAttributeValuesAfter);
		
		// Check that it didn't duplicate the former record
		boolean noDuplicate = grammarAttributeValuesAfter.size() == grammarAttributeValues.size();
		LogUltility.log(test, logger, "The edited grammar attribute didn't duplicate: "+noDuplicate);
		assertTrue(noDuplicate);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--723:Verify user cannot delete the last Transcription value
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_723_Verify_user_cannot_delete_the_last_Transcription_value() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--723", "Verify user cannot delete the last Transcription value");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
		// Choose a value from Volume[POS] dropdown
		List<String> POSs = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		Random random = new Random();
		String volumePOS = POSs.get(random.nextInt(POSs.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,30);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available transcriptions before delleting transcription
		List<String> transcriptionsBeforeList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
		LogUltility.log(test, logger, "Transcriptions list before adding: " + transcriptionsBeforeList);
		
		// Check if having more than one transcription value 
		int transcriptions =  transcriptionsBeforeList.size();
//		if (transcriptions > 1) {
			
			for (int i = 0; i < transcriptions; i++) {
			// remove the transcription
			String deletedTranscription = CurrentPage.As(DictionaryTab.class).removeTranscription();
			LogUltility.log(test, logger, "Deleted transcription: " + deletedTranscription);
			}
			
//			// Then go to the next record
//			CurrentPage.As(DictionaryTab.class).bt_next.click();
//			randomRecord = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records).get(0);
//			LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
//			
//			// Get the available transcriptions after moving forward
//			transcriptionsBeforeList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
//			transcriptions = transcriptionsBeforeList.size();
	


		// Verify that the popup displayed
		String textToCheck = "You cannot leave the record with no spellings. Change its value if you want!";
		boolean popupDisplayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "User can't remove the transcription and popup displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Click Ok and close window
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Ok to close the window");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--724:Verify user cannot delete the last Spellings value
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_724_Verify_user_cannot_delete_the_last_Spellings_value() throws InterruptedException, IOException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests -  DIC_TC--724", "Verify user cannot delete the last Spellings value");
		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		// Choose a value from Volume[POS] dropdown
		List<String> POSs = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		Random random = new Random();
		String volumePOS = POSs.get(random.nextInt(POSs.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records,30);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the available spellings before adding new one
		List<String> spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list before adding: " + spellingsBeforeList);
		
		// Check if having more than one Spelling value 
				int Spellings =  spellingsBeforeList.size();
//				if (transcriptions > 1) {
					
					for (int i = 0; i < Spellings; i++) {
					// remove the transcription
					String deletedSpelling = CurrentPage.As(DictionaryTab.class).removeSpelling();
					LogUltility.log(test, logger, "Deleted transcription: " + deletedSpelling);
					}
		
		
		
//		// Check if having more than one spelling value 
//		int spellings =  spellingsBeforeList.size();
//		if (spellings > 1) {
//			// Then go to the next record
//			CurrentPage.As(DictionaryTab.class).bt_next.click();
//			randomRecord = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records).get(0);
//			LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
//						
//			// Get the available spellings 
//			spellingsBeforeList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
//			spellings =  spellingsBeforeList.size();
//		}
//
//		// try to remove the transcription
//		String deletedSpelling = CurrentPage.As(DictionaryTab.class).removeSpelling();
//		LogUltility.log(test, logger, "Try to Delete spelling: " + deletedSpelling);
				
		// Verify that the popup displayed
		String textToCheck = "You cannot leave the record with no spellings. Change its value if you want!";
		boolean popupDisplayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "User can't remove the spelling and popup displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Click Ok and close window
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Ok to close the window");
				
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--725:Verify user cannot delete the last Transcription value after edit - bug 1080
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_725_Verify_user_cannot_delete_the_last_Transcription_value_after_edit_bug_1080() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--725", "Verify user cannot delete the last Transcription value after edit - bug 1080");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
   		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose random record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);	
		
		// Get Transcriptions list
		List<String> transList = DictionaryTab.getValuesFromApp(DictionaryTab.lstTranscriptions);
		LogUltility.log(test, logger, "Transcription list: " + transList);
		
//		// Check if having more than one transcription value 
//		int transcriptions =  transList.size();
//		if (transcriptions > 1) {	
//		// Then go to the next record
//		CurrentPage.As(DictionaryTab.class).bt_next.click();
//		chosenValue = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records).get(0);
//		LogUltility.log(test, logger, "Choose from record list: " + chosenValue);
//		
//		// Get the available transcriptions after moving forward
//		transList = CurrentPage.As(DictionaryTab.class).getTranscriptionsValues();
//		transcriptions = transList.size();
//		}
				
		// Choose transcription value to edit
		int transIndex = randomizer.nextInt(transList.size());
		String chosenTrans = transList.get(transIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lstTranscriptions, chosenTrans);
		LogUltility.log(test, logger, "Choose transcription: " + chosenTrans);
		
		// Edit transcription
		DictionaryTab.editTranscriptions(test,logger);
		
		// Check if having more than one transcription value 
		int transcriptions =  transList.size();
//		if (transcriptions > 1) {
			
			for (int i = 0; i < transcriptions; i++) {
			// remove the transcription
			String deletedTranscription = CurrentPage.As(DictionaryTab.class).removeTranscription();
			LogUltility.log(test, logger, "Deleted transcription: " + deletedTranscription);
			}
		
		// Verify that the popup displayed
		String textToCheck = "You cannot leave the record with no spellings. Change its value if you want!";
		boolean popupDisplayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "User can't remove the transcription and popup displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Click Ok and close window
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Ok to close the window");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--726:Verify user cannot delete the last Spellings value after edit - bug 1080
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_726_Verify_user_cannot_delete_the_last_Spellings_value_after_edit_bug_1080() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--726", "Verify user cannot delete the last Spellings value after edit - bug 1080");
	
		// Initialize current page
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Focus on dictionary tab
		DictionaryTab.tabDictionary.click();
		
		// Get random POS
		List<String> posList = DictionaryTab.getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random POS
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
				
   		// Get records list
   		List<String> retrivedRecords = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records,20);
   		LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
		
		// Choose random record
		String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records , chosenValue);
		LogUltility.log(test, logger, "Choose record: "+chosenValue);	
		
		// Get the available spellings
		List<String> spellingsList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
		LogUltility.log(test, logger, "Spelling list before editing: " + spellingsList);

//		// Check if having more than one spelling value 
//		int spellings =  spellingsList.size();
//		if (spellings > 1) {
//			// Then go to the next record
//			CurrentPage.As(DictionaryTab.class).bt_next.click();
//			chosenValue = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records).get(0);
//			LogUltility.log(test, logger, "Choose from record list: " + chosenValue);
//						
//			// Get the available spellings 
//			spellingsList = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
//			spellings =  spellingsList.size();
//		}

				
		// Choose spelling value to edit
		int spellIndex = randomizer.nextInt(spellingsList.size());
		String chosenSpell = spellingsList.get(spellIndex);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.Spellings_list, chosenSpell);
		LogUltility.log(test, logger, "Choose spelling: " + chosenSpell);
		
		// Edit spelling
		DictionaryTab.editSpelling(test,logger);
		
		
		// Check if having more than one Spelling value 
		int Spellings =  spellingsList.size();
//		if (transcriptions > 1) {
			
			for (int i = 0; i < Spellings; i++) {
			// remove the transcription
			String deletedSpelling = CurrentPage.As(DictionaryTab.class).removeSpelling();
			LogUltility.log(test, logger, "Deleted Spelling: " + deletedSpelling);
			}
		
//		// try to remove the spelling
//		String deletedSpell = CurrentPage.As(DictionaryTab.class).removeSpelling();
//		LogUltility.log(test, logger, "Try to Delete spelling: " + deletedSpell);
		
		// Verify that the popup displayed
		String textToCheck = "You cannot leave the record with no spellings. Change its value if you want!";
		boolean popupDisplayed = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "User can't remove the spelling and popup displayed: "+popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Click Ok and close window
		CurrentPage.As(DictionaryTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Ok to close the window");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--730:check choosing a record after canceling attach � bug 1091
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_730_check_choosing_a_record_after_canceling_attach_bug_1091() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--730", "check choosing a record after canceling attach � bug 1091");
		
		if(Setting.Language.equals("HE")) return;

		
		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		
	   if (Setting.Language.equals("HE"))
		   return;
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Click the attach button
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button");
		
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).btnOKPopupWindow.click();
		LogUltility.log(test, logger, "Click OK on the confirmation window");
		
		// Choose another record to attach to
		recordsList.remove(randomRecord);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecordSecond);
		
		// Click the attach button again
		CurrentPage.As(DictionaryTab.class).bt_attach.click();
		LogUltility.log(test, logger, "Click the attach button again");
			
		// Click OK on the confirmation message
		CurrentPage.As(DictionaryTab.class).bt_utility_accept.click();
		LogUltility.log(test, logger, "Click attach button");

		// Check in case additional popup was displayed
		String titleToCheck = "DictionaryAttaching same key Message:";
		String textToCheck = "This target record has already a spelling key";	
		boolean additionalPopup;
		try {
			 additionalPopup = CurrentPage.As(DictionaryTab.class).popupWindowMessage(test, logger, titleToCheck, textToCheck);
		}catch (Exception e) {
			// e.printStackTrace();
			additionalPopup = true;
		}
		
		if(additionalPopup)
		{
			CurrentPage.As(DictionaryTab.class).bt_ignore.click();
			LogUltility.log(test, logger, "Click ignore button");
		}
		
		// Click cancel on the confirmation popup
		CurrentPage.As(DictionaryTab.class).Cancel.click();
		LogUltility.log(test, logger, "Click cancel button");
		
		// Navigate to a new record
		recordsList.remove(randomRecordSecond);
		String randomRecordThird = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordThird);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecordThird);
		
		// Check if its info displayed without any exceptions
		String keyForm = CurrentPage.As(DictionaryTab.class).txtKeyForm.getText();
		boolean infoDisplayed = randomRecordThird.contains(keyForm);
		LogUltility.log(test, logger, "The suitable info were displayed without any errors: "+infoDisplayed);
		assertTrue(infoDisplayed);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	/**
	 * DIC_TC--237:Verify merge button works successfully
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_237_Verify_merge_button_works_successfully() throws IOException, InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Dictionary Tests - DIC_TC--237", "Verify merge button works successfully");
		
		// Focus on dictionary tab
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).CurrentPage.As(DictionaryTab.class);
		DictionaryTab.tabDictionary.click();
		
		// Generate merge file with minimum = 1 line , maximum = 5 lines
		Random random = new Random();
		int size = random.nextInt(5)+1;
		DictionaryTab.buildMergeFile(test, logger, size);
		
		// Click merge button
		DictionaryTab.bt_merge_files.click();
		LogUltility.log(test, logger, "click the merge button");
		
		// Get list values - should be one value for now
		List<String> filesList = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
		LogUltility.log(test, logger, "Merge files list: "+filesList);
		
		// Choose the correct file
		String chosenFile ="";
		for(String str : filesList)
			if(str.contains("Dictionary_merge"))
				{chosenFile = str;break;}
		
		// Choose the file
		LogUltility.log(test, logger, "Choose file: " +chosenFile);
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue,chosenFile);
	
		// Click merge button
		DictionaryTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click merge button in window");
		
		int sizeLoop = size;
		while(sizeLoop-- > 0)
		{	
			LogUltility.log(test, logger, "Click yes button to add the new record");
			DictionaryTab.btnYes.click();
		}
		
		// If merge warning still being displayed 
		String warningTitle = "Merge warning";
		boolean mergeWarning = DictionaryTab.popupWindowMessage(test, logger, warningTitle,"Click");
		if(mergeWarning)
			DictionaryTab.Cancel.click();
		
		// Get replacement window
		boolean isPopupDisplayed = false;
		try {
			isPopupDisplayed =DictionaryTab.replacementWindow.isDisplayed();
		}catch (Exception e) {
			// e.printStackTrace();
		}
		
		if(isPopupDisplayed)
		{
			LogUltility.log(test, logger, "Click abort button");
//			DictionaryTab.bt_abort.click();
			DictionaryTab.btnpopupCancelWindow.click();
			
			LogUltility.log(test, logger, "Click cancel button");
			DictionaryTab.Cancel.click();
		}
		

		
		// Check that merge was completed successfully
		String textToCheck=	"Merging records ended successfuly";
		boolean mergeSuccess = DictionaryTab.popupWindowMessage(test, logger, textToCheck);
		LogUltility.log(test, logger, "Merged successfully completed: "+mergeSuccess);
		assertTrue(mergeSuccess);
		DictionaryTab.btnOk.click();
		
		// Get clean records from the Dictionary_merge.txt file
		List<String> cleanMergeRecords = DictionaryTab.getRecordsFromDictionaryMerge();
		LogUltility.log(test, logger, "Get clean records from the merge file: "+cleanMergeRecords);
		
		// Check that the records added to dictionary
		for(int i=0;i<size;i++)
		{
			String searchWord = cleanMergeRecords.get(i);
			DictionaryTab.searchInDictionary(test, logger, "form", "equal", searchWord, "all records");
				
			// Get records list
			List<String> recordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);
			    
			// Choose the record
			DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, searchWord);
			LogUltility.log(test, logger, "Choose record from the list contains: " + searchWord);
			
			String displayedKey =	DictionaryTab.txtKeyForm.getText();
			boolean same = displayedKey.equals(searchWord);
			LogUltility.log(test, logger, "The displayed key: "+displayedKey+", equals the searched word:"+same);
			assertTrue(same);
		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Dictionary",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
