package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Corpus_Search_Tests_Adding_filter extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Corpus tab - Search");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Corpus tab - Search");
		logger.info("Corpus tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true){
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
			
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Smorfet app focus
		try {
			CurrentPage = GetInstance(CorpusTab.class);
//			CurrentPage.As(CorpusTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Corpus");
		} catch (Exception e) {
		}
		
	}
	
	/**
	 * DIC_TC--200:Verify user can add filter to the search
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_200_Verify_user_can_add_filter_to_the_search() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--200", "Verify user can add filter to the search");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus 
		String searchWord =Setting.Language.equals("EN")? "Micro" : "שבל";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required search word "Empty result"
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger, searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).contains(searchWord))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result: " + searchedWordFound);	
		
		// Search in corpus by adding filter 
		String filterWord = Setting.Language.equals("EN")? "soft": "ונה";
		String newSearchWord = searchWord.concat(filterWord);
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"add filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean filterFound = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).contains(newSearchWord))
					filterFound = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!filterFound)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			// Reset to default
			CorpusTab.bt_reset_search.click();
			LogUltility.log(test, logger, "Reset to default state");
			
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--201:Verify user can change filter while searching
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_201_Verify_user_can_change_filter_while_searching() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--201", "Verify user can change filter while searching");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus 
		String searchWord =Setting.Language.equals("EN")? "Micro" : "שבל";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required search word "Empty result"
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger, searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).toLowerCase().contains(searchWord.toLowerCase()))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result: " + searchedWordFound);	
		
		// Search in corpus by adding filter 
		String filterWord = Setting.Language.equals("EN")? "soft": "ונה";
		String newSearchWord = searchWord.concat(filterWord);
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"add filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean filterFound = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).toLowerCase().contains(newSearchWord.toLowerCase()))
					filterFound = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!filterFound)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			
		assertTrue(filterFound);
		LogUltility.log(test, logger, "Comparation result: " + filterFound);	
		
		// Search in corpus by changing filter 
		filterWord = Setting.Language.equals("EN")? "waveable" : "יטא";
		newSearchWord = searchWord.concat(filterWord);
		
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"change filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean changingFilterOk = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).toLowerCase().contains(newSearchWord.toLowerCase()))
					changingFilterOk = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!changingFilterOk)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			
		assertTrue(changingFilterOk);
		LogUltility.log(test, logger, "Comparation result: " + changingFilterOk);	
		
		// Reset to default
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Reset to default state");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
		
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Corpus/Search/Adding filter",method.getName());
			
//					System.out.println("tryCount: " + tryCount);
//					System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//					        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//					        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}

/**
 * Closing the application after running all the TCs
 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}

}
