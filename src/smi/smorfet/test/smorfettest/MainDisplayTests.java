package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MainDisplayWindow;

/**
 * 
 * All tests for Corpus Syncing
 *
 */
public class MainDisplayTests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Main Display");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite = Main Display");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
	}

	
	
	/**
	 * DIC_TC--407:Check the user can select the user name from the user name list
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_407_Check_the_user_can_select_the_user_name_from_the_user_name_list() throws IOException, InterruptedException
	{
		test = extent.createTest("Main display - DIC_TC--407", "Check the user can select the user name from the user name list");

		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Open smorfet
		LogUltility.log(test, logger, "Open smorfet window");
		MainDisplayWindow.openSmorfetMainWindow();

		// Get usernames from the list
		List<String> usernamesList = CommonFunctions.getValuesFromApp(MainDisplayWindow.dd_start_user);
		LogUltility.log(test, logger, "User name list: "+usernamesList);
		
		// Choose random value
		Random random = new Random();
		String username = usernamesList.get(random.nextInt(usernamesList.size()));
		LogUltility.log(test, logger, "Choose random username: "+username);
		CommonFunctions.chooseValueInDropdown(MainDisplayWindow.dd_start_user, username);
		
		// Verify that the value has been selected
		String chosenUsername = CommonFunctions.getChosenValueInDropdown(MainDisplayWindow.dd_start_user).get(0);
		LogUltility.log(test, logger, "The chosen username is: "+chosenUsername);
		boolean same = chosenUsername.equals(username);
		LogUltility.log(test, logger, "The selected username is displayed: "+same);
		assertTrue(same);
		
		// Close smorfet window
		MainDisplayWindow.bt_abort.click();
		LogUltility.log(test, logger, "Click 'abort' to smorfet window");
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
		
	/**
	 * DIC_TC--408:Check the SmorphDesktop button
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_408_Check_the_SmorphDesktop_button() throws IOException, InterruptedException
	{
		test = extent.createTest("Main display - DIC_TC--408", "Check the SmorphDesktop button");

		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Open smorfet
		LogUltility.log(test, logger, "Open smorfet window");
		MainDisplayWindow.openSmorfetMainWindow();

		// Get usernames from the list
		List<String> usernamesList = CommonFunctions.getValuesFromApp(MainDisplayWindow.dd_start_user);
		LogUltility.log(test, logger, "User name list: "+usernamesList);
		
		// Choose random value
		Random random = new Random();
		String username = usernamesList.get(random.nextInt(usernamesList.size()));
		LogUltility.log(test, logger, "Choose random username: "+username);
		CommonFunctions.chooseValueInDropdown(MainDisplayWindow.dd_start_user, username);
		
		// Click on smorphDesktop button and launch the application
		LogUltility.log(test, logger, "Launch SmorphDesktop");	
		MainDisplayWindow.LaunchSmorphDesktopWait();
		
		// Get displayed tabs names
		List<String> displayedTabs = CommonFunctions.getDisplayedTabs();
		LogUltility.log(test, logger, "Displayed tabs: "+displayedTabs);
		
		// It should contain only the 'smorph' and 'maintenance' tabs
		List<String> expectedTabs = new ArrayList<String>();
		expectedTabs.add("Smorph");
		expectedTabs.add("Maintenance");
		Collections.sort(expectedTabs);
		Collections.sort(displayedTabs);
		LogUltility.log(test, logger, "Expected tabs: "+expectedTabs);
		
		boolean sameTabs = displayedTabs.equals(expectedTabs);
		LogUltility.log(test, logger, "The displyed tabs in smorphDesktop as expected: "+sameTabs);
		assertTrue(sameTabs);
		
		// Close the app
		CommonFunctions.close_smorfet.click();
		LogUltility.log(test, logger, "Close the smorphDesktop");	
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--409:Check the Full smorfet button
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_409_Check_the_Full_smorfet_button() throws IOException, InterruptedException
	{
		test = extent.createTest("Main display - DIC_TC--409", "Check the Full smorfet button");

		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);

		// Launch smorfet
		LogUltility.log(test, logger, "Launch smorfet");	
		MainDisplayWindow.LaunchSmorfetWait();
		
		// Get displayed tabs names
		List<String> displayedTabs = CommonFunctions.getDisplayedTabs();
		LogUltility.log(test, logger, "Displayed tabs: "+displayedTabs);
		
		// Smorfet should contain all the tabs
		List<String> expectedTabs = new ArrayList<String>();
		expectedTabs.add("Smorph");
		expectedTabs.add("Maintenance");
		expectedTabs.add("Dictionary");
		expectedTabs.add("Compounds");
		expectedTabs.add("Lexicon");
		expectedTabs.add("Corpus");
		expectedTabs.add("Prosody");
		expectedTabs.add("Rules");
		expectedTabs.add("Disambiguation");
		expectedTabs.add("Monitor");
		Collections.sort(expectedTabs);
		Collections.sort(displayedTabs);
		LogUltility.log(test, logger, "Expected tabs: "+expectedTabs);
		
		boolean sameTabs = displayedTabs.equals(expectedTabs);
		LogUltility.log(test, logger, "The displyed tabs in smorfet as expected: "+sameTabs);
		assertTrue(sameTabs);
		
		// Close the app
		MainDisplayWindow.closeSmorfetNo(test, logger);
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--410:Check the Abort button
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_410_Check_the_Abort_button() throws IOException, InterruptedException
	{
		test = extent.createTest("Main display - DIC_TC--410", "Check the Abort button");

		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Open smorfet
		LogUltility.log(test, logger, "Open smorfet window");
		MainDisplayWindow.openSmorfetMainWindow();

		// Click abort
		LogUltility.log(test, logger, "Click 'abort' button");
		MainDisplayWindow.bt_abort.click();
		
		// Check that the window isn't displayed anymore
		boolean isDisplayed;
		try {
			isDisplayed = MainDisplayWindow.Start_Window.isDisplayed();
			
		}catch (NoSuchElementException e) {
			e.printStackTrace();
			isDisplayed = false;
		}
		
		LogUltility.log(test, logger, "The window is open: "+ isDisplayed);
		assertFalse(isDisplayed);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--738:Verify that smorfet opens when both languages checkboxes are selected - BUG 1377
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_738_Verify_that_smorfet_opens_when_both_languages_checkboxes_are_selected_BUG_1377() throws IOException, InterruptedException
	{
		test = extent.createTest("Main display - DIC_TC--738", "Verify that smorfet opens when both languages checkboxes are selected - BUG 1377");

		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		
		// Open smorfet
		LogUltility.log(test, logger, "Open smorfet window");
		MainDisplayWindow.openSmorfetMainWindow();

		 // Choose language
		String checkboxLanguage ="";
	    switch(Setting.Language)
	    {
	    case "HE":
	    	CommonFunctions.chooseValueInDropdown(MainDisplayWindow.dd_main_language, "Hebrew");
	    	checkboxLanguage = "English";break;
	    case "EN":
	     	CommonFunctions.chooseValueInDropdown(MainDisplayWindow.dd_main_language, "English");
	     	checkboxLanguage = "Hebrew";break;
	    }
	    
		LogUltility.log(test, logger, "Choose language from dropdown: "+Setting.Language);	

	    
	    // Select the other language checkbox , DOBULE CLICK	
	    CommonFunctions.chooseValueInDropdown(MainDisplayWindow.cb_languages, checkboxLanguage);
	    CommonFunctions.chooseValueInDropdown(MainDisplayWindow.cb_languages, checkboxLanguage);
		LogUltility.log(test, logger, "Select language from the checkbox: "+checkboxLanguage);	

		  // Click the smorfet button
		MainDisplayWindow.bt_Smorfet.click();
		
//	    Thread.sleep(220000);
	    // Wait until smorfet is opened
		try {
		WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SMI_Root_GUI']")));
		}catch (Exception e) {
//			e.printStackTrace();
		}
		
		boolean isVisible = false;

		try {
			if(MainDisplayWindow.SMI_Root_GUI.isDisplayed())
      		  isVisible = true;
		}catch (Exception e) {
//			e.printStackTrace();
		}
		
		try {
			if(!isVisible)
			{
				if(MainDisplayWindow.namesWarningPopup.isDisplayed())
					{MainDisplayWindow.btnOk.click();	isVisible = true;}
				WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SMI_Root_GUI']")));	
			}
				
		} catch (Exception e) {}

	  LogUltility.log(test, logger, "Smorfet has been launched successfully: "+isVisible);	
  	  assertTrue(isVisible);
  	  
  	  // Close the app
  	  MainDisplayWindow.closeSmorfetNo(test, logger);
  	  
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Main Display",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}
