package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.RulesTab;

/**
 * 
 *  Tests for Prosody outputs Rules
 *
 */
public class Rules_Nor_Outputs_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");

		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Rules tab - Normalization Output");
	
		// Logger
		logger = LogManager.getLogger(RulesTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Rules tab - Normalization Output");
		logger.info("Rules tab Tests - FrameworkInitilize");
		
	//con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	//	logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(RulesTab.class);
//		CurrentPage.As(RulesTab.class).Start_Window.click();
//		CurrentPage.As(RulesTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(RulesTab.class);
//			CurrentPage.As(RulesTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Rules");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 * DIC_TC--484:check the process of adding new outputs for rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_484_check_the_process_of_adding_new_outputs_for_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--484", "Check the process of adding new outputs for rules");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Click the normalization rules tab
		RulesTab.normalizationTab.click();
		
		// Get all the normalization rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
		LogUltility.log(test, logger, "get all the normalization rules as list: " +rulesList);
			
		// Choose random record from the middle of the displayed list
		rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Add new output condition and check if it was added
		RulesTab.rulesOutputAreaAddRuleAndCheck(test,logger);
		
	   //Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	
	
	/**
	 * DIC_TC--485:check the process of removing outputs from rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_485_check_the_process_of_removing_outputs_from_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--485", "Check the process of removing outputs from rules");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Click the normalization rules tab
		RulesTab.normalizationTab.click();
		
		// Get all the normalization rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
		LogUltility.log(test, logger, "get all the normalization rules as list: " +rulesList);
			
		// Choose random record from the middle of the displayed list
		rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
		// Pick a random record
		Random randomizer = new Random();
//		String randomrecord = "066 : ROMAN NUMERAL before a.d./BC/BCE";
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Add new output condition incase there wasn't any
//		RulesTab.rulesOutputArea();
//		LogUltility.log(test, logger, "Add new output condition incase there wasn't any");
		
		// Get the amount of output area panes
		int panesAmount = RulesTab.getDisplayedOutputPanesAmount();
		LogUltility.log(test, logger, "Number of output panes/conditions: "+panesAmount);
		
		// if there is no output panes, then add one 
		if(panesAmount == 0) { 	
		LogUltility.log(test, logger, "Add output condition");
		RulesTab.rulesOutputArea();
		panesAmount = 1;
		}
		
		// Get the amount of output area panes
		int paneIndex = randomizer.nextInt(panesAmount);
		LogUltility.log(test, logger, "Delete output pane at index: "+paneIndex);
		 
		// Remove the first output condition and verify if it was deleted
//		RulesTab.rulesOutputAreaRemoveRule(test, logger, paneIndex);
		
		// Remove the first output condition and verify if it was deleted
		RulesTab.rulesOutputAreaRemovePane(paneIndex);
		
		// Get number of panes after removing
		int panesAfter = RulesTab.getDisplayedOutputPanesAmount();
		LogUltility.log(test, logger, "The pane amount after removing: "+panesAfter);
		
		boolean paneRemoved = panesAfter == panesAmount -1;
		LogUltility.log(test, logger, "The pane has been removed: "+paneRemoved);
				
	   //Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--486:Check the word in position value could be changed and saved in outputs
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_486_Check_the_word_in_position_value_could_be_changed_and_saved_in_outputs() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--486", "Check the word in position value could be changed and saved in outputs");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Click the normalization rules tab
		RulesTab.normalizationTab.click();
		
		// Get all the normalization rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
		LogUltility.log(test, logger, "get all the normalization rules as list: " +rulesList);
			
		// Choose random record from the middle of the displayed list
		rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
		// Pick a random record
		Random randomizer = new Random();
//		String randomrecord = "066 : ROMAN NUMERAL before a.d./BC/BCE";
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the amount of output area panes
		int panesAmount = RulesTab.getDisplayedOutputPanesAmount();
		LogUltility.log(test, logger, "Number of output panes/conditions: "+panesAmount);
		
		// if there is no output panes, then add one 
		if(panesAmount == 0) { 	
		LogUltility.log(test, logger, "Add output condition");
		RulesTab.rulesOutputArea();
		panesAmount = 1;
		}
		
		// Get random pane index to change
		int paneIndex = randomizer.nextInt(panesAmount);
		LogUltility.log(test, logger, "Change WIP value for pane number: "+paneIndex);
		
		// Get the word in position(WIP) value of the first condition before the change
		List<String> chosenValueB4Change = RulesTab.readRulesOutputArea(paneIndex);
		LogUltility.log(test, logger, "Value before change is: "+chosenValueB4Change.get(0));
			
		// get the wip available values
		List<String> wipValues = RulesTab.readRulesOutputtAreaDropDownValues(paneIndex,0);
		wipValues.remove(chosenValueB4Change.get(0));
		
		// Pick a new random WIP value
		Random randomizerWIP = new Random();
		String randomWip = wipValues.get(randomizerWIP.nextInt(wipValues.size()));
		LogUltility.log(test, logger, "Choose new valueP: "+randomWip);
		int value = Integer.parseInt(randomWip);

		// Change the WIP value
		RulesTab.rulesOutputArea(paneIndex, value);
		LogUltility.log(test, logger, "Change the word in position value to: "+value);

		//Press the save button  
		Thread.sleep(3000);
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		Thread.sleep(3000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
		
		// Click the rule again to refresh
		CommonFunctions.chooseValueInDropdown(RulesTab.normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Click the rule to refresh it ");
		
		// Get the word in position(WIP) value of the first condition after the change
		List<String> chosenValueAfterChange = RulesTab.readRulesOutputArea(paneIndex);
			
		boolean isDiffrenet = !chosenValueAfterChange.get(0).equals(chosenValueB4Change.get(0));
		LogUltility.log(test, logger, "The value before change: "+chosenValueB4Change.get(0)+" is diffrenet from the value after change: "+chosenValueAfterChange.get(0)+
				" the values are differenet: "+isDiffrenet);
		
		assertTrue(isDiffrenet); 
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--712:Check the output body
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_712_Check_the_output_body() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--712", "Check the output body");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);

		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Click the normalization rules tab
		RulesTab.normalizationTab.click();
		
		// Get all the normalization rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
		LogUltility.log(test, logger, "get all the normalization rules as list: " +rulesList);
			
		// Choose random record from the middle of the displayed list
		rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
		// Pick a random record
		Random randomizer = new Random();
//		String randomrecord = "066 : ROMAN NUMERAL before a.d./BC/BCE";
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the amount of output area panes
		int panesAmount = RulesTab.getDisplayedOutputPanesAmount();
		LogUltility.log(test, logger, "Number of output panes/conditions: "+panesAmount);
		
		// if there is no output panes, then add one 
		if(panesAmount == 0) { 	
		LogUltility.log(test, logger, "Add output condition");
		RulesTab.rulesOutputArea();
		panesAmount = 1;
		}
				
		// Get random pane index to change
		int paneIndex = randomizer.nextInt(panesAmount);
		LogUltility.log(test, logger, "Check the pane number: "+paneIndex);
	
		// Get values from the output pane
		List<String> paneValues = RulesTab.readRulesOutputAreaValues(paneIndex);
		LogUltility.log(test, logger, "Output pane values: "+paneValues);
		
		// Delete button 'X' element
		boolean xElement = RulesTab.rulesOutputAreaIfExist(paneIndex,"Button", "X");

		// Get the word in position(WIP) value of the first condition before the change
		String WIPValue= RulesTab.readRulesOutputArea(paneIndex).get(0);
		LogUltility.log(test, logger, "Value before change is: "+WIPValue);
				
		// Word in position dropdown element
		boolean WIPDD =  RulesTab.rulesOutputAreaIfExist(paneIndex,"ComboBox", WIPValue);
		
		// Second dropdown element
		boolean DD2 = RulesTab.rulesOutputAreaIfExist(paneIndex,"ComboBox", paneValues.get(1));
		
		// Verify that all elements were found
		boolean elementsFound = xElement && WIPDD && DD2 ;
		
		LogUltility.log(test, logger, "elements found: "+elementsFound);
		 
	   //Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
			 	
		 assertTrue(elementsFound);
		 
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	/**
	 * DIC_TC--499:Check user can save the value for "replace word" in outputs
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_499_Check_user_can_save_the_value_for_replace_word_in_outputs() throws InterruptedException, FileNotFoundException, AWTException
	{
		test = extent.createTest("Rules Test - DIC_TC_499", "Check user can save the value for \"replace word\" in outputs");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);	
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();	
		RulesTab.clickTabs("normalization");
		
		// get all the normalization  rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
		LogUltility.log(test, logger, "get all the normalization  rules as list: " +rulesList);
		
		// Pick a random record
		Random randomizer = new Random();
//		String randomrecord = "066 : ROMAN NUMERAL before a.d./BC/BCE";
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the amount of output area panes
		int panesAmount = RulesTab.getDisplayedOutputPanesAmount();
		LogUltility.log(test, logger, "Number of output panes/conditions: "+panesAmount);
		
		// if there is no output panes, then add one 
		if(panesAmount == 0) { 	
		LogUltility.log(test, logger, "Add output condition");
		RulesTab.rulesOutputArea();
		panesAmount = 1;
		}
		
		// Get random pane index to change
		int paneIndex = randomizer.nextInt(panesAmount);
		LogUltility.log(test, logger, "Check the pane number: "+paneIndex);
	
		// Get values from the output pane
		List<String> paneValues = RulesTab.readRulesOutputAreaValues(paneIndex);
		LogUltility.log(test, logger, "Output pane values: "+paneValues);
		
		// Change second dropdown value
		RulesTab.rulesOutputArea(0, paneValues.get(1), "replace word");
		LogUltility.log(test, logger, "Change the dropdown value to 'replace word' ");	
		
		// Enter value in the empty field
		String randomText = RulesTab.RandomString(5);
		RulesTab.rulesOutputArea(paneIndex ,0, randomText);
		LogUltility.log(test, logger, "In the empty text field enter:"+randomText);
		
		//Press the save button  
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		Thread.sleep(3000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");

		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Choose record again
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Verify that the dropdown is "replace word" and value is the same as filled in 
		boolean Dropdown = RulesTab.rulesOutputAreaIfExist(paneIndex,"ComboBox", "replace word");
		LogUltility.log(test, logger, "Verify that the dropdown \"replace word\" is exist and the value should be True : " + Dropdown);
		assertTrue(Dropdown);
		boolean textBox = RulesTab.rulesOutputAreaIfExist(paneIndex, "Edit", randomText);
		LogUltility.log(test, logger, "Verify that the text field :"+randomText+" ,is exist and the value should be True : " + textBox);
		assertTrue(textBox);
				
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	
	@AfterMethod(alwaysRun = true)
	  public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
	    {		
			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Rules/Disambiguation/Rules Outputs",method.getName());
			
//				System.out.println("tryCount: " + tryCount);
//				System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//				        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//				        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}

	/**
	 * Closing the application after running all the TCs
	 */
	@AfterClass(alwaysRun = true) 
	public void CloseBrowser() {
		 
		 //DriverContext._Driver.quit();
			}

}
