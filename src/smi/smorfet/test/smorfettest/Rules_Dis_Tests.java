package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.RulesTab;

/**
 * 
 * All tests for Disambiguation Rules
 *
 */
public class Rules_Dis_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
 * Initialize - run once before all tests
 * @throws IOException
 * @throws ClassNotFoundException 
 */
@Parameters("testlink")
@BeforeClass(alwaysRun = true)
public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
	
	// Get rid of the report html warnings
//	freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
	System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");

	// Get settings
	ConfigReader.GetAllConfigVariable();

	// Initialize the report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Rules tab - Disambiguation");

	// Logger
	logger = LogManager.getLogger(RulesTab.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= Rules tab - Disambiguation");
	logger.info("Rules tab Tests - FrameworkInitilize");
	
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
	
	// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Initialize Smorfet Application
	InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	
	
	// Do go and close the Start window that was opened with the initialized Driver and continue
	// with an already opened Smorfet app
//		CurrentPage = GetInstance(RulesTab.class);
//		CurrentPage.As(RulesTab.class).Start_Window.click();
//		CurrentPage.As(RulesTab.class).bt_abort.click();
	
}

/**
 * Run this before each test
 * @param method for test information
 * @throws Exception
 */
@BeforeMethod(alwaysRun = true)
public void beforeTest(Method method) throws Exception {
	logger.info("");
	logger.info("#####################################################");
	logger.info("Starting Test: " + method.getName());
	
	// Reset string builder
	LogUltility.sbClean();
	
	// Count how many a test was processed
	tryCount++;
	
	try {
		// Smorfet app focus
		CurrentPage = GetInstance(RulesTab.class);
//		CurrentPage.As(RulesTab.class).TitleBar.click();
		CommonFunctions.clickTabs("Rules");
		CurrentPage.As(RulesTab.class).scrollUpRulesInputArea();
		
	} catch (Exception e) {
//		e.printStackTrace();
	}
}

	/**
		 * DIC_TC--20:verify user can delete a Rule
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_20_verify_user_can_delete_a_Rule() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC_20", "verify user can delete a Rule");

			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			//Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
			
			// Press the Disambiguation rules tab 
			RulesTab.disambiguationTab.click();	
			
			// get all the disambiguation rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);

			// Choose random record from the middle of the displayed list
			rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// get the rule number 
			String ruleNumberFromName = randomrecord.split(" : ")[0];
			LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
			
            // get the text from the rule name field
			String ruleName = RulesTab.ruleName.getText();
			LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
			
			//get the rule Number form Number field
			String ruleNumber = RulesTab.numberBox.getText();
			LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
			
			// Press the Delete button 
			RulesTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the Delete button");
			
			// Click yes button on the Delete poup 
			RulesTab.yesDeleteRule_bt.click();
			LogUltility.log(test, logger, "Click the Yes button on the Delete poup");
			
			//Press the OK button in the saving Dialog
			Thread.sleep(3000);
			RulesTab.OKSavingDisRuleDialog.click();
			LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
			
			boolean lexiconMessage = RulesTab.popupWindowMessage(test, logger, "Deleting disambiguation rule", "This rule might be referred to from the lexicon.\rWould myou like to regenerate it?");
			if(lexiconMessage)
				RulesTab.noDeleteRule_bt.click();
			
			//Verify that the deleted rule doesn`t appear in the list anymore 
			// get all the disambiguation rules as list
			List<String> rulesListAfterDelete= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesListAfterDelete);
			
			boolean ruleDeleted = ! rulesListAfterDelete.contains(ruleName);
			assertTrue(ruleDeleted);
			LogUltility.log(test, logger, " Verify that the list " + rulesListAfterDelete+ " doesn`t contain the deleted rule " + ruleName + " the value should be : " +ruleDeleted);
//				
			// Verify that the rule deleted from the  rules files
			String ruleFile= "disambiguation_rules.txt";
			boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, ruleName ,ruleFile);
			assertFalse(isRuleExist);
			LogUltility.log(test, logger, "Verify that the Deleted rule do not appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is " +ruleName+"  and the value should be False: " +isRuleExist);
			
//			RulesTab.noDeleteRule_bt.click();
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
	/**
	 * DIC_TC--25: Verify user can remove conditions successfully
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_25_Verify_user_can_remove_conditions_successfully() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_25", "Verify user can remove conditions successfully");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();
		
	// get all the disambiguation rules which has more than one condition as list from file
	List<String> rulesList= RulesTab.getDisRulesWithMultiConditions();;
	LogUltility.log(test, logger, "get all the disambiguation rules which has more than one condition as list: " +rulesList);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "055 : Dr = EN{n}drive~1 after {x}";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(RulesTab.disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
	// get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
		
	// Get the number of displayed panes/conditions of the rule
	int numberOfPanes = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
	LogUltility.log(test, logger, "The number of conditions/panes: " +numberOfPanes);
	
	//Click on the red [x] in the left screen to delete the condition 
	// Delete any condition rather than the first one
	int randomCondition = randomizer.nextInt(numberOfPanes-1)+1;
	LogUltility.log(test, logger, "Click on the red [x] in the left screen to delete the condition number: "+randomCondition);
	RulesTab.rulesInputAreaRemoveRule(test, logger,randomizer.nextInt(numberOfPanes-1)+1);
	
	 //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
		 
	 // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
	 LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--26: Delete rule's condition
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_26_Delete_rules_condition() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_26", "Delete rule's condition");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);

	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();

	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getDisRulesWithMultiSubConditions();
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "087 : {z} before {n, q, d, r, a}";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(RulesTab.disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
	// get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Get the pane index which has more than one sub condition
	List<Integer> panesIndexes = RulesTab.getDisRulePaneIndexesForSubCondition(ruleNumberFromName);
	 LogUltility.log(test, logger, "Panes which has more than one subconditions: "+panesIndexes);
	 
	 // Choose random pane to delete sub condition from
	 int paneIndex = panesIndexes.get(randomizer.nextInt(panesIndexes.size()));
	 LogUltility.log(test, logger, "Delete sub condition from pane: "+paneIndex);
	 
	//Click on the right red X button to delete sub condition
	 RulesTab.rulesInputAreaDeleteSubCondition(paneIndex,1);
	 LogUltility.log(test, logger, "Click on the right red X button to delete sub condition");
		
	   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--27: Add new sub condition to a rule
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_27_Add_new_sub_condition_to_a_rule() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_27", "Add new sub condition to a rule");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);

	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();
	
	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "015 : {Q} before {P}";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
	// get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	 // Get the amount of panes/condition for the rule
	 int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
	 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
	 
	 // Add new pane/condition
	 RulesTab.rulesInputArea();
	LogUltility.log(test, logger, "Add new condition/pane" );
	
	//Click on the right green plus + button
	 RulesTab.rulesInputArea(panesAmount, "add condition");
	 LogUltility.log(test, logger, "Click on the right green plus + button near the condition");
	 
	 // Verify that a new subcondition added with operator OR
	 String buttonValue = RulesTab.rulesInputAreaCheckOR(panesAmount,1,0);
	 LogUltility.log(test, logger, "Verify that the button value is or : " +buttonValue);
	 boolean buttonChanged = buttonValue.equals("or");
	 LogUltility.log(test, logger, "Verify that the button value is or, so the value should be tru: " +buttonChanged);
		 assertTrue(buttonChanged);
	
	   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	
	/**
	 * DIC_TC--28: Add new condition
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_28_Add_new_condition() throws InterruptedException, FileNotFoundException, AWTException
	{
		test = extent.createTest("Rules Test - DIC_TC_28", "Add new condition");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();
	
	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList,20);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "021 : EN{v}pay~3 after EN{m}will~1";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(RulesTab.disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	//*****************************
	RulesTab.scrollUpRulesInputArea();
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
	// get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Get displayed panes amount before adding
	int panesB4 = RulesTab.getDisplayedRulePanesAmount();
	LogUltility.log(test, logger, "Panes/Conditions amount BEFORE adding: "+panesB4);
	
	//Click on the right green plus + button on the upper right side 
//	RulesTab.rulesInputAreaAddRuleAndCheck(test, logger);
	 RulesTab.rulesInputArea();
	LogUltility.log(test, logger, "Click on the right green plus + button on the upper right side ");
	
	// Get displayed panes amount after adding
	int panesAfter = RulesTab.getDisplayedRulePanesAmount();
	LogUltility.log(test, logger, "Panes/Conditions amount AFTER adding: "+panesAfter);
	
	// Verify it was added
	boolean paneAdded = panesAfter>panesB4;
	LogUltility.log(test, logger, "The condition has been added successfully: "+paneAdded);
	assertTrue(paneAdded);
	 
	   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--30:Check the save button
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_30_Check_the_save_button() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_30", "Check the save button");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();
	
	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "029 : a_amount after a_amount";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the text from the rule name field
	String textFromFieldB4Change = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
		
	// Get number of conditions
	int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
	LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
	
	// Choose random pane to work with
	int paneIndex = randomizer.nextInt(panesAmount);
	LogUltility.log(test, logger, "Change values for pane number: "+paneIndex);
	
	// Get pane dropdown values
	List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex);
	LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
	
	// Get the first list values
	List<String> keys = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 1);
	keys.remove(paneValues.get(1));
	String key = keys.get(randomizer.nextInt(keys.size()));
	LogUltility.log(test, logger, "Choose new key value: " + key);
	
	//Change the  first dropdown values
	WebElement dropdownElement = RulesTab.rulesInputArea(paneIndex,0,paneValues.get(1), key);
	List<String> fisrtdropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement);
	LogUltility.log(test, logger, "the first dropdown element is : " + fisrtdropdownvalue);
	
	// Get the second list values
	List<String> secDropdown = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 2);
	secDropdown.remove(paneValues.get(2));
	String key2 = secDropdown.get(randomizer.nextInt(secDropdown.size()));
	LogUltility.log(test, logger, "Choose second dropdown value: " + key2);
		
	//Change the  second dropdown values
	WebElement dropdownElement2 =RulesTab.rulesInputArea(paneIndex,0,paneValues.get(2), key2);
	List<String> seconddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement2);
	LogUltility.log(test, logger, "the second dropdown element is : " + seconddropdownvalue);
	
	// Get the third list values
	List<String> thirdDropdown = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 3);
	String key3 = thirdDropdown.get(randomizer.nextInt(thirdDropdown.size()));
	LogUltility.log(test, logger, "Choose third dropdown value: " + key3);
	
	// Since changing the first dropdown will reset the third dropdown value
	List<String> paneValues2 = RulesTab.readRulesInputAreaValues(paneIndex);
		
	//Changee the  third dropdown values
	WebElement dropdownElement3 =RulesTab.rulesInputArea(paneIndex,0,paneValues2.get(3),key3);
	List<String> thirddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement3);
	LogUltility.log(test, logger, "the third dropdown element is : " + thirddropdownvalue);
	
	//Press the save button  
	Thread.sleep(3000);
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(3000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	//Select the same rule
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// Verify that the new values still appear in the dropdowns
	boolean FDD = fisrtdropdownvalue.get(0).equals(key);
	assertTrue(FDD);
	LogUltility.log(test, logger, "The value in the first dropdown is:"+key+" and value should be True: "+FDD );
	
	boolean SDD = seconddropdownvalue.get(0).equals(key2);
	assertTrue(SDD);
	LogUltility.log(test, logger, "The value in the second dropdown is:"+key2+" and value should be True: "+SDD );
	
	boolean THDD = thirddropdownvalue.get(0).equals(key3);
	assertTrue(THDD);
	LogUltility.log(test, logger, "The value in the third dropdown is:"+key3+" and value should be True: "+THDD );


	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
				 * DIC_TC--31:Add a new rule
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_31_Add_a_new_rule() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_31", "Add a new rule");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
				
				// Press the new button
				RulesTab.btnNew.click();
				LogUltility.log(test, logger, "Click the new button ");
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "The new Rule number from the Number field: " +ruleNumber);

				// Type a record name in the key form field
				String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) ;
				RulesTab.ruleName.click();
				String newRandomText = randomText;
				RulesTab.ruleName.sendKeys(newRandomText);
				LogUltility.log(test, logger, "New rule name: " + newRandomText);
				
	          // get the text from the rule name field
				String textFromFieldB4Save = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before Save: " +textFromFieldB4Save);
				
				//Press the save button  
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
	//                // get the text from the rule name field
	//				String textFromFieldafterChange = RulesTab.ruleName.getText();
	//				LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldafterChange);
				
				// verify that the rule name was changed 
				String ruleFile= "disambiguation_rules.txt";
				boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, textFromFieldB4Save, ruleFile);
				assertTrue(isRuleExist);
				LogUltility.log(test, logger, "Verify that the new rule do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +textFromFieldB4Save+" ,  and the value should be True: " +isRuleExist);
				
				
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
	 * DIC_TC--32: Check the "All" button
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_32_Check_the_All_button() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--32", "Check the �All� button ");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the disambiguation rules tab
	RulesTab.disambiguationTab.click();
	
	// Click the All button
	RulesTab.allButton.click();
	LogUltility.log(test, logger, "Click the All button");
	
	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(5000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Get the checkbox list status from the file
	Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Dis");
	
	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
	// Check if all checkboxes are active
	boolean markedCheckBoxes = true;
	for(int i=0;i<checkBoxList.size();i++)
	{
		String ruleName = rulesList.get(i).split(" : ")[1];
		if(checkBoxList.get(ruleName).equals("disabled"))
			markedCheckBoxes = false;
	}
	
	LogUltility.log(test, logger, "All checkboxes are marked: "+markedCheckBoxes);	
	assertTrue(markedCheckBoxes);
	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--33:Check the "None" button
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_33_Check_the_None_button() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--33", "Check the �None� button");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the disambiguation rules tab
	RulesTab.disambiguationTab.click();
	
	// Click the none button
	RulesTab.noneButton.click();
	LogUltility.log(test, logger, "Click the None button");
	
	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(5000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Get the checkbox list status from the file
	Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Dis");
	
	// get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
	// Check if all checkboxes are disabled
	boolean unmarkedCheckBoxes = true;
	for(int i=0;i<checkBoxList.size();i++)
	{
		String ruleName = rulesList.get(i).split(" : ")[1];
		if(checkBoxList.get(ruleName).equals("active"))
			unmarkedCheckBoxes = false;
	}
	
	LogUltility.log(test, logger, "All checkboxes are unmarked: "+unmarkedCheckBoxes);	
	assertTrue(unmarkedCheckBoxes);
	
	LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
				 * DIC_TC--51: Verify "Revert" button working successfully
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_51_Verify_Revert_button_working_successfully() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_51", "Verify \"Revert\" button working successfully");
	
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				// Get the text from the Description field 
				String ruleDescriptionb4change = RulesTab.descriptionBox.getText();
				LogUltility.log(test, logger, "Rule description before change: " +ruleDescriptionb4change);
				
				
				// Type a new description on the field 
				String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5);
				RulesTab.descriptionBox.click();
				String newRandomText = randomText;
				RulesTab.descriptionBox.sendKeys(newRandomText);
				LogUltility.log(test, logger, "New rule Description: " + newRandomText);
				
				// Get the text from the Description field after the change 
				String ruleDescriptionAchange = RulesTab.descriptionBox.getText();
				LogUltility.log(test, logger, "Rule description after change: " +ruleDescriptionAchange);
				
				//Verify that the rule have a new description that different than the first one 
				boolean descriptionChanged = ! ruleDescriptionb4change.equals(ruleDescriptionAchange);               
				assertTrue(descriptionChanged);
				LogUltility.log(test, logger, " Verify that the First Description : " + ruleDescriptionb4change + " different than the new Description : " + ruleDescriptionAchange + " the value should be True : " +descriptionChanged);
				
				
				//Press the revert button  
				Thread.sleep(3000);
				RulesTab.bt_revert.click();
				LogUltility.log(test, logger, "Click the revert button ");
				
				//Press the OK button in the revert Dialog
				Thread.sleep(3000);
				RulesTab.revertFileMsgOK.click();
				LogUltility.log(test, logger, "Click the OK  button on the revert popup ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				// Get the text from the Description field after press the revert button 
				String ruleDescriptionArevert= RulesTab.descriptionBox.getText();
				LogUltility.log(test, logger, "Rule description after press the revert button: " +ruleDescriptionArevert);
				
				
	//				// Get the text from the Description field 
	//				String ruleDescription = RulesTab.descriptionBox.getText();
	//				LogUltility.log(test, logger, "Rule description before change: " +ruleDescription);
				
				
				//Verify that the rule have a new priority that different than the first one 
				boolean descriptionChangedARevert = ruleDescriptionb4change.equals(ruleDescriptionArevert);               
				assertTrue(descriptionChangedARevert);
				LogUltility.log(test, logger, " Verify that the First Description : " + ruleDescriptionb4change + " is the same as the Description after revert : " + ruleDescriptionArevert + " the value should be True : " +descriptionChangedARevert);		
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--57: Verify the arrows buttons are working
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_57_Verify_the_arrows_buttons_are_working() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_57", "Verify the arrows buttons are working");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//Press the arrows
				// Click the Right arrow
				// CurrentPage.As(RulesTab.class).Next_button.click();
				 CurrentPage.As(RulesTab.class).Next_button.click();
				 LogUltility.log(test, logger, "Click the Next arrow");
				// Get the selected record in the results area after press the next button
				List<String> chosenRecordNext= CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList);
				LogUltility.log(test, logger, "The selected Record after press the next arrow is: " + chosenRecordNext);
				 boolean nextRecord = !randomrecord.equals(chosenRecordNext);
				 assertTrue(nextRecord);
				 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ randomrecord +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +nextRecord);
				// Click the Left arrow
				 CurrentPage.As(RulesTab.class).Previous_button.click();
				 LogUltility.log(test, logger, "Click the Previous arrow");
				 //Get the selected record in the results area after press the Previous button
				 List<String> chosenRecordPrevious= CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList);
				 LogUltility.log(test, logger, "The selected Record after press the Previous arrow is: " + chosenRecordPrevious);
				 boolean previousRecord = !chosenRecordPrevious.equals(chosenRecordNext);
				 assertTrue(previousRecord);
				 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ chosenRecordPrevious +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +previousRecord);			
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
			 * DIC_TC--179:Check the two arrows in the footer
			 * @throws InterruptedException 
			 * @throws FileNotFoundException 
			 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_179_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC_179", "Check the two arrows in the footer");
			
			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			// Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
	
			// Press the Disambiguation rules tab 
			RulesTab.disambiguationTab.click();
					
			// get all the disambiguation rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
			// get the first rule name from the list
			String firstName = rulesList.get(0);
			String secondName = rulesList.get(1);
			
			// Click the first rule
			CommonFunctions.chooseValueInDropdown(RulesTab.disambiguationRulesList, firstName);
			LogUltility.log(test, logger, "Click the first rule to select it ");

			// Press the right arrow 
			RulesTab.Next_button.click();
			LogUltility.log(test, logger, "Click the next (right) button ");
			
			// Get the chosen rules list
			List<String> chosenRules = RulesTab.getChosenValueInDropdown(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "Chosen rules list after press the right arrow :  " +chosenRules);
		     
			//Check that the chosen rule is the same as the second rule 
		     boolean sameSecondRule = chosenRules.get(0).equals(secondName);
		 	LogUltility.log(test, logger, "Verify that the second rule is selected: " +chosenRules.get(0)+ " and it`s equal to the second rule from the list : " +secondName + " and value should be true :  " +sameSecondRule);
		     assertTrue(sameSecondRule);

			// Press the left arrow 
			RulesTab.Previous_button.click();
			LogUltility.log(test, logger, "Click the previous (left) button ");

			
			// Get the chosen rules list
			List<String> chosenRules2 = RulesTab.getChosenValueInDropdown(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "Chosen rules list after press the right arrow :  " +chosenRules2);
			
		     boolean sameFirstRule = chosenRules2.get(0).equals(firstName);
		 	LogUltility.log(test, logger, "Verify that the first rule is selected: " +chosenRules2.get(0)+ " and it`s equal to the second rule from the list : " +firstName + " and value should be true :  " +sameFirstRule);
		     assertTrue(sameFirstRule);
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
	
			/**
				 * DIC_TC--180:Check the rule number is displayed in the number field
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_180_Check_the_rule_number_is_displayed_in_the_number_field() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_180", "Check the rule number is displayed in the number field");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
				//Convert the rule number from the name  to integer
				int ruleNumberFromNameInt= Integer.parseInt(ruleNumberFromName);
				LogUltility.log(test, logger, "Convert the number to integer: " +ruleNumberFromNameInt);
				
	            // get the text from the rule number field
				String RuleNumberFromField = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +RuleNumberFromField);
				
				//Convert therule number from field to integer
				int RuleNumberFromFieldInt= Integer.parseInt(RuleNumberFromField);
				LogUltility.log(test, logger, "Convert the number to integer: " +ruleNumberFromNameInt);
					
				// verify that the rule number is the same in the field and in the name 
				boolean sameName = ruleNumberFromNameInt==RuleNumberFromFieldInt;
				LogUltility.log(test, logger, "Rule number from the rule name : " +ruleNumberFromNameInt+" is equal for the number from the field  : " +RuleNumberFromFieldInt+ " and value should be true : "+sameName );
				assertTrue(sameName);
			
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--181:Check application does not save a rule with an empty part
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_181_Check_application_does_not_save_a_rule_with_an_empty_part() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_181", "Check application does not save a rule with an empty part");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "021 : EN{v}pay~3 after EN{m}will~1";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
				// Get number of conditions
				int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
				LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
				
				//Click on the right green plus + button on the upper right side 
//				RulesTab.rulesInputAreaAddRuleAndCheck(test, logger);
				 RulesTab.rulesInputArea();
				LogUltility.log(test, logger, "Click on the right green plus + button on the upper right side ");
	
				// Get pane dropdown values
				List<String> paneValues = RulesTab.readRulesInputAreaValues(panesAmount);
				LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
				
				// Get the first list values
				List<String> keys = RulesTab.readRulesInputAreaDropDownValues(panesAmount, 1);
				keys.remove(paneValues.get(1));
				String key = keys.get(randomizer.nextInt(keys.size()));
				LogUltility.log(test, logger, "Choose new key value: " + key);
				
				//Change the  first dropdown values
				WebElement dropdownElement = RulesTab.rulesInputArea(panesAmount,0,paneValues.get(1), key);
				// Choose same value again to reset the third dropdown
				 dropdownElement = RulesTab.rulesInputArea(panesAmount,0,key, key);
				List<String> fisrtdropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement);
				LogUltility.log(test, logger, "the first dropdown element is : " + fisrtdropdownvalue);	
				
				// Press the Save button
				RulesTab.btnSave.click();
				
				// Verify that the "One of the drop downs is empty!. Correct that before you save!"" message appear 
				boolean emptyDDMsgappear = RulesTab.popupWindowMessage(test, logger, "", "One of the drop downs is empty!. Correct that before you save!");
				assertTrue(emptyDDMsgappear);
				LogUltility.log(test, logger, "Is Empty Dropdown message appear, should be true: " +emptyDDMsgappear);
				
				// Press the cancel button on the rule`s name popup ruleNameCancelbtn
				RulesTab.emptyDDOKbtn.click();
				LogUltility.log(test, logger, "Press the OK button on the Empty DD popup");

				// click the Revert  button 
				Thread.sleep(3000);
				RulesTab.bt_revert.click();
				LogUltility.log(test, logger, "Click the Revert button, to revert the changes ");
				
				// press the OK button on the revert file popup revertFileMsgOK
				Thread.sleep(3000);
				RulesTab.revertFileMsgOK.click();
				LogUltility.log(test, logger, "Click the OK button, to revert the changes ");
				
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--183: Check the position of the word could be chagned and saved
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_183_Check_the_position_of_the_word_could_be_chagned_and_saved() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_183", "Check the position of the word could be chagned and saved");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
		
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field: " +ruleName);
			
				// Get number of conditions
				int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
				LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
				
				// Choose random pane to work with
				int paneIndex = randomizer.nextInt(panesAmount);
				LogUltility.log(test, logger, "Change values for pane number: "+paneIndex);
				
				// Get pane dropdown values
				List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex);
				LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
				
				// get the selected Word in position in the required row
				String selectedWIP = paneValues.get(0);
				LogUltility.log(test, logger, "the selected word in position before the change is: "+selectedWIP );
				
				// Get the first list values
				List<String> WIPvalues = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 0);
				WIPvalues.remove(selectedWIP);
				String newWIP = WIPvalues.get(randomizer.nextInt(WIPvalues.size()));
				LogUltility.log(test, logger, "Choose new key value: " + newWIP);
				
				// Choose a value from word in position dropdown
				RulesTab.rulesInputArea(paneIndex,Integer.parseInt(newWIP));
			
				//Press the save button  
//				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the selected Word in position in the required row
				List<String> paneValuesAfter = RulesTab.readRulesInputAreaValues(paneIndex);
				LogUltility.log(test, logger, "Displayed pane values After change: "+paneValuesAfter);
				
				String selectedWIP2 = paneValuesAfter.get(0);
				LogUltility.log(test, logger, "the selected word in position After the change is: "+selectedWIP2 );
				
				String selectedWIPb4change=selectedWIP;
				String selectedWIPAfterchange=selectedWIP2;
				
				//Verify that the rule have a new score that different than the first one 
				boolean wipChanged = ! selectedWIPb4change.equals(selectedWIPAfterchange);               
				assertTrue(wipChanged);
				LogUltility.log(test, logger, " Verify that the First word in position  " + selectedWIPb4change+ " was changed and  different than the new word in position  " + selectedWIPAfterchange + " the value should be True : " +wipChanged);
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--184: Check the rule score could be chagned and saved
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_184_Check_the_rule_score_could_be_chagned_and_saved() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_184", "Check the rule score could be chagned and saved");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule score form Score field 
				List<String> chosenScore =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field: " +chosenScore);
				
				//Get the score list
				List<String> lst_Scores = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).ScoreDD);
				LogUltility.log(test, logger, "Score valueslist: " + lst_Scores);
				lst_Scores.remove(chosenScore.get(0));
				LogUltility.log(test, logger, "Score values list after removing the chosen value: " + lst_Scores);
				
				// Press the score dropdown and chose a random value 
				// Pick a random record
				Random randomizerScore = new Random();
				String randomScore = lst_Scores.get(randomizerScore.nextInt(lst_Scores.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, randomScore);
				LogUltility.log(test, logger, "Choose from record list: " + randomScore);
				
				
				// Choose a value from score dropdown
				String newScore = randomScore;
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, newScore);
				LogUltility.log(test, logger, "Choose new score from the dropdown: " + newScore);
				
				//Press the save button  
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
							
				//get the rule score form Score field after the change 
				List<String> ruleScoreAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field after the change: " +ruleScoreAChange);

				// convert the chosen list to string 
				String ruleScore = chosenScore.get(0);
				LogUltility.log(test, logger, "Rule Score from the chosen list: " +ruleScore);
				
				//Verify that the rule have a new score that different than the first one 
				boolean scoreChanged = ! ruleScore.equals(ruleScoreAChange.get(0));               
				assertTrue(scoreChanged);
				LogUltility.log(test, logger, " Verify that the First score " + ruleScore+ " different than the new score " + ruleScoreAChange + " the value should be True : " +scoreChanged);	
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--185: Check the drop down options in the conditions
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_185_Check_the_drop_down_options_in_the_conditions() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_185", "Check the drop down options in the conditions");
	
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "029 : a_amount after a_amount";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
				//Changee the  first dropdown values
				WebElement dropdownElement = RulesTab.rulesInputArea(1,0,"semantic groups", "gender");
				LogUltility.log(test, logger, "Select rule 29 , in the second row change the semantic groups dropdown to gender");
				List<String> fisrtdropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement);
				LogUltility.log(test, logger, "the first dropdown element is : " + fisrtdropdownvalue);
				
				//Changee the  second dropdown values
				WebElement dropdownElement2 =RulesTab.rulesInputArea(1,0,"has key", "has no key");
				LogUltility.log(test, logger, "in the second row change the 'has key'  dropdown  to 'has no key'");
				List<String> seconddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement2);
				LogUltility.log(test, logger, "the second dropdown element is : " + seconddropdownvalue);
				
				//Changee the  third dropdown values
				WebElement dropdownElement3 =RulesTab.rulesInputArea(1,0,"unknown", "male");
				List<String> thirddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement3);
				LogUltility.log(test, logger, "the third dropdown element is : " + thirddropdownvalue);
				LogUltility.log(test, logger, "in the third row change the 'unknown'  dropdown  to 'male'");
				
				
				//Press the save button  
				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				// Verify that the new values still appear in the dropdowns
				boolean FDD = fisrtdropdownvalue.get(0).equals("gender");
				assertTrue(FDD);
				LogUltility.log(test, logger, "The value in the first dropdown is 'gender' and value should be True: "+FDD );
				
				boolean SDD = seconddropdownvalue.get(0).equals("has no key");
				assertTrue(SDD);
				LogUltility.log(test, logger, "The value in the first dropdown is 'has no key' and value should be True: "+SDD );
				
				boolean THDD = thirddropdownvalue.get(0).equals("male");
				assertTrue(THDD);
				LogUltility.log(test, logger, "The value in the first dropdown is 'male' and value should be True: "+THDD );
				

				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--208: Verify user can delete sub conditions in each rules
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_208_Verify_user_can_delete_sub_conditions_in_each_rules() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_208", "Verify user can delete sub conditions in each rules");
				
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				 
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "063 : EN{n}1~0 before n_quantity";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				// Get the amount of panes/condition for the rule
				 int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
				 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
				 
				 // Add new pane/condition
				 RulesTab.rulesInputArea();
				LogUltility.log(test, logger, "Add new condition/pane" );		
				RulesTab.rulesInputArea(panesAmount,0,"unknown", "article");
						
				//Click on the gray �or� button to add condition
				 RulesTab.rulesInputArea(panesAmount,0,0);
				 LogUltility.log(test, logger, "Click on the gray �or� button to add dropdown , New dropdown added");
				 
				 // Check that the dropdown removed after pressing the OR button for the second time
				 boolean DDExist =  RulesTab.rulesInputAreaIfExist(panesAmount, 0, "ComboBox", "unknown");
				 LogUltility.log(test, logger, "Verify that the Dropdown was added after press the 'OR' button for the first time. value should be true : " + DDExist);
				 assertTrue(DDExist);
				 
				 // verify that new dropdown added , by selecting new value than the unknown 
				WebElement dropdownElement3 =RulesTab.rulesInputArea(panesAmount,0,"unknown", "name");
				List<String> thirddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement3);
				LogUltility.log(test, logger, "the first dropdown element is : " + thirddropdownvalue);
				LogUltility.log(test, logger, "in the second row change the 'unknown'  dropdown  to 'name'");
				 
	
				boolean THDD = thirddropdownvalue.get(0).equals("name");
				assertTrue(THDD);
				LogUltility.log(test, logger, "The value in the first dropdown is 'name' and value should be True: "+THDD );
				
				
				//Click on the gray �or� button to add condition
				 RulesTab.rulesInputArea(panesAmount,0,1);
				 LogUltility.log(test, logger, "Click on the gray �or� button to delete the last created dropdown , Last Dropdown removed");
				
				 // Check that the dropdown removed after pressing the OR button for the second time
				 boolean DDDeleted =  RulesTab.rulesInputAreaIfExist(panesAmount, 0, "ComboBox", "name");
				 LogUltility.log(test, logger, "Verify that the Dropdown was deleted after press the 'OR' button for the second time. value should be False : " + DDDeleted);
				 assertFalse(DDDeleted);
				
	           //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--211:Verify user can change the rule name
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_211_Verify_user_can_change_the_rule_name() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_211", "Verify user can change the rule name");
		
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList,20);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
				
				// get the first rule name from the list
				String firstName = rulesList.get(0).split(" : ")[1];
				LogUltility.log(test, logger, "The first rule text is: " +firstName);
				
				// Click the first rule
				// Click the first rule
				CommonFunctions.chooseValueInDropdown(RulesTab.disambiguationRulesList, rulesList.get(0));
				LogUltility.log(test, logger, "Click the first rule to select it ");
				
	            // get the text from the rule name field
				String textFromFieldB4Change = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change);
				
				// verify that the rule name is the same before change 
				boolean sameName = textFromFieldB4Change.equals(firstName);
				LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change+" is equal for the text from the list : " +firstName+ " and value should be true : "+sameName );
				assertTrue(sameName);
				
				
				// Type a record name in the key form field
				String randomText = RulesTab.RandomString(5);
				RulesTab.ruleName.click();
				String newRandomText = RulesTab.ruleName.getText() + randomText;
				RulesTab.ruleName.sendKeys(newRandomText);
				LogUltility.log(test, logger, "New rule name: " + newRandomText);
				
				//Press the save button  
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
	            // get the text from the rule name field
				String textFromFieldafterChange = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldafterChange);
				
				// verify that the rule name was changed 
				boolean differentName = !textFromFieldafterChange.equals(firstName);
				LogUltility.log(test, logger, "text from the rule name field after change: " +textFromFieldafterChange+" is different than the text from the list : " +firstName+ " and value should be true : "+differentName );
				assertTrue(differentName);
							
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
		 * DIC_TC--222:Check application don't save an empty rule
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_222_Check_application_dont_save_an_empty_rule() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC_222", "Check application don't save an empty rule");
			
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the Disambiguation rules tab 
		RulesTab.disambiguationTab.click();
		
		// Press the new button
		RulesTab.btnNew.click();
		
		// Press the Save button
		RulesTab.btnSave.click();
		
		// Verify that the new rule message appear ruleNameMsg
		boolean ruleNameMsgappear = RulesTab.popupWindowMessage(test, logger, "Rule's name", "This rule's name already exists");
		assertTrue(ruleNameMsgappear);
		LogUltility.log(test, logger, "Is the exist rule name message appear : " +ruleNameMsgappear);
		
		// Press the cancel button on the rule`s name popup ruleNameCancelbtn
		RulesTab.ruleNameCancelbtn.click();
		LogUltility.log(test, logger, "Press the cancel button on the rule`s name popup");
		
		// Press the yes button on the new rule popup 
		RulesTab.newRuleYesbtn.click();
		LogUltility.log(test, logger, "Press the Yes button on the new rule  popup");
		
		
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
				 * DIC_TC--246:Check the new added rule info are saved correctly in the database file
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_246_Check_the_new_added_rule_info_are_saved_correctly_in_the_database_file() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_246", "Check the new added rule info are saved correctly in the database file");
						
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
				
				// get the first rule name from the list
				String firstName = rulesList.get(0).split(" : ")[1];
				LogUltility.log(test, logger, "The first rule text is: " +firstName);
					
				// Press the new button 
				RulesTab.btnNew.click();
				LogUltility.log(test, logger, "Click the new button");
				
				// Type a record name in the key form field
				String randomText = RulesTab.RandomString(5)+ "  " +RulesTab.RandomString(5);
				RulesTab.ruleName.click();
				String newRandomText = RulesTab.ruleName.getText() + randomText;
				RulesTab.ruleName.sendKeys(newRandomText);
				LogUltility.log(test, logger, "New rule name: " + newRandomText);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//Press the save button  
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Press the save button  
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				// Verify that the rule saved in the rules files
				String ruleFile= "disambiguation_rules.txt";
				boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, ruleName , ruleFile);
				assertTrue(isRuleExist);
				LogUltility.log(test, logger, "Verify that the created rule appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is " +ruleName+"  and the value should be true: " +isRuleExist);

				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--255:Check the deleted rule info are saved correctly in the database file
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_255_Check_the_deleted_rule_info_are_saved_correctly_in_the_database_file() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_255", "Check the deleted rule info are saved correctly in the database file");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				// Press the Delete button 
				RulesTab.bt_delete.click();
				LogUltility.log(test, logger, "Click the Delete button");
				
				// Click yes button on the Delete poup 
				RulesTab.yesDeleteRule_bt.click();
				LogUltility.log(test, logger, "Click the Yes button on the Delete poup");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Verify that the deleted rule doesn`t appear in the list anymore 
				// get all the disambiguation rules as list
				List<String> rulesListAfterDelete= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesListAfterDelete);
				
				boolean ruleDeleted = ! rulesListAfterDelete.contains(ruleName);
				assertTrue(ruleDeleted);
				LogUltility.log(test, logger, " Verify that the list " + rulesListAfterDelete+ " doesn`t contain the deleted rule " + ruleName + " the value should be : " +ruleDeleted);
	//				
				// Verify that the rule deleted from the  rules files
				String ruleFile= "disambiguation_rules.txt";
				boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, ruleName, ruleFile );
				assertFalse(isRuleExist);
				LogUltility.log(test, logger, "Verify that the Deleted rule do not appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is " +ruleName+"  and the value should be False: " +isRuleExist);
	
				
//				RulesTab.noDeleteRule_bt.click();
				
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--312: Check the rule priority could be changed and saved
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_312_Check_the_rule_priority_could_be_changed_and_saved() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_312", "Check the rule priority could be changed and saved");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule Priority form priority field 
				List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
				
				//Get the priority list
				List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
				LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
				lst_Priority.remove(chosenPriority.get(0));
				LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
				
				// Press the Priority dropdown and chose a random value 
				// Pick a random record
				Random randomizerPriority = new Random();
				String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
				//List<WebElement> dropdownbutton= RulesTab.priorityDD.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
				CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
				LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
							
				//Press the save button  
				Thread.sleep(1000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(1000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				//get the rule priority form Priority field after the change 
				List<String> rulePriorityAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Priority from the Priority field after the change: " +rulePriorityAChange);
						
				// convert the chosen list to string 
				String rulePriority = chosenPriority.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriority);
				
				// convert the priority list to string after the change 
				String rulePriorityAChange1 = rulePriorityAChange.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriorityAChange1);
				
				//Verify that the rule have a new priority that different than the first one 
				boolean priorityChanged = ! rulePriority.equals(rulePriorityAChange1);               
				assertTrue(priorityChanged);
				LogUltility.log(test, logger, " Verify that the First Priority " + rulePriority+ " different than the new Priority " + rulePriorityAChange1 + " the value should be True : " +priorityChanged);
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--313: Check the Score value does not reset
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_313_Check_the_Score_value_does_not_reset() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_313", "Check the Score value does not reset");

				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number    ,m , , m, m ,
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule score form Score field 
				List<String> chosenScore =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field: " +chosenScore);
				
				//Get the score list
				List<String> lst_Scores = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).ScoreDD);
				LogUltility.log(test, logger, "Score valueslist: " + lst_Scores);
				lst_Scores.remove(chosenScore.get(0));
				lst_Scores.remove(lst_Scores.size()-1);
				LogUltility.log(test, logger, "Score values list after removing the chosen value: " + lst_Scores);
				
				// Press the score dropdown and chose a random value 
				Random randomizerScore = new Random();
				String randomScore = lst_Scores.get(randomizerScore.nextInt(lst_Scores.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, randomScore);
				LogUltility.log(test, logger, "Choose from record list: " + randomScore);
	
				// Choose a value from score dropdown
				String newScore = randomScore;
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, newScore);
				LogUltility.log(test, logger, "Choose new score from the dropdown: " + newScore);
				
				// press the blus button to add new condition addbutton
				Thread.sleep(1000);
				RulesTab.addbutton.click();
				LogUltility.log(test, logger, "Click the add button , + button ");
				
				//get the rule score form Score field after the change 
				List<String> ruleScoreAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field after the change: " +ruleScoreAChange);

				// convert the chosen list to string 
				String ruleScore = ruleScoreAChange.get(0);
				LogUltility.log(test, logger, "Rule Score from the chosen list: " +ruleScore);
				
				//Verify that the rule have a new score that different than the first one 
				boolean scoreChanged = newScore.equals(ruleScore);               
				assertTrue(scoreChanged);
				LogUltility.log(test, logger, " Verify that the selected " + ruleScore+ " the same as the new score " + newScore + " and not reseted, the value should be True : " +scoreChanged);
				

				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--314: Check the Priority value does not reset
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_314_Check_the_Priority_value_does_not_reset() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_314", "Check the Priority value does not reset");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule Priority form priority field 
				List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
				
				//Get the priority list
				List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
				LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
				lst_Priority.remove(chosenPriority.get(0));
				LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
				
				// Press the Priority dropdown and chose a random value 
				// Pick a random record
				Random randomizerPriority = new Random();
				String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
				//List<WebElement> dropdownbutton= RulesTab.priorityDD.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
				CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
				LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
				
	//				
				// press the blus button to add new condition addbutton
				Thread.sleep(3000);
				RulesTab.addbutton.click();
				LogUltility.log(test, logger, "Click the add button , + button ");
				
				//get the rule Priority form Priority field after the change 
				List<String> rulePriorityAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Priority from the Priority field after the change: " +rulePriorityAChange);
	
				// convert the chosen list to string 
				String rulePriority = chosenPriority.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriority);
				
				// convert the priority list to string after the change 
				String rulePriorityAChange1 = rulePriorityAChange.get(0);
				LogUltility.log(test, logger, "Rule Priority from Priority field after the change: " +rulePriorityAChange1);
				
				//Verify that the rule have a new score that different than the first one 
				boolean priorityChanged = randomPriority.equals(rulePriorityAChange1);
				assertTrue(priorityChanged);
				LogUltility.log(test, logger, " Verify that the First Priority " + randomPriority+ " is the same as the new Priority " + rulePriorityAChange1 + " the value should be True : " +priorityChanged);
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--322: Check the �or�, �and� values in the sub conditions
				 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws AWTException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_322_Check_the_or_and_values_in_the_sub_conditions() throws InterruptedException, IOException, AWTException
			{
				test = extent.createTest("Rules Test - DIC_TC_322", "Check the �or�, �and� values in the sub conditions");
				
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.leftArrow.click();
				RulesTab.disambiguationTab.click();
				 
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "270 : letter after letter";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				 // Get the amount of panes/condition for the rule
				 int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
				 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
				 
				 // Add new pane/condition
				 WebElement newPaneE = RulesTab.rulesInputAreaWebElement();
				LogUltility.log(test, logger, "Add new condition/pane" );
				if(newPaneE.getAttribute("IsOffscreen").equals("True"))
					RulesTab.scrollDownRulesInputArea();
				
				//Click on the right green plus + button / add sub condition
				 RulesTab.rulesInputArea(panesAmount, "add condition");
				 LogUltility.log(test, logger, "Click on the right green plus + button near the condition");
				
				
				//Click on the blue �or� button to change it to and
				 RulesTab.rulesInputArea(panesAmount,1,1);
				 LogUltility.log(test, logger, "Click on the blue �or� button to change it to and , and appear instead of or");
				 
				 // Verify that the OR changed to and
				 String buttonValue = RulesTab.rulesInputAreaCheckOR(panesAmount,1,0);
				 LogUltility.log(test, logger, "Verify that the button value is and : " +buttonValue);
				 boolean buttonChanged = buttonValue.equals("and");
				 LogUltility.log(test, logger, "Verify that the button value is and, so the value should be true: " +buttonChanged);
				 assertTrue(buttonChanged);
				
				//Click on the blue �or� button to change it to and
				 RulesTab.rulesInputArea(panesAmount,1,1);
				 LogUltility.log(test, logger, "Click on the blue �and� button to change it to or , or appear instead of and");
				 
				 // Verify that the and changed to or
				 String buttonValue2time = RulesTab.rulesInputAreaCheckOR(panesAmount,1,0);
				 LogUltility.log(test, logger, "Verify that the button value is and : " +buttonValue2time);
				 boolean buttonChanged2time = buttonValue2time.equals("or");
				 LogUltility.log(test, logger, "Verify that the button value is and, so the value should be tru: " +buttonChanged2time);
				 assertTrue(buttonChanged2time);				
	
					
			   //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
					 
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
		 * DIC_TC--334: Check condition type
		 * @throws InterruptedException 
	 * @throws IOException 
		 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_334_Check_condition_type() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_334", "Check condition type");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the Disambiguation rules tab 
		RulesTab.disambiguationTab.click();
		
		// Create new rule with 3 conditions
		// Press the new button
		RulesTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Add two more conditions
		RulesTab.rulesInputArea();
		RulesTab.rulesInputArea();
		LogUltility.log(test, logger, "Add new panes");
		
		// Change the Word In Position value in the middle pane
		RulesTab.rulesInputArea(1, 1);
		LogUltility.log(test, logger, "Change Word In Position value in the middle pane");
		
		// Change the Word In Position value in the last pane
		RulesTab.rulesInputArea(2, 2);
		LogUltility.log(test, logger, "Change Word In Position value in the last pane");

		// Check that the condition type 
		boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
		LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
		assertTrue(conditionType);
	
		//Click on the middle row to change Mandatory to Optional
		 RulesTab.rulesInputArea(1, "Mandatory");
		 LogUltility.log(test, logger, "Click on the middle row to change Mandatory to Optional");
		 
		// Check the condition type after press the Mandatory , it should become Optional
		boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Optional");
		LogUltility.log(test, logger, "Verify that the condition type is Optional and the value should be True: " +newConditionType);
		assertTrue(newConditionType);
		 
		//Click on the middle row to change Optional to Mandatory  
		 RulesTab.rulesInputArea(1, "Optional");
		 LogUltility.log(test, logger, "Click on the middle row to change Optional to Mandatory");
		 
		// Check that the condition type become mandatory after press the optional button
		boolean firstConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
		LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +firstConditionType);
	
	   //Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
	  // Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
		 
		 // Click yes 
		 RulesTab.yesDeleteRule_bt.click();
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
				 * DIC_TC--399: Priority value is not mixed in normalization tab
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_399_Priority_value_is_not_mixed_in_normalization_tab() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_399", "Priority value is not mixed in normalization tab");
				
		//		// Get records from the Rules file
		//		CurrentPage = GetInstance(RulesTab.class);
		//		List<String> recordsFromFile = CurrentPage.As(RulesTab.class).getRecordsFromRules();
		//		
		//		// Pick a random record
		//		Random randomizer = new Random();
		//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// press the Normalization rules tab button to add new condition 
				Thread.sleep(3000);
				RulesTab.normalizationTab.click();
				LogUltility.log(test, logger, "Click the normalization rules tab ");
				
				//get the rule Priority form priority field 
				List<String> chosenNorPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule priority from the priority field For normalization rule: " +chosenNorPriority);
				
				// Click the Disambiguation rules tab disambiguationTab
				RulesTab.disambiguationTab.click();
				LogUltility.log(test, logger, "Click the Disambiguation rules tab ");
				
	
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule Priority form priority field 
				List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
				
				// convert the chosen list to string 
				String rulePriority = chosenNorPriority.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriority);
				
				//Get the priority list
				List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
				LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
				lst_Priority.remove(chosenPriority.get(0));
				lst_Priority.remove(chosenNorPriority.get(0));
				LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
				
				// Press the Priority dropdown and chose a random value 
				// Pick a random record
				Random randomizerPriority = new Random();
				String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
				//List<WebElement> dropdownbutton= RulesTab.priorityDD.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
				CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
				LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
				
				
	//				// Choose a value from Priority dropdown
	//				String newPriority = randomPriority;
	//				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD, newPriority);
	//				LogUltility.log(test, logger, "Choose new Priority from the dropdown: " + newPriority);
	//				
				// press the blus button to add new condition addbutton
				Thread.sleep(3000);
				RulesTab.normalizationTab.click();
				LogUltility.log(test, logger, "Click the normalization rules tab ");
				
				
	//				//Press the save button  
	//				Thread.sleep(3000);
	//				RulesTab.btnSave.click();
	//				LogUltility.log(test, logger, "Click the Save button ");
	//				
	//				//Press the OK button in the saving Dialog
	//				Thread.sleep(3000);
	//				RulesTab.OKSavingDisRuleDialog.click();
	//				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	//				
	//				//Select the same rule
	//				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
	//				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				//get the rule Priority form Priority field after the change 
				List<String> rulePriorityAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Priority from the Priority field after the change for normalization rule: " +rulePriorityAChange);
				
				
	//				String ruleScoreAChange = RulesTab.ScoreDD.getText();
	//				LogUltility.log(test, logger, "Rule Score from the Score field: " +ruleScoreAChange);
				
				// convert the priority list to string after the change 
				String rulePriorityAChange1 = rulePriorityAChange.get(0);
				LogUltility.log(test, logger, "Rule Priority from Priority field after the change: " +rulePriorityAChange1);
				
				//Verify that the rule have a new score that different than the first one 
				boolean priorityChanged = rulePriority.equals(rulePriorityAChange1);
				assertTrue(priorityChanged);
				LogUltility.log(test, logger, " Verify that the First Priority for normalization rule " + rulePriority+ " Is the same as the  Priority after the change " + rulePriorityAChange1 + " and the value should be True : " +priorityChanged);
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--479: Check the user gets cannot remove last condition popup
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
	 * @throws AWTException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_479_Check_the_user_gets_cannot_remove_last_condition_popup() throws InterruptedException, FileNotFoundException, AWTException
			{
				test = extent.createTest("Rules Test - DIC_TC_479", "Check the user gets cannot remove last condition popup");
						
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "002 : {V} / {x, o} _ {n, I, x}xfqtw";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				 // Add new pane/condition
				 WebElement newPaneE = RulesTab.rulesInputAreaWebElement();
				LogUltility.log(test, logger, "Add new condition/pane" );
				if(newPaneE.getAttribute("IsOffscreen").equals("True"))
					RulesTab.scrollDownRulesInputArea();
				
				// Get displayed conditions 
				int panesAmount = RulesTab.getDisplayedRulePanesAmount();
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//Click on the right red X button to delete sub condition
				 RulesTab.rulesInputAreaDeleteSubCondition(panesAmount-1,0);
				 LogUltility.log(test, logger, "Click on the right red X button to delete sub condition");
				 
				 //Verify that the Cannot remove last condition popup appear
				 boolean cannotRemoveLastConditionPopup =RulesTab.popupWindowMessage( test,  logger, "Cannot remove last condition", "Cannot remove last condition in the condition list");
				 LogUltility.log(test, logger, "Verify that the Cannot remove last condition popup appear, value should be true: " +cannotRemoveLastConditionPopup);
				 assertTrue(cannotRemoveLastConditionPopup);
				
				 //Press the OK button on the Cannot remove last condition popup to close it
				 RulesTab.cannotRemoveLastConditionpopupOK_btn.click();
				 LogUltility.log(test, logger, "Press the OK button to close the Cannot remove last condition popup");
				 
	           //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--495: Check change "pos" to "Form" in conditions words dose not changes
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_495_Check_change_pos_to_Form_in_conditions_words_dose_not_changes() throws InterruptedException, IOException
	{
		test = extent.createTest("Rules Test - DIC_TC_495", "Check change \"pos\" to \"Form\" in conditions words dose not changes");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the Disambiguation rules tab 
		RulesTab.disambiguationTab.click();
		
		// get all the disambiguation rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
		LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
		// Choose random record from the middle of the displayed list
		rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
		// Pick a random record
		Random randomizer = new Random();
//		String randomrecord = "002 : {V} / {x, o} _ {n, I, x}";
		String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// get the rule number 
		String ruleNumberFromName = randomrecord.split(" : ")[0];
		LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
		
	    // get the text from the rule name field
		String ruleName = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
		
		// Get number of conditions
		int panesAmount = RulesTab.getDisRuleNumberOfConditions(ruleNumberFromName);
		LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
		
		// Choose random pane to work with
		int paneIndex = randomizer.nextInt(panesAmount);
		LogUltility.log(test, logger, "Change values for pane number: "+paneIndex);
		
		// Get pane dropdown values
		List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex);
		LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
		
		// Change the dropdown to "POS"
		RulesTab.rulesInputArea(paneIndex,0,paneValues.get(1), "pos");
		LogUltility.log(test, logger, "Change dropdown value to: pos ");
		
		// Get the values from the first condition before the change  
	    boolean FirstDDEb4change =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "ComboBox", "pos");
	    LogUltility.log(test, logger, "Verify that the first Dropdown value is 'pos' and value is true : " + FirstDDEb4change);
	    assertTrue(FirstDDEb4change);
	    boolean secondDDEb4change =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "ComboBox", "equal");
	    LogUltility.log(test, logger, "Verify that the second Dropdown value is 'equal' and value is true : " + secondDDEb4change);
	    assertTrue(secondDDEb4change);
	    boolean thirdDDEb4change =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "ComboBox", "unknown");
	    LogUltility.log(test, logger, "Verify that the thirdd Dropdown value is 'unknown' and value is true : " + thirdDDEb4change);
	    LogUltility.log(test, logger, "Verify that the first Dropdownvalue is 'pos' and value is true : " + FirstDDEb4change+" and the third dropdown is 'unknown' andvalue is true : " + thirdDDEb4change);
	
		
		//Change the value in the first dropdown from POS to Form
		RulesTab.rulesInputArea(paneIndex, 0, "pos", "form");
		LogUltility.log(test, logger, "Change the value in the first dropdown from POS to Form");
		
		// Get the values from the first condition after the change 
	    boolean FirstDDAFchange =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "ComboBox", "form");
	    LogUltility.log(test, logger, "Verify that the first Dropdown value is 'form' and value is true : " + FirstDDAFchange);
	    assertTrue(FirstDDAFchange);
	    boolean secondDDAFchange =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "ComboBox", "equal");
	    LogUltility.log(test, logger, "Verify that the second Dropdown value is 'equal' and value is true : " + secondDDAFchange);
	    assertTrue(secondDDAFchange);
	    boolean thirdDDAFchange =  RulesTab.rulesInputAreaIfExist(paneIndex, 0, "Edit", "");
	    LogUltility.log(test, logger, "Verify that the third Dropdown value is : '' and value is true : " + thirdDDAFchange);
	    assertTrue(thirdDDAFchange);
	    LogUltility.log(test, logger, "Verify that the first Dropdownvalue is 'form' and value is true : " + FirstDDAFchange+" and the third dropdown is '' and value is true : " + thirdDDAFchange);
		 
	   //Press the save button
	    RulesTab.btnSave.click();
	    
	    // Verify that the empty dropdown poup appear
	   boolean  emptydropdown = RulesTab.popupWindowMessage(test, logger, "", "One of the drop downs is empty!. Correct that before you save!");
	   LogUltility.log(test, logger, "Verify that the empty dropdown popup displayed ");
	   assertTrue(emptydropdown); 
	   LogUltility.log(test, logger, "The empty dropdown popup displayed and the value should be True: " +emptydropdown);
	   
	  // press the OK button to close the empty dropdown popup
	   RulesTab.okEmptyDropdown.click();
	   LogUltility.log(test, logger, "Click the OK button to close the Empty dropdown popup");
	   
	   //Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
	   //Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
				 * DIC_TC--500: Check some of the keys in conditions
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_500_Check_some_of_the_keys_in_conditions() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_500", "Check some of the keys in conditions");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList,20);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "270 : letter after letter";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				// Get number of conditions
				int panesAmount = RulesTab.getDisplayedRulePanesAmount();
				LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
			
				// Choose random pane to work with
				int paneIndex = randomizer.nextInt(panesAmount);
				LogUltility.log(test, logger, "Change values for pane number: "+paneIndex);
				
				// Get number of sub conditions
				int subPanesAmount = RulesTab.getSubConditionsAmountinPane(paneIndex);
				LogUltility.log(test, logger, "The amount of sub conditions in the pane: "+subPanesAmount);
				
				// Choose random sub condition to work with
				int subIndex = randomizer.nextInt(subPanesAmount);
				LogUltility.log(test, logger, "Change values for sub condition number: "+subIndex);
				
				// Get pane dropdown values
				List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex,subIndex);
				LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
				
				// Get the first list values
				List<String> keys = new ArrayList<String>();
				keys.add("language");
				keys.add("lexical type");
				keys.add("spilling");
				keys.add("transcription");
				String key = keys.get(randomizer.nextInt(keys.size()));
				LogUltility.log(test, logger, "Choose new key value: " + key);
				
				
				//Change the  first dropdown values
				Thread.sleep(1000);
				WebElement dropdownElement = RulesTab.rulesInputArea(paneIndex,subIndex,paneValues.get(1), key);
				LogUltility.log(test, logger, "Change the Dropdown from: "+paneValues.get(1)+" ,to: "+key);
				List<String> fisrtdropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement);
				LogUltility.log(test, logger, "the first dropdown element is : " + fisrtdropdownvalue);

				//Press the save button  
				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// Verify that the new values still appear in the dropdowns
				boolean FDD = fisrtdropdownvalue.get(0).equals(key);
				assertTrue(FDD);
				LogUltility.log(test, logger, "The value in the first dropdown is 'language' and value should be True: "+FDD );
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--503: Check Priority and score value does not reset after save process
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_503_Check_Priority_and_score_value_does_not_reset_after_save_process() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_503", "Check Priority and score value does not reset after save process");
				
		//		// Get records from the Rules file
		//		CurrentPage = GetInstance(RulesTab.class);
		//		List<String> recordsFromFile = CurrentPage.As(RulesTab.class).getRecordsFromRules();
		//		
		//		// Pick a random record
		//		Random randomizer = new Random();
		//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				//get the rule score form Score field 
				List<String> chosenScore =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field: " +chosenScore);
				
				//Get the score list
				List<String> lst_Scores = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).ScoreDD);
				LogUltility.log(test, logger, "Score valueslist: " + lst_Scores);
				lst_Scores.remove(chosenScore.get(0));
				LogUltility.log(test, logger, "Score values list after removing the chosen value: " + lst_Scores);
				
				// Press the score dropdown and chose a random value 
				// Pick a random record
				Random randomizerScore = new Random();
				String randomScore = lst_Scores.get(randomizerScore.nextInt(lst_Scores.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, randomScore);
				LogUltility.log(test, logger, "Choose from record list: " + randomScore);
				
				
				// Choose a value from score dropdown
				String newScore = randomScore;
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD, newScore);
				LogUltility.log(test, logger, "Choose new score from the dropdown: " + newScore);
				
				//get the rule Priority form priority field 
				List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
				
				//Get the priority list
				List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
				LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
				lst_Priority.remove(chosenPriority.get(0));
				LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
				
				// Press the Priority dropdown and chose a random value 
				// Pick a random record
				Random randomizerPriority = new Random();
				String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
				//List<WebElement> dropdownbutton= RulesTab.priorityDD.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
				CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
				LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
				
				//Press the save button  
				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				//get the rule score form Score field after the change 
				List<String> ruleScoreAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).ScoreDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Score from the Score field after the change: " +ruleScoreAChange);
				
				// convert the chosen list to string 
				String ruleScoreAChange1 = ruleScoreAChange.get(0);
				LogUltility.log(test, logger, "Rule Score from the chosen list: " +ruleScoreAChange1);
				
				
	//				String ruleScoreAChange = RulesTab.ScoreDD.getText();
	//				LogUltility.log(test, logger, "Rule Score from the Score field: " +ruleScoreAChange);
				
				// convert the chosen list to string 
				String ruleScore = chosenScore.get(0);
				LogUltility.log(test, logger, "Rule Score from the chosen list: " +ruleScore);
				
				//Verify that the rule have a new score that different than the first one 
				boolean scoreChanged = ! ruleScore.equals(ruleScoreAChange1);               
				assertTrue(scoreChanged);
				LogUltility.log(test, logger, " Verify that the First score " + ruleScore+ " different than the new score " + ruleScoreAChange1 + " the value should be True : " +scoreChanged);
				
				
				//get the rule priority form Priority field after the change 
				List<String> rulePriorityAChange =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
				//String ruleScore = RulesTab.ScoreDD.getText();
				LogUltility.log(test, logger, "Rule Priority from the Priority field after the change: " +rulePriorityAChange);
				
				
	//				String ruleScoreAChange = RulesTab.ScoreDD.getText();
	//				LogUltility.log(test, logger, "Rule Score from the Score field: " +ruleScoreAChange);
				
				// convert the chosen list to string 
				String rulePriority = chosenPriority.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriority);
				
				// convert the priority list to string after the change 
				String rulePriorityAChange1 = rulePriorityAChange.get(0);
				LogUltility.log(test, logger, "Rule Priority from the chosen list: " +rulePriorityAChange1);
				
				//Verify that the rule have a new priority that different than the first one 
				boolean priorityChanged = ! rulePriority.equals(rulePriorityAChange1);               
				assertTrue(priorityChanged);
				LogUltility.log(test, logger, " Verify that the First Priority " + rulePriority+ " different than the new Priority " + rulePriorityAChange1 + " the value should be True : " +priorityChanged);
				
				
				
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--540: Check the description box
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_540_Check_the_description_box() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_540", "Check the description box");
				
		//		// Get records from the Rules file
		//		CurrentPage = GetInstance(RulesTab.class);
		//		List<String> recordsFromFile = CurrentPage.As(RulesTab.class).getRecordsFromRules();
		//		
		//		// Pick a random record
		//		Random randomizer = new Random();
		//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
	
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				//get the rule Number form Number field  
				String ruleNumber = RulesTab.numberBox.getText();
				LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
				
				// Get the text from the Description field 
				String ruleDescription = RulesTab.descriptionBox.getText();
				LogUltility.log(test, logger, "Rule description before change: " +ruleDescription);
				
				
				// Type a new description on the field 
				String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5);
				RulesTab.descriptionBox.click();
				String newRandomText = randomText;
				RulesTab.descriptionBox.sendKeys(newRandomText);
				LogUltility.log(test, logger, "New rule Description: " + newRandomText);
				
				
				//Press the save button  
				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
				//Press the OK button in the saving Dialog
				Thread.sleep(3000);
				RulesTab.OKSavingDisRuleDialog.click();
				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				//Select the same rule
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				
				// Get the text from the Description field 
				String ruleDescriptionAchange = RulesTab.descriptionBox.getText();
				LogUltility.log(test, logger, "Rule description after change: " +ruleDescriptionAchange);
				
				
	//				// Get the text from the Description field 
	//				String ruleDescription = RulesTab.descriptionBox.getText();
	//				LogUltility.log(test, logger, "Rule description before change: " +ruleDescription);
				
				
				//Verify that the rule have a new priority that different than the first one 
				boolean descriptionChanged = ! ruleDescription.equals(ruleDescriptionAchange);               
				assertTrue(descriptionChanged);
				LogUltility.log(test, logger, " Verify that the First Description : " + ruleDescription + " different than the new Description : " + ruleDescriptionAchange + " the value should be True : " +descriptionChanged);
				
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--557: Check the Overlapping condition lists popup
				 * @throws InterruptedException 
				 * @throws FileNotFoundException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_557_Check_the_Overlapping_condition_lists_popup() throws InterruptedException, FileNotFoundException
			{
				test = extent.createTest("Rules Test - DIC_TC_557", "Check the Overlapping condition lists popup");
				
	
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get all the disambiguation rules as list
				List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Choose random record from the middle of the displayed list
				rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
				// Pick a random record
				Random randomizer = new Random();
				//String randomrecord = "086 : * EN{u}be~5 before {s, t, P}";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				//Click on the right green plus + button on the upper right side 
//				RulesTab.rulesInputAreaAddRuleAndCheck(test, logger);
				 RulesTab.rulesInputArea();
				LogUltility.log(test, logger, "Click on the right green plus + button on the upper right side ");
	
				//Press the save button  
				Thread.sleep(3000);
				RulesTab.btnSave.click();
				LogUltility.log(test, logger, "Click the Save button ");
				
	//				//Press the OK button in the saving Dialog
	//				Thread.sleep(3000);
	//				RulesTab.OKSavingDisRuleDialog.click();
	//				LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
				
				// Verify that the overlapping popup appear
				boolean overlappingPopupAppear = RulesTab.popupWindowMessage(test, logger, "Overlapping Condition Lists", "Your rule has more than one condition list per word, please correct that before continuing.");
				LogUltility.log(test, logger, "Is the Overlapping popup appear ? value should be True : " + overlappingPopupAppear);
				assertTrue(overlappingPopupAppear);
				
				// Click the OK button on the overlapping popup
				RulesTab.overlappingOK_btn.click();
				LogUltility.log(test, logger, "Click the OK button on the overlapping popup ");
				
	           //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
				 * DIC_TC--560: Check changing condition for the first word
				 * @throws InterruptedException 
	 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_560_Check_changing_condition_for_the_first_word() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_560", "Check changing condition for the first word");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get disambiguation rules with 2 conditions/panes as list
				List<String> rulesList= RulesTab.getDisRulesWithMultiConditions(2);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "108 : EN{r}about~5 before EN{A}to~0";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				// Change the first WIP pane value to zero
				RulesTab.rulesInputArea(0, 0);
				LogUltility.log(test, logger, "Change Word In Position value in the first pane");
				
				// Check that the condition type 
				boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(0,"Mandatory");
				LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
				assertTrue(conditionType);
	
				//Click on the last row to change Mandatory to Optional
				 RulesTab.rulesInputArea(0, "Mandatory");
				 LogUltility.log(test, logger, "Click on the first row to change Mandatory to Optional");
				 
				// Verify that the condition List referring to word popup appear
				boolean conditionPopupAppear = RulesTab.popupWindowMessage(test, logger, "", "The condition List referring to word IN POSITION 0 cannot be optional!");
				LogUltility.log(test, logger, "Is The condition List referring to word IN POSITION 0 cannot be optional! popup appear ? value should be True : " + conditionPopupAppear);
				assertTrue(conditionPopupAppear);
				
				// press the OK button for the The condition List referring to word IN POSITION 0 cannot be optional! popup
				 RulesTab.conditionpopupOK_btn.click();
				 LogUltility.log(test, logger, "Press the OK button for the The condition List referring popup");
			 
				// Check the condition type still Mandatory 
				boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(0,"Mandatory");
				LogUltility.log(test, logger, "Verify that the condition type not changed and still Mandatory, and the value should be True: " +newConditionType);
				assertTrue(newConditionType);
				 
	           //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
			/**
				 * DIC_TC--561: Check changing condition for the last word
				 * @throws InterruptedException 
			 * @throws IOException 
				 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void DIC_TC_561_Check_changing_condition_for_the_last_word() throws InterruptedException, IOException
			{
				test = extent.createTest("Rules Test - DIC_TC_561", "Check changing condition for the last word");
				
				// define the page as variable 
				CurrentPage = GetInstance(RulesTab.class);
				RulesTab RulesTab = CurrentPage.As(RulesTab.class);
				
				//Press the Rules tab
				RulesTab.rulesTab.click();
				
				// Focus on Rules tab
				RulesTab.rulesFrame.click();
				
				// Press the Disambiguation rules tab 
				RulesTab.disambiguationTab.click();
				
				// get disambiguation rules with 2 conditions/panes as list
				List<String> rulesList= RulesTab.getDisRulesWithMultiConditions(2);
				LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
				// Pick a random record
				Random randomizer = new Random();
//				String randomrecord = "108 : EN{r}about~5 before EN{A}to~0";
				String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
				
				// get the rule number 
				String ruleNumberFromName = randomrecord.split(" : ")[0];
				LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
				
	            // get the text from the rule name field
				String ruleName = RulesTab.ruleName.getText();
				LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
				
				// Get amount of panes
				int panesAmount = RulesTab.getDisplayedRulePanesAmount();
				// If there is only one condition add new one
				if(panesAmount == 1)
				{
					RulesTab.rulesInputArea();
					// Change its value
					RulesTab.rulesInputArea(panesAmount, 1);
				}
				
				int paneIndex = RulesTab.getEdgePaneIndex();
				
				// Check that the condition type 
				boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
				LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
				assertTrue(conditionType);
	
				//Click on the last row to change Mandatory to Optional
				 RulesTab.rulesInputArea(paneIndex, "Mandatory");
				 LogUltility.log(test, logger, "Click on the last row to change Mandatory to Optional");
				 
				// Verify that the condition List referring to word.... popup appear
				boolean conditionPopupAppear = RulesTab.popupWindowMessage(test, logger, "", "The condition List referring to an EDGE word cannot be optional!");
				LogUltility.log(test, logger, "Is The condition List referring to an EDGE word cannot be optional! popup appear ? value should be True : " + conditionPopupAppear);
				assertTrue(conditionPopupAppear);
				
				// press the OK button for the The condition List referring to word IN POSITION 0 cannot be optional! popup
				 RulesTab.conditionpopupOK_btn.click();
				 LogUltility.log(test, logger, "Press the OK button for the The condition List referring popup");
			 
				// Check the condition type still Mandatory 
				boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
				LogUltility.log(test, logger, "Verify that the condition type not changed and still Mandatory, and the value should be True: " +newConditionType);
				assertTrue(newConditionType);
	
	           //Click the revert button to revert the changes
				 RulesTab.bt_revert.click();
				 LogUltility.log(test, logger, "Press the revert button");
				 
			  // Press the OK button on the revert popup message revertFileMsgOK
				 RulesTab.revertFileMsgOK.click();
				 LogUltility.log(test, logger, "Press the OK button on the revert message");
	
				LogUltility.log(test, logger, "Test Case PASSED");	
			}
	
	/**
	 * DIC_TC--709:Verify user can sort the rules in the rules list by different categories part 1 by number
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_709_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part1_by_number() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--709", "Verify user can sort the rules in the rules list by different categories part 1 by number");
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	// Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
				
	// Press the Disambiguation rules tab 
	RulesTab.disambiguationTab.click();	
	
	// Press on the Sort by dropdown list
	LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
	
	// Choose to sort by "Number" from the dropdown list
	CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Number");
	LogUltility.log(test, logger, "Click on Number from the dropdown list ");
	
	// Get all the disambiguation rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
	LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
	
	// Get the rules numbers from the rules list
	ArrayList<Integer> rulesNumbers = new ArrayList<Integer>() ;
	
	for(int i=0;i<rulesList.size();i++)
	{
		String ruleNumber = rulesList.get(i).split(" : ")[0];
		rulesNumbers.add(Integer.parseInt(ruleNumber));
	}
	
	// Check if the rules numbers are sorted in ascending order
	boolean isSorted = true;
	for(int i=0,j=1;j<rulesNumbers.size();i++,j++)
		if(rulesNumbers.get(i) > rulesNumbers.get(j))
			{isSorted = false;break;}
	
	assertTrue(isSorted);
	LogUltility.log(test,logger,"The value should be true: "+isSorted);
	
	LogUltility.log(test, logger, "Test Case PASSED");
		
	}
		
		/**
		 * DIC_TC--709:Verify user can sort the rules in the rules list by different categories part 2 by name
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_709_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part2_by_name() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC--709", "Verify user can sort the rules in the rules list by different categories part 2 by name");
			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			// Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
						
			// Press the Disambiguation rules tab 
			RulesTab.disambiguationTab.click();	
			
			// Press on the Sort by dropdown list
			LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
			
			// Choose to sort by "Name" from the dropdown list
			CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Name");
			LogUltility.log(test, logger, "Click on Name from the dropdown list ");
			
			// Get all the disambiguation rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
			// Get the rules names from the rules list
			ArrayList<String> rulesNames = new ArrayList<String>() ;
			
			for(int i=0;i<rulesList.size();i++)
			{
				String ruleName = rulesList.get(i).split(" : ")[1];
				// if it's a letter
				if((ruleName.charAt(0)<91 && ruleName.charAt(0)>64)|| (ruleName.charAt(0)<123 && ruleName.charAt(0)>96))
					rulesNames.add(ruleName.toLowerCase());
			}

			// Check if the rules names are sorted in alphabetical order
			boolean isSorted = true;
			String previous = rulesNames.get(0);  
			rulesNames.remove(0);
			for (String current: rulesNames)
			{
				int currentAscii = current.charAt(0);
				int prevAscii = previous.charAt(0);
			    if (currentAscii < prevAscii)
			    	{isSorted =  false;break;}
			    previous = current;
			}
			
			assertTrue(isSorted);
			LogUltility.log(test,logger,"The value should be true: "+isSorted);

			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		
		/**
		 * DIC_TC--709:Verify user can sort the rules in the rules list by different categories part 3 by priority
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_709_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part3_by_priority() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC--709", "Verify user can sort the rules in the rules list by different categories part 3 by priority");
			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			// Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
						
			// Press the Disambiguation rules tab 
			RulesTab.disambiguationTab.click();	
			
			// Press on the Sort by dropdown list
			LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
			
			// Choose to sort by "priority" from the dropdown list
			CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Priority");
			LogUltility.log(test, logger, "Click on Priority from the dropdown list ");
			
			// Get all the disambiguation rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
			// Get the rules priorities 	
			String ruleFile= "disambiguation_rules.txt";
			Hashtable<Integer,Integer> rulesPriority = RulesTab.getRecordsPriorityFromRules(ruleFile);
			ArrayList<Integer> sortedPriority = new ArrayList<Integer>();
			
			for(int i=0;i<rulesList.size();i++)
			{
				int ruleNumber = Integer.parseInt(rulesList.get(i).split(" : ")[0]);
				sortedPriority.add(rulesPriority.get(ruleNumber));
			}
			
			LogUltility.log(test, logger, "sorted priority: " +sortedPriority);
			
			// Check if the rules numbers are sorted in ascending order
			boolean isSorted = true;
			for(int i=0,j=1;j<sortedPriority.size();i++,j++)
				if(sortedPriority.get(i) < sortedPriority.get(j))
					{isSorted = false;break;}
			
			assertTrue(isSorted);
			LogUltility.log(test,logger,"The value should be true: "+isSorted);

			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
	
		
		
		
		/**
		 * DIC_TC--709:Verify user can sort the rules in the rules list by different categories part 4 by status
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_709_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part4_by_status() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC--709", "Verify user can sort the rules in the rules list by different categories part 4 by Status");
			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			// Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
						
			// Click the disambiguation rules tab
			RulesTab.disambiguationTab.click();
			
			// Press on the Sort by dropdown list
			LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
			
			// Choose to sort by "Status" from the dropdown list
			CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Status");
			LogUltility.log(test, logger, "Click on Status from the dropdown list ");
			
			// Get all the disambiguation rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
			LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
			
			// Get the checkbox list status from the file
			Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Dis");
				
			// Count how many checboxes are unmarked and insert the sorted rules names into list
			int counter = 0;
			ArrayList<String> ruleNames = new ArrayList<String>();
			for(int i=0;i<checkBoxList.size();i++)
			{
				String ruleName = rulesList.get(i).split(" : ")[1];
				counter = checkBoxList.get(ruleName).equals("disabled") ? counter+1 : counter;
				ruleNames.add(ruleName);
			}
			
			
			// Check if the rules are sorted
			boolean isSorted = true;
			for(int i=0;i<ruleNames.size();i++)
				if(i<counter)
				{
					if(!checkBoxList.get(ruleNames.get(i)).equals("disabled"))
							{isSorted = false;break;}
				}
				else
				{
					if(!checkBoxList.get(ruleNames.get(i)).equals("active"))
						{isSorted = false;break;}
				}
				
			
			assertTrue(isSorted);
			LogUltility.log(test,logger,"The value should be true: "+isSorted);

			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		
		/**
		 * DIC_TC--728:Get unneeded popup after deleting new rule immediately - BUG #1213
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_728_Get_unneeded_popup_after_deleting_new_rule_immediately_BUG_1213() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--728", "Get unneeded popup after deleting new rule immediately - BUG #1213");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the Disambiguation rules tab 
		RulesTab.disambiguationTab.click();
		
		// get all the disambiguation rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
		LogUltility.log(test, logger, "get all the disambiguation rules as list: " +rulesList);
		
		// Press the new button
		RulesTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button ");
		
		// Press delete button
		
		// Press the Delete button 
		RulesTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the Delete button");
		
		// Click yes button on the Delete poup 
		RulesTab.yesDeleteRule_bt.click();
		LogUltility.log(test, logger, "Click the Yes button on the Delete poup");
		
		//Press the OK button 
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK button");
		
		// Press No
//		RulesTab.noDeleteRule_bt.click();
//		LogUltility.log(test, logger, "Press 'No' to ignore generating the lexicon");
		
		// Check that the following popup isn't being displayed
		String titleToCheck = "New Rule";
		String textToCheck = "You are in the middle of adding a new rule!\r\rwould you like to abort?";
		
		boolean noPopup = false;
		try {
		 noPopup = !RulesTab.popupWindowMessage(test, logger, titleToCheck, textToCheck);
		}
		
		catch (Exception e) {
			e.printStackTrace();
			noPopup = true;
		}
		
		LogUltility.log(test, logger, "There no additional/uneeded popup after deleting the rule: "+noPopup);
		assertTrue(noPopup);
		
		// get all the disambiguation rules as list
		List<String> rulesListAfter= RulesTab.getValuesFromApp(RulesTab.disambiguationRulesList);
		LogUltility.log(test, logger, "get all the disambiguation rules as list After: " +rulesList);
		
		// The rule was deleted successfully
		boolean successDelete = rulesListAfter.size() == rulesList.size();
		LogUltility.log(test, logger, "The rule has been deleted successfully: "+successDelete);	
		assertTrue(successDelete);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
		
	}
	
		
		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
	    {		
			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Rules/Disambiguation",method.getName());
			
//				System.out.println("tryCount: " + tryCount);
//				System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//				        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//				        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}

	/**
	 * Closing the application after running all the TCs
	 */
	@AfterClass(alwaysRun = true) 
	public void CloseBrowser() {
		 
		 //DriverContext._Driver.quit();
			}

}
